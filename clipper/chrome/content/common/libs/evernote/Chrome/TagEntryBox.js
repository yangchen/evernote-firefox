if ( typeof Evernote.Chrome == "undefined" ) {
    Evernote.Chrome = { };
}

Evernote.Chrome.TagEntryBox = function TagEntryBox( tab, resizeCallback ) {
    // We're just a special div.
    var div = document.createElement( "div" );
    div.className = "tagEntryBox";

    var maxTags = 20;
    var tagTrie = new Evernote.Chrome.TagTrie();

    // Various text placeholders.
    var placeholders = {
        "ADD": "quickNote_addTags",
        "DISABLED": "quickNote_tagsDisabled",
        "BLANK": "",
        "FULL": "tagNamesNotInRange"
    };

    var mouseInAutoComplete = false;

    // Add our child elements.
    var existingTagContainer = document.createElement( "div" );
    var tagEntryField = document.createElement( "input" );
    var clearAllControl = document.createElement( "img" );
    var autoCompleteBox = document.createElement( "div" );

    existingTagContainer.className = "existingTagContainer";

    autoCompleteBox.className = "autoComplete";
    autoCompleteBox.style.zIndex = "9999";
    var autoCompleteVisible = false;
    var autoCompleteSelection = null;

    clearAllControl.className = "removeTags";
    tagEntryField.type = "text";
    tagEntryField.maxLength = "30";
    tagEntryField.placeholder = Evernote.localizer.getMessage( placeholders["ADD"] );

    //clearAllControl.setAttribute("title", Evernote.localizer.getMessage( "clearTagsToolTip" ));
    //clearAllControl.setAttribute("alt", Evernote.localizer.getMessage( "clearTagsToolTip" ));
    //clearAllControl.setAttribute("tooltiptext", "!!!");

    clearAllControl.src = "resource://evernote_images/clear_icon.png";

    var lastTextValue = "";
    var isTagEntryFocused = false;

    tagEntryField.addEventListener( "keyup", handleTextEntry, false );
    tagEntryField.addEventListener("focus", focusTextEntry, false);
    tagEntryField.addEventListener( "blur", blurTextEntry, false );

    clearAllControl.addEventListener( "click", clearAll, false );

    function autoCompleteMouseOverFunc() {
        mouseInAutoComplete = true;
    }
    function autoCompleteMouseOutFunc() {
        mouseInAutoComplete = false;
    }
    function autoCompleteStopPropagationFunc( event ) {
        event.stopPropagation();
    }

    autoCompleteBox.addEventListener( "click", autoCompleteStopPropagationFunc, false );
    // We want to be able to detect when the user is scrolling this.
    autoCompleteBox.addEventListener( "mouseover", autoCompleteMouseOverFunc, false );
    autoCompleteBox.addEventListener( "mouseout", autoCompleteMouseOutFunc, false );

    div.addEventListener( "click", focusEntry, false );

    // We attach a click handler to the whole window, so that if anyone clicks on it, and we don't capture that, then
    // we'll close the autocomplete box.
    tab.addEventListener( "click", hideAutoComplete, false );
    tab.addEventListener( "keyup", hideAutoCompleteFromEsc, false );

    div.appendChild( existingTagContainer );
    existingTagContainer.appendChild( tagEntryField );
    div.appendChild( clearAllControl );
    div.appendChild( autoCompleteBox );

    // The list of tags we've added.
    var tags = [];
    var tagMap = {};

    // The elements we have for displaying tags.
    var tagElements = {};

    // The list of tags we'll autocomplete from.
    var autoCompleteTags = [];

    function focusEntry( evt ) {
        tagEntryField.focus();
    }

    function clearAll() {
        for ( var i = tags.length - 1; i >= 0; i-- ) {
            clearTag( null, tags[i] );
        }
    }

    function focusTextEntry() {
        isTagEntryFocused = true;
    }

    function blurTextEntry( evt ) {
        if ( !autoCompleteVisible || !mouseInAutoComplete ) {
            resetPlaceholder();
        }
        isTagEntryFocused = false;
    }

    function resetPlaceholder() {
        // Simualte the user pressing enter.
        handleTextEntry.call( tagEntryField, {keyCode:9} );
        if ( tags.length == maxTags ) {
            tagEntryField.placeholder = Evernote.localizer.getMessage( "tagNamesNotInRange", [ Evernote.Limits.EDAM_NOTE_TAGS_MIN,
                                                                                               Evernote.Limits.EDAM_NOTE_TAGS_MAX ] );
            tagEntryField.className = "wide";
            tagEntryField.disabled = true;
        }
        else {
            tagEntryField.placeholder = Evernote.localizer.getMessage( placeholders["ADD"] );
            tagEntryField.className = "";
            tagEntryField.disabled = false;
        }
        hideAutoComplete();
    }

    function createTag( tagName ) {
        var span = document.createElement( "span" );
        span.className = "autoTag";

        var img = document.createElement( "img" );
        img.src = "resource://evernote_images/tag_lozenge_dismiss.png";
        img.addEventListener( "click", function ( evt ) {
            clearTag( evt, tagName )
        }, false );

        span.textContent = tagName;
        span.appendChild( img );
        tagElements[tagName] = span;
        tags.push( tagName );
        tagMap[tagName] = true;
        existingTagContainer.insertBefore( span, tagEntryField );
        clearAllControl.style.display = "inline";

        clearAllControl.style.top = (Evernote.jQuery("#tagControlContainer").position().top + 1) + "px";
    }

    function clearTag( evt, tagName ) {
        if ( !tagName ) return;
        var tagIndex = tags.indexOf( tagName );
        if ( tagIndex == -1 ) return;

        var element = tagElements[tagName];
        if ( element ) {
            element.parentNode.removeChild( element );
            delete tagElements[tagName];
        }
        tags.splice( tagIndex, 1 );
        delete tagMap[tagName];

        if ( tags.length < maxTags ) {
            tagEntryField.disabled = false;
            tagEntryField.placeholder = Evernote.localizer.getMessage( placeholders["ADD"] );
            tagEntryField.className = "";
        }
        tagEntryField.focus();
        if ( tags.length == 0 ) {
            clearAllControl.style.display = "none";
        }

        if ( typeof resizeCallback == "function" ) {
            resizeCallback();
        }
    }

    function selectAutoComplete( evt ) {
        addTag( evt.target.textContent, true );
        tagEntryField.value = "";
        hideAutoComplete();
    }

    function setAutoCompleteList( tagList ) {
        autoCompleteTags = tagList;
        hideAutoComplete();
        tagTrie = new Evernote.Chrome.TagTrie();
        for ( var i = 0; i < autoCompleteTags.length; i++ ) {
            var tagEntry = document.createElement( "div" );
            tagEntry.textContent = autoCompleteTags[i];
            tagEntry.addEventListener( "click", selectAutoComplete, false );
            tagEntry.addEventListener( "mouseover", hoverAutoComplete, false );
            tagTrie.insert( autoCompleteTags[i], tagEntry );
        }
    }

    function hoverAutoComplete() {
        var original = autoCompleteSelection;
        if ( original ) {
            original.className = original.className.replace( /\s*selected/, "" );
        }
        this.className += " selected";
        autoCompleteSelection = this;
    }

    function getSelectedTags() {
        return tags;
    }

    function addTag( tag, focusEntryField ) {
        // Can't add blank tag.
        if ( !tag ) return;
        tag = tag.trim();
        // Already have this tag.
        if ( tags.indexOf( tag ) != -1 ) return;

        createTag( tag );
        if ( typeof resizeCallback == "function" ) {
            resizeCallback();
        }

        if ( tags.length == maxTags ) {
            tagEntryField.value = "";
            tagEntryField.disabled = true;
            tagEntryField.placeholder = Evernote.localizer.getMessage( "tagNamesNotInRange", [ Evernote.Limits.EDAM_NOTE_TAGS_MIN,
                                                                                               Evernote.Limits.EDAM_NOTE_TAGS_MAX ] );
            tagEntryField.className = "wide";
        }

        else if ( focusEntryField ) {
            tagEntryField.focus();
            tagEntryField.placeholder = "";
        }

        clearAllControl.style.top = (Evernote.jQuery("#tagControlContainer").position().top + 1) + "px";
    }

    function setTabIndex( index ) {
        tagEntryField.tabIndex = index;
    }

    function showAutoComplete() {
        autoCompleteVisible = true;
        var offsets = getOffsets( tagEntryField );
        var textHeight = tab.getComputedStyle( tagEntryField ).height.replace( /[^0-9.-]/g, "" );
        autoCompleteBox.style.left = offsets[0] + "px";
        autoCompleteBox.style.top = offsets[1] + parseInt( textHeight ) + 5 + "px";
        autoCompleteBox.style.display = "block";
    }

    function updateAutoComplete( val ) {
        var matches = tagTrie.getMatching( val );

        if ( matches.length ) {
            var list = document.createDocumentFragment();
            matches = matches.sort( function ( a, b ) {
                if ( a[0] === b[0] ) return 0;
                if ( a[0] < b[0] ) return -1;
                return 1;
            } );

            var matchCount = 0;
            for ( var i = 0; i < matches.length; i++ ) {
                if ( !tagMap[matches[i][0]] ) {
                    if ( matchCount % 2 ) {
                        matches[i][1].className = "striped";
                    }
                    else {
                        matches[i][1].className = "unstriped";
                    }
                    list.appendChild( matches[i][1] );
                    matchCount++;
                }
            }

            // Delete all existing nodes and then add the new ones.
            while ( autoCompleteBox.hasChildNodes() ) {
                autoCompleteBox.removeChild( autoCompleteBox.lastChild );
            }
            autoCompleteBox.appendChild( list );

            if ( matchCount ) showAutoComplete();
            else hideAutoComplete();
        }
        else {
            hideAutoComplete();
        }
    }

    function hideAutoComplete() {
        mouseInAutoComplete = false;
        autoCompleteVisible = false;
        autoCompleteSelection = null;
        autoCompleteBox.style.display = "none";
        /*
         for (var i = 0; i < autoCompleteBox.children.length; i++) {
         var c = autoCompleteBox.children[i];
         c.className = "";
         c.style.display = "none";
         }
         */
    }

    function getOffsets( element ) {
        var left = element.offsetLeft;
        var top = element.offsetTop;
        return [left, top];
    }

    function incrementAutoSelect( backwards ) {
        var current = autoCompleteSelection;

        // Set the initial state.
        if ( !current ) {
            current = autoCompleteBox.firstChild;
            if ( current ) {
                current.className += " selected";
                autoCompleteSelection = current;
            }
            return;
        }

        var original = current; // We'll clear this if we find a replacement.
        if ( !backwards ) {
            current = current.nextSibling;
        }
        else {
            current = current.previousSibling;
        }

        if ( !current ) current = original;

        // We found another node.
        if ( current ) {
            original.className = original.className.replace( /\s*selected/, "" );
            current.className = " selected";
            autoCompleteSelection = current;
            //current.scrollIntoViewIfNeeded( false );
            current.scrollIntoView( false );
        }
    }

    function hideAutoCompleteFromEsc( evt ) {
        if ( evt.keyCode ) {
            if ( evt.keyCode == 27 ) {
                if ( autoCompleteVisible ) {
                    hideAutoComplete();
                    tagEntryField.focus();
                }
            }
        }
    }

    function handleTextEntry( evt ) {
        // If after the *last* keystroke we were already empty, then we'll try and remove the last tag from the list.
        if ( lastTextValue == "" ) {
            // Backspace.
            if ( evt.keyCode == 8 ) {
                if ( tags.length ) {
                    clearTag( null, tags[tags.length - 1] );
                }
            }
        }

        // In case the user types, for example ", " before starting a new tag, we'll strip leading whitespace.
        var autoCompleteVal = this.value.replace( /^\s*/, "" );
        if ( autoCompleteVal != "" ) {
            updateAutoComplete( autoCompleteVal );
        }
        else {
            hideAutoComplete();
        }

        hideAutoCompleteFromEsc( evt );

        // And we'll store the new value of the field.
        lastTextValue = this.value;
        var refocus = true;

        // We don't want to re-grab focus in this case (9 is TAB).
        if ( evt.keyCode == 9 ) {
            refocus = false;
        }

        // Otherwise, 'tab' and 'enter' have the same behavior.
        if ( evt.keyCode == 9 || evt.keyCode == 13 ) {
            if ( autoCompleteVisible && autoCompleteSelection ) {
                this.value = autoCompleteSelection.textContent;
            }
            this.value = this.value + ",";
        }

        // Up and down arrows for autocomplete.
        if ( evt.keyCode == 38 || evt.keyCode == 40 ) {
            if ( autoCompleteVisible ) {
                var backwards = (evt.keyCode == 38);
                incrementAutoSelect( backwards );
            }
        }

        if ( this.value.match( /,/ ) ) {
            var tagList = this.value.split( /\s*,\s*/ );
            for ( var i = 0; i < tagList.length - 1; i++ ) {
                addTag( tagList[i], refocus );
                hideAutoComplete();
            }
            this.value = tagList[tagList.length - 1].trim();
        }

        // Don't let this pass up the tree. It's wrapped in an 'if' because sometimes we send this function fake events.
        if ( evt.stopPropagation ) evt.stopPropagation();
    }

    function setDisabled( disabled ) {
        var style;
        var className;
        var clearAllControlStyle;
        if ( disabled ) {
            tagEntryField.disabled = true;
            tagEntryField.placeholder = Evernote.localizer.getMessage( placeholders["DISABLED"] );
            style = "none";
            clearAllControlStyle = "none";
            className = "wide";
        }
        else {
            tagEntryField.disabled = false;
            resetPlaceholder();
            style = "inline-block";
            clearAllControlStyle = "inline";
            className = "";
        }
        var tags = div.getElementsByTagName( "span" );
        for ( var i = 0; i < tags.length; i++ ) {
            tags[i].style.display = style;
        }
        tagEntryField.className = className;
        if ( clearAllControlStyle == "none" || tags.length ) {
            // Always hide this on disable, but only show it if the tag list isn't empty.
            clearAllControl.style.display = clearAllControlStyle;
        }
    }

    function cleanUp() {
        tagEntryField.removeEventListener( "keyup", handleTextEntry, false );
        tagEntryField.removeEventListener("focus", focusTextEntry, false);
        tagEntryField.removeEventListener( "blur", blurTextEntry, false );

        clearAllControl.removeEventListener( "click", clearAll, false );

        autoCompleteBox.removeEventListener( "click", autoCompleteStopPropagationFunc, false );
        autoCompleteBox.removeEventListener( "mouseover", autoCompleteMouseOverFunc, false );
        autoCompleteBox.removeEventListener( "mouseout", autoCompleteMouseOutFunc, false );

        div.removeEventListener( "click", focusEntry, false );

        tab.removeEventListener( "click", hideAutoComplete, false );
        tab.removeEventListener( "keyup", hideAutoCompleteFromEsc, false );
        tab = null;
    }

    this.domElement = div;
    this.blurTextEntry = blurTextEntry;
    this.setAutoCompleteList = setAutoCompleteList;
    this.getSelectedTags = getSelectedTags;
    this.addTag = addTag;
    this.setTabIndex = setTabIndex;
    this.setDisabled = setDisabled;
    this.cleanUp = cleanUp;
    this.isFocused = function() {
        return isTagEntryFocused;
    };
};

Evernote.Chrome.TagTrie = function TagTrie() {
    var trie = {};
    var matching = [];

    function insert( tag, data ) {
        if ( !tag ) return;
        var current = trie;
        for ( var i = 0; i < tag.length; i++ ) {
            if ( !current[tag[i]] ) {
                current[tag[i]] = {};
            }
            current = current[tag[i]];
        }
        current.name = tag;
        current.value = data;
    }

    function fillMatching( current ) {
        if ( current.name && current.value ) {
            matching.push( [current.name, current.value] );
        }
        for ( var i in current ) {
            if ( i !== "name" && i !== "value" ) {
                fillMatching( current[i] );
            }
        }
    }

    function getMatching( tag ) {
        matching = [];
        if ( !tag ) return matching;
        var current = trie;

        for ( var i = 0; i < tag.length; i++ ) {
            if ( !current[tag[i]] ) {
                return matching;
            }
            current = current[tag[i]];
        }
        fillMatching( current );
        return matching;
    }

    this.insert = insert;
    this.getMatching = getMatching;
    this.trie = trie;
};