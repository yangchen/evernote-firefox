//"use strict";

Evernote.Chrome.LinkedNotebooksSource = function LinkedNotebooksSource() {
    this._jsonRpc = new Evernote.Chrome.JsonRpc( null, ["NoteStore.listLinkedNotebooks"] );
    this._jsonRpc.initWithAuthToken( Evernote.context.authenticationToken );
};

Evernote.Chrome.LinkedNotebooksSource.prototype._jsonRpc = null;
Evernote.Chrome.LinkedNotebooksSource.prototype._linkedNotebooks = null;

Evernote.Chrome.LinkedNotebooksSource.prototype.getLastLinkedNotebooks = function( callback ) {
    var linkedNotebooks = new Evernote.Chrome.LinkedNotebooks( Evernote.context.authenticationToken, this._jsonRpc, function() {
        var books = linkedNotebooks.getWritableLinkedNotebooks();
        if ( books.count == 0 ) {
            if ( typeof callback == "function" ) {
                callback( [ ] );
            }
            return;
        }

        var bookArray = [ ];
        for ( var i in books ) {
            if ( books[i].name ) { // Keeps us from including 'count'.
                bookArray.push( new Evernote.LinkedNotebook( books[i] ) );
            }
        }

        Evernote.context.linkedNotebooks = bookArray;
        if ( typeof callback == "function" ) {
            callback( bookArray );
        }
    } );
    
    this._linkedNotebooks = linkedNotebooks;
};

Evernote.Chrome.LinkedNotebooksSource.prototype.addLinkedNotebooksToSelect = function( doc, selectElem, callback ) {
    this.getLastLinkedNotebooks( function( bookArray ) {
        if ( doc && selectElem ) {
            // Sort alpahbetically by user then name.
            var group = doc.createElement( "optgroup" );
            group.label = Evernote.localizer.getMessage( "linkedNotebooksOptGroupTitle" );

            bookArray.sort( function ( a, b ) {
                var aOwner = a.owner ? a.owner.toLowerCase() : "";
                var bOwner = b.owner ? b.owner.toLowerCase() : "";
                var aName = a.name ? a.name.toLowerCase() : "";
                var bName = b.name ? b.name.toLowerCase() : "";

                if ( aOwner == bOwner ) {
                    if ( aName == bName ) return 0;
                    return ((aName < bName) ? -1 : 1);
                }
                else {
                    return ((aOwner < bOwner) ? -1 : 1);
                }
            } );

            for ( var i = 0; i < bookArray.length; ++i ) {
                var opt = doc.createElement( "option" );
                opt.textContent = Evernote.localizer.getMessage( "linkedNotebookOptionText", [bookArray[i].name, bookArray[i].owner] );
                opt.value = bookArray[i].guid;
                group.appendChild( opt );
            }

            if ( bookArray.length > 0 ) {
                selectElem.appendChild( group );
            }
            if ( typeof callback == "function" ) {
                callback();
            }
        }
    } );
};

Evernote.Chrome.LinkedNotebooksSource.prototype.getWritableLinkedNotebookByGuid = function( guid, callback ) {
    var self = this;
    this.getLastLinkedNotebooks( function( /*bookArray*/ ) {
        if ( typeof callback == "function" ) {
            callback( self._linkedNotebooks.getWritableLinkedNotebookByGuid( guid ) );
        }
    } );
};