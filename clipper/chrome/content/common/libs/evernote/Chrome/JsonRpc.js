if (typeof Evernote.Chrome == "undefined") {
    Evernote.Chrome = { };
}

Evernote.Chrome.JsonRpc = function( shardId, methodList ) {
    if (!methodList) {
        Evernote.logger.warn("No methodList provided to JsonRpc object. Consider providing this for performance reasons.");
    }

    // List of functions we need to run when initialization completes.
    var callbacks = [];

    // lame trinary variable combo.
    var initialized = false;
    var initializing = false;

    var shardRpcClient = null;
    var auth = null;

    var baseUrl = Evernote.getSecureServiceUrl();
    function executeCallbacks() {
        initialized = true;
        for (var i = 0; i < callbacks.length; i++) {
            callbacks[i]();
        }
    }

    function initWithUrl(url, callback) {
        // If initialization is complete, we'll just call this now.
        if (initialized && callback) {
            callback();
            return;
        }

        // Otherwise we need to save it for later.
        if (callback) {
            callbacks.push(callback);
        }

        // Fire off this request only if we haven't before.
        if (!initializing) {
            initClient(url);
        }

        // And keep us from doing it a second time.
        var initializing = true;
    }

    function initWithAuthToken(_auth, callback) {
        auth = _auth;
        var matches = auth.match(/^"?S=([^:]+)/);
        if (matches && !shardId) {
            shardId = matches[1];
        }
        initWithUrl(baseUrl + "/shard/" + shardId + "/json", callback);
    }

    function initClient(url) {
        if (methodList) {
            shardRpcClient = new Evernote.JSONRpcClient(methodList, url);
            executeCallbacks();
        }
        else {
            shardRpcClient = new Evernote.JSONRpcClient(executeCallbacks, url);
        }
    }

    // This will set up a JSONRpcCall object and when it's ready, call each callback.
    function initFromCookieAuth(cookie) {

        auth = cookie.value;
        if (!auth) {
            // @TODO: potentially not a warning.
            Evernote.logger.warn("Couldn't get an auth cookie for domain: " + baseUrl);
        }

        // Grab one from our cookie if it wasn't supplied.
        if (!shardId) {
            var matches = auth.match(/^"?S=([^:]+)/);
            if (matches) {
                shardId = matches[1];
            }
        }

        // If we still don't have one, then we're in trouble.
        if (!shardId) {
            Evernote.logger.warn("Couldn't get a shard to initialize to.");
        }

        initClient(baseUrl + "/shard/" + shardId + "/json");
    }

    function defaultExceptionHandler(exception) {
        Evernote.logger.error("JsonRpc Exception: " + JSON.stringify(exception));
    }

    // Call this to set up this object based on the current value stored in our cookies.
    // 'callback' will run when we've completed.
    function initWithCookieAuth(callback) {
        // If initialization is complete, we'll just call this now.
        if (initialized && callback) {
            callback();
            return;
        }

        // Otherwise we need to save it for later.
        if (callback) {
            callbacks.push(callback);
        }

        // Fire off this request only if we haven't before.
        if (!initializing) {
            initFromCookieAuth( Evernote.context.authenticationToken );
        }

        // And keep us from doing it a second time.
        var initializing = true;
    }

    function listify(list) {
        if (!list) {
            list = [];
        }
        return {list: list, javaClass: "java.util.ArrayList"};
    }

    Evernote.JSONRpcClient.toplevel_ex_handler = defaultExceptionHandler;

    this.initWithCookieAuth = initWithCookieAuth;
    this.initWithUrl = initWithUrl;
    this.initWithAuthToken = initWithAuthToken;
    this.listify = listify;

    // read-only properties.
    this.__defineGetter__( "authToken", function() {
        return auth;
    } );
    this.__defineGetter__( "client", function() {
        return shardRpcClient;
    } );
    this.__defineGetter__( "shardId", function() {
        return shardId;
    } );
};

