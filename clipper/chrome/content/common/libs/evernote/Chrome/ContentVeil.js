if ( typeof Evernote.Chrome == "undefined" ) {
    Evernote.Chrome = { };
}

Evernote.Chrome.ContentVeil = function ContentVeil( tab ) {
    // @TODO: save() and restore() aren't properly used here, so if we do things like add transforms in founctions,
    // we probably break other functions' notion of how to render things.

    var veil = tab.document.createElement( "canvas" );
    var context = veil.getContext( '2d' );

    var defaultFill = "rgba(0, 0, 0, 0.7)";
    var defaultStroke = "rgba(255, 255, 0, 0.7)";
    var clearFill = "rgba(0, 0, 0, 1)";

    var defaultLineWidth = 5;
    var defaultLineJoin = "round";

    // We keep a record of what we're currently showing (at least in some cases) so that we can update it in case the
    // state of the page changes (like if the user scrolls).
    var currentlyShownRect = null;
    var currentRectOffsetTop = 0;
    var currentRectOffsetLeft = 0;
    var currentlyStatic = false;

    var scroller = new Evernote.Chrome.Scroller( tab );

    function reset() {
        currentlyShownRect = null;
        currentRectOffsetTop = 0;
        currentRectOffsetLeft = 0;

        showElements( "embed" );
        showElements( "iframe" );

        veil.style.position = "fixed";
        veil.style.top = "0px";
        veil.style.left = "0px";
        veil.style.zIndex = "999999990";

        context.fillStyle = defaultFill;
        context.strokeStyle = defaultStroke;

        context.lineWidth = defaultLineWidth;
        context.lineJoin = defaultLineJoin;

        blank();
    }

    function blank() {
        veil.height = tab.innerHeight;
        veil.width = tab.innerWidth;

        var oldFillStyle = context.fillStyle;
        context.fillStyle = defaultFill;
        context.fillRect( 0, 0, veil.width, veil.height );
        context.fillStyle = oldFillStyle;
    }

    function show() {
        if ( !veil.parentNode ) {
            tab.document.documentElement.appendChild( veil );
        }
    }

    function hide() {
        if ( veil.parentNode ) {
            veil.parentNode.removeChild( veil );
        }
    }

    // Doesn't actually draw this path, simply sets it as the current path.
    function setRoundedRectPath( x, y, width, height, radius ) {
        context.beginPath();
        context.moveTo( x + radius, y );
        context.arcTo( x + width, y, x + width, y + radius, radius );
        context.arcTo( x + width, y + height, x + width - radius, y + height, radius );
        context.arcTo( x, y + height, x, y + height - radius, radius );
        context.arcTo( x, y, x + radius, y, radius );
        context.closePath();
    }

    // Makes a rectangle bigger in all directions by the number of pixels specified (or smaller, if 'amount' is
    // negative). Returns the new rectangle.
    function expandRect( rect, amount ) {
        return {
            top:(rect.top - amount),
            left:(rect.left - amount),
            bottom:(rect.bottom + amount),
            right:(rect.right + amount),
            width:(rect.width + (2 * amount)),
            height:(rect.height + (2 * amount))
        };
    }

    function revealRect( rect, drawStroke, staticView ) {
        // Save this info.
        currentlyShownRect = rect;
        currentRectOffsetTop = tab.pageYOffset;
        currentRectOffsetLeft = tab.pageXOffset;
        currentlyStatic = staticView;

        // We expand the rectangle for two reasons.
        // 1) we want to expand it by half the width of the stroke, so that when we draw out outline, the inner edge of the
        //    outline is still on the outside edge of the rectangle.
        // 2) We want to leave a little extra room for the rounded corners, so they don't cut off content.
        rect = expandRect( rect, 7 );
        var x = rect.left;
        var y = rect.top;
        var width = rect.width;
        var height = rect.height;

        // We're going to use rounded-corner rectangles.
        setRoundedRectPath( x, y, width, height, 8 );

        // Set this to the operation that will clear the shape we're drawing. See here:
        // https://developer.mozilla.org/en/Canvas_tutorial/Compositing
        var oldGCO = context.globalCompositeOperation;
        context.globalCompositeOperation = "destination-out";
        var oldFill = context.fillStyle;
        context.fillStyle = clearFill;
        context.fill(); // Fill with blankness.
        context.globalCompositeOperation = oldGCO; // restore to actual drawing composite operation.
        context.fillStyle = oldFill;
        if ( drawStroke ) {
            context.strokeStyle = defaultStroke;
            context.lineWidth = defaultLineWidth;
            context.stroke();
        }
    }

    function revealStaticRect( rect, drawStroke ) {
        revealRect( rect, drawStroke, true );
    }

    function outlineElement( element, scrollTo ) {
        var rect = element.getBoundingClientRect();
        if ( rect ) {
            var mutableRect = {
                top:rect.top,
                bottom:rect.bottom,
                left:rect.left,
                right:rect.right,
                width:rect.width,
                height:rect.height
            };

            // We don't want to adjust ourselves into odd positions if the page is scrolled.
            var sLeft = tab.document.documentElement.scrollLeft;
            var sTop = tab.document.documentElement.scrollTop;

            var BORDER_MIN = 9;
            if ( mutableRect.left < (BORDER_MIN - sLeft) ) {
                mutableRect.width -= (BORDER_MIN - sLeft) - mutableRect.left;
                mutableRect.left = (BORDER_MIN - sLeft);
            }
            if ( mutableRect.top < (BORDER_MIN - sTop) ) {
                mutableRect.height -= (BORDER_MIN - sTop) - mutableRect.top;
                mutableRect.top = (BORDER_MIN - sTop);
            }

            // Get the wider of our two possible widths.
            var width = Math.max( tab.document.documentElement.scrollWidth, tab.document.defaultView.innerWidth );
            if ( mutableRect.right > (width - BORDER_MIN - sLeft) ) {
                mutableRect.right = (width - BORDER_MIN - sLeft);
                mutableRect.width = mutableRect.right - mutableRect.left;
            }

            reset();
            revealRect( mutableRect, true, false );

            if ( scrollTo ) {
                var left = rect.left - (tab.innerWidth / 2) + sLeft;
                var top = rect.top - (tab.innerHeight / 2) + sTop;
                scroller.scrollTo( { x: left, y: top }, 120, 20 );
            }
            hideElements( "embed", element );
            hideElements( "iframe", element );
            show();
        }
        else {
            console.warn( "Couldn't create rectangle from element: " + element.toString() );
        }
    }

    function hideElements( tagName, exceptInElement ) {
        var els = tab.document.getElementsByTagName( tagName );
        for ( var i = 0; i < els.length; i++ ) {
            els[i].enSavedVisibility = els[i].style.visibility;
            els[i].style.visibility = "hidden";
        }
        showElements( tagName, exceptInElement );
    }

    function showElements( tagName, inElement ) {
        if ( !inElement ) {
            inElement = tab.document;
        }
        var els = inElement.getElementsByTagName( tagName );
        for ( var i = 0; i < els.length; i++ ) {
            if ( typeof els[i].enSavedVisibility !== "undefined" ) {
                els[i].style.visibility = els[i].enSavedVisibility;
                delete els[i].enSavedVisibility;
            }
        }
        inElement = null;
    }

    this._scrollFn = function () {
        if ( currentlyShownRect && !currentlyStatic ) {
            var rect = {
                top:currentlyShownRect.top,
                bottom:currentlyShownRect.bottom,
                left:currentlyShownRect.left,
                right:currentlyShownRect.right,
                width:currentlyShownRect.width,
                height:currentlyShownRect.height
            };

            var vert = tab.pageYOffset - currentRectOffsetTop;
            var horiz = tab.pageXOffset - currentRectOffsetLeft;
            if ( !vert && !horiz ) {
                return;
            }

            rect.top -= vert;
            rect.bottom -= vert;
            rect.left -= horiz;
            rect.right -= horiz;

            blank();
            revealRect( rect, true );
        }
    };

    // If we're currently showing a rectangle, and it's not static, we'll redraw on scroll.
    tab.addEventListener( "scroll", this._scrollFn, false );

    function cleanUp() {
        scroller.cleanUp();
        scroller = null;

        tab.removeEventListener( "scroll", this._scrollFn, false );
        tab = null;
    }

    // Public API:
    this.reset = reset;
    this.show = show;
    this.hide = hide;
    this.revealRect = revealRect;
    this.revealStaticRect = revealStaticRect;
    this.outlineElement = outlineElement;
    this.expandRect = expandRect;
    this.cleanUp = cleanUp;
};

