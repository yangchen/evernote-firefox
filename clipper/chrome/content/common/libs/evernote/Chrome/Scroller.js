if ( typeof Evernote.Chrome == "undefined" ) {
    Evernote.Chrome = { };
}

Evernote.Chrome.Scroller = function Scroller( tab ) {
    this.initialize( tab );
};

Evernote.Chrome.Scroller.prototype._tab = null;

Evernote.Chrome.Scroller.prototype.initialize = function ( tab ) {
    this._tab = tab;
    this.initialPoint = {
        x: this._tab.document.defaultView.pageXOffset,
        y: this._tab.document.defaultView.pageYOffset
    };
};

Evernote.Chrome.Scroller.prototype.scrollTo = function ( endPoint, time, resolution ) {
    this.abort();

    this.endPoint = endPoint;
    this.step = 0;
    this.calculatePath( time, resolution );
    var self = this;
    this.proc = setInterval( function () {
                                 if ( !self.doScroll() ) {
                                     self.abort();
                                 }
                             },
                             resolution );
};

Evernote.Chrome.Scroller.prototype.calculatePath = function ( time, resolution ) {
    this.path = [];
    var sx = this.initialPoint.x;
    var sy = this.initialPoint.y;
    var ex = this.endPoint.x;
    var ey = this.endPoint.y;
    var k = (Math.PI * resolution) / time;
    for ( var i = -(Math.PI / 2); i < (Math.PI / 2); i += k ) {
        var c = ((1 + Math.sin( i )) / 2);
        this.path.push( {
                            x:(sx + c * (ex - sx)),
                            y:(sy + c * (ey - sy))
                        } );
    }
};

Evernote.Chrome.Scroller.prototype.doScroll = function () {
    var s = this.path[++this.step];
    if ( !s ) {
        return false;
    }
    this._tab.document.defaultView.scrollTo( s.x, s.y );
    return true;
};

Evernote.Chrome.Scroller.prototype.abort = function () {
    if ( this.proc ) {
        clearInterval( this.proc );
        this.proc = null;
    }
};

Evernote.Chrome.Scroller.prototype.cleanUp = function() {
    this.abort();
    this._tab = null;
};
