//"use strict";

Evernote.ClipFullStylingStrategy = function ClipFullStylingStrategy( tab ) {
    this.initialize( tab );
};

Evernote.inherit( Evernote.ClipFullStylingStrategy, Evernote.ClipStylingStrategy, true );

Evernote.ClipFullStylingStrategy.prototype._inIDOMUtils = Components.classes[ "@mozilla.org/inspector/dom-utils;1" ].getService( Components.interfaces.inIDOMUtils );
Evernote.ClipFullStylingStrategy.prototype._CIStyleRule = Components.interfaces.nsIDOMCSSStyleRule;
Evernote.ClipFullStylingStrategy.prototype._CIElement = Components.interfaces.nsIDOMElement;

Evernote.ClipFullStylingStrategy.prototype.styleForNode = function ( node, root, fullPage, clipStyleType ) {
    Evernote.logger.debug( "ClipStylingStrategy.styleForNode()" );

    if ( clipStyleType == null ) {
        clipStyleType = Evernote.ClipStyleType.Default;
    }

    var bodyStyles = new Evernote.ClipStyle( [ ], function ( prop, value ) {
        return value != ""
    } );
    var inhFontStyles = new Evernote.ClipStyle( [ ] );
    var inhBgStyles = [ ];

    if ( (clipStyleType & Evernote.ClipStyleType.NodeStyle) == Evernote.ClipStyleType.NodeStyle ) {
        Evernote.logger.debug( "ClipStylingStrategy.styleForNode(): get node style" );

        if ( node.nodeName.toLowerCase() == "body" ) {
            for ( var attrName in Evernote.ClipStyle.STYLE_ATTRIBUTES ) {
                if ( node.hasAttribute( attrName ) ) {
                    var cssPropName = Evernote.ClipStyle.STYLE_ATTRIBUTES[ attrName ];
                    var style = { };
                    style[ cssPropName ] = node.getAttribute( attrName );
                    bodyStyles.addStyle( style );
                }
            }
        }

        var evaluatedStyles = this.getNodeStyle( node );
        if ( node.nodeName.toLowerCase() == "table" && !evaluatedStyles.getStyle( "font-size" ) ) {
            evaluatedStyles.addStyle( {"font-size":"1em"} );
        }

        if ( node.nodeName.toLowerCase() == "img" ) {
            style = new Evernote.ClipStyle( this.getNodeView( node ).getComputedStyle( node, null ), function ( prop, value ) {
                return value != ""
            } );
            evaluatedStyles.addStyle( { height:style.getStyle( "height" ) } );
            evaluatedStyles.addStyle( { width:style.getStyle( "width" ) } );
        }

        if ( evaluatedStyles.getStyle( "background-image" ) ) {
            var regExp = /url\((.*?)\)/;
            evaluatedStyles.addStyle( { "background-image":this.getNodeView( node ).getComputedStyle( node, null ).getPropertyValue( "background-image" ).replace( regExp, "url('$1')" )} );
        }

        if ( evaluatedStyles.getStyle( "height" ) == "100%" && this.getNodeView( node ).getComputedStyle( node, null ).getPropertyValue( "height" ) == "0px" ) {
            evaluatedStyles.addStyle( { height:"0px" } );
        }

        bodyStyles.mergeStyle( evaluatedStyles, true );
    }

    if ( node == root && !fullPage ) {
        if ( (clipStyleType & Evernote.ClipStyleType.InheritedFontStyle) == Evernote.ClipStyleType.InheritedFontStyle ) {
            Evernote.logger.debug( "ClipStylingStrategy.styleForNode(): get inherited font style" );
            inhFontStyles = this.inheritFontForNode( node, true );
            Evernote.logger.debug( "ClipStylingStrategy.styleForNode(): inherited fonts " );
        }

        if ( (clipStyleType & Evernote.ClipStyleType.InheritedBgStyle) == Evernote.ClipStyleType.InheritedBgStyle ) {
            Evernote.logger.debug( "ClipStylingStrategy.styleForNode(): get inherited bg style" );
            inhBgStyles = this.inheritBackgroundForNode( node, true );
        }
    }

    bodyStyles.mergeStyle( inhFontStyles, true );
    return {
        nodeStyle:bodyStyles,
        inheritedFonts:inhFontStyles,
        inheritedBackground:inhBgStyles,
        evaluated:bodyStyles
    };
};

Evernote.ClipFullStylingStrategy.prototype.getNodeStyle = function ( node, filterFn, pseudo ) {
    Evernote.logger.debug( "ClipStylingStrategy.getNodeStyle()" );

    if ( pseudo == null || typeof pseudo == "undefined" ) {
        pseudo = "";
    }

    Evernote.logger.debug( "Pseudo : " + pseudo );

    var clipStyle = new Evernote.ClipStyle( [ ], filterFn );
    filterFn = function ( prop, value ) {
        return value != "" && !(prop == "height" && (value == "1%")) && (value != "physical") && (value != "inherit");
    };

    if( pseudo != "" && ( Evernote.Platform.isFF4() || Evernote.Platform.isFF5() ) ) {
        return clipStyle;
    }

    var rules = this._inIDOMUtils.getCSSStyleRules( node, pseudo );
    if ( rules && node && typeof node.nodeType == 'number' && node.nodeType == 1 ) {
        var processedStyles = [ ];
        for ( var i = (rules.Count() - 1); i >= 0; i-- ) {
            var cssRule = rules.GetElementAt( i ).QueryInterface( this._CIStyleRule );
            var styleSheetHref = cssRule.parentStyleSheet.href;

            if ( styleSheetHref != null && (styleSheetHref.indexOf( "resource:" ) >= 0 || styleSheetHref.indexOf( "about:" ) >= 0) ) {
                continue;
            }

            Evernote.logger.debug( "node " + node.nodeName + " " + node.getAttribute( "class" ) + ": rule" + i );

            var style = cssRule.style;
            var cssText = cssRule.cssText.split( /[\{\}]/ )[1];
            var regExp = /([a-zA-Z\-]* *):.*?;/gmi;
            var res = cssText.match( regExp );

            var tempStyle = null;
            var propArray = [ ];
            if ( res ) {
                Evernote.logger.debug( "use text rule!!!" );
                for ( var propIndex = 0; propIndex < res.length; ++propIndex ) {
                    var temp = res[ propIndex ].replace( /:.*/, "" ).replace( /[^a-zA-Z\-]/, "" );
                    var extCssRules = Evernote.ClipStyle.CSS_GROUP.getExtForStyle( temp );
                    if ( extCssRules ) {
                        if ( processedStyles.indexOf( temp ) < 0 ) {
                            propArray = propArray.concat( extCssRules );
                        }
                    }
                    else {
                        if ( processedStyles.indexOf( temp ) < 0 ) {
                            propArray.push( temp );
                        }
                    }
                }
                processedStyles = processedStyles.concat( res );
                res = null;
                tempStyle = new Evernote.ClipStyle( style, filterFn, propArray );

                Evernote.logger.debug( "csstext: " + cssText + " --> " + propArray );
                Evernote.logger.debug( "tempStyle: " + tempStyle.toString() );

            }
            else if ( !(/.*?\{[^a-zA-Z]*\}/im.test( cssText )) ) {
                Evernote.logger.warn( "use style rule!!!" );
                Evernote.logger.warn( "csstext: " + cssText );
                tempStyle = new Evernote.ClipStyle( style, filterFn );
            }

            if ( tempStyle ) {
                clipStyle.mergeStyle( tempStyle, false );
            }
        }

        if ( node.hasAttribute( "style" ) && pseudo == "" ) {
            Evernote.logger.debug( node.nodeName + "attribute inline style : " + node.getAttribute( "style" ) );

            var element = node.QueryInterface( this._CIElement );
            Evernote.logger.debug( element.nodeName + " element attribute inline style : " + element.getAttribute( "style" ) );

            var inlineClipStyle = new Evernote.ClipStyle( element.style, filterFn );
            clipStyle.mergeStyle( inlineClipStyle, true );
            Evernote.logger.debug( node.nodeName + "(inlineClipStyle) inline style : " + inlineClipStyle.toString() );
        }
    }

    return clipStyle;
};

Evernote.ClipFullStylingStrategy.prototype.inheritFontForNode = function ( node, recur ) {
    Evernote.logger.debug( "ClipFullStylingStrategy.inheritFontForNode()" );

    var fontStyle = new Evernote.ClipStyle();
    if ( !node ) {
        return fontStyle;
    }

    var parent = node;
    var styles = [ ];
    var nodes = [ ];

    var dynamicUnit = ["%", "em"];
    var sizeUnitRegExp = /(.*?)(em|%|px|pt)/;

    while ( parent ) {
        nodes.push( parent );
        styles.push( new Evernote.ClipStyle( this.getNodeStyle( parent ), function ( prop, value ) {
            return (Evernote.ClipStyle.INHERITED_STYLES.indexOf( prop ) > 0 && value != "inherit" );
        } ) );


        Evernote.logger.debug( "Inh parent style:" + styles[styles.length - 1].toString() );

        if ( !recur || parent == this._tab.document.body ) {
            break;
        }
        else {
            parent = parent.parentElement;
        }
    }

    //merge styles starting from low-priority parent styles
    Evernote.logger.debug( "Styles inh for processing:" + (styles.length - 1) );
    for ( var i = styles.length - 1; i >= 0; i-- ) {
        var style = styles[ i ];
        var fontSize = fontStyle.getStyle( "font-size" );
        var overFontStyle = style.getStyle( "font-size" );
        Evernote.logger.debug( "fontSize:" + fontSize + "    ;overFontStyle: " + overFontStyle );
        if ( fontSize && overFontStyle ) {
            var resFontSize = fontSize.match( sizeUnitRegExp );
            if ( resFontSize == null ) {
                continue;
            }
            var sizeVal = resFontSize[1];
            var sizeUnit = resFontSize[2];
            var resOverFontSize = overFontStyle.match( sizeUnitRegExp );

            if ( resOverFontSize == null ) {
                continue;
            }
            var overSizeVal = resOverFontSize[1];
            var overSizeUnit = resOverFontSize[2];

            if ( dynamicUnit.indexOf( overSizeUnit ) != -1 ) {
                if ( overSizeUnit == "%" ) {
                    style.addStyle( { "font-size":(parseFloat( sizeVal ) * parseFloat( overSizeVal ) / 100).toString() + sizeUnit } );
                }
                else {
                    style.addStyle( { "font-size":(parseFloat( sizeVal ) * parseFloat( overSizeVal )).toString() + ((sizeUnit != "em") ? sizeUnit : overSizeUnit) } );
                }
            }
            Evernote.logger.debug( "Style: " + i + "   ;Eval inh style:" + style.toString() );
        }

        fontStyle.mergeStyle( style, true );
    }

    return fontStyle;
};

Evernote.ClipFullStylingStrategy.prototype.inheritBackgroundForNode = function ( node, recur ) {
    Evernote.logger.debug( "ClipFullStylingStrategy.inheritBackgroundForNode()" );

    var bgStyle = new Evernote.ClipStyle();
    if ( !node ) {
        return bgStyle;
    }

    var parent = node;
    var styles = [ ];
    var nodes = [ ];
    var topElement = (this._tab.document.body.parentNode) ? this._tab.document.body.parentNode : this._tab.document.body;

    while ( parent ) {
        nodes.push( parent );
        var filterFn = function ( prop, value ) {
            return !(prop == "background-repeat" && (value == "no-repeat" || value == "repeat-y"));
        };
        var nodeStyle = new Evernote.ClipStyle( this.getNodeStyle( parent ), filterFn, Evernote.ClipStyle.CSS_GROUP.getExtForStyle( "background" ) );

        if ( !nodeStyle.getStyle( "background-repeat" ) ) {
            nodeStyle.removeStyle( "background-image" );
        }
        if ( !nodeStyle.getStyle( "background-color" ) && parent.getAttribute( "bgcolor" ) ) {
            Evernote.logger.debug( "Set bgcolor attribute: " + parent.getAttribute( "bgcolor" ) );
            nodeStyle.addStyle( {"background-color":parent.getAttribute( "bgcolor" )} );
        }

        nodeStyle = this.evalBgPosition( node, parent, nodeStyle );
        if ( nodeStyle.getStylesNames().length > 0 ) {
            styles.push( nodeStyle );
            Evernote.logger.debug( "Add inh bg style " + nodeStyle.toString() );
        }

        if ( !recur || parent == topElement ) {
            break;
        }
        else {
            parent = parent.parentNode;
        }
    }

    return styles;
};

Evernote.ClipFullStylingStrategy.prototype.evalBgPosition = function ( node, inhNode, nodeBgStyle ) {
    Evernote.logger.debug( "Dettermining background image offset" );

    var strPosToPercent = {
        "center":"50%",
        "top":"0%",
        "bottom":"100%",
        "right":"100%",
        "left":"0%"
    };

    var regExp = /url\((.*?)\)/;

    if ( !regExp.test( nodeBgStyle.getStyle( "background-image" ) ) || (nodeBgStyle.getStyle( "background-image" ).indexOf( "data:image" ) >= 0) ) {
        Evernote.logger.debug( "bgStyle: " + nodeBgStyle.toString() );
        return nodeBgStyle;
    }

    nodeBgStyle.addStyle( { "background-image":this.getNodeView( inhNode ).getComputedStyle( inhNode, null ).getPropertyValue( "background-image" ).replace( regExp, "url('$1')" ) } );

    var actualImage = new Image();
    actualImage.src = nodeBgStyle.getStyle( "background-image" ).match( regExp )[ 1 ].replace( /["']/g, "" );
    var bgNodeRect = this.getOffsetRect( inhNode );
    var nodeRect = this.getOffsetRect( node );
    var yDelta = nodeRect.top - bgNodeRect.top;
    var xDelta = nodeRect.left - bgNodeRect.left;

    var bgNodeBgPosX = 0;
    var bgNodeBgPosY = 0;
    var origPosX = 0;
    var origPosY = 0;

    if ( nodeBgStyle.getStyle( "background-position" ) ) {
        var bgPosition = nodeBgStyle.getStyle( "background-position" ).split( " " );
        bgNodeBgPosX = strPosToPercent[bgPosition[ 0 ]] != null ? strPosToPercent[bgPosition[ 0 ]] : bgPosition[ 0 ];
        bgNodeBgPosY = strPosToPercent[bgPosition[ 1 ]] != null ? strPosToPercent[bgPosition[ 1 ]] : bgPosition[ 1 ];

        if ( bgNodeBgPosX.indexOf( "%" ) > 0 ) {
            origPosX = parseInt( bgNodeRect.width ) * (parseInt( bgNodeBgPosX ) / 100);
            var origBgImgPosX = parseInt( actualImage.width ) * (parseInt( bgNodeBgPosX ) / 100);
            origPosX -= origBgImgPosX;
        }
        else {
            origPosX = parseInt( bgNodeBgPosX );
        }

        if ( bgNodeBgPosY.indexOf( "%" ) > 0 ) {
            origPosY = parseInt( bgNodeRect.height ) * (parseInt( bgNodeBgPosY ) / 100);
            var origBgImgPosY = parseInt( actualImage.height ) * (parseInt( bgNodeBgPosY ) / 100);
            origPosY -= origBgImgPosY;
        }
        else {
            origPosY = parseInt( bgNodeBgPosY );
        }
    }

    if ( isNaN( origPosX ) ) {
        origPosX = 0;
    }
    if ( isNaN( origPosY ) ) {
        origPosY = 0;
    }

    var xOffset = 0 - xDelta + origPosX;
    var yOffset = 0 - yDelta + origPosY;

    nodeBgStyle.addStyle( { "background-position":(xOffset + "px " + yOffset + "px") } );
    Evernote.logger.debug( "bgStyle: " + nodeBgStyle.toString() );
    return nodeBgStyle;
};

Evernote.ClipFullStylingStrategy.prototype.getOffsetRect = function ( elem ) {
    Evernote.logger.debug( "ClipCSSStyleWalker.getOffsetRect()" );

    var box = elem.getBoundingClientRect();
    var body = elem.ownerDocument.body;
    var docElem = elem.ownerDocument.documentElement;

    var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
    var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;

    var clientTop = docElem.clientTop || body.clientTop || 0;
    var clientLeft = docElem.clientLeft || body.clientLeft || 0;

    var top = box.top + scrollTop - clientTop;
    var left = box.left + scrollLeft - clientLeft;

    return { top:Math.round( top ), left:Math.round( left ), width:box.width, height:box.height };
};