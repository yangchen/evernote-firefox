//"use strict";

Evernote.SerializedNode = function SerializedNode( node, parent ) {
    this.__defineGetter__( "style", this.getStyle );
    this.__defineSetter__( "style", this.setStyle );
    this.__defineGetter__( "translateTo", this.getTranslateTo );
    this.__defineGetter__( "node", this.getNode );
    this.__defineGetter__( "parentNode", this.getParent );

    this._node = node;
    this._parentSerializedNode = parent;
    this._translateTo = [ ];
};

Evernote.SerializedNode.prototype._translateTo = null;
Evernote.SerializedNode.prototype._node = null;
Evernote.SerializedNode.prototype._parentSerializedNode = null;

Evernote.SerializedNode.prototype.setStyle = function ( clipStyle ) {
    this._clipStyle = clipStyle;
};

Evernote.SerializedNode.prototype.getStyle = function () {
    return this._clipStyle;
};

Evernote.SerializedNode.prototype.getTranslateTo = function () {
    return this._translateTo;
};

Evernote.SerializedNode.prototype.getNode = function () {
    return this._node;
};

Evernote.SerializedNode.prototype.getParent = function () {
    return this._parentSerializedNode;
};
