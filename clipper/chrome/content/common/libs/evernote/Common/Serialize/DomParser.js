//"use strict";

Evernote.DomParser = function DomParser( tab, range ) {
    this.initialize( tab, range );
};

Evernote.DomParser.prototype._tab = null;
Evernote.DomParser.prototype._range = null;

Evernote.DomParser.prototype.initialize = function ( tab, range ) {
    Evernote.logger.debug( "DomSerializer.initialize()" );

    this._tab = tab;
    this._range = range;
};

Evernote.DomParser.prototype.isNodeForSerialize = function ( node ) {
    if ( !node || Evernote.ClipRules.isRejectedNode( node ) || node.id == "evernoteContentClipperWait" ) {
        return false;
    }
    return (!this._range || this.isNodeInRange( node )) ? true : false;
};

Evernote.DomParser.prototype.isNodeInRange = function ( node ) {
    Evernote.logger.debug( "DomParser.isNodeInRange()" );

    if ( node && this._range ) {
        var nodeRange = node.ownerDocument.createRange();
        try {
            nodeRange.selectNode( node );
        }
        catch ( e ) {
            nodeRange.selectNodeContents( node );
        }

        return this._range.compareBoundaryPoints( Range.START_TO_END, nodeRange ) == 1
            && this._range.compareBoundaryPoints( Range.END_TO_START, nodeRange ) == -1;
    }

    return false;
};

Evernote.DomParser.prototype.isNodeVisible = function ( node ) {
    Evernote.logger.debug( "DomParser.isNodeVisible()" );

    if ( !node ) {
        return false;
    }

    var compStyles = this._tab.getComputedStyle( node, null );
    return compStyles.getPropertyValue( "display" ) != "none";
};

Evernote.DomParser.prototype.parse = function ( root, fullPage, serializer ) {
    Evernote.logger.debug( "DomParser.parse()" );

    if ( !root ) {
        throw new Error( "No root element for parsing" );
    }

    var node = root;
    var parentNode = null;
    while ( node ) {
        if ( node != root && node.parentNode ) {
            parentNode = node.parentNode.serializedNode;
        }

        if ( this.isNodeForSerialize( node ) ) {
            if ( node.nodeType == Node.TEXT_NODE ) {
                serializer.textNode( node, this._range );
            }
            else if ( node.nodeType == Node.ELEMENT_NODE && this.isNodeVisible( node ) ) {
                node.serializedNode = serializer.startNode( new Evernote.SerializedNode( node, parentNode ), root, fullPage );
                if ( node.hasChildNodes() ) {
                    node = node.childNodes[ 0 ];
                    continue;
                }
                else {
                    serializer.endNode( node.serializedNode );
                    if ( node.serializedNode ) {
                        delete node.serializedNode;
                    }
                }
            }
        }

        if ( node.nextSibling && node != root ) {
            node = node.nextSibling;
        }
        else if ( node != root ) {
            while ( node.parentNode && node != root ) {
                node = node.parentNode;
                serializer.endNode( node.serializedNode );
                delete node.serializedNode;

                if ( node.nextSibling && node != root ) {
                    node = node.nextSibling;
                    break;
                }
            }

            if ( node == root ) {
                break;
            }
        }
        else {
            break;
        }
    }
};

