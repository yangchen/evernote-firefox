//"use strict";

Evernote.Clip = function Clip( tab, styleStrategy ) {
    this.__defineGetter__( "title", this.getTitle );
    this.__defineGetter__( "url", this.getUrl );
    this.__defineGetter__( "content", this.getContent );
    this.__defineGetter__( "fullPage", this.getFullPage );
    this.__defineGetter__( "length", this.getLength );
    this.__defineGetter__( "contentLength", this.getContentLength );
    this.__defineGetter__( "imagesUrls", this.getImagesUrls );
    this.__defineGetter__( "docBase", this.getDocBase );
    this.initialize( tab, styleStrategy );
};

Evernote.Clip.prototype._tab = null;
Evernote.Clip.prototype._selectionFinder = null;
Evernote.Clip.prototype._content = "";
Evernote.Clip.prototype._styleStrategy = null;
Evernote.Clip.prototype._imagesUrls = null;
Evernote.Clip.prototype._docBase = null;

Evernote.Clip.prototype.initialize = function( tab, styleStrategy ) {
    Evernote.logger.debug( "Clip.initialize()" );

    this._tab = tab;
    this._selectionFinder = new Evernote.SelectionFinder( tab.document );
    this._styleStrategy = styleStrategy;
};

Evernote.Clip.prototype.clipArticle = function( elem ) {
    Evernote.logger.debug( "Clip.clipArticle()" );

    this.reset();
    try {
        var root = elem || Evernote.DOMHelpers.getArticleNode( this._tab.document );
        var serializer = new Evernote.NodeSerializer( this._tab, this._styleStrategy );
        var parser = new Evernote.DomParser( this._tab, null );

        parser.parse( root, false, serializer );
        this._content = serializer.serializedContent;
        this._imagesUrls = serializer.imagesUrls;
        this._docBase = serializer.docBase;
    }
    catch ( e ) {
        Evernote.logger.error( "Clip.clipArticle() failed: error = " + e );
        this.reset();
    }
};

Evernote.Clip.prototype.clipFullPage = function() {
    Evernote.logger.debug( "Clip.clipFullPage()" );

    this.reset();
    try {
        if ( !this.hasBody() ) {
            Evernote.logger.warn( "Clip.clipFullPage(): document has no body" );
            return;
        }

        var start = new Date().getTime();
        var root = this._tab.document.body.parentNode || this._tab.document.body;

        var serializer = new Evernote.NodeSerializer( this._tab, this._styleStrategy );
        var parser = new Evernote.DomParser(this._tab, null);

        parser.parse( root, true, serializer );
        this._content = serializer.serializedContent;
        this._imagesUrls = serializer.imagesUrls;
        this._docBase = serializer.docBase;

        var end = new Date().getTime();
        Evernote.logger.debug( "Clip.clipFullPage(): clipped body in " + (end - start) + " milliseconds" );
    }
    catch ( e ) {
        Evernote.logger.error( "Clip.clipFullPage() failed: error = " + e );
        this.reset();
    }
};

Evernote.Clip.prototype.clipImage = function( imgNode ) {
    Evernote.logger.debug( "Clip.clipImage()" );

    this.reset();
    try {
        var serializer = new Evernote.NodeSerializer( this._tab, this._styleStrategy, false );
        var parser = new Evernote.DomParser(this._tab, null);
        parser.parse( imgNode, false, serializer );

        var imgContent = serializer.serializedContent;

        var imgSrc = imgNode.getAttribute( "src" );
        if ( imgSrc && imgSrc.indexOf( "data:image" ) < 0 ) {
            if (imgSrc.indexOf("http") < 0 ) {
                imgSrc = "http:" + ((imgSrc[0] != '/') ? ("//" + imgSrc) : imgSrc);
            }

            this._content = "<a href='" + imgSrc + "'>" + imgContent + "</a>";
            this._imagesUrls = serializer.imagesUrls;
            this._docBase = serializer.docBase;
        }
        else {
            this._content = imgContent;
            this._imagesUrls = [ ];
        }
    }
    catch ( e ) {
        Evernote.logger.error( "Clip.clipImage() failed: error = " + e );
        this.reset();
    }
};

Evernote.Clip.prototype.clipSelection = function() {
    Evernote.logger.debug( "Clip.clipSelection()" );

    this.reset();
    try {
        if ( !this.hasSelection() ) {
            Evernote.logger.warn( "Clip.clipSelection(): no selection to clip" );
            return;
        }

        var range = this._selectionFinder.getRange();
        if ( !range ) {
            Evernote.logger.warn( "Clip.clipSelection(): no range in selection" );
            return;
        }

        var start = new Date().getTime();
        var ancestor = (this._styleStrategy && range.commonAncestorContainer.nodeType == Node.TEXT_NODE
                        && range.commonAncestorContainer.parentNode) ? range.commonAncestorContainer.parentNode : range.commonAncestorContainer;

        while ( typeof Evernote.ClipRules.NON_ANCESTOR_NODES[ ancestor.nodeName.toUpperCase() ] != 'undefined' && ancestor.parentNode ) {
            if ( ancestor.nodeName.toUpperCase() == "BODY" ) {
                break;
            }
            ancestor = ancestor.parentNode;
        }

        var serializer = new Evernote.NodeSerializer( this._tab, this._styleStrategy );
        var parser = new Evernote.DomParser(this._tab, range);
        parser.parse( ancestor, false, serializer );

        this._content = serializer.serializedContent;
        this._imagesUrls = serializer.imagesUrls;
        this._docBase = serializer.docBase;

        var end = new Date().getTime();
        Evernote.logger.debug( "Clip.clipSelection(): clipped selection in " + (end - start) + " milliseconds" );
    }
    catch ( e ) {
        Evernote.logger.error( "Clip.clipSelection() failed: error = " + e );
        this.reset();
    }
};

Evernote.Clip.prototype.clipUrl = function() {
    Evernote.logger.debug( "Clip.clipUrl()" );

    this.reset();
    try {
        var favIconUrl = "http://www.google.com/s2/favicons?domain=" + Evernote.UrlHelpers.urlDomain( this.url ).toLowerCase();
        var elem = Evernote.DOMHelpers.createUrlClipContent( this._tab.document, this.title, this.url, favIconUrl );

        // can be XUL element on some prohibited urls
        this._content = (elem.innerHTML) ? elem.innerHTML : this._tab.location.href;
        this._imagesUrls = [ favIconUrl ];
        this._docBase = "";
    }
    catch ( e ) {
        Evernote.logger.error( "Clip.clipUrl() failed: error = " + e );
        this.reset();
    }
};

Evernote.Clip.prototype.reset = function() {
    this._content = "";
    this._imagesUrls = null;
};

Evernote.Clip.prototype.hasBody = function() {
    return this._tab && this._tab.document && this._tab.document.body && this._tab.document.body.tagName.toLowerCase() == "body";
};

Evernote.Clip.prototype.hasSelection = function() {
    Evernote.logger.debug( "Clip.hasSelection()" );

    if ( this._selectionFinder.hasSelection() ) {
        return true;
    }
    else {
        this._selectionFinder.find( true );
        return this._selectionFinder.hasSelection();
    }
};

Evernote.Clip.prototype.getTitle = function() {
    var title = this._tab.document.title;
    if ( title ) {
        return title;
    }

    var match = this._tab.location.href.match( /^file:.+\/(.+)$/i );
    if ( match && match[ 1 ] ) {
        return match[ 1 ];
    }

    return "";
};

Evernote.Clip.prototype.getUrl = function() {
    return this._tab.location.href;
};

Evernote.Clip.prototype.getContent = function() {
    return this._content;
};

Evernote.Clip.prototype.getFullPage = function() {
    return !this.hasSelection();
};

Evernote.Clip.prototype.getLength = function() {
    var total = 0;
    var obj = this.toJSON();

    for ( var i in obj ) {
        total += ("" + obj[ i ]).length + i.length + 2;
    }
    total -= 1;

    return total;
};

Evernote.Clip.prototype.getContentLength = function() {
    return this._content.length;
};

Evernote.Clip.prototype.getImagesUrls = function() {
    return this._imagesUrls;
};

Evernote.Clip.prototype.getDocBase = function() {
    return this._docBase;
};

Evernote.Clip.prototype.toJSON = function() {
    return {
        "content" : this.content,
        "title" : this.title,
        "url" : this.url,
        "fullPage" : this.fullPage,
        "sizeExceeded" : false
    };
};