//"use strict";

/**
 * SelectionFinder provides mechanism for finding selection on the page via
 * find(). It is able to traverse frames in order to find a selection. It will
 * report whether there's a selection via hasSelection(). After doing find(),
 * the selection is stored in the selection property, and the document property
 * will contain the document in which the selection was found. Find method will
 * only recurse documents if it was invoked as find(true), specifying to do
 * recursive search. You can use reset() to undo find().
 */
Evernote.SelectionFinder = function SelectionFinder( doc ) {
    this._document = doc;
};

Evernote.SelectionFinder.prototype._document = null;
Evernote.SelectionFinder.prototype._selection = null;

Evernote.SelectionFinder.prototype.hasSelection = function() {
    Evernote.logger.debug( "SelectionFinder.hasSelection()" );

    var range = this.getRange();
    if ( range && (range.startContainer != range.endContainer
                   || (range.startContainer == range.endContainer && range.startOffset != range.endOffset)) ) {
        return true;
    }

    return false;
};

Evernote.SelectionFinder.prototype.find = function( deep ) {
    Evernote.logger.debug( "SelectionFinder.find()" );

    var result = this.findSelectionInDocument( this._document, deep );
    this._document = result.document;
    this._selection = result.selection;
};

Evernote.SelectionFinder.prototype.getRange = function() {
    Evernote.logger.debug( "SelectionFinder.getRange()" );

    if ( !this._selection || this._selection.rangeCount == 0 ) {
        return null;
    }

    if ( typeof this._selection.getRangeAt == 'function' ) {
        return this._selection.getRangeAt( 0 );
    }

    if ( this._selection instanceof Range ) {
        return this._selection;
    }

    var range = this._document.createRange();
    range.setStart( this._selection.anchorNode, this._selection.anchorOffset );
    range.setEnd( this._selection.focusNode, this._selection.focusOffset );

    return range;
};

Evernote.SelectionFinder.prototype.findSelectionInDocument = function( doc, deep ) {
    Evernote.logger.debug( "SelectionFinder.findSelectionInDocument()" );

    var sel = null;
    var hasSelection = false;
    var win = null;

    try {
        win = (doc.defaultView) ? doc.defaultView : window;
    }
    catch ( e ) {
        win = window;
    }

    if ( typeof win.getSelection == 'function' ) {
        sel = win.getSelection();
        if ( sel && typeof sel.rangeCount != 'undefined' && sel.rangeCount > 0 ) {
            hasSelection = true;
        }
    }
    else if ( win.selection && typeof win.selection.createRange == 'function' ) {
        sel = win.selection.createRange();
        if ( win.selection.type == 'Text' && typeof sel.htmlText == 'string' && sel.htmlText.length > 0 ) {
            hasSelection = true;
        }
    }
    else if ( doc.selection && typeof doc.selection.createRange == 'function' ) {
        sel = doc.selection.createRange();
        if ( doc.selection.type == 'Text' && typeof sel.htmlText == 'string' && sel.htmlText.length > 0 ) {
            hasSelection = true;
        }
    }

    if ( sel && !hasSelection && deep ) {
        var nestedDocs = Evernote.DOMHelpers.getNestedDocuments( doc );
        for ( var i = 0; i < nestedDocs.length; ++i ) {
            if ( nestedDocs[ i ] ) {
                var framedSel = this.findSelectionInDocument( nestedDocs[ i ], deep );
                if ( framedSel && framedSel.selection && framedSel.selection.rangeCount > 0 ) {
                    return framedSel;
                }
            }
        }
    }

    //if do not find any selection in document, try to find selection in HTMLTextArea|Input.
    //Get Selection object for TextArea, and set selection as a Range object
    Evernote.logger.debug( "Check selection in INPUT TEXT area (input, textarea), for active element :" + doc.activeElement );

    var activeEl = doc.activeElement;
    if ( activeEl && ( (activeEl instanceof HTMLInputElement && activeEl.type == "text") || activeEl instanceof HTMLTextAreaElement ) ) {
        if ( activeEl.selectionStart != activeEl.selectionEnd ) {
            var range = doc.createRange();
            var textNode = doc.createTextNode( activeEl.value );

            range.setStart( textNode, activeEl.selectionStart );
            range.setEnd( textNode, activeEl.selectionEnd );
            sel = range;
        }
    }

    return {
        document : doc,
        selection : sel
    };
};
