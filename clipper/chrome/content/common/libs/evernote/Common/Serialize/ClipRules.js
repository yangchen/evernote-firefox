//"use strict";

Evernote.ClipRules = {
    isNoKeepNodeAttr : function( attribute ) {
        if ( !attribute ) {
            return true;
        }
        
        var attrName = attribute.name.toLowerCase();
        var attrValue = attribute.value.toLowerCase();
        return typeof this.NOKEEP_NODE_ATTRIBUTES[ attrName ] != 'undefined'
               || attrName.substring( 0, 2 ) == "on"
               || (attrName == "href" && attrValue.substring( 0, 11 ) == "javascript:");
    },

    isConditionalNode : function( node ) {
        return node && typeof this.CONDITIONAL_NODES[ node.nodeName.toUpperCase() ] != 'undefined';
    },

    translateNode : function( node ) {
        var nodeName = this.NODE_NAME_TRANSLATIONS[ node.nodeName.toUpperCase() ] || node.nodeName.toUpperCase();
        return (typeof this.SUPPORTED_NODES[ nodeName ] != "undefined") ? nodeName : this.NODE_NAME_TRANSLATIONS[ "*" ];
    },

    isSupportedNode : function( node ) {
        return node && typeof this.SUPPORTED_NODES[ node.nodeName.toUpperCase() ] != 'undefined';
    },

    isRejectedNode : function( node ) {
        return node && typeof this.REJECTED_NODES[ node.nodeName.toUpperCase() ] != 'undefined';
    },

    isNonAncestorNode : function( node ) {
        return node && typeof this.NON_ANCESTOR_NODES[ node.nodeName.toUpperCase() ] != 'undefined';
    },

    isSelfClosingNode : function( node ) {
        return node && typeof this.SELF_CLOSING_NODES[ node.nodeName.toUpperCase() ] != 'undefined';
    }
};

Evernote.ClipRules.NOKEEP_NODE_ATTRIBUTES = {
    "style" : null,
    "tabindex" : null
};

Evernote.ClipRules.CONDITIONAL_NODES = {
    "EMBED" : null,
    "OBJECT" : null,
    "IMG" : null,
    "VIDEO" : null
};

Evernote.ClipRules.NODE_NAME_TRANSLATIONS = {
    "HTML" : "DIV",
    "BODY" : "DIV",
    "FORM" : "DIV",
    "CANVAS" : "DIV",
    "CUFON" : "DIV",
    "EMBED" : "IMG",
    "BUTTON" : "SPAN",
    "INPUT" : "SPAN",
    "BDI" : "SPAN",
    "*" : "DIV"
};

Evernote.ClipRules.SUPPORTED_NODES = {
    "A" : null,
    "ABBR" : null,
    "ACRONYM" : null,
    "ADDRESS" : null,
    "AREA" : null,
    "B" : null,
    "BASE" : null,
    "BASEFONT" : null,
    "BDO" : null,
    "BIG" : null,
    "BLOCKQUOTE" : null,
    "BR" : null,
    "BUTTON" : null,
    "CAPTION" : null,
    "CENTER" : null,
    "CITE" : null,
    "CODE" : null,
    "COL" : null,
    "COLGROUP" : null,
    "DD" : null,
    "DEL" : null,
    "DFN" : null,
    "DIR" : null,
    "DIV" : null,
    "DL" : null,
    "DT" : null,
    "EM" : null,
    "FIELDSET" : null,
    "FONT" : null,
    "FORM" : null,
    "FRAME" : null,
    "FRAMESET" : null,
    "H1" : null,
    "H2" : null,
    "H3" : null,
    "H4" : null,
    "H5" : null,
    "H6" : null,
    "HR" : null,
    "HTML" : null,
    "I" : null,
    "IFRAME" : null,
    "IMG" : null,
    "INPUT" : null,
    "INS" : null,
    "KBD" : null,
    "LABEL" : null,
    "LEGEND" : null,
    "LI" : null,
    "LINK" : null,
    "MAP" : null,
    "MENU" : null,
    "META" : null,
    "NOBR" : null,
    "NOFRAMES" : null,
    "OBJECT" : null,
    "OL" : null,
    "P" : null,
    "PRE" : null,
    "Q" : null,
    "QUOTE" : null,
    "S" : null,
    "SAMP" : null,
    "SMALL" : null,
    "SPAN" : null,
    "STRIKE" : null,
    "STRONG" : null,
    "SUB" : null,
    "SUP" : null,
    "TABLE" : null,
    "TBODY" : null,
    "TD" : null,
    "TEXTAREA" : null,
    "TFOOT" : null,
    "TH" : null,
    "THEAD" : null,
    "TITLE" : null,
    "TR" : null,
    "TT" : null,
    "U" : null,
    "UL" : null,
    "VAR" : null,
    "VIDEO" : null
};

Evernote.ClipRules.REJECTED_NODES = {
    "SCRIPT" : null,
    "STYLE" : null,
    "SELECT" : null,
    "OPTION" : null,
    "OPTGROUP" : null,
    "NOSCRIPT" : null,
    "PARAM" : null,
    "HEAD" : null,
    "EVERNOTEDIV" : null,
    "CUFONTEXT" : null,
    "NOEMBED" : null
};

Evernote.ClipRules.NON_ANCESTOR_NODES = {
    "OL" : null,
    "UL" : null,
    "LI" : null
};

Evernote.ClipRules.SELF_CLOSING_NODES = {
    "IMG" : null,
    //"INPUT" : null,
    "BR" : null
};