//"use strict";

Evernote.ClipTextStylingStrategy = function ClipTextStylingStrategy( tab ) {
    this.initialize( tab );
};

Evernote.inherit( Evernote.ClipTextStylingStrategy, Evernote.ClipStylingStrategy, true );

Evernote.ClipTextStylingStrategy.COLOR_THRESH = 50;
Evernote.ClipTextStylingStrategy.FORMAT_NODE_NAMES = {
    "b" : null,
    "big" : null,
    "em" : null,
    "i" : null,
    "small" : null,
    "strong" : null,
    "sub" : null,
    "sup" : null,
    "ins" : null,
    "del" : null,
    "s" : null,
    "strike" : null,
    "u" : null,
    "code" : null,
    "kbd" : null,
    "samp" : null,
    "tt" : null,
    "var" : null,
    "pre" : null,
    "listing" : null,
    "plaintext" : null,
    "xmp" : null,
    "abbr" : null,
    "acronym" : null,
    "address" : null,
    "bdo" : null,
    "blockquote" : null,
    "q" : null,
    "cite" : null,
    "dfn" : null
};

Evernote.ClipTextStylingStrategy.STYLE_ATTRS = {
    "font" : null,
    "text" : null,
    "color": null
};

Evernote.ClipTextStylingStrategy.prototype.isFormatNode = function( node ) {
    return node && node.nodeType == 1 && typeof this.constructor.FORMAT_NODE_NAMES[ node.nodeName.toLowerCase() ] != 'undefined';
};

Evernote.ClipTextStylingStrategy.prototype.hasTextNodes = function( node ) {
    if ( node && node.nodeType == 1 ) {
        for ( var i = 0; i < node.childNodes.length; ++i ) {
            if ( node.childNodes[ i ].nodeType == 3 ) {
                return true;
            }
        }
    }

    return false;
};

Evernote.ClipTextStylingStrategy.prototype.styleFilter = function( style ) {
    if ( typeof style != 'string' ) {
        return false;
    }
    
    var i = style.indexOf( "-" );
    var prefix = ( i > 0 ) ? style.substring( 0, i ) : style;

    return typeof Evernote.ClipTextStylingStrategy.STYLE_ATTRS[ prefix.toLowerCase() ] != 'undefined';
};

Evernote.ClipTextStylingStrategy.prototype.styleForNode = function(  node/*, root, fullPage, clipStyleType*/ ) {
    Evernote.logger.debug( "ClipTextStylingStrategy.styleForNode()" );

    var nodeStyle = new Evernote.ClipStyle();
    if ( this.isFormatNode( node ) || this.hasTextNodes( node ) ) {
        nodeStyle = this.getNodeStyle( node, this.styleFilter );
    }

    if ( nodeStyle ) {
        var color = nodeStyle.getStyle( "color" );
        if ( color ) {
            var colorParts = color.replace( /[^0-9,\s]+/g, "" ).replace( /[,\s]+/g, " " ).split( /\s+/ );

            var r = parseInt( colorParts[0] );
            r = (isNaN( r )) ? 0 : r;

            var g = parseInt( colorParts[1] );
            g = (isNaN( g )) ? 0 : r;

            var b = parseInt( colorParts[2] );
            b = (isNaN( b )) ? 0 : b;

            if ( (r + g + b) > (255 - this.constructor.COLOR_THRESH) * 3 ) {
                r = Math.max( 0, r - this.constructor.COLOR_THRESH );
                g = Math.max( 0, g - this.constructor.COLOR_THRESH );
                b = Math.max( 0, b - this.constructor.COLOR_THRESH );
            }
    
            nodeStyle.addSimpleStyle( "color", (colorParts.length == 4) ? ("rgba(" + [r, g, b, 1].join( ", " ) + ")") : ("rgb(" + [r, g, b].join( ", " ) + ")") );
        }
    }

    return {
        nodeStyle : nodeStyle,
        inheritedFonts : null,
        inheritedBackground : null,
        evaluated : nodeStyle
    };
};