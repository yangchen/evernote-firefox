//"use strict";

Evernote.PrefsManager = function PrefsManager() {
    this.__defineGetter__( "rootBranch", this.getRootBranch );
    this.initialize();
};

Evernote.mixin( Evernote.PrefsManager, Evernote.BaseListener );

Evernote.PrefsManager.prototype._listeners = null;

Evernote.PrefsManager.prototype.initialize = function() {
    Evernote.logger.debug( "PrefsManager.initialize()" );

    this._listeners = [ ];
    
    var prefBranch = this.rootBranch.QueryInterface( Components.interfaces.nsIPrefBranch2 );
    prefBranch.addObserver( "", this, false );

    Evernote.uninstaller.addListener( this );
};

Evernote.PrefsManager.prototype.addPref = function( name, value ) {
    if ( typeof name != "string" ) {
        return;
    }

    Evernote.logger.debug( "PrefsManager.addPref(): name = " + name + ", value = " + value );
    try {
        var rootBranch = this.rootBranch;
        if ( typeof value == "boolean" ) {
            rootBranch.setBoolPref( name, value );
        }
        else if ( typeof value == "number" ) {
            rootBranch.setIntPref( name, value );
        }
        else if ( typeof value == "string" ) {
            rootBranch.setCharPref( name, value );
        }
    }
    catch ( e ) {
        Evernote.logger.error( "PrefsManager.addPref() failed: name = " + name + ", value = " + value + ", error = " + e );
    }
};

Evernote.PrefsManager.prototype.getPref = function( name, type ) {
    if ( typeof name != "string" ) {
        return null;
    }

    Evernote.logger.debug( "PrefsManager.getPref(): name = " + name );
    var rootBranch = this.rootBranch;

    try {
        if ( type == "boolean" ) {
            return rootBranch.getBoolPref( name );
        }
        else if ( type == "number" ) {
            return rootBranch.getIntPref( name );
        }
        else if ( type == "string" ) {
            return rootBranch.getCharPref( name );
        }
    }
    catch ( e ) {
    }

    return null;
};

Evernote.PrefsManager.prototype.deletePref = function( name ) {
    if ( typeof name != "string" ) {
        return;
    }

    try {
        Evernote.logger.debug( "PrefsManager.deletePref(): name = " + name );
        this.rootBranch.clearUserPref( name );
    }
    catch ( e ) {
        Evernote.logger.error( "PrefsManager.deletePref() failed: name = " + name + ", error = " + e );
    }
};

Evernote.PrefsManager.prototype.deletePrefs = function( names ) {
    if ( !(names instanceof Array) ) {
        return;
    }

    for ( var i = 0; i < names.length; ++i ) {
        this.deletePref( names[ i ] );
    }
};

Evernote.PrefsManager.prototype.observe = function( subject, topic, data ) {
    if ( topic == "nsPref:changed" ) {
        this.notify( "onPrefChanged", data );
    }
};

Evernote.PrefsManager.prototype.onPluginDisabled = function() {
    Evernote.logger.debug( "PrefsManager.onPluginDisabled()" );
    this.deletePrefs( [ Evernote.PrefKeys.PROCESSING_CLIPLIST ] );
};

Evernote.PrefsManager.prototype.onPluginUninstalled = function() {
    Evernote.logger.debug( "PrefsManager.onPluginUninstalled()" );
    this.deletePrefs( [ Evernote.PrefKeys.PROCESSING_CLIPLIST, Evernote.PrefKeys.IS_START_PAGE_SHOWED ] );
};

Evernote.PrefsManager.prototype.cleanUp = function() {
    Evernote.logger.debug( "PrefsManager.cleanUp()" );

    Evernote.uninstaller.removeListener( this );

    var prefBranch = this.rootBranch.QueryInterface( Components.interfaces.nsIPrefBranch2 );
    prefBranch.removeObserver( "", this );
    
    this._listeners = [ ];
};

Evernote.PrefsManager.prototype.getRootBranch = function() {
    return Components.classes[ "@mozilla.org/preferences;1" ].getService( Components.interfaces.nsIPrefService ).getBranch( null );
};

Evernote.PrefsManager.prototype.deleteUnusedPrefs = function() {
    try {
        this.deletePrefs( [ "webclipper3.is_first_start",
                            "webclipper3.is_desctop_selected",
                            "webclipper3.is_desktop_selected",
                            "extensions.evernote.webclipper3.currentUserName",
                            "extensions.evernote.webclipper3.currentUser",
                            "extensions.evernote.webclipper3.connectionExist",
                            "extensions.evernote.webclipper3.FFjustStarted",
                            "evernote.webclipper3.1.button.initialized",
                            "extensions.evernote.webclipper3.1.button.initialized" ] );
    }
    catch ( e ) {
    }
};