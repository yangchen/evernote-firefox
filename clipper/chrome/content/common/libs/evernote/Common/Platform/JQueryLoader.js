//"use strict";

/**
 * This object should be used to load
 * JQuery inside main Firefox window
 */

Evernote.JQueryLoader = {
    JQUERY_PATHS : [ "chrome://webclipper3-common/content/libs/jquery/jquery-1.4.2.xul.min.js" ],

    load : function() {
        Evernote.logger.debug( "JQueryLoader.load()" );

        try {
            var prev$ = window.$;
            window.$ = null;

            var prevJQ = window.jQuery;
            window.jQuery = null;

            var scope = { };
            var globals_before = [ ];
            var globals_after = [ ];

            for ( var prop in window ) {
                try {
                    if ( window.hasOwnProperty( prop ) && !window.__lookupSetter__( prop )
                         && !window.__lookupGetter__( prop ) && window[ prop ] ) {
                        globals_before.push( prop );
                    }
                }
                catch ( e ) {
                    Evernote.logger.error( "JQueryLoader.load() error: prop = " + prop + " " + e );
                }
            }

            for ( var i = 0; i < this.JQUERY_PATHS.length; ++i ) {
                Evernote.ScriptLoader.load( this.JQUERY_PATHS[ i ] );
            }

            for ( prop in window ) {
                try {
                    if ( window.hasOwnProperty( prop ) && !window.__lookupGetter__( prop )
                         && !window.__lookupSetter__( prop ) && window[ prop ] ) {
                        globals_after.push( prop );
                    }
                }
                catch ( e ) {
                    Evernote.logger.error( "JQueryLoader.load() error: prop = " + prop + " " + e );
                }
            }

            // Diff the global arrays
            var new_globals = globals_after.filter( function( item ) {
                return globals_before.indexOf( item ) < 0;
            } );

            // Populate the scope object with the new global objects
            for ( i = 0; i < new_globals.length; ++i ) {
                var new_global = new_globals[ i ].toLowerCase();
                if ( new_global != "jquery" && new_global != "$" ) {
                    continue;
                }

                scope[ new_globals[ i ] ] = window[ new_globals[ i ] ];
                window[ new_globals[ i ] ] = null;
                delete window[ new_globals[ i ] ];
            }

            Evernote.jQuery = scope.$ || scope.jQuery;

            if ( prev$ ) {
                window.$ = prev$;
            }
            if ( prevJQ ) {
                window.jQuery = prevJQ;
            }
        }
        catch ( e ) {
            Evernote.logger.error( "JQueryLoader.load() failed: error = " + e );
        }
    }
};