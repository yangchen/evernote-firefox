//"use strict";

Evernote.TabManager = function TabManager() {
    this.initialize();
};

Evernote.mixin( Evernote.TabManager, Evernote.BaseListener );

Evernote.TabManager.prototype._onTabSelectFn = null;
Evernote.TabManager.prototype._onTabCloseFn = null;
Evernote.TabManager.prototype._listeners = null;

Evernote.TabManager.prototype.initialize = function() {
    Evernote.logger.debug( "TabManager.initialize()" );

    this._listeners = [ ];

    gBrowser.addTabsProgressListener( this );
    var self = this;

    this._onTabSelectFn = function( event ) {
        self.onTabSelect( event )
    };
    this._onTabCloseFn = function( event ) {
        self.onTabClose( event )
    };

    gBrowser.tabContainer.addEventListener( "TabSelect", this._onTabSelectFn, false );
    gBrowser.tabContainer.addEventListener( "TabClose", this._onTabCloseFn, false );
};

Evernote.TabManager.prototype.create = function( param ) {
    var browser = window.top.document.getElementById( "content" );
    browser.selectedTab = browser.addTab( param.url );
};

Evernote.TabManager.prototype.getSelectedTab = function() {
    return window.top.getBrowser().selectedTab;
};

Evernote.TabManager.prototype.getSelectedTabId = function() {
    return window.top.getBrowser().selectedTab.linkedPanel;
};

Evernote.TabManager.prototype.onLocationChange = function( aBrowser/*, webProgress, request, location*/ ) {
    Evernote.logger.debug( "TabManager.onLocationChange()" );
    this.notify( "onLocationChange", aBrowser );
};

Evernote.TabManager.prototype.onTabSelect = function( event ) {
    Evernote.logger.debug( "TabManager.onTabSelect()" );

    var tabId = event.target.linkedPanel;
    this.notify( "onTabSelect", tabId );
};

Evernote.TabManager.prototype.onTabClose = function( event ) {
    Evernote.logger.debug( "TabManager.onTabClose()" );

    var tabId = event.target.linkedPanel;
    this.notify( "onTabClose", tabId );
};

Evernote.TabManager.prototype.cleanUp = function() {
    Evernote.logger.debug( "TabManager.cleanUp()" );

    gBrowser.tabContainer.removeEventListener( "TabClose", this._onTabCloseFn, false );
    gBrowser.tabContainer.removeEventListener( "TabSelect", this._onTabSelectFn, false );
    gBrowser.removeTabsProgressListener( this );

    this._listeners = [ ];
};