//"use strict";

Evernote.AddonListener = function AddonListener () {
    this.initialize();
};

Evernote.mixin( Evernote.AddonListener, Evernote.BaseListener );

Evernote.AddonListener.prototype._disabled = false;
Evernote.AddonListener.prototype._uninstalled = false;
Evernote.AddonListener.prototype._listeners = null;

Evernote.AddonListener.prototype.initialize = function() {
    Evernote.logger.debug( "AddonListener.initialize()" );

    this._listeners = [ ];

    AddonManager.addAddonListener( this );
    AddonManager.addInstallListener( this );

    var observerService = Components.classes[ "@mozilla.org/observer-service;1" ].getService( Components.interfaces.nsIObserverService );
    observerService.addObserver( this, "quit-application-granted", false );
};

Evernote.AddonListener.prototype.onDisabling = function( addon/*, needsRestart*/ ) {
    if ( addon.id != Evernote.EXTENSIOD_ID ) {
        return;
    }

    Evernote.logger.debug( "AddonListener.onDisabling()" );
    this._disabled = true;
};

Evernote.AddonListener.prototype.onUninstalling = function( addon/*, needsRestart*/ ) {
    if ( addon.id != Evernote.EXTENSIOD_ID ) {
        return;
    }

    Evernote.logger.debug( "AddonListener.onUninstalling()" );
    this._uninstalled = true;
};

Evernote.AddonListener.prototype.onOperationCancelled = function( addon ) {
    if ( addon.id != Evernote.EXTENSIOD_ID ) {
       return;
    }

    Evernote.logger.debug( "AddonListener.onOperationCancelled()" );
    if ( this._uninstalled ) {
        this._uninstalled = false;
    }
    else if ( this._disabled ) {
        this._disabled = false;
    }
};

Evernote.AddonListener.prototype.onNewInstall = function( install ) {
    if ( install.addon.id != Evernote.EXTENSIOD_ID ) {
        return;
    }

    Evernote.logger.debug( "AddonListener: plugin has updated" );
    Evernote.prefsManager.deletePref( Evernote.PrefKeys.IS_START_PAGE_SHOWED );
};

Evernote.AddonListener.prototype.observe = function( subject, topic/*, data*/ ) {
    if ( topic == "quit-application-granted" ) {
        if ( this._disabled ) {
            this.notify( "onPluginDisabled" );
        }
        else if ( this._uninstalled ) {
            this.notify( "onPluginUninstalled" );
        }
    }
};

Evernote.AddonListener.prototype.getExtensionVersion = function( callback ) {
    AddonManager.getAddonByID( Evernote.EXTENSIOD_ID, function( addon ) {
        if ( typeof callback == "function" ) {
            callback( addon.version );
        }
    } );
};

Evernote.AddonListener.prototype.cleanUp = function() {
    Evernote.logger.debug( "AddonListener.cleanUp()" );

    var observerService = Components.classes[ "@mozilla.org/observer-service;1" ].getService( Components.interfaces.nsIObserverService );
    observerService.removeObserver( this, "quit-application-granted" );

    AddonManager.removeInstallListener( this );
    AddonManager.removeAddonListener( this );

    this._listeners = [ ];
};







