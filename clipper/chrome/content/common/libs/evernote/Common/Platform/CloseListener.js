//"use strict";

Evernote.CloseListener = function CloseListener() {
    this.initialize();
};

Evernote.mixin( Evernote.CloseListener, Evernote.BaseListener );

Evernote.CloseListener.prototype._onWindowCloseFn = null;
Evernote.CloseListener.prototype._listeners = null;

Evernote.CloseListener.prototype.initialize = function() {
    Evernote.logger.debug( "CloseListener.initialize()" );

    this._listeners = [ ];

    var self = this;
    this._onWindowCloseFn = function( event ) {
        self.notify( "onWindowClose", event );
        if ( typeof evernote_finish == "function" ) {
            evernote_finish();
        }
        self.cleanUp();
    };

    window.addEventListener( "unload", this._onWindowCloseFn, false );
};

Evernote.CloseListener.prototype.cleanUp = function() {
    Evernote.logger.debug( "CloseListener.cleanUp()" );

    window.removeEventListener( "unload", this._onWindowCloseFn, false );
    this._listeners = [ ];
};


