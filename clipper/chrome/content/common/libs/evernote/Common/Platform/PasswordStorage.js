//"use strict";

Evernote.PasswordStorage = function PasswordStorage() {
    this.__defineGetter__( "service", this.getService );
};

Evernote.PasswordStorage.prototype._service = null;

Evernote.PasswordStorage.prototype.savePassword = function( userName, password ) {
    Evernote.logger.debug( "PasswordStorage.savePassword(): username = " + userName );

    try {
        var newInfo = this.createLoginInfo( userName, password );
        var prevInfo = this.getLoginInfo( userName );

        if ( prevInfo ) {
            this.service.modifyLogin( prevInfo, newInfo );
        }
        else {
            this.service.addLogin( newInfo );
        }
    }
    catch ( e ) {
        Evernote.logger.error( "PasswordStorage.savePassword() failed: username = "  + userName + ", error = " + e );
    }
};

Evernote.PasswordStorage.prototype.loadPassword = function( userName ) {
    Evernote.logger.debug( "PasswordStorage.loadPassword(): username = " + userName );

    try {
        var loginInfo = this.getLoginInfo( userName );
        if ( loginInfo ) {
            return loginInfo.password;
        }
    }
    catch ( e ) {
        Evernote.logger.error( "PasswordStorage.loadPassword() failed: username = " + userName + ", error = " + e );
    }
};

Evernote.PasswordStorage.prototype.removePassword = function( userName ) {
    Evernote.logger.debug( "PasswordStorage.removePassword(): username = " + userName );

    try {
        var info = this.getLoginInfo( userName );
        if ( info ) {
            this.service.removeLogin( info );
        }
    }
    catch ( e ) {
        Evernote.logger.error( "PasswordStorage.removePassword() failed: username = " + userName + ", error = " + e );
    }
};

Evernote.PasswordStorage.prototype.hasMasterPassword = function() {
    Evernote.logger.debug( "PasswordStorage.hasMasterPassword()" );

    try {
        var tokendb = Components.classes[ "@mozilla.org/security/pk11tokendb;1" ].createInstance( Components.interfaces.nsIPK11TokenDB );
        var token = tokendb.getInternalKeyToken();
        return token.needsLogin();
    }
    catch ( e ) {
        Evernote.logger.error( "PasswordStorage.hasMasterPassword() failed: error = " + e );
    }

    return true;
};

Evernote.PasswordStorage.prototype.createLoginInfo = function( userName, password ) {
    var nsLoginInfo = new Components.Constructor( "@mozilla.org/login-manager/loginInfo;1", Components.interfaces.nsILoginInfo, "init" );
    var url = Evernote.getSecureServiceUrl();
    return new nsLoginInfo( url, url, null, userName, password, "username", "password" );
};

Evernote.PasswordStorage.prototype.getLoginInfo = function( userName ) {
    var url = Evernote.getSecureServiceUrl();
    var loginInfos = this.service.findLogins( { }, url, url, null );

    for ( var i = 0; i < loginInfos.length; ++i ) {
        if ( loginInfos[ i ].username == userName ) {
            return loginInfos[ i ];
        }
    }
};

Evernote.PasswordStorage.prototype.getService = function() {
    if ( !this._service ) {
        this._service = Components.classes[ "@mozilla.org/login-manager;1" ].getService( Components.interfaces.nsILoginManager );
    }

    return this._service;
};


