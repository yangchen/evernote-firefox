//"use strict";

Evernote.Platform = {
    isWin : function() {
        return (navigator.platform == "Win32" || navigator.platform == "Win64") && navigator.oscpu.toLowerCase().indexOf( "windows" ) >= 0;
    },

    isMacOS : function () {
        return navigator.platform == "MacIntel" && ( navigator.oscpu.indexOf("Intel Mac OS X 10") != -1 );
    },

    isLinux: function() {
        return navigator.platform.toLowerCase().indexOf( "linux" ) >=0;
    },

    isFF4 : function() {
        return navigator.userAgent.toLowerCase().indexOf( "firefox/4" ) >= 0;
    },

    isFF5 : function() {
        return navigator.userAgent.toLowerCase().indexOf( "firefox/5" ) >= 0;
    },

    isFF6 : function () {
        return navigator.userAgent.toLowerCase().indexOf( "firefox/6" ) >= 0;
    },

	isFF7 : function () {
        return navigator.userAgent.toLowerCase().indexOf( "firefox/7" ) >= 0;
    },

    isFF8 : function () {
        return navigator.userAgent.toLowerCase().indexOf( "firefox/8" ) >= 0;
    },

    isFF9 : function () {
        return navigator.userAgent.toLowerCase().indexOf( "firefox/9" ) >= 0;
    },

    isFF10 : function () {
        return navigator.userAgent.toLowerCase().indexOf( "firefox/10" ) >= 0;
    },

    isFF11 : function () {
        return navigator.userAgent.toLowerCase().indexOf( "firefox/11" ) >= 0;
    },

    isFF12 : function () {
        return navigator.userAgent.toLowerCase().indexOf( "firefox/12" ) >= 0;
    },

    getFirefoxVersion : function() {
        var userAgent = navigator.userAgent.toLowerCase();
        var pos = userAgent.indexOf( "firefox/" );
        if ( pos > 0 ) {
            return parseInt( userAgent.substr( pos + 8, 10 ) );
        }
                
        return 4;
    }
};


