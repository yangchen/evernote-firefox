//"use strict";

/**
 * Evernote.ScopedLocalizableError is like a Evernote.SimpleError but provides an error code and
 * optionally a message and a parameter. Parameter indicates which part of the
 * request this error refers to.
 */

Evernote.ScopedLocalizableError = function ScopedLocalizableError( errorCodeOrObj, message, parameter ) {
    this.initialize( errorCodeOrObj, message, parameter );
};

Evernote.ScopedLocalizableError.javaClass = "net.sourceforge.stripes.validation.ScopedLocalizableError";
Evernote.inherit( Evernote.ScopedLocalizableError, Evernote.SimpleError, true );

