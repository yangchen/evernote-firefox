//"use strict";

Evernote.EvernoteError = function EvernoteError( errorCodeOrObj, message, parameter ) {
    this.__defineGetter__( "modelName", this.getModelName );
    this.__defineGetter__( "errorCode", this.getErrorCode );
    this.__defineSetter__( "errorCode", this.setErrorCode );
    this.__defineGetter__( "message", this.getMessage );
    this.__defineSetter__( "message", this.setMessage );
    this.__defineGetter__( "parameter", this.getParameter );
    this.__defineSetter__( "parameter", this.setParameter );
    this.initialize( errorCodeOrObj, message, parameter );
};

Evernote.EvernoteError.CLASS_FIELD = "javaClass";
Evernote.EvernoteError.ERROR_CODE_FIELD = "errorCode";
Evernote.EvernoteError.MESSAGE_FIELD = "message";
Evernote.EvernoteError.PARAMETER_FIELD = "parameter";
Evernote.EvernoteError.javaClass = "com.evernote.web.EvernoteError";

Evernote.EvernoteError.prototype._errorCode = null;
Evernote.EvernoteError.prototype._message = null;
Evernote.EvernoteError.prototype._parameter = null;

Evernote.EvernoteError.prototype.handleInheritance = function() {
    if ( typeof this.prototype.constructor[ this.constructor.CLASS_FIELD ] == 'string' ) {
        this.constructor.Registry[ this.prototype.constructor[ this.constructor.CLASS_FIELD ] ] = this;
    }

    this.prototype.constructor.modelName = this.prototype.constructor.toString().replace( /\n/g, "" ).replace( /^.*function\s+([^\(]+).*$/, "$1" );
};

Evernote.EvernoteError.prototype.initialize = function( errorCodeOrObj, message, parameter ) {
    Evernote.logger.debug( "EvernoteError.initialize(): message = " + message );

    if ( errorCodeOrObj && typeof errorCodeOrObj == 'object' ) {
        if ( typeof errorCodeOrObj[ this.constructor.ERROR_CODE_FIELD ] != 'undefined' ) {
            this.errorCode = errorCodeOrObj[ this.constructor.ERROR_CODE_FIELD ];
        }

        if ( typeof errorCodeOrObj[ this.constructor.MESSAGE_FIELD ] != 'undefined' ) {
            this.message = errorCodeOrObj[ this.constructor.MESSAGE_FIELD ];
        }

        if ( typeof errorCodeOrObj[ this.constructor.PARAMETER_FIELD ] != 'undefined' ) {
            this.parameter = errorCodeOrObj[ this.constructor.PARAMETER_FIELD ];
        }
    }
    else {
        if ( typeof errorCodeOrObj != 'undefined' ) {
            this.errorCode = errorCodeOrObj;
        }

        if ( typeof message != 'undefined' ) {
            this.message = message;
        }

        if ( typeof parameter != 'undefined' ) {
            this.parameter = parameter;
        }
    }

    this.constructor.modelName = this.modelName;
};

Evernote.EvernoteError.prototype.getClassName = function() {
    return (typeof this.constructor[ this.constructor.CLASS_FIELD ] != 'undefined') ? this.constructor[ this.constructor.CLASS_FIELD ] : null;
};

Evernote.EvernoteError.prototype.getShortClassName = function() {
    var className = this.getClassName();
    if ( typeof className == 'string' ) {
        var parts = className.split( "." );
        className = parts[parts.length - 1];
    }
    
    return className;
};

Evernote.EvernoteError.prototype.toString = function() {
    var className = this.getShortClassName();
    if ( className == null ) {
        return "Evernote.EvernoteError";
    }

    return className + " (" + this.errorCode + ") [" + this.parameter + "]" + ((typeof this.message == 'string') ? this.message : "");
};

Evernote.EvernoteError.Registry = { };

Evernote.EvernoteError.responsibleConstructor = function( className ) {
    if ( typeof this.Registry[ className ] == 'function' ) {
        return this.Registry[ className ];
    }
    else {
        return this;
    }
};

Evernote.EvernoteError.fromObject = function( obj ) {
    Evernote.logger.debug( "EvernoteError.fromObject()" );

    if ( obj instanceof Evernote.EvernoteError ) {
        return obj;
    }
    else if ( typeof obj == 'object' && obj ) {
        return this.unmarshall( obj );
    }
    else {
        return new this();
    }
};

Evernote.EvernoteError.unmarshall = function( obj ) {
    Evernote.logger.debug( "EvernoteError.unmarshall()" );

    var newObj = null;
    if ( typeof obj != 'object' || !obj ) {
        return obj;
    }

    if ( typeof obj[ this.CLASS_FIELD ] == 'string' && typeof this.Registry[ obj[ this.CLASS_FIELD ] ] == 'function' ) {
        var constr = this.Registry[ obj[ this.CLASS_FIELD ] ];
        newObj = new constr();
    }
    else {
        newObj = new this();
    }

    if ( newObj ) {
        if ( obj.errorCode ) {
            newObj.errorCode = obj.errorCode;
        }

        if ( obj.message ) {
            newObj.message = obj.message;
        }
        
        if ( obj.parameter ) {
            newObj.parameter = obj.parameter;
        }
    }

    return newObj;
};

Evernote.EvernoteError.prototype.getModelName = function() {
    return this.constructor.toString().replace( /\n/g, "" ).replace( /^.*function\s+([^\(]+).*$/, "$1" );
};

Evernote.EvernoteError.prototype.getErrorCode = function() {
    return this._errorCode;
};

Evernote.EvernoteError.prototype.setErrorCode = function( errorCode ) {
    this._errorCode = (isNaN( parseInt( errorCode ) )) ? null : parseInt( errorCode );
};

Evernote.EvernoteError.prototype.getMessage = function() {
    return this._message;
};

Evernote.EvernoteError.prototype.setMessage = function( message ) {
    this._message = (typeof message == "string") ? message : null;
};

Evernote.EvernoteError.prototype.getParameter = function() {
    return this._parameter;
};

Evernote.EvernoteError.prototype.setParameter = function( param ) {
    this._parameter = param;
};