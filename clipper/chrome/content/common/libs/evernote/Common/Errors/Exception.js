//"use strict";

Evernote.Exception = function Exception( errorCodeOrObj, message ) {
    this.initialize( errorCodeOrObj, message, null );
};

Evernote.Exception.javaClass = "java.lang.Exception";
Evernote.inherit( Evernote.Exception, Evernote.EvernoteError, true );

Evernote.Exception.prototype._errorCode = Evernote.EDAMErrorCode.UNKNOWN;

