//"use strict";

Evernote.EDAMErrorCode = {
    UNKNOWN : 1,
    BAD_DATA_FORMAT : 2,
    PERMISSION_DENIED : 3,
    INTERNAL_ERROR : 4,
    DATA_REQUIRED : 5,
    LIMIT_REACHED : 6,
    QUOTA_REACHED : 7,
    INVALID_AUTH : 8,
    AUTH_EXPIRED : 9,
    DATA_CONFLICT : 10,
    ENML_VALIDATION : 11,
    SHARD_UNAVAILABLE : 12,
    // this one is not official
    VERSION_CONFLICT : 900
};

Evernote.EDAMResponseErrorCode = {
    UNKNOWN : 1,
    INVALID_RESPONSE : 2
};

