//"use strict";

/**
 * Evernote.EDAMResponseException holds various errors associated with actual responses
 * from the server
 */

Evernote.EDAMResponseException = function EDAMResponseException( errorCodeOrObj, message ) {
    this.initialize( errorCodeOrObj, message );
};

Evernote.EDAMResponseException.javaClass = "com.evernote.edam.error.EDAMResponseException";
Evernote.inherit( Evernote.EDAMResponseException, Evernote.EvernoteError, true );
