//"use strict";

Evernote.EDAMUserException = function EDAMUserException( errorCodeOrObj, message, parameter ) {
    this.initialize( errorCodeOrObj, message, parameter );
};

Evernote.EDAMUserException.javaClass = "com.evernote.edam.error.EDAMUserException";
Evernote.inherit( Evernote.EDAMUserException, Evernote.Exception, true );
