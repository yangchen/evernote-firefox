//"use strict";

/**
 * Evernote.SimpleError is a simple type of validation error. It has no error codes
 * associated with it. Just messages.
 */

Evernote.SimpleError = function SimpleError( message ) {
    this.initialize( null, message, null );
};

Evernote.SimpleError.javaClass = "net.sourceforge.stripes.validation.SimpleError";
Evernote.inherit( Evernote.SimpleError, Evernote.ValidationError, true );

