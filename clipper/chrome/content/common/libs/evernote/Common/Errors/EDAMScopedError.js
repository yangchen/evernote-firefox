//"use strict";

/**
 * Evernote.EDAMScopedError is like a Evernote.ScopedLocalizableError but is bound to
 * Evernote.EDAMErrorCode's. These are typically included in a response when a custom
 * validation failed.
 */

Evernote.EDAMScopedError = function EDAMScopedError( errorCodeOrObj, message, parameter ) {
    this.initialize( errorCodeOrObj, message, parameter );
};

Evernote.EDAMScopedError.javaClass = "com.evernote.web.Evernote.EDAMScopedError";
Evernote.inherit( Evernote.EDAMScopedError, Evernote.ScopedLocalizableError, true );

