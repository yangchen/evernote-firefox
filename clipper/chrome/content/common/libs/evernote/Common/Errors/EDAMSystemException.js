//"use strict";

Evernote.EDAMSystemException = function EDAMSystemException( errorCodeOrObj, message ) {
    this.initialize( errorCodeOrObj, message );
};

Evernote.EDAMSystemException.javaClass = "com.evernote.edam.error.EDAMSystemException";
Evernote.inherit( Evernote.EDAMSystemException, Evernote.Exception, true );