//"use strict";

/**
 * Evernote.ValidationError is a base for various types of validation errors. Validation
 * errors are responses from the server indicating that there was a problem
 * validating request with its parameters.
 */

Evernote.ValidationError = function ValidationError( errorCodeOrObj, message, parameter ) {
    this.initialize( errorCodeOrObj, message, parameter );
};

Evernote.ValidationError.GLOBAL_PARAMETER = "__stripes_global_error";

Evernote.ValidationError.javaClass = "net.sourceforge.stripes.validation.ValidationError";
Evernote.inherit( Evernote.ValidationError, Evernote.EvernoteError, true );

Evernote.ValidationError.prototype.isGlobal = function() {
    return this.parameter == null || this.parameter == this.constructor.GLOBAL_PARAMETER;
};
