//"use strict";

Evernote.IS_WINDOWS = (navigator.platform.indexOf( "Win" ) != -1);
Evernote.SLASH = (Evernote.IS_WINDOWS) ? "\\" : "/";

Evernote.RequestType = {
    UNKNOWN : 0,
    // indicates that a clip was made from a page
    PAGE_CLIP_SUCCESS : 1,
    // indicates that a clip failed to be created from a page
    PAGE_CLIP_FAILURE : 2,
    // indicates that a clip with content was made from a page
    PAGE_CLIP_CONTENT_SUCCESS : 3,
    // indicates that a clip with content failed to be created from a page
    PAGE_CLIP_CONTENT_FAILURE : 4,
    // indicates that clip was synchronized with the server
    CLIP_SUCCESS : 11,
    // indicates that clip failed to synchronize with the server
    CLIP_FAILURE : 12,
    // indicates that there was an HTTP transport error while syncing page clip to
    // the server
    CLIP_HTTP_FAILURE : 22,
    // indicates that clip was filed on the server
    CLIP_FILE_SUCCESS : 31,
    // indicates that clip failed to fil on the server
    CLIP_FILE_FAILURE : 32,
    // indicates that there was an HTTP transport error while filing a note on the
    // server
    CLIP_FILE_HTTP_FAILURE : 42,
    // used to signal listener to cancel a timer that's waiting on page clip
    CANCEL_PAGE_CLIP_TIMER : 100,
    // used to signal that options have been updated
    OPTIONS_UPDATED : 320,
    // used to signal that content script loading timed out
    CONTENT_SCRIPT_LOAD_TIMEOUT : 401,
    // used to clear clip preview
    PREVIEW_CLIP_ACTION_CLEAR : 4010,
    // indicates user-preference for previewing full page
    PREVIEW_CLIP_ACTION_FULL_PAGE : 4011,
    // indicates user-preference for previewing article portion of the page
    PREVIEW_CLIP_ACTION_ARTICLE : 4012,
    // indicates user-preference for previewing user-selected portion of the page
    PREVIEW_CLIP_ACTION_SELECTION : 4013,
    // indicates user-preference for previewing just the URL to the page
    PREVIEW_CLIP_ACTION_URL : 4014,
    // indicates logout action
    LOGOUT : 10000
};

Evernote.Limits = {
    EDAM_USER_USERNAME_LEN_MIN : 1,
    EDAM_USER_USERNAME_LEN_MAX : 64,
    EDAM_USER_USERNAME_REGEX : "^[a-z0-9]([a-z0-9_-]{0,62}[a-z0-9])?$",

    EDAM_USER_PASSWORD_LEN_MIN : 6,
    EDAM_USER_PASSWORD_LEN_MAX : 64,
    EDAM_USER_PASSWORD_REGEX : "^[A-Za-z0-9!#$%&'()*+,./:;<=>?@^_`{|}~\\[\\]\\\\-]{6,64}$",

    EDAM_NOTE_TITLE_LEN_MIN : 0,
    EDAM_NOTE_TITLE_LEN_MAX : 255,
    EDAM_NOTE_TITLE_REGEX : "^$|^[^\\s\\r\\n\\t]([^\\n\\r\\t]{0,253}[^\\s\\r\\n\\t])?$",

    EDAM_TAG_NAME_LEN_MIN : 1,
    EDAM_TAG_NAME_LEN_MAX : 100,
    EDAM_TAG_NAME_REGEX : "^[^,\\s\\r\\n\\t]([^,\\n\\r\\t]{0,98}[^,\\s\\r\\n\\t])?$",

    EDAM_NOTE_TAGS_MIN : 1,
    EDAM_NOTE_TAGS_MAX : 20,

    CLIP_NOTE_CONTENT_LEN_MAX : 10485760,
    CLIP_CONTENT_LENGTH_MAX : 5242880
};

Evernote.CLIPPER_PREFIX = "extensions.evernote.webclipper3.";

Evernote.PrefKeys = {
    IS_DESKTOP_SELECTED : Evernote.CLIPPER_PREFIX + "is_desktop_selected",
    IS_FIRST_START : Evernote.CLIPPER_PREFIX + "is_first_start",
    PROCESSING_CLIPLIST : Evernote.CLIPPER_PREFIX + "processing_cliplist",
    IS_START_PAGE_SHOWED : Evernote.CLIPPER_PREFIX + "is_start_page_showed",
    IS_INPUT_DISABLED : Evernote.CLIPPER_PREFIX + "is_input_disabled",
    IS_CLIP_METHOD_SELECTED : Evernote.CLIPPER_PREFIX + "is_clip_method_selected",
    LAST_IS_DESKTOP_INSTALLED : Evernote.CLIPPER_PREFIX + "last_is_desktop_installed",
    SUPPRESS_SMART_FILLING_NOTICE: Evernote.CLIPPER_PREFIX + "suppress_smart_filing_notice"
};

Evernote.CLIPPER_NAME = "EvernoteFirefoxClipper";

Evernote.WebPaths = {
    LOGIN_PATH : "/jclip.action?login",
    LOGOUT_PATH : "/jclip.action?logout",
    CLIP_PATH : "/jclip.action?clip",
    SYNC_STATE_PATH : "/jclip.action?syncState",
    REGISTRATION_PATH : "/Registration.action",
    HOME_PATH : "/Home.action",
    ACCOUNT_SETTINGS_PATH: "/Settings.action",
    FILE_NOTE_PATH : "/jclip.action?file",
    DELETE_NOTE_PATH : "/jclip.action?delete",
    FIND_NOTES_PATH : "/jclip.action?find",
    FIND_META_NOTES_PATH : "/jclip.action?findMeta",
    SHARD_PATH : "/shard",
    NOTE_VIEW_PATH : "/view/",
    EVERNOTE_SEARCH_PATH : "/Home.action",
    NEW_NOTE_PATH : "/edit.jsp?newNote&locale=",
    COUNT_NOTES_PATH : "/jclip.action?countNotes",
    NOTE_SNIPPETS_PATH : "/jclip.action?noteSnippets",
    NOTE_EDIT_PATH : "/edit/",
    ACCOUNT_PATH : "/Login.action",
    SET_AUTH_TOKEN_PATH : "/SetAuthToken.action",
    CHECK_VERSION_PATH : "/jclip.action?checkVersion",
    START_PAGE_PATH: "/about/download/web_clipper.php"
};
























