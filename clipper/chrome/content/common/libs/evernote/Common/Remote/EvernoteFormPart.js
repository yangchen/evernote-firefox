//"use strict";

Evernote.EvernoteFormPart = function( obj ) {
    this.__defineGetter__( "name", this.getName );
    this.__defineSetter__( "name", this.setName );
    this.__defineGetter__( "data", this.getData );
    this.__defineSetter__( "data", this.setData );
    this.initialize( obj );
};

Evernote.EvernoteFormPart.HEADER_CONTENT_DISPOSITION = "Content-Disposition: ";
Evernote.EvernoteFormPart.HEADER_FORM_DATA = "form-data";
Evernote.EvernoteFormPart.HEADER_SEPARATOR = "; ";
Evernote.EvernoteFormPart.HEADER_KEYVAL_SEPARATOR = "=";
Evernote.EvernoteFormPart.HEADER_NAME = "name";
Evernote.EvernoteFormPart.CR = "\r\n";

Evernote.EvernoteFormPart.prototype._name = null;
Evernote.EvernoteFormPart.prototype._data = null;

Evernote.EvernoteFormPart.prototype.initialize = function( obj ) {
    Evernote.logger.debug( "EvernoteFormPart.initialize()" );

    if ( typeof obj == 'object' && obj != null ) {
        this.name = (typeof obj.name != 'undefined') ? obj.name : null;
        this.data = (typeof obj.data != 'undefined') ? obj.data : null;
    }
};

Evernote.EvernoteFormPart.prototype.getHeader = function() {
    return this.constructor.HEADER_CONTENT_DISPOSITION + this.constructor.HEADER_FORM_DATA + this.constructor.HEADER_SEPARATOR
           + this.constructor.HEADER_NAME + this.constructor.HEADER_KEYVAL_SEPARATOR + "\"" + this.name + "\"";
};

Evernote.EvernoteFormPart.prototype.setName = function( name ) {
    if ( name == null ) {
        this._name = null;
    }
    else {
        this._name = name + "";
    }
};

Evernote.EvernoteFormPart.prototype.getName = function() {
    return this._name;
};

Evernote.EvernoteFormPart.prototype.setData = function( data ) {
    if ( data == null ) {
        this._data = null;
    }
    else {
        this._data = data + "";
    }
};

Evernote.EvernoteFormPart.prototype.getData = function() {
    return this._data;
};

Evernote.EvernoteFormPart.prototype.toJSON = function() {
    return {
        name : this.name,
        data : this.data
    };
};

Evernote.EvernoteFormPart.prototype.toString = function() {
    return this.getHeader() + this.constructor.CR + this.constructor.CR + (this.data + "");
};

