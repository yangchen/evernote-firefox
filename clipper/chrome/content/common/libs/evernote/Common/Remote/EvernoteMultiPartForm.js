//"use strict";

Evernote.EvernoteMultiPartForm = function( data ) {
    this.__defineGetter__( "data", this.getData );
    this.__defineSetter__( "data", this.setData );
    this.initialize( data );
};

Evernote.EvernoteMultiPartForm.CONTENT_TYPE = "multipart/form-data";
Evernote.EvernoteMultiPartForm.BOUNDARY_MARK = "--";
Evernote.EvernoteMultiPartForm.BOUNDARY_PREFIX = "----EvernoteFormBoundary";
Evernote.EvernoteMultiPartForm.HEADER_SEPARATOR = "; ";
Evernote.EvernoteMultiPartForm.HEADER_BOUNDARY = "boundary";
Evernote.EvernoteMultiPartForm.CR = "\r\n";

Evernote.EvernoteMultiPartForm.createBoundary = function() {
    return this.BOUNDARY_PREFIX + (new Date().getTime());
};

Evernote.EvernoteMultiPartForm.prototype._data = null;
Evernote.EvernoteMultiPartForm.prototype._boundary = null;

Evernote.EvernoteMultiPartForm.prototype.initialize = function( data ) {
    Evernote.logger.debug( "EvernoteMultiPartForm.initialize()" );

    this._boundary = this.constructor.createBoundary();
    this.data = data;
};

Evernote.EvernoteMultiPartForm.prototype.setData = function( data ) {
    if ( typeof data == 'object' ) {
        this._data = data;
    }
};

Evernote.EvernoteMultiPartForm.prototype.getData = function() {
    return this._data;
};

Evernote.EvernoteMultiPartForm.prototype.getContentTypeHeader = function() {
    return this.constructor.CONTENT_TYPE + this.constructor.HEADER_SEPARATOR + this.constructor.HEADER_BOUNDARY + "=" + this._boundary;
};

Evernote.EvernoteMultiPartForm.prototype.toJSON = function() {
    return {
        contentType : this.getContentTypeHeader(),
        boundary : this._boundary,
        data : this.data
    };
};

Evernote.EvernoteMultiPartForm.prototype.toString = function() {
    var str = "";
    if ( this._data ) {
        for ( var i in this._data ) {
            if ( this._data[ i ] == null || (this._data[ i ] + "").length == 0 ) {
                continue;
            }

            str += this.constructor.BOUNDARY_MARK + this._boundary + this.constructor.CR;
            str += (new Evernote.EvernoteFormPart( { name : i, data : this._data[ i ] } )).toString();
            str += this.constructor.CR;
        }
    }
    
    str += this.constructor.BOUNDARY_MARK + this._boundary + this.constructor.BOUNDARY_MARK + this.constructor.CR;
    return str;
};
