//"use strict";

Evernote.EvernoteRemote = function EvernoteRemote() {
    this._clipSubmitter = new Evernote.Chrome.JsonRpc( null, ["NoteStoreExtra.clipNote"] );
};

Evernote.EvernoteRemote.CLASS_IDENTIFIER = "javaClass";
Evernote.EvernoteRemote.RESPONSE_ERROR_KEY = "errors";
Evernote.EvernoteRemote.RESPONSE_RESULT_KEY = "result";

Evernote.EvernoteRemote.prototype._clipSubmitter = null;

Evernote.EvernoteRemote.prototype.authenticate = function( username, password, rememberMe, success, failure, processResponse ) {
    Evernote.logger.debug( "EvernoteRemote.authenticate()" );

    rememberMe = (typeof rememberMe == 'undefined' || !(rememberMe)) ? false : true;
    var data = {
        username : username,
        password : password,
        rememberMe : rememberMe
    };

    return this.postJson( Evernote.getLoginUrl(), data, success, failure, processResponse );
};

Evernote.EvernoteRemote.prototype.logout = function( success, failure, processResponse ) {
    Evernote.logger.debug( "EvernoteRemote.logout()" );
    return this.postJson( Evernote.getLogoutUrl(), { }, success, failure, processResponse );
};

Evernote.EvernoteRemote.prototype.getSyncState = function( updateCount, success, failure, processResponse ) {
    Evernote.logger.debug( "EvernoteRemote.getSyncState(): updateCount = " + updateCount );

    updateCount = parseInt( updateCount );
    var data = { };
    if ( updateCount > 0 ) {
        data.updateCount = updateCount;
    }

    try {
        Evernote.logger.debug( "Get: " + Evernote.getSyncStateUrl() + "&" + ((data) ? JSON.stringify( data ).replace( /,/g, "&" ) : "") );
    }
    catch ( e ) {
    }

    return this.postJson( Evernote.getSyncStateUrl(), data, success, failure, processResponse );
};

Evernote.EvernoteRemote.prototype.clip = function( clipNote, success, failure, processResponse ) {
    Evernote.logger.debug( "EvernoteRemote.clip()" );

    var data = null;
    if ( clipNote instanceof Evernote.ClipNote ) {
        data = clipNote.toStorable();
    }
    else if ( typeof clipNote == 'object' && clipNote != null ) {
        Evernote.logger.warn( "Passed object was not a Evernote.ClipNote... trying to make one" );
        data = (new Evernote.ClipNote( clipNote )).toStorable();
    }

    var guid = clipNote.notebookGuid;
    if ( guid.match( /shared/ ) ) {
        this.clipToSharedNotebook( clipNote, success, failure, processResponse );
    }
    else {
        return this.postJson( Evernote.getClipUrl(), data, success, failure, processResponse, true );
    }
};

Evernote.EvernoteRemote.prototype.clipToSharedNotebook = function( clipNote, success, failure, processResponse ) {
    var linkedNotebook = Evernote.context.getLinkedNotebookByGuid( clipNote.notebookGuid );
    if ( linkedNotebook ) {
        var auth = linkedNotebook.auth;
        var url = Evernote.getSecureServiceUrl();

        var matches = auth.match( /^"?S=(s\d+)/ );
        if ( matches ) {
            var shardId = matches[1];
            url += "/shard/" + shardId;
        }

        var httpErrorHandler = function() {
            Evernote.logger.error( "EvernoteRemote.clipToSharedNotebook() failed: HTTP error" );

            var response = {
                status : 0
            };
            if ( typeof failure == "function" ) {
                failure( response, "error", null );
            }
        };

        var appLayerSuccessHandler = function( noteGuid ) {
            Evernote.logger.debug( "EvernoteRemote.clipToSharedNotebook(): successfull response" );

            var xhr = {
                status: 200
            };
            var data = {
                note : {
                    title: clipNote.title,
                    guid: noteGuid,
                    url: clipNote.url,
                    shareKey: linkedNotebook.shareKey,
                    shardId: shardId
                }
            };

            var response = new Evernote.EDAMResponse();
            response.result = data;
            response.type = Evernote.EDAMResponse.TYPE_OBJECT;

            if ( typeof success == "function" ) {
                success( response, "success", xhr );
            }
        };

        var appLayerErrorHandler = function( exception ) {
            Evernote.logger.debug( "EvernoteRemote.clipToSharedNotebook(): application layer error" );

            var response = new Evernote.EDAMResponse();
            response.type = Evernote.EDAMResponse.TYPE_ERROR;

            if ( exception && exception.trace ) {
                if ( exception.trace.match( /EDAMNotFoundException/ ) && exception.trace.match( /identifier:Note\.notebookGuid/ ) ) {
                    var error = new Evernote.EvernoteError( Evernote.EDAMErrorCode.BAD_DATA_FORMAT );
                }
                else if ( exception.trace.match( /EDAMUserException/ ) && exception.trace.match( /QUOTA_REACHED/ ) ) {
                    error = new Evernote.EvernoteError( Evernote.EDAMErrorCode.QUOTA_REACHED );
                }
                else if ( exception.trace.match( /MalformedURLException/ ) ) {
                    error = new Evernote.EvernoteError( Evernote.EDAMErrorCode.ENML_VALIDATION );
                }
                else {
                    error = new Evernote.EvernoteError( Evernote.EDAMErrorCode.INVALID_AUTH );
                }
           }

            response.addError( error );
            if ( typeof failure == "function" ) {
                failure( response, "success", null );
            }
        };

        var guid = linkedNotebook.guid.replace( /shared_/, "" );
        var noteFilling = {
            notebookGuid : guid,
            comment : clipNote.comment
        };

        var submitter = this._clipSubmitter;
        submitter.initWithUrl( url + "/json", function () {
            submitter.client.NoteStoreExtra.clipNote( function( response, exception ) {
                if ( exception ) {
                    if ( exception.code === 0 && exception.message === "" ) {
                        // Looks like a transport error.
                        httpErrorHandler();
                    }
                    else {
                        appLayerErrorHandler( exception );
                    }
                }
                else {
                    appLayerSuccessHandler( response );
                }
            },
            auth, noteFilling, clipNote.title, clipNote.url, clipNote.content );
        } );
    }
};

Evernote.EvernoteRemote.prototype.file = function( note, success, failure, processResponse ) {
    Evernote.logger.debug( "EvernoteRemote.fileNote()" );

    var data = { };
    if ( note instanceof Evernote.Note ) {
        if ( note.guid ) {
            data.noteGuid = note.guid;
        }

        if ( note.title ) {
            data.title = note.title;
        }

        if ( note.comment ) {
            data.comment = note.comment;
        }

        if ( note.notebookGuid ) {
            data.notebookGuid = note.notebookGuid;
        }

        var tagNames = note.getTagNamesAsString();
        if ( tagNames && tagNames.length > 0 ) {
            data.tagNames = tagNames;
        }
    }

    return this.postJson( Evernote.getFileNoteUrl(), data, success, failure, processResponse );
};

Evernote.EvernoteRemote.prototype.deleteNote = function( note, success, failure, processResponse ) {
    Evernote.logger.debug( "EvernoteRemote.deleteNote()" );

    var data = { };
    if ( note instanceof Evernote.Note ) {
        if ( note.guid ) {
            data.noteGuid = note.guid;
        }
    }

    return this.postJson( Evernote.getDeleteNoteUrl(), data, success, failure, processResponse );
};

Evernote.EvernoteRemote.prototype.findNotes = function( noteFilter, offset, maxNotes, success, failure, processResponse ) {
    Evernote.logger.debug( "EvernoteRemote.findNotes()" );

    var data = {
        offset : parseInt( offset ),
        maxNotes : parseInt( maxNotes )
    };

    if ( noteFilter instanceof Evernote.NoteFilter ) {
        var storable = noteFilter.toStorable();
        for ( var i in storable ) {
            if ( typeof storable[ i ] != 'undefined' && storable[ i ] != null ) {
                data[ "noteFilter." + i ] = storable[ i ];
            }
        }
    }

    return this.postJson( Evernote.getFindNotesUrl(), data, success, failure, processResponse );
};

Evernote.EvernoteRemote.prototype.findMetaNotes = function( noteFilter, resultSpec, offset, maxNotes, success, failure, processResponse ) {
    Evernote.logger.debug( "EvernoteRemote.findMetaNotes()" );

    var data = {
        offset : parseInt( offset ),
        maxNotes : parseInt( maxNotes )
    };

      if ( noteFilter instanceof Evernote.NoteFilter ) {
        var storable = noteFilter.toStorable();
        for ( var i in storable ) {
            if ( typeof storable[ i ] != 'undefined' && storable[ i ] != null ) {
                data[ "noteFilter." + i ] = storable[ i ];
            }
        }
    }

    if ( resultSpec instanceof Evernote.NotesMetadataResultSpec ) {
        storable = resultSpec.toStorable();
        for ( i in storable ) {
            if ( typeof storable[ i ] != 'undefined' && storable[ i ] != null ) {
                data[ "resultSpec." + i ] = storable[ i ];
            }
        }
    }

    return this.postJson( Evernote.getFindMetaNotesUrl(), data, success, failure, processResponse );
};

Evernote.EvernoteRemote.prototype.countNotes = function( noteFilter, success, failure, processResponse ) {
    Evernote.logger.debug( "EvernoteRemote.countNotes()" );

    var data = { };
    if ( noteFilter instanceof Evernote.NoteFilter ) {
        var storable = noteFilter.toStorable();
        for ( var i in storable ) {
            if ( typeof storable[ i ] != 'undefined' && storable[ i ] != null ) {
                data[ "noteFilter." + i ] = storable[ i ];
            }
        }
    }

    return this.postJson( Evernote.getCountNotesUrl(), data, success, failure, processResponse );
};

Evernote.EvernoteRemote.prototype.noteSnippets = function( guids, snippetLength, textOnly, success, failure, processResponse ) {
    Evernote.logger.debug( "EvernoteRemote.noteSnippets()" );

    guids = [ ].concat( guids );
    var data = {
        noteGuids : guids,
        snippetLength : snippetLength,
        textOnly : (textOnly) ? true : false
    };

    return this.postJson( Evernote.getNoteSnippetsUrl(), data, success, failure, processResponse );
};

Evernote.EvernoteRemote.prototype.checkVersion = function( clipperName, clientVersion, success, failure, processResponse ) {
    Evernote.logger.debug( "EvernoteRemote.checkVersion(): clipperName = " + clipperName + ",  clientVersion = " + clientVersion );

    var data = {
        clientName : clipperName,
        clientVersion : clientVersion
    };

    return this.postJson( Evernote.getCheckVersionUrl(), data, success, failure, processResponse );
};

Evernote.EvernoteRemote.prototype.handleHttpSuccess = function( data, textStatus, xhr ) {
    Evernote.logger.debug( "EvernoteRemote.handleHttpSuccess()" );

    try {
        var response = this.createResponse( data, function( result ) {
            if ( result != null ) {
                Evernote.logger.debug( "Unmarshalling result" );
                result = Evernote.syncManager.unmarshaller( result );
            }
            else {
                Evernote.logger.debug( "Cannot unmarshall result" );
            }

            return result;
        } );
    }
    catch ( e ) {
        var msg = "";
        if ( e instanceof Evernote.EvernoteError ) {
            msg = e.errorCode;
        }
        else if ( e instanceof Error ) {
            msg = e.message;
        }

        Evernote.logger.debug( "Exception creating Evernote.EDAMResponse: " + e );
        response = new Evernote.EDAMResponse();
        response.addError( e );
    }
    
    Evernote.logger.debug( "HTTP [" + xhr.status + ", " + textStatus + "] Type: " + response.type );
    return response;
};

Evernote.EvernoteRemote.prototype.handleHttpError = function( xhr, textStatus, error ) {
    Evernote.logger.error( "EvernoteRemote.handleHttpError(): HTTP [" + xhr.status + "," + textStatus + "] " + error );
};

Evernote.EvernoteRemote.prototype.postJson = function( url, data, success, failure, processResponse, multipart ) {
    return this.doRequest( "POST", "json", url, data, success, failure, processResponse, multipart );
};

Evernote.EvernoteRemote.prototype.getJson = function( url, data, success, failure, processResponse ) {
    return this.doRequest( "GET", "json", url, data, success, failure, processResponse );
};

Evernote.EvernoteRemote.prototype.doRequest = function( meth, dataType, url, data, success, failure, processResponse, multipart ) {
    Evernote.logger.debug( "EvernoteRemote.doRequest()" );

    if ( !Evernote.context.clientEnabled ) {
        if ( typeof success == 'function' ) {
            var userException = new Evernote.EDAMUserException( Evernote.EDAMErrorCode.VERSION_CONFLICT );
            var response = new Evernote.EDAMResponse();
            response.errors = [ userException ];

            setTimeout( function() {
                success( response, "success", xhr );
            }, 0 );
        }
        
        var xhr = { readyState : 4 };
        xhr.__proto__ = new XMLHttpRequest();
        return xhr;
    }

    var syncState = Evernote.context.syncState;
    if ( syncState instanceof Evernote.SyncState && syncState.updateCount && typeof data.updateCount == 'undefined' ) {
        data.updateCount = syncState.updateCount;
    }

    var userName = Evernote.context.currentUserName;
    if ( typeof userName == 'string' && userName.length > 0 ) {
        data.username = userName;
    }

    var dataStr = "";
    try {
        dataStr = JSON.stringify( data ).replace( /\"password\":\"[^\"]+\"/, "\"password\":\"******\"" );
    }
    catch ( e ) {
    }

    Evernote.logger.debug( "doRequest(" + meth + ", " + dataType + ", " + url + ", " + dataStr + ")" );

    var self = this;
    var errorHandler = function( xhr, textStatus, error ) {
        Evernote.logger.debug( "EvernoteRemote.doRequest() failed: response in state " + xhr.readyState );
        if ( processResponse ) {
            self.handleHttpError( xhr, textStatus, error );
        }

        if ( typeof failure == 'function' ) {
            failure( xhr, textStatus, error );
        }
    };

    var successHandler = function( data, textStatus, xhr ) {
        if ( xhr.status == 0 ) {
            return errorHandler( xhr, textStatus, data );
        }

        Evernote.logger.debug( "EvernoteRemote.doRequest(): successfull response" );
        var response = data;
        if ( processResponse && data ) {
            Evernote.logger.debug( "Logging response from server: " + data.toSource() );
            response = self.handleHttpSuccess( data, textStatus, xhr );
        }

        if ( typeof success == 'function' ) {
            success( response, textStatus, xhr );
        }
    };

    var ajaxOpts = {
        url : url,
        async : true,
        cache : false,
        data : data,
        dataType : dataType,
        error : errorHandler,
        success : successHandler,
        type : meth
    };

    if ( multipart && meth == "POST" ) {
        var form = new Evernote.EvernoteMultiPartForm( data );
        ajaxOpts.contentType = form.getContentTypeHeader();
        ajaxOpts.data = form.toString();
    }

    //Evernote.Utils.errorConsole("2 " + JSON.stringify(data));

    return Evernote.jQuery.ajax( ajaxOpts );
};

Evernote.EvernoteRemote.prototype.createResponse = function( data, resultTransform ) {
    Evernote.logger.debug( "EvernoteRemote.createResponse()" );

    var response = new Evernote.EDAMResponse();
    var dataErrors = data[ this.constructor.RESPONSE_ERROR_KEY ];
    var dataResults = data[ this.constructor.RESPONSE_RESULT_KEY ];

    if ( typeof dataErrors == 'object' && dataErrors != null ) {
        var errors = (dataErrors instanceof Array) ? dataErrors : [ dataErrors ];
        for ( var i = 0; i < errors.length; ++i ) {
            var error = errors[ i ];
            if ( typeof error == 'object' && typeof error[ this.constructor.CLASS_IDENTIFIER ] == 'string' ) {
                var eClass = Evernote.EvernoteError.responsibleConstructor( error[ this.constructor.CLASS_IDENTIFIER ] );
                if ( typeof eClass == 'function' ) {
                    response.addError( new eClass( error ) );
                }
                else {
                    response.addError( new Evernote.Exception( error ) );
                }
            }
            else if ( typeof error == 'object' ) {
                response.addError( new Evernote.Exception( error ) );
            }
            else if ( error != null ) {
                response.addError( new Error( error ) );
            }
        }

        response.type = Evernote.EDAMResponse.TYPE_ERROR;
    }
    else if ( (typeof dataResults == 'object' && dataResults != null)
              || typeof dataResults == 'string' || typeof dataResults == 'number' || typeof dataResults == 'boolean' ) {
        if ( typeof resultTransform == 'function' ) {
            response.result = resultTransform( dataResults );
        }
        else {
            response.result = dataResults;
        }

        if ( typeof response.result == 'object' && response.result != null ) {
            response.type = Evernote.EDAMResponse.TYPE_OBJECT;
        }
        else if ( typeof response.result == 'string' && response.result.match( /<[^>]+>/ ) ) {
            response.type = Evernote.EDAMResponse.TYPE_HTML;
        }
        else if ( response.result != null ) {
            response.type = Evernote.EDAMResponse.TYPE_TEXT
        }
    }
    else {
        Evernote.logger.debug( "Invalid response data: " + JSON.stringify( data ) );
        throw new Evernote.EDAMResponseException( Evernote.EDAMResponseErrorCode.INVALID_RESPONSE );
    }

    return response;
};