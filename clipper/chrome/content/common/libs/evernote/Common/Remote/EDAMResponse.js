//"use strict";

Evernote.EDAMResponse = function( data ) {
    this.__defineGetter__( "errors", this.getErrors );
    this.__defineSetter__( "errors", this.setErrors );
    this.__defineGetter__( "result", this.getResult );
    this.__defineSetter__( "result", this.setResult );
    this.__defineGetter__( "type", this.getType );
    this.__defineSetter__( "type", this.setType );
    this.initialize( data );
};

Evernote.EDAMResponse.TYPE_UNKNOWN = 0;
Evernote.EDAMResponse.TYPE_ERROR = 1;
Evernote.EDAMResponse.TYPE_TEXT = 2;
Evernote.EDAMResponse.TYPE_HTML = 3;
Evernote.EDAMResponse.TYPE_OBJECT = 4;

Evernote.EDAMResponse.prototype._result = null;
Evernote.EDAMResponse.prototype._errors = null;
Evernote.EDAMResponse.prototype._type = Evernote.EDAMResponse.TYPE_UNKNOWN;

Evernote.EDAMResponse.prototype.initialize = function( data ) {
    Evernote.logger.debug( "EDAMResponse.initialize()" );

    if ( typeof data == 'object' && typeof data[ Evernote.EvernoteRemote.RESPONSE_ERROR_KEY ] != 'undefined' ) {
        this.errors = data[ Evernote.EvernoteRemote.RESPONSE_ERROR_KEY ];
    }
    else if ( typeof data == 'object' && typeof data[ Evernote.EvernoteRemote.RESPONSE_RESULT_KEY ] != 'undefined' ) {
        this.result = data[ Evernote.EvernoteRemote.RESPONSE_RESULT_KEY ];
    }
};

Evernote.EDAMResponse.prototype.isEmpty = function() {
    return this.result == null && this.errors == null;
};

Evernote.EDAMResponse.prototype.isError = function() {
    return this.errors != null;
};

Evernote.EDAMResponse.prototype.isResult = function() {
    return this.result != null;
};

Evernote.EDAMResponse.prototype.hasAuthenticationError = function() {
    return this.isMissingAuthTokenError() || this.isPermissionDeniedError() || this.isAuthTokenExpired() || this.isInvalidAuthentication();
};

Evernote.EDAMResponse.prototype.isAuthTokenExpired = function() {
    return this.hasErrorCode( Evernote.EDAMErrorCode.AUTH_EXPIRED );
};

Evernote.EDAMResponse.prototype.isPermissionDeniedError = function() {
    return this.hasErrorCode( Evernote.EDAMErrorCode.PERMISSION_DENIED );
};

Evernote.EDAMResponse.prototype.isMissingAuthTokenError = function() {
    return this.hasErrorCodeWithParameter( Evernote.EDAMErrorCode.DATA_REQUIRED, "authenticationToken" );
};

Evernote.EDAMResponse.prototype.isInvalidAuthentication = function() {
    return this.hasErrorCode( Evernote.EDAMErrorCode.INVALID_AUTH );
};

Evernote.EDAMResponse.prototype.isVersionConflictError = function() {
    return this.hasErrorCode( Evernote.EDAMErrorCode.VERSION_CONFLICT );
};

Evernote.EDAMResponse.prototype.hasValidationErrors = function() {
    return this.hasErrorClass( Evernote.ValidationError )
};

Evernote.EDAMResponse.prototype.hasExceptions = function() {
    return this.hasErrorClass( Evernote.Exception );
};

Evernote.EDAMResponse.prototype.hasErrorCodeWithParameter = function( errorCode, parameter ) {
    Evernote.logger.debug( "EDAMResponse.hasErrorCodeWithParameter(): errorCode = " + errorCode );

    if ( !this.isError() ) {
        return false;
    }

    for ( var i = 0; i < this._errors.length; ++i ) {
        if ( this._errors[ i ].errorCode == errorCode && typeof this._errors[ i ].parameter != 'undefined'
             && this._errors[ i ].parameter == parameter ) {
            return true;
        }
    }

    return false;
};

Evernote.EDAMResponse.prototype.hasErrorClass = function( constructor ) {
    Evernote.logger.debug( "EDAMResponse.hasErrorClass()" );

    if ( !this.isError() ) {
        return false;
    }

    for ( var i = 0; i < this._errors.length; ++i ) {
        if ( this._errors[ i ] instanceof constructor ) {
            return true;
        }
    }

    return false;
};

Evernote.EDAMResponse.prototype.hasErrorCode = function( errorCode ) {
    Evernote.logger.debug( "EDAMResponse.hasErrorCode(): errorCode = " + errorCode );

    if ( !this.isError() ) {
        return false;
    }

    for ( var i = 0; i < this._errors.length; ++i ) {
        if ( this._errors[ i ].errorCode == errorCode ) {
            return true;
        }
    }

    return false;
};

Evernote.EDAMResponse.prototype.selectErrors = function( filterFn ) {
    Evernote.logger.debug( "EDAMResponse.selectErrors()" );

    var errors = [ ];
    if ( typeof filterFn == 'function' && this.isError() ) {
        for ( var i = 0; i < this._errors.length; i++ ) {
            if ( filterFn( this._errors[ i ] ) ) {
                errors.push( this._errors[ i ] );
            }
        }
    }
    
    return errors;
};

Evernote.EDAMResponse.prototype.addError = function( error ) {
    if ( !this._errors ) {
        this._errors = [ ];
    }
    
    this._errors.push( error );
};

Evernote.EDAMResponse.prototype.getErrors = function() {
    return this._errors;
};

Evernote.EDAMResponse.prototype.setErrors = function( errors ) {
    if ( !errors ) {
        this._errors = null;
    }
    else {
        this._errors = (errors instanceof Array) ? errors : [ errors ];
    }
};

Evernote.EDAMResponse.prototype.getResult = function() {
    return this._result;
};

Evernote.EDAMResponse.prototype.setResult = function( result ) {
    this._result = result;
};

Evernote.EDAMResponse.prototype.getType = function() {
    return this._type;
};

Evernote.EDAMResponse.prototype.setType = function( type ) {
    this._type = parseInt( type );
};

Evernote.EDAMResponse.prototype.toString = function() {
    return "Evernote.EDAMResponse (" + this.type + ") " + ((this.isError()) ? "ERROR" : "RESULT");
};