//"use strict";

Evernote.SyncManager = function SyncManager() {
    this.__defineGetter__( "marshaller", this.getMarshaller );
    this.__defineGetter__( "unmarshaller", this.getUnmarshaller );
    this.initialize();
};

Evernote.SyncManager.prototype._marshaller = null;
Evernote.SyncManager.prototype._unmarshaller = null;
Evernote.SyncManager.prototype._notebooks = null;
Evernote.SyncManager.prototype._tags = null;
Evernote.SyncManager.prototype._searches = null;
Evernote.SyncManager.prototype._syncState = null;
Evernote.SyncManager.prototype._syncChunk = null;

Evernote.SyncManager.prototype.initialize = function() {
    Evernote.logger.debug( "SyncManager.initialize()" );

    Evernote.cookieManager.addListener( this );
    Evernote.closeListener.addListener( this );
};

Evernote.SyncManager.prototype.remoteSyncState = function( successFn, failureFn ) {
    Evernote.logger.debug( "SyncManager.remoteSyncState()" );
    return Evernote.remote.getSyncState( null, successFn, failureFn, true );
};

Evernote.SyncManager.prototype.remoteSyncChunk = function( successFn, failureFn ) {
    Evernote.logger.debug( "SyncManager.remoteSyncChunk()" );

    var self = this;
    this.remoteSyncState(
        function( /*response, textStatus, xhr*/ ) {
            Evernote.logger.debug( "Successfully got SyncState from server" );

            var successCallback = successFn;
            var failureCallback = failureFn;

            self.remoteSyncState(
                function( /*response, textStatus, xhr*/ ) {
                    Evernote.logger.debug( "Successfully got SyncChunk from server" );
                    if ( typeof successCallback == "function" ) {
                        successCallback();
                    }
                },
                function( xhr/*, textStatus, error*/ ) {
                    Evernote.Utils.logHttpErrorMessage( xhr, "get SyncChunk" );
                    if ( typeof failureCallback == "function" ) {
                        failureCallback();
                    }
                }
            );
        },
        function( xhr/*, textStatus, error*/ ) {
            Evernote.Utils.logHttpErrorMessage( xhr, "get SyncState" );
        }
    );
};

Evernote.SyncManager.prototype.getMarshaller = function() {
    if ( !this._marshaller ) {
        this._marshaller = function( data ) {
            Evernote.logger.debug( "Marshalling data (" + (typeof data) + ")" );
            Evernote.AppModel.marshall( data );
        }
    }

    return this._marshaller;
};

Evernote.SyncManager.prototype.getUnmarshaller = function() {
    if ( !this._unmarshaller ) {
        var self = this;
        this._unmarshaller = function( result ) {
            Evernote.logger.debug( "Unmarshalling data" );
            if ( typeof result == 'object' ) {
                for ( var resultKey in result ) {
                    if ( result[ resultKey ] != null ) {
                        Evernote.logger.debug( result[ resultKey ][ "javaClass" ] );
                        var model = Evernote.AppModel.unmarshall( result[ resultKey ] );
                        result[ resultKey ] = model;
                    }
                    else {
                        model = null;
                    }

                    try {
                        switch ( resultKey ) {
                            case "authenticationResult":
                                Evernote.logger.debug( "Auth result: " + JSON.stringify( model ) );

                                if ( typeof model.user != "undefined" ){
                                    Evernote.logger.debug( "Getting user from auth result" );
                                    Evernote.context.user = model.user;
                                    self.processDelayed();
                                }
                                break;

                            case "user":
                                Evernote.logger.debug( "Getting user from server" );
                                Evernote.context.user = model;
                                self.processDelayed();
                                break;

                            case "notebooks":
                                Evernote.logger.debug( "Getting notebooks from server'" );
                                if ( !Evernote.context.currentUserName ) {
                                    self._notebooks = model;
                                }
                                else {
                                    Evernote.context.notebooks = model;
                                }
                                break;

                            case "tags":
                                Evernote.logger.debug( "Getting tags from servre" );
                                if ( !Evernote.context.currentUserName ) {
                                    self._tags = model;
                                }
                                else {
                                    Evernote.context.tags = model;
                                }
                                break;

                            case "searches":
                                Evernote.logger.debug( "Getting searches from server" );
                                if ( !Evernote.context.currentUserName ) {
                                    self._searches = model;
                                }
                                else {
                                    Evernote.context.searches = model;
                                }
                                break;

                            case "syncState":
                                Evernote.logger.debug( "Getting SyncState from server" );
                                if ( !Evernote.context.currentUserName ) {
                                    self._syncState = model;
                                }
                                else {
                                     Evernote.context.syncState = model;
                                }
                                break;

                            case "syncChunk":
                                Evernote.logger.debug( "Getting SyncChunk from server" );
                                if ( !Evernote.context.currentUserName ) {
                                    self._syncChunk = model;
                                }
                                else {
                                     Evernote.context.processSyncChunk( model );
                                }
                                break;
                        }
                    }
                    catch ( e ) {
                        Evernote.logger.error( "Unmarshalling data failed error = " + e.message );
                        Evernote.viewManager.showError( Evernote.localizer.getMessage( "syncError", e ) );
                    }
                }
            }

            Evernote.logger.debug( "Unmarshalling done" );
            return result;
        };
    }

    return this._unmarshaller;
};

Evernote.SyncManager.prototype.processDelayed = function() {
    Evernote.logger.debug( "SyncManager.processDelayed()" );

    try {
        if ( this._notebooks ) {
            Evernote.context.notebooks = this._notebooks;
            this._notebooks = null;
        }

        if ( this._tags ) {
            Evernote.context.tags = this._tags;
            this._tags = null;
        }

        if ( this._searches ) {
            Evernote.context.searches = this._searches;
            this._searches = null;
        }

        if ( this._syncState ) {
            Evernote.context.syncState = this._syncState;
            this._syncState = null;
        }

        if ( this._syncChunk ) {
            Evernote.context.processSyncChunk( this._syncChunk );
            this._syncChunk = null;
        }
    }
    catch ( e ) {
        Evernote.logger.error( "SyncManager.processDelayed() failed: error = " + e );
    }
};

Evernote.SyncManager.prototype.onCookieChange = function( subject, data ) {
    Evernote.logger.debug( "SyncManager.onCookieChange(): subject = " + subject.name + ", data = " + data );

    if ( subject.name.toLowerCase() != "auth" ) {
        return;
    }

    if ( data == "added" ) {
        Evernote.logger.debug( "Making login" );
        this.remoteSyncChunk();
    }
    else if ( data == "deleted" ) {
        Evernote.logger.debug( "Making logout" );
    }
};

Evernote.SyncManager.prototype.onWindowClose = function() {
    Evernote.logger.debug( "SyncManager.onWindowClose()" );
    Evernote.cookieManager.removeListener( this );
};