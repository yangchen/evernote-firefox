//"use strict";

Evernote.CookieManager = function CookieManager() {
    this.initialize();
};

Evernote.mixin( Evernote.CookieManager, Evernote.BaseListener );

Evernote.CookieManager.prototype._impl = null;
Evernote.CookieManager.prototype._listeners = null;

Evernote.CookieManager.prototype.initialize = function() {
    Evernote.logger.debug( "CookieManager.initialize()" );

    this._listeners = [ ];
    
    var implClass = Evernote.CookieManagerImplFactory.getImplementationFor( navigator );
    this._impl = (typeof implClass == 'function') ? new implClass() : null;

    var observerService = Components.classes[ "@mozilla.org/observer-service;1" ].getService( Components.interfaces.nsIObserverService );
    observerService.addObserver( this, "cookie-changed", false );
};

Evernote.CookieManager.prototype.observe = function( subject, topic, data ) {
    try {
        subject.QueryInterface( Components.interfaces.nsICookie );
    }
    catch ( e ) {
        return;
    }

    var host = subject.host.toLowerCase();
    if ( host.indexOf( "evernote.com" ) < 0 && host.indexOf( "yinxiang.com" ) < 0 ) {
       return;
    }

    if ( topic == "cookie-changed" ) {
        this.notify( "onCookieChange", subject, data );
    }
};

Evernote.CookieManager.prototype.get = function( url, name ) {
    if ( this._impl ) {
        return this._impl.get( url, name );
    }

    return null;
};

Evernote.CookieManager.prototype.set = function( url, name, value ) {
    if ( this._impl ) {
        this._impl.set( url, name, value );
    }
};

Evernote.CookieManager.prototype.getAll = function( url ) {
    if ( this._impl ) {
        return this._impl.getAll( url );
    }

    return null;
};

Evernote.CookieManager.prototype.remove = function( url, name ) {
    if ( this._impl ) {
        this._impl.remove( url, name );
    }
};

Evernote.CookieManager.prototype.removeAll = function( url ) {
    if ( this._impl ) {
        this._impl.removeAll( url );
    }
};

Evernote.CookieManager.prototype.cleanUp = function() {
    Evernote.logger.debug( "CookieManager.cleanUp()" );

    var observerService = Components.classes[ "@mozilla.org/observer-service;1" ].getService( Components.interfaces.nsIObserverService );
    observerService.removeObserver( this, "cookie-changed" );

    this._listeners = [ ];
};






