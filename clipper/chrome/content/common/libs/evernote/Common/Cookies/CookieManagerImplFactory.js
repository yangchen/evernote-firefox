//"use strict";

Evernote.CookieManagerImplFactory = {
    getImplementationFor : function( navigator ) {
        var reg = this.ClassRegistry;
        for ( var i = 0; i < reg.length; i++ ) {
            if ( typeof reg[ i ] == 'function' && typeof reg[ i ].isResponsibleFor == 'function'
                 && reg[ i ].isResponsibleFor( navigator ) ) {
                return reg[ i ];
            }
        }
        
        return null;
    }
};

Evernote.CookieManagerImplFactory.ClassRegistry = [ ];