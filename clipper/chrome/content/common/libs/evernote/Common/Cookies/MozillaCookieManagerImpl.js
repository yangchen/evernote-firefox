//"use strict";

Evernote.MozillaCookieManagerImpl = function MozillaCookieManagerImpl() {
    this.__defineGetter__( "ioService", this.getIOService );
    this.__defineGetter__( "cookieManagerService", this.getCookieManagerService );
};

Evernote.inherit( Evernote.MozillaCookieManagerImpl, Evernote.CookieManagerImpl, true );

Evernote.MozillaCookieManagerImpl.isResponsibleFor = function( navigator ) {
    var ua = navigator.userAgent.toLowerCase();
    return ua.indexOf( "firefox" ) >= 0;
};

Evernote.MozillaCookieManagerImpl.prototype._ioSrv = null;
Evernote.MozillaCookieManagerImpl.prototype._cookieManagerSrv = null;

Evernote.MozillaCookieManagerImpl.prototype.get = function( url, name ) {
    Evernote.logger.debug( "MozillaCookieManagerImpl.get(): url = " + url + ", name = " + name );

    try {
        var uri = this.ioService.newURI( url, null, null );
        for ( var e = this.cookieManagerService.enumerator; e.hasMoreElements(); ) {
            var cookie = e.getNext().QueryInterface( Components.interfaces.nsICookie );
            if ( cookie && cookie.host == uri.host && cookie.name == name ) {
                return new Evernote.Cookie( cookie );
            }
        }
    }
    catch ( e ) {
        Evernote.logger.error( "MozillaCookieManagerImpl.get() failed: url = " + url + ", name = " + name + ", error = " + e );
    }

    return null;
};

Evernote.MozillaCookieManagerImpl.prototype.getAll = function( url ) {
    Evernote.logger.debug( "MozillaCookieManagerImpl.getAll(), url = " + url );

    try {
        var cookies = [ ];
        var uri = (url) ? this.ioService.newURI( url, null, null ) : null;

        for ( var e = this.cookieManagerService.enumerator; e.hasMoreElements(); ) {
            var cookie = e.getNext().QueryInterface( Components.interfaces.nsICookie );
            if ( !url || uri.host == cookie.host ) {
                cookies.push( new Evernote.Cookie( cookie ) );
            }
        }
    }
    catch ( e ) {
        Evernote.logger.error( "MozillaCookieManagerImpl.getAll() failed: url = " + url + ", error = " + e );
    }

    return cookies;
};

Evernote.MozillaCookieManagerImpl.prototype.set = function( url, cookie ) {
    Evernote.logger.debug( "MozillaCookieManagerImpl.set(), url =  " + url );

    try {
        var uri = (typeof url == 'string') ? this.ioService.newURI( url, null, null ) : null;
        if ( cookie instanceof Evernote.Cookie && typeof cookie.name == 'string' && cookie.name.length > 0 ) {
            this.ioService.setCookieString( uri, null, (cookie.name + "=" + cookie.value + ";"), null );
        }
    }
    catch ( e ) {
        Evernote.logger.error( "MozillaCookieManagerImpl.set() failed, url = " + url + ", error = " + e );
    }
};

Evernote.MozillaCookieManagerImpl.prototype.remove = function( url, name ) {
    Evernote.logger.debug( "MozillaCookieManagerImpl.remove(): url = " + url + ", name = " + name );

    try {
        var urlParts = url.split( "://", 2 );
        var domain = (urlParts.length == 2) ? urlParts[ 1 ] : urlParts[ 0 ];
        urlParts = domain.split( "/", 2 );
        var path = (urlParts.length == 2) ? urlParts[ 1 ] : null;

        this.cookieManagerService.remove( domain, name, path, false );
    }
    catch ( e ) {
        Evernote.logger.error( "MozillaCookieManagerImpl.remove() failed: url = " + url + ", name = " + name + ", error = " + e );
    }
};

Evernote.MozillaCookieManagerImpl.prototype.removeAll = function( url ) {
    Evernote.logger.debug( "MozillaCookieManagerImpl.removeAll(): url = " + url );

    try {
        if ( !url ) {
            this.cookieManagerService.removeAll();
        }
        else {
            var cookies = this.getAll( url );
            for ( var i = 0; i < cookies.length; i++ ) {
                this.remove( cookies[ i ].name, url );
            }
        }
    }
    catch ( e ) {
        Evernote.logger.error( "MozillaCookieManagerImpl.removeAll() failed: url = " + url + ", error = " + e );
    }
};

Evernote.MozillaCookieManagerImpl.prototype.getIOService = function() {
    if ( !this._ioSrv ) {
        this._ioSrv = Components.classes[ "@mozilla.org/network/io-service;1" ].getService( Components.interfaces.nsIIOService );
    }

    return this._ioSrv;
};

Evernote.MozillaCookieManagerImpl.prototype.getCookieManagerService = function() {
    if ( !this._cookieManagerSrv ) {
        this._cookieManagerSrv = Components.classes[ "@mozilla.org/cookiemanager;1" ].getService( Components.interfaces.nsICookieManager );
    }

    return this._cookieManagerSrv;
};
