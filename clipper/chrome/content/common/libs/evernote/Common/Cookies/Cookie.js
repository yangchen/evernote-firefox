//"use strict";

Evernote.Cookie = function Cookie( obj ) {
    this.initialize( obj );
};

Evernote.Cookie.prototype.name = null;
Evernote.Cookie.prototype.value = null;
Evernote.Cookie.prototype.host = null;
Evernote.Cookie.prototype.path = null;
Evernote.Cookie.prototype.secure = null;
Evernote.Cookie.prototype.expires = null;

Evernote.Cookie.prototype.initialize = function( obj ) {
    Evernote.logger.debug( "Cookie.initialize()" );

    if ( typeof obj == 'object' && obj ) {
        for ( var i in obj ) {
            if ( typeof obj[ i ] != 'function' && obj[ i ] ) {
                this[ i ] = obj[ i ];
            }
        }
    }
};