//"use strict";

Evernote.CookieManagerImpl = function CookieManagerImpl() {
};

Evernote.CookieManagerImpl.handleInheritance = function( child/*, parent*/ ) {
    Evernote.CookieManagerImplFactory.ClassRegistry.push( child );
};

Evernote.CookieManagerImpl.isResponsibleFor = function( /*navigator*/ ) {
    return false;
};

Evernote.CookieManagerImpl.prototype.get = function( /*url, name*/ ) {
};

Evernote.CookieManagerImpl.prototype.getAll = function( /*url*/ ) {
};

Evernote.CookieManagerImpl.prototype.set = function( /*url, cookie*/ ) {
};

Evernote.CookieManagerImpl.prototype.remove = function( /*url, name*/ ) {
};

Evernote.CookieManagerImpl.prototype.removeAll = function( /*url*/ ) {
};