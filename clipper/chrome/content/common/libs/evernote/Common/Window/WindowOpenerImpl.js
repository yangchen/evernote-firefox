//"use strict";

Evernote.WindowOpenerImpl = function WindowOpenerImpl() {
};

Evernote.WindowOpenerImpl.handleInheritance = function( child/*, parent*/ ) {
    Evernote.WindowOpenerImplFactory.ClassRegistry.push( child );
};

Evernote.WindowOpenerImpl.isResponsibleFor = function( /*navigator*/ ) {
    return false;
};

Evernote.WindowOpenerImpl.prototype.openUrl = function( /*url, winName, winOpts*/ ) {
};
