//"use strict";

Evernote.WindowOpener = function WindowOpener() {
    this.initialize();
};

Evernote.WindowOpener.prototype._impl = null;

Evernote.WindowOpener.prototype.initialize = function() {
    Evernote.logger.debug( "WindowOpener.initialize()" );

    var implClass = Evernote.WindowOpenerImplFactory.getImplementationFor( navigator );
    if ( typeof implClass == "function" ) {
        this._impl = new implClass();
    }
};

Evernote.WindowOpener.prototype.openUrl = function( url, winName, winOptions ) {
    if ( this._impl ) {
        return this._impl.openUrl( url, winName, winOptions );
    }

    return null;
};