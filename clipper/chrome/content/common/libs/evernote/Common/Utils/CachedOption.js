/**
 * Represent options which value is cached for specified amount of time.
 * After that period the value is reloaded when the value is retrieved.
 * @param reloadInterval - number of seconds to cache the value
 * @param reloadFn - function which reloads the value (no arguments is passed when invoked)
 * @constructor
 */
Evernote.CachedOption = function (reloadInterval, reloadFn) {
    this.reloadInterval = reloadInterval * 1000;
    this.reloadFn = reloadFn;
    this.reload();
};

/**
 * Get value for the option.
 * If last retrieve time is less than reloadInterval then cached value is returned,
 * otherwise the value is reloaded and the new value is returned.
 * @return {*}
 */
Evernote.CachedOption.prototype.getValue = function() {
    var currentTime = new Date().getTime();
    if(currentTime <= this.lastReloadTime + this.reloadInterval) {
        return this.value;
    }
    else {
        this.reload();
        return this.value;
    }
};

/**
 * Reloads value of the option.
 * Also update last reload time (after this call getValue will return cached value).
 */
Evernote.CachedOption.prototype.reload = function() {
    this.value = this.reloadFn();
    this.lastReloadTime = new Date().getTime();
};