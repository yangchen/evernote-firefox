//"use strict";

Evernote.UrlHelpers = { };

Evernote.UrlHelpers.urlTopDomain = function( url ) {
    var topDomain = url;
    if ( typeof url == 'string' ) {
        topDomain = this.urlDomain( url );
        if ( topDomain.toLowerCase().indexOf( "www." ) == 0 ) {
            topDomain = topDomain.substring( 4 );
        }
    }

    return topDomain;
};

Evernote.UrlHelpers.urlDomain = function( url, includePort ) {
    if ( typeof url == 'string' ) {
        var re = new RegExp( "^[^:]+:\/+([^\/" + ((includePort) ? "" : ":") + "]+).*$" );
        var domain = url.replace( re, "$1" );
    }

    Evernote.logger.debug( "UrlHelpers.urlDomain(): domain is " + domain + " for url " + url );
    return (domain) ? domain : url;
};

Evernote.UrlHelpers.urlWithoutHash = function( url ) {
    return url.replace( /^([^#]+).*$/, "$1" );
};

Evernote.UrlHelpers.urlToSearchQuery = function( url, searchPrefix ) {
    Evernote.logger.debug( "UrlHelpers.urlToSearchQuery()" );

    var proto = this.urlProto( url );
    if ( proto && proto.indexOf( "http" ) == 0 ) {
        return this.httpUrlToSearchQuery( url, searchPrefix );
    }
    else if ( proto && proto == "file" ) {
        return this.fileUrlToSearchQuery( url, searchPrefix );
    }
    else if ( proto ) {
        return this.anyProtoUrlToSearchQuery( url, searchPrefix );
    }
    else {
        return this.anyUrlToSearchQuery( url, searchPrefix );
    }
};

Evernote.UrlHelpers.httpUrlToSearchQuery = function( url, searchPrefix ) {
    var pfx = (typeof searchPrefix == 'string') ? searchPrefix : "";
    var domain = this.urlDomain( (url + "").toLowerCase() );
    var allUrls = [ (pfx + "http://" + domain + "*"), (pfx + "https://" + domain + "*") ];

    if ( domain.match( /[^0-9\.]/ ) ) {
        var secondaryDomain = (domain.indexOf( "www." ) == 0) ? domain.substring( 4 ) : ("www." + domain);
        allUrls = allUrls.concat( [ (pfx + "http://" + secondaryDomain + "*"), (pfx + "https://" + secondaryDomain + "*") ] );
    }

    var query = "any: " + allUrls.join( " " );
    Evernote.logger.debug( "UrlHelpers.httpUrlToSearchQuery(): url = " + url + ", searchPrefix = " + searchPrefix + ", query = " + query );
    return query;
};

Evernote.UrlHelpers.fileUrlToSearchQuery = function( url, searchPrefix ) {
    var pfx = (typeof searchPrefix == 'string') ? searchPrefix : "";
    var query = pfx + "file:*";

    Evernote.logger.debug( "UrlHelpers.fileUrlToSearchQuery(): url = " + url + ", searchPrefix = " + searchPrefix + ", query = " + query );
    return query;
};

Evernote.UrlHelpers.anyProtoUrlToSearchQuery = function( url, searchPrefix ) {
    var pfx = (typeof searchPrefix == 'string') ? searchPrefix : "";
    var proto = this.urlProto( url );
    var secProto = proto + "s";

    if ( proto.indexOf( "s" ) == (proto.length - 1) ) {
        proto = proto.substring( 0, (proto.length - 1) );
        secProto = proto + "s";
    }

    var domain = this.urlDomain( url );
    var allUrls = [ (pfx + proto + "://" + domain + "*"), (pfx + secProto + "://" + domain + "*") ];

    var query = "any: " + allUrls.join( " " );
    Evernote.logger.debug( "UrlHelpers.anyProtoUrlToSearchQuery(): url = " + url + ", searchPrefix = " + searchPrefix + ", query = " + query );
    return query;
};

Evernote.UrlHelpers.anyUrlToSearchQuery = function( url, searchPrefix ) {
    var pfx = (typeof searchPrefix == 'string') ? searchPrefix : "";
    var query = pfx + url + "*";

    Evernote.logger.debug( "UrlHelpers.anyUrlToSearchQuery(): url = " + url + ", searchPrefix = " + searchPrefix + ", query = " + query );
    return query;
};

Evernote.UrlHelpers.urlProto = function( url ) {
    if ( typeof url == 'string' ) {
        var x = -1;
        if ( (x = url.indexOf( ":/" )) > 0 ) {
            return url.substring( 0, x ).toLowerCase();
        }
    }

    return null;
};

Evernote.UrlHelpers.shortUrl = function( url, maxLength ) {
    var urlSuffix = "...";
    var shortUrl = url;
    
    if ( typeof url == 'string' ) {
        if ( shortUrl.indexOf( "file:" ) == 0 ) {
            shortUrl = decodeURIComponent( shortUrl.replace( /^file:.*\/([^\/]+)$/, "$1" ) );
            if ( typeof maxLength == 'number' && !isNaN( maxLength ) && shortUrl.length > maxLength ) {
                shortUrl = shortUrl.substring( 0, maxLength );
                shortUrl += "" + urlSuffix;
            }
        }
        else {
            shortUrl = shortUrl.replace( /^([a-zA-Z]+:\/+)?([^\/]+).*$/, "$2" );
            if ( typeof maxLength == 'number' && !isNaN( maxLength ) && shortUrl.length > maxLength ) {
                shortUrl = shortUrl.substring( 0, maxLength );
                shortUrl += "" + urlSuffix;
            }
            else if ( url.substring( url.indexOf( shortUrl ) + shortUrl.length ).length > 2 ) {
                shortUrl += "/" + urlSuffix;
            }
        }
    }

    Evernote.logger.debug( "UrlHelpers.shortUrl(): url = " + url + ", shortUrl = " + shortUrl );
    return shortUrl;
};

Evernote.UrlHelpers.urlQueryValue = function( key, url ) {
    if ( typeof url == 'string' && typeof key == 'string' && url.indexOf( "?" ) >= 0 ) {
        var queries = url.split( /[\?\#\&]+/ ).slice( 1 );
        var k = key.toLowerCase();
        var results = [ ];

        for ( var i = 0; i < queries.length; i++ ) {
            var kv = queries[i].split( "=", 2 );
            if ( kv[ 0 ].toLowerCase() == k ) {
                var r = (kv[ 1 ]) ? kv[ 1 ].replace( /\+/g, " " ) : kv[ 1 ];
                results.push( decodeURIComponent( r ) );
            }
        }

        if ( results.length > 0 ) {
            var query = results[ results.length - 1 ];
            Evernote.logger.debug( "UrlHelpers.urlQueryValue(): query = " + query + ", key = " + key + ", url = " + url );
            return query;
        }
    }

    return null;
};

Evernote.UrlHelpers.urlPath = function( url ) {
    Evernote.logger.debug( "UrlHelpers.urlPath(): url = " + url );

    if ( typeof url == 'string' ) {
        var path = url.replace( /^[^:]+:\/+([^\/]+)/, "" );
        if ( path.indexOf( "/" ) == 0 ) {
            return path.replace( /^(\/[^\?\#]*).*$/, "$1" );
        }
    }

    return "";
};

Evernote.UrlHelpers.makeAbsolutePath = function ( base, href ) {
    function parseURI( url ) {
        var m = String( url ).replace( /^\s+|\s+$/g, '' ).match( /^([^:\/?#]+:)?(\/\/(?:[^:@]*(?::[^:@]*)?@)?(([^:\/?#]*)(?::(\d*))?))?([^?#]*)(\?[^#]*)?(#[\s\S]*)?/ );
        // authority = '//' + user + ':' + pass '@' + hostname + ':' port
        return (m ? {
            href : m[ 0 ] || '',
            protocol : m[ 1 ] || '',
            authority : m[ 2 ] || '',
            host : m[ 3 ] || '',
            hostname : m[ 4 ] || '',
            port : m[ 5 ] || '',
            pathname : m[ 6 ] || '',
            search : m[ 7 ] || '',
            hash : m[ 8 ] || ''
        } : null);
    }

    function absolutizeURI( base, href ) {// RFC 3986
        function removeDotSegments( input ) {
            var output = [];
            input.replace( /^(\.\.?(\/|$))+/, '' )
                .replace( /\/(\.(\/|$))+/g, '/' )
                .replace( /\/\.\.$/, '/../' )
                .replace( /\/?[^\/]*/g, function ( p ) {
                              if ( p === '/..' ) {
                                  output.pop();
                              }
                              else {
                                  output.push( p );
                              }
                          } );
            return output.join( '' ).replace( /^\//, input.charAt( 0 ) === '/' ? '/' : '' );
        }

        href = parseURI( href || '' );
        base = parseURI( base || '' );

        return !href || !base ? null : (href.protocol || base.protocol) +
                                       (href.protocol || href.authority ? href.authority : base.authority) +
                                       removeDotSegments( href.protocol || href.authority || href.pathname.charAt( 0 ) === '/' ? href.pathname : (href.pathname ? ((base.authority && !base.pathname ? '/' : '') + base.pathname.slice( 0, base.pathname.lastIndexOf( '/' ) + 1 ) + href.pathname) : base.pathname) ) +
                                       (href.protocol || href.authority || href.pathname ? href.search : (href.search || base.search)) +
                                       href.hash;
    }

    return absolutizeURI( base, href );
};















