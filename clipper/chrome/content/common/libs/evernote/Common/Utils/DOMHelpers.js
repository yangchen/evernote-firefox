//"use strict";

Evernote.DOMHelpers = { };

Evernote.DOMHelpers.BAD_FAV_ICON_URLS = [ "http://localhost/favicon.ico" ];

Evernote.DOMHelpers.updateSelectElementWidth = function( el, textCallback ) {
    Evernote.logger.debug( "DOMHelpers.updateSelectElementWidth()" );

    var $el = (el instanceof Evernote.jQuery) ? el : Evernote.jQuery( el );
    var val = $el.val();
    if ( typeof textCallback == 'function' ) {
        val = textCallback( val );
    }

    var paddingLeft = parseInt( $el.css( "paddingLeft" ) );
    var paddingRight = parseInt( $el.css( "paddingRight" ) );
    var delta = ((!isNaN( paddingLeft )) ? paddingLeft : 0) + ((!isNaN( paddingRight )) ? paddingRight : 0);

    var width = this.getTextSize( val ).width;
    width = (width) ? width : 0;
    var newWidth = width + delta;
    // adjust by another 10 pixels - mainly for Windows platform
    newWidth += 30;
    $el.css( {
        minWidth : newWidth + "px",
        width : newWidth + "px"
    } );
};

Evernote.DOMHelpers.resizeElement = function( el, size, handler ) {
    Evernote.logger.debug( "DOMHelpers.resizeElement()" );

    var $el = (el instanceof Evernote.jQuery) ? el : Evernote.jQuery( el );
    var cssObj = { };
    var sizeObj = { };

    if ( size && typeof size.width == 'number' ) {
        var paddingLeft = parseInt( $el.css( "paddingLeft" ) );
        var paddingRight = parseInt( $el.css( "paddingRight" ) );
        var delta = ((!isNaN( paddingLeft )) ? paddingLeft : 0) + ((!isNaN( paddingRight )) ? paddingRight : 0);

        sizeObj.width = size.width + delta;
        cssObj.minWidth = sizeObj.width + "px";
        cssObj.width = sizeObj.width + "px";
    }

    if ( size && typeof size.height == 'number' ) {
        var paddingTop = parseInt( $el.css( "paddingTop" ) );
        var paddingBottom = parseInt( $el.css( "paddingBottom" ) );
        delta = ((!isNaN( paddingTop )) ? paddingTop : 0) + ((!isNaN( paddingBottom )) ? paddingBottom : 0);

        sizeObj.height = size.height + delta;
        cssObj.minHeight = sizeObj.height + "px";
        cssObj.height = sizeObj.height + "px";
    }

    $el.css( cssObj );
    if ( typeof handler == 'function' ) {
        handler( $el, sizeObj );
    }
};

Evernote.DOMHelpers.getTextSize = function( text ) {
    Evernote.logger.debug( "DOMHelpers.getTextSize()" );

    var el = Evernote.jQuery( "<div>" );
    el.text( text );
    el.css( {
        position : "absolute",
        top : "0px",
        left : "0px",
        padding : "0px",
        margin : "0px",
        display : "block",
        zIndex : "0",
        color : "rgba(0, 0, 0, 0)",
        background : "rgba(0, 0, 0, 0)"
    } );

    Evernote.jQuery( "body" ).append( el );
    var size = {
        width : el.width(),
        height : el.height()
    };

    el.remove();
    return size;
};

Evernote.DOMHelpers.getElementForNode = function ( node ) {
    if ( node && node.nodeType == Node.ELEMENT_NODE ) {
        return node;
    }
    else {
        return (node.parentNode) ? node.parentNode : null;
    }
};

Evernote.DOMHelpers.getAbsoluteBoundingClientRect = function( node ) {
    Evernote.logger.debug( "DOMHelpers.getAbsoluteBoundingClientRect()" );

    var elem = this.getElementForNode( node );
    var rect = elem.getBoundingClientRect();
    var wnd = node.ownerDocument.defaultView;

    return this.makeAbsoluteClientRect( rect, wnd );
};

Evernote.DOMHelpers.addElementClass = function ( elem, classname ) {
    Evernote.logger.debug( "DOMHelpers.addElementClass(): classname = " + classname );

    if ( elem && classname ) {
        var classes = elem.getAttribute( "class" );
        if ( classes ) {
            classes = classes.split( /\s+/ );
            if ( classes.indexOf( classname ) < 0 ) {
                classes.push( classname );
            }
            elem.setAttribute( "class", classes.join( " " ) );
        }
        else {
            elem.setAttribute( "class", classname );
        }
    }
};

Evernote.DOMHelpers.makeAbsoluteClientRect = function ( rect, wnd ) {
    if ( !wnd ) {
        wnd = window;
    }

    return {
        top: rect.top + wnd.pageYOffset,
        bottom: rect.bottom + wnd.pageYOffset,
        left: rect.left + wnd.pageXOffset,
        right: rect.right + wnd.pageXOffset,
        width: rect.width,
        height: rect.height
    };
};

Evernote.DOMHelpers.getHead = function( doc ) {
    var head = doc.head;
    if ( !head ) {
        var heads = doc.getElementsByTagName( "head" );
        if ( heads.length > 0 ) {
            return heads[ 0 ];
        }
    }

    return (head) ? head : null;
};

Evernote.DOMHelpers.getArticleNode = function( doc ) {
    Evernote.logger.debug( "DOMHelpers.getArticleNode()" );

    try {
        var extractor = new Evernote.ExtractContentJS.LayeredExtractor();
        extractor.addHandler( extractor.factory.getHandler( "Heuristics" ) );
        var result = extractor.extract( doc );

        if ( result.isSuccess ) {
            var resultNode = result.content.asNode();
            resultNode = this.getElementForNode( resultNode );
        }

      return resultNode;
    }
    catch ( e ) {
    }
};

Evernote.DOMHelpers.removeNode = function( doc, nodeId ) {
    if ( doc ) {
        var node = doc.getElementById( nodeId );
        if ( node && node.parentNode ) {
            node.parentNode.removeChild( node );
        }
    }
};

Evernote.DOMHelpers.createUrlClipContent = function( doc, title, url, favIcoUrl ) {
    Evernote.logger.debug( "DOMHelpers.createUrlClipContent(): url = " + url );

    title = (title) ? Evernote.Utils.escapeXML( title ) : "";

    var link = doc.createElement( "a" );
    link.setAttribute( "title", title );
    link.setAttribute( "href", url );
    this.setElemInlineStyles( link, { "font-size" : "12pt", "line-height" : "18px", "display" : "inline" } );

    var text = doc.createTextNode( url );
    link.appendChild( text );

    var div = doc.createElement( "div" );
    if ( typeof favIcoUrl == 'string' && favIcoUrl.length > 0 && this.BAD_FAV_ICON_URLS.indexOf( favIcoUrl.toLowerCase() ) < 0 ) {
        var img = doc.createElement( "img" );
        img.setAttribute( "title", title );
        img.setAttribute( "src", favIcoUrl );
        this.setElemInlineStyles( img, { "display" : "inline", "border" : "none", "width" : "16px", "height" : "16px", "padding" : "0px", "margin" : "0px 8px -2px 0px" } );

        div.appendChild( img );
        div.appendChild( link );
    }
    else {
        var span = doc.createElement( "span" );
        span.appendChild( link );
        div.appendChild( span );
    }

    return div;
};

Evernote.DOMHelpers.addClassName = function( element, className ) {
    if ( element instanceof Element ) {
        element.className += ((element.className) ? " " : "") + className;
    }
};

Evernote.DOMHelpers.removeClassName = function( element, className ) {
    var re = new RegExp( className + "?", "g" );
    if ( element instanceof Element && element.className ) {
        element.className = element.className.replace( re, "" );
    }
};

Evernote.DOMHelpers.getBodyHeight = function( tab ) {
    return Math.max( tab.document.body.parentNode.scrollHeight, tab.document.body.scrollHeight );
};

Evernote.DOMHelpers.getBodyWidth = function( tab ) {
    return Math.max( tab.document.body.parentNode.scrollWidth, tab.document.body.scrollWidth );
};

Evernote.DOMHelpers.setElemInlineStyles = function( elem, styles ) {
    if ( !(elem instanceof Element) ) {
        return;
    }

    var style = elem.style;
    for ( var key in styles ) {
        if ( typeof styles[ key ] == "string" ) {
            style.setProperty( key, styles[ key ], "important" );
        }
    }
};

Evernote.DOMHelpers.getNestedDocuments = function( doc ) {
    Evernote.logger.debug( "DOMHelpers.getNestedDocuments()" );

    var docs = [ ];
    var frames = ( doc ) ? doc.getElementsByTagName( "frame" ) : [ ];
    for ( var i = 0; i < frames.length; ++i ) {
        if ( frames[ i ].contentDocument ) {
            docs.push( frames[ i ].contentDocument );
        }
    }

    var iframes = ( doc ) ? doc.getElementsByTagName( "iframe" ) : [ ];
    for ( i = 0; i < iframes.length; ++i ) {
        if ( iframes[ i ].contentDocument ) {
            docs.push( iframes[ i ].contentDocument );
        }
    }

    return docs;
};