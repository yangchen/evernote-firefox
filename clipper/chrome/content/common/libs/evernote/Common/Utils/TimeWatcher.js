Evernote.TimeWatcher = {
    timers : []
};

Evernote.TimeWatcher.getTimer = function( id ) {
    var timer = Evernote.TimeWatcher.timers[id]
    if( timer==null ) {
        timer = new Evernote.TimeWatcher.Timer( id );
        Evernote.TimeWatcher.timers[id] = timer;
    }
    return Evernote.TimeWatcher.timers[id];
};

Evernote.TimeWatcher.printEvents = function() {
    for (var i in  Evernote.TimeWatcher.timers) {
        Evernote.TimeWatcher.timers[i].printEvents();
    }
};

Evernote.TimeWatcher.reset = function() {
    for (var i in  Evernote.TimeWatcher.timers) {
        Evernote.TimeWatcher.timers[i].destroy();
        delete Evernote.TimeWatcher.timers[i];
    }
};

Evernote.TimeWatcher.Timer = function Timer( id ) {
    this._id = id;
    this._events = new Array();
};

Evernote.TimeWatcher.Timer.prototype.printEvents = function() {
    var sum = 0;
    for(var i=0; i<this._events.length; i++) {
        sum += this._events[i]._duration;
    }

    if( this._events.length > 0 )
        Evernote.logger.error(" Timer id: " + this._id + "(count = " + this._events.length+ ") time = " + sum + "\n(start time = " + this._events[0]._startTime + "; stop time = " + this._events[this._events.length-1]._stopTime + ");\n");
};

Evernote.TimeWatcher.Timer.prototype.destroy = function() {
    for(var i=0; i<this._events.length; i++) {
        delete this._events[i];
    }
};

Evernote.TimeWatcher.Timer.prototype._id = null;
Evernote.TimeWatcher.Timer.prototype._events = null;
Evernote.TimeWatcher.Timer.prototype._currentEvent = null;


Evernote.TimeWatcher.Timer.prototype.start = function() {
    this._currentEvent = new Evernote.TimeWatcher.TimerEvent();
};

Evernote.TimeWatcher.Timer.prototype.stop = function() {
    if( this._currentEvent ) {
        this._currentEvent.stop();
        this._events.push( this._currentEvent );
        this._currentEvent = null;
    }
};

Evernote.TimeWatcher.TimerEvent = function TimerEvent() {
    this._startTime = new Date();
};

Evernote.TimeWatcher.TimerEvent._startTime = null;
Evernote.TimeWatcher.TimerEvent._stopTime = null;
Evernote.TimeWatcher.TimerEvent._duration = null;
Evernote.TimeWatcher.TimerEvent.prototype.stop = function () {
    this._stopTime = new Date();
    this._duration = this._stopTime - this._startTime;
};