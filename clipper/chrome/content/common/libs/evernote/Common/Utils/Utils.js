//"use strict";

Evernote.Utils = { };

Evernote.Utils.XML_ESCAPE_CHAR_MAP = {
    "&" : "&amp;",
    "<" : "&lt;",
    ">" : "&gt;",
    "\"" : "&quot;",
    "'" : "&apos;"
};

Evernote.Utils.PROHIBITED_URLS = [ "chrome.google.com/extensions", "chrome.google.com/webstore" ];

Evernote.Utils.MAX_ARTICLE_XOFFSET_RATIO = 0.6;
Evernote.Utils.MIN_ARTICLE_AREA = 30000;
Evernote.Utils.MIN_ARTICLE_RATIO = 1 / 16;

Evernote.Utils.separateString = function( str, separator ) {
    if ( typeof str != 'string' ) {
        return str;
    }

    if ( typeof separator != 'string' ) {
        separator = ",";
    }

    var parts = str.split( separator );
    var returnParts = [ ];
    for ( var i = 0; i < parts.length; i++ ) {
        if ( typeof parts[i] != 'string' ) {
            continue;
        }
        var clean = this.trim( parts[i] );
        if ( clean && clean.length > 0 ) {
            returnParts.push( clean );
        }
    }

    return returnParts;
};

Evernote.Utils.trim = function( str ) {
    if ( typeof str != 'string' ) {
        return str;
    }

    return str.replace( /^\s+/, "" ).replace( /\s+$/, "" );
};

Evernote.Utils.isForbiddenUrl = function( url ) {
    if ( typeof url == 'string' ) {
        var urlLC = url.toLowerCase();
        for ( var i = 0; i < this.PROHIBITED_URLS.length; ++i ) {
            if ( urlLC.indexOf( this.PROHIBITED_URLS[ i ] ) >= 0 ) {
                return true;
            }
        }
    }

    return typeof url == 'string' && !url.toLowerCase().match( /^https?:\/\// );
};

Evernote.Utils.isNoArticleByDefaultUrl = function( url ) {
    if ( typeof url != "string" ) {
        return false;
    }

    var domainLC = Evernote.UrlHelpers.urlDomain( url ).toLowerCase();
    return domainLC.indexOf( ".google." ) >= 0 || domainLC.indexOf( ".bing." ) >= 0;
};

Evernote.Utils.rectIntersection = function ( first, second ) {
    if ( !(second.left > first.right || second.right < first.left || second.top > first.bottom || second.bottom < first.top) ) {
        var e = {
            left: Math.max( first.left, second.left ),
            top: Math.max( first.top, second.top ),
            right: Math.min( first.right, second.right ),
            bottom: Math.min( first.bottom, second.bottom )
        };

        e.width = e.right - e.left;
        e.height = e.bottom - e.top;
        return e;
    }
};

Evernote.Utils.alert = function( title, text ) {
    var prompts = Components.classes[ "@mozilla.org/embedcomp/prompt-service;1" ].getService( Components.interfaces.nsIPromptService );
    prompts.alert( content, title, text );
};

Evernote.Utils.confirm = function( title, text ) {
    var prompts = Components.classes[ "@mozilla.org/embedcomp/prompt-service;1" ].getService( Components.interfaces.nsIPromptService );
    return prompts.confirm( content, title, text );
};

Evernote.Utils.escapeXML = function( str ) {
    var a = str.split( "" );
    for ( var i = 0; i < a.length; i++ ) {
        if ( this.XML_ESCAPE_CHAR_MAP[ a[ i ] ] ) {
            a[ i ] = this.XML_ESCAPE_CHAR_MAP[ a[ i ] ];
        }
    }

    return a.join( "" );
};

Evernote.Utils.HTMLEncode = function( str ) {
    var result = "";
    for ( var i = 0; i < str.length; i++ ) {
        var charcode = str.charCodeAt( i );
        var aChar = str[ i ];

        if ( charcode > 0x7f ) {
            result += "&#" + charcode + ";";
        }
        else if ( aChar == '>' ) {
            result += "&gt;";
        }
        else if ( aChar == '<' ) {
            result += "&lt;";
        }
        else if ( aChar == '&' ) {
            result += "&amp;";
        }
        else {
            result += str[ i ];
        }
    }

    return result;
};

Evernote.Utils.shortWord = function( word, length ) {
    if ( word.length <= length ) {
        return word;
    }

    return word.substring( 0, length - 3 ) + "...";
};

Evernote.Utils.expungeModels = function( models, guids ) {
    Evernote.logger.debug( "Utils.expungeModels()" );

    if ( models instanceof Array && guids instanceof Array ) {
        var guidKeys = { };
        var result = [ ];

        for ( var i = 0; i < guids.length; i++ ) {
            Evernote.logger.debug( "Adding guid: " + guids[ i ] );
            guidKeys[ guids[ i ] ] = guids[ i ];
        }

        for ( i = 0; i < models.length; i++ ) {
            Evernote.logger.debug( "Checking if have: " + models[ i ].guid );
            if ( typeof guidKeys[ models[ i ].guid ] == 'undefined' ) {
                result.push( models[ i ] );
            }
        }

        return result;
    }

    Evernote.logger.warn( "There was nothing to expunge" );
    return models;
};

Evernote.Utils.mergeModels = function( oldModels, newModels ) {
    Evernote.logger.debug( "Utils.mergeModels()" );

    if ( oldModels instanceof Array && newModels instanceof Array ) {
        var map = { };
        var result = [ ];
        for ( var i = 0; i < newModels.length; i++ ) {
            if ( typeof newModels[ i ].guid != 'undefined' ) {
                map[ newModels[ i ].guid ] = newModels[ i ];
            }
        }

        var newModelCount = 0;
        var oldModelCount = 0;
        var updatedModelCount = 0;

        for ( i = 0; i < oldModels.length; i++ ) {
            if ( typeof map[ oldModels[ i ].guid ] != 'undefined' ) {
                updatedModelCount++;
                result.push( map[ oldModels[ i ].guid ] );
                delete map[ oldModels[ i ].guid ];
            }
            else {
                oldModelCount++;
                result.push( oldModels[ i ] );
            }
        }

        for ( i in map ) {
            newModelCount++;
            result.push( map[ i ] );
        }

        return result;
    }
    else if ( newModels instanceof Array ) {
        Evernote.logger.warn( "There were no models to merge into" );
        return newModels;
    }

    Evernote.logger.warn( "There were no models to merge" );
    return oldModels;
};

Evernote.Utils.sortModelsByField = function( models, fieldName ) {
    Evernote.logger.debug( "Utils.sortModelsByField(): fieldName =  " + fieldName );

    if ( models instanceof Array ) {
        return models.sort( function( a, b ) {
            var aField = "";
            var bField = "";

            if ( typeof a[ fieldName ] != 'undefined' ) {
                aField = a[ fieldName ].toLowerCase();
            }

            if ( typeof b[ fieldName ] != 'undefined' ) {
                bField = b[ fieldName ].toLowerCase();
            }

            if ( aField == bField ) {
                return 0;
            }
            else if ( aField < bField ) {
                return -1;
            }
            else {
                return 1;
            }
        } );
    }
    
    return models;
};

Evernote.Utils.errorConsole = function( val ) {
    Components.utils.reportError( val );
};

Evernote.Utils.isDesktopInstalled = function() {
    try {
        var options = Components.classes[ "@Evernote/Options;1" ].createInstance( Components.interfaces.IOptions );
        return options.IsDesktopInstalled();
    }
    catch ( e ) {
        Evernote.logger.error( "isDesktopInstalled() failed: error = " + e );
        return false;
    }
};

Evernote.Utils.generateGuid = function() {
    return generateQuad() + generateQuad() + "-" + generateQuad() + "-" + generateQuad() + "-"
           + generateQuad() + "-" + generateQuad() + generateQuad() + generateQuad();

    function generateQuad() {
        return (((1 + Math.random()) * 0x10000) | 0).toString( 16 ).substring( 1 );
    }
};

Evernote.Utils.getBrowserTab = function( browser ) {
    if ( browser ) {
        var doc = browser.contentDocument;
        var browserIndex = gBrowser.getBrowserIndexForDocument( doc );
        return gBrowser.tabContainer.childNodes[ browserIndex ];
    }

    return null;
};

Evernote.Utils.isArticleSane = function( pageInfo, tab ) {
    Evernote.logger.debug( "Utils.isArticleSane()" );

    if ( !pageInfo || !pageInfo.articleBoundingClientRect || !pageInfo.articleNode
         || pageInfo.articleNode == tab.document.body ) {
        return false;
    }

    var pageArea = pageInfo.documentWidth * pageInfo.documentHeight;
    var articleArea = pageInfo.articleBoundingClientRect.width * pageInfo.articleBoundingClientRect.height;

    if ( pageInfo.articleBoundingClientRect.top > pageInfo.documentHeight * this.MAX_ARTICLE_XOFFSET_RATIO ) {
        return false;
    }
    else if ( articleArea < this.MIN_ARTICLE_AREA && articleArea < pageArea * this.MIN_ARTICLE_RATIO ) {
        return false;
    }

    return true;
};

/**
 * Returns true if Evernote desktop clipper is selected in options and is installed on user machine.
 * Otherwise returns false.
 * The cached value is used, so there may the case when user uninstalls Evernote desktop,
 * but this method will return true for a while (before next update of the option)
 */
Evernote.Utils.isDesktopClipperSelected = function() {
    Evernote.logger.debug( "Utils.isDesktopClipperSelected()" );

    var isDesktopClipperSelected = Evernote.prefsManager.getPref( Evernote.PrefKeys.IS_DESKTOP_SELECTED, "boolean" );
    if(!isDesktopClipperSelected)
        return false;

    if(!this.desktopClipperSelectedOption) {
        this.desktopClipperSelectedOption = new Evernote.CachedOption(1800 /* 30 min */, function() {
            var nativeClipper = new Evernote.NativeClipper();
            return nativeClipper.isEnabled() && Evernote.prefsManager.getPref( Evernote.PrefKeys.IS_DESKTOP_SELECTED, "boolean" );
        });
    }
    return this.desktopClipperSelectedOption.getValue();
};

Evernote.Utils.logHttpErrorMessage = function( xhr, desc ) {
    if ( xhr && xhr.readyState == 4 ) {
        Evernote.logger.error( "Failed to " + desc + " due to transport errors (status: " + xhr.status + ")" );
    }
    else if ( xhr ) {
        Evernote.logger.error( "Failed to " + desc + " to transport errors (readyState: " + xhr.readyState + ")" );
    }
    else {
        Evernote.logger.error( "Failed to " + desc + " due to unknown transport errors" );
    }
};

Evernote.Utils.getExtensionDir = function() {
    return Components.classes[ "@mozilla.org/file/directory_service;1" ].getService( Components.interfaces.nsIProperties ).
               get( "ProfD", Components.interfaces.nsIFile ).path + "/extensions/" + Evernote.EXTENSIOD_ID;
};


