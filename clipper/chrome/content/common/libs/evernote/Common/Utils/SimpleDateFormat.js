/**
 * Copyright 2007 Tim Down.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * simpledateformat.js
 *
 * A faithful JavaScript implementation of Java's SimpleDateFormat's format
 * method. All pattern layouts present in the Java implementation are
 * implemented here except for z, the text version of the date's time zone.
 *
 * Thanks to Ash Searle (http://hexmen.com/blog/) for his fix to my
 * misinterpretation of pattern letters h and k.
 *
 * See the official Sun documentation for the Java version:
 * http://java.sun.com/j2se/1.5.0/docs/api/java/text/SimpleDateFormat.html
 *
 * Author: Tim Down <tim@timdown.co.uk>
 * Last modified: 6/2/2007
 * Website: http://www.timdown.co.uk/code/simpledateformat.php
 */

/* ------------------------------------------------------------------------- */

Evernote.SimpleDateFormat = function SimpleDateFormat( formatString ) {
    this.formatString = formatString;
};

Evernote.SimpleDateFormat.regex = /('[^']*')|(G+|y+|M+|w+|W+|D+|d+|F+|E+|a+|H+|k+|K+|h+|m+|s+|S+|Z+)|([a-zA-Z]+)|([^a-zA-Z']+)/;
Evernote.SimpleDateFormat.monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
Evernote.SimpleDateFormat.dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
Evernote.SimpleDateFormat.TEXT2 = 0;
Evernote.SimpleDateFormat.TEXT3 = 1;
Evernote.SimpleDateFormat.NUMBER = 2;
Evernote.SimpleDateFormat.YEAR = 3;
Evernote.SimpleDateFormat.MONTH = 4;
Evernote.SimpleDateFormat.TIMEZONE = 5;

Evernote.SimpleDateFormat.types = {
    G : Evernote.SimpleDateFormat.TEXT2,
    y : Evernote.SimpleDateFormat.YEAR,
    M : Evernote.SimpleDateFormat.MONTH,
    w : Evernote.SimpleDateFormat.NUMBER,
    W : Evernote.SimpleDateFormat.NUMBER,
    D : Evernote.SimpleDateFormat.NUMBER,
    d : Evernote.SimpleDateFormat.NUMBER,
    F : Evernote.SimpleDateFormat.NUMBER,
    E : Evernote.SimpleDateFormat.TEXT3,
    a : Evernote.SimpleDateFormat.TEXT2,
    H : Evernote.SimpleDateFormat.NUMBER,
    k : Evernote.SimpleDateFormat.NUMBER,
    K : Evernote.SimpleDateFormat.NUMBER,
    h : Evernote.SimpleDateFormat.NUMBER,
    m : Evernote.SimpleDateFormat.NUMBER,
    s : Evernote.SimpleDateFormat.NUMBER,
    S : Evernote.SimpleDateFormat.NUMBER,
    Z : Evernote.SimpleDateFormat.TIMEZONE
};

Evernote.SimpleDateFormat.ONE_DAY = 24 * 60 * 60 * 1000;
Evernote.SimpleDateFormat.ONE_WEEK = 7 * Evernote.SimpleDateFormat.ONE_DAY;
Evernote.SimpleDateFormat.DEFAULT_MINIMAL_DAYS_IN_FIRST_WEEK = 1;

Evernote.SimpleDateFormat.prototype._isUndefined = function( obj ) {
    return typeof obj == "undefined";
};

Evernote.SimpleDateFormat.prototype._newDateAtMidnight = function( year, month, day ) {
    var d = new Date( year, month, day, 0, 0, 0 );
    d.setMilliseconds( 0 );
    return d;
};

Evernote.SimpleDateFormat.prototype.getDifference = function(dateStart,  dateEnd ) {
    return dateStart.getTime() - dateEnd.getTime();
};

Evernote.SimpleDateFormat.prototype.isBefore = function( dateStart,  dateEnd ) {
    return dateStart.getTime() < dateEnd.getTime();
};

Evernote.SimpleDateFormat.prototype.getUTCTime = function( date ) {
    return Date.UTC( date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds() );
};

Evernote.SimpleDateFormat.prototype.getTimeSince = function( dateStart, dateEnd ) {
    return this.getUTCTime( dateStart ) - this.getUTCTime( dateEnd );
};

Evernote.SimpleDateFormat.prototype.getPreviousSunday = function( date ) {
    // Using midday avoids any possibility of DST messing things up
    var midday = new Date( date.getFullYear(), date.getMonth(), date.getDate(), 12, 0, 0 );
    var previousSunday = new Date( midday.getTime() - date.getDay() * Evernote.SimpleDateFormat.ONE_DAY );
    return this._newDateAtMidnight( previousSunday.getFullYear(), previousSunday.getMonth(), previousSunday.getDate() );
};

Evernote.SimpleDateFormat.prototype.getWeekInYear = function( date, minimalDaysInFirstWeek ) {
    if ( this._isUndefined( this.minimalDaysInFirstWeek ) ) {
        minimalDaysInFirstWeek = Evernote.SimpleDateFormat.DEFAULT_MINIMAL_DAYS_IN_FIRST_WEEK;
    }

    var startOfYear = this._newDateAtMidnight( this.getFullYear(), 0, 1 );
    var numberOfSundays = this.isBefore( date, startOfYear ) ? 0 : (1 + Math.floor( this.getTimeSince( date, startOfYear ) / Evernote.SimpleDateFormat.ONE_WEEK ));
    var numberOfDaysInFirstWeek = 7 - startOfYear.getDay();

    var weekInYear = numberOfSundays;
    if ( numberOfDaysInFirstWeek < minimalDaysInFirstWeek ) {
        weekInYear--;
    }
    return weekInYear;
};

Evernote.SimpleDateFormat.prototype.getWeekInMonth = function( date, minimalDaysInFirstWeek ) {
    if ( this._isUndefined( this.minimalDaysInFirstWeek ) ) {
        minimalDaysInFirstWeek = Evernote.SimpleDateFormat.DEFAULT_MINIMAL_DAYS_IN_FIRST_WEEK;
    }
    
    var startOfMonth = this._newDateAtMidnight( this.getFullYear(), this.getMonth(), 1 );
    var numberOfSundays = this.isBefore( date, startOfMonth ) ? 0 : (1 + Math.floor( (this.getTimeSince( date, startOfMonth )) / Evernote.SimpleDateFormat.ONE_WEEK ));
    var numberOfDaysInFirstWeek = 7 - startOfMonth.getDay();

    var weekInMonth = numberOfSundays;
    if ( numberOfDaysInFirstWeek >= minimalDaysInFirstWeek ) {
        weekInMonth++;
    }

    return weekInMonth;
};

Evernote.SimpleDateFormat.prototype.getDayInYear = function( date ) {
    var startOfYear = this._newDateAtMidnight( this.getFullYear(), 0, 1 );
    return 1 + Math.floor( this.getTimeSince( date, startOfYear ) / Evernote.SimpleDateFormat.ONE_DAY );
};

Evernote.SimpleDateFormat.prototype.setMinimalDaysInFirstWeek = function( days ) {
    this.minimalDaysInFirstWeek = days;
};

Evernote.SimpleDateFormat.prototype.getMinimalDaysInFirstWeek = function( /*days*/ ) {
    return this._isUndefined( this.minimalDaysInFirstWeek ) ? Evernote.SimpleDateFormat.DEFAULT_MINIMAL_DAYS_IN_FIRST_WEEK : this.minimalDaysInFirstWeek;
};

Evernote.SimpleDateFormat.prototype.format = function( date ) {
    var formattedString = "";
    var result;

    var padWithZeroes = function( str, len ) {
        while ( str.length < len ) {
            str = "0" + str;
        }
        return str;
    };

    var formatText = function( data, numberOfLetters, minLength ) {
        return (numberOfLetters >= 4) ? data : data.substr( 0, Math.max( minLength, numberOfLetters ) );
    };

    var formatNumber = function( data, numberOfLetters ) {
        var dataString = "" + data;
        // Pad with 0s as necessary
        return padWithZeroes( dataString, numberOfLetters );
    };

    var searchString = this.formatString;
    while ( (result = Evernote.SimpleDateFormat.regex.exec( searchString )) ) {
        var quotedString = result[1];
        var patternLetters = result[2];
        var otherLetters = result[3];
        var otherCharacters = result[4];

        // If the pattern matched is quoted string, output the text between the quotes
        if ( quotedString ) {
            if ( quotedString == "''" ) {
                formattedString += "'";
            }
            else {
                formattedString += quotedString.substring( 1, quotedString.length - 1 );
            }
        }
        else if ( otherLetters ) {
            // Swallow non-pattern letters by doing nothing here
        }
        else if ( otherCharacters ) {
            // Simply output other characters
            formattedString += otherCharacters;
        }
        else if ( patternLetters ) {
            // Replace pattern letters
            var patternLetter = patternLetters.charAt( 0 );
            var numberOfLetters = patternLetters.length;
            var rawData = "";

            switch ( patternLetter ) {
                case "G":
                    rawData = "AD";
                    break;
                case "y":
                    rawData = date.getFullYear();
                    break;
                case "M":
                    rawData = date.getMonth();
                    break;
                case "w":
                    rawData = this.getWeekInYear( date, this.getMinimalDaysInFirstWeek() );
                    break;
                case "W":
                    rawData = this.getWeekInMonth( date, this.getMinimalDaysInFirstWeek() );
                    break;
                case "D":
                    rawData = this.getDayInYear( date );
                    break;
                case "d":
                    rawData = date.getDate();
                    break;
                case "F":
                    rawData = 1 + Math.floor( (date.getDate() - 1) / 7 );
                    break;
                case "E":
                    rawData = Evernote.SimpleDateFormat.dayNames[date.getDay()];
                    break;
                case "a":
                    rawData = (date.getHours() >= 12) ? "PM" : "AM";
                    break;
                case "H":
                    rawData = date.getHours();
                    break;
                case "k":
                    rawData = date.getHours() || 24;
                    break;
                case "K":
                    rawData = date.getHours() % 12;
                    break;
                case "h":
                    rawData = (date.getHours() % 12) || 12;
                    break;
                case "m":
                    rawData = date.getMinutes();
                    break;
                case "s":
                    rawData = date.getSeconds();
                    break;
                case "S":
                    rawData = date.getMilliseconds();
                    break;
                case "Z":
                    rawData = date.getTimezoneOffset(); // This is returns the number of minutes since GMT was this time.
                    break;
            }

            // Format the raw data depending on the type
            switch ( Evernote.SimpleDateFormat.types[patternLetter] ) {
                case Evernote.SimpleDateFormat.TEXT2:
                    formattedString += formatText( rawData, numberOfLetters, 2 );
                    break;

                case Evernote.SimpleDateFormat.TEXT3:
                    formattedString += formatText( rawData, numberOfLetters, 3 );
                    break;

                case Evernote.SimpleDateFormat.NUMBER:
                    formattedString += formatNumber( rawData, numberOfLetters );
                    break;

                case Evernote.SimpleDateFormat.YEAR:
                    if ( numberOfLetters <= 3 ) {
                        // Output a 2-digit year
                        var dataString = "" + rawData;
                        formattedString += dataString.substr( 2, 2 );
                    }
                    else {
                        formattedString += formatNumber( rawData, numberOfLetters );
                    }
                    break;

                case Evernote.SimpleDateFormat.MONTH:
                    if ( numberOfLetters >= 3 ) {
                        formattedString += formatText( Evernote.SimpleDateFormat.monthNames[rawData], numberOfLetters, numberOfLetters );
                    }
                    else {
                        // NB. Months returned by getMonth are zero-based
                        formattedString += formatNumber( rawData + 1, numberOfLetters );
                    }
                    break;

                case Evernote.SimpleDateFormat.TIMEZONE:
                    var isPositive = (rawData > 0);
                    // The following line looks like a mistake but isn't
                    // because of the way getTimezoneOffset measures.
                    var prefix = isPositive ? "-" : "+";
                    var absData = Math.abs( rawData );

                    // Hours
                    var hours = "" + Math.floor( absData / 60 );
                    hours = padWithZeroes( hours, 2 );
                    // Minutes
                    var minutes = "" + (absData % 60);
                    minutes = padWithZeroes( minutes, 2 );

                    formattedString += prefix + hours + minutes;
                    break;
            }
        }
        searchString = searchString.substr( result.index + result[0].length );
    }

    return formattedString;
};