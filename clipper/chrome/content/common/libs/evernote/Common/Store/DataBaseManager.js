//"use strict";

Evernote.DataBaseManager = function DataBaseManager() {
    this.__defineGetter__( "dbConnection", this.getDBConnection );
    this._stores = { };
};

Evernote.DataBaseManager.LOCAL_STORE_FILENAME = "evernote_webclipper.sqlite";

Evernote.DataBaseManager.prototype._stores = null;
Evernote.DataBaseManager.prototype._dbConnection = null;

Evernote.DataBaseManager.prototype.get = function( tableName, key ) {
    if ( typeof tableName != "string" || typeof key != "string" ) {
        return null;
    }

    var store = this.getStore( tableName );
    if ( store ) {
        Evernote.logger.debug( "DataBaseManager.get(): key = " + key + ", tableName = " + tableName );
        return store.get( key );
    }

    return null;
};

Evernote.DataBaseManager.prototype.put = function( tableName, key, value ) {
    if ( typeof tableName != "string" || typeof key != "string" ) {
        return;
    }

    var store = this.getStore( tableName );
    if ( store ) {
        Evernote.logger.debug( "DataBaseManager.put(): key = " + key + ", tableName = " + tableName );
        if ( value != null ) {
            this.getStore( tableName ).put( key, value );
        }
        else {
            this.getStore( tableName ).remove( key );
        }
    }
};

Evernote.DataBaseManager.prototype.getStore = function( tableName ) {
    if ( typeof tableName != "string" ) {
        return;
    }

    Evernote.logger.debug( "DataBaseManager.getStore(): tableName = " + tableName );
    if ( !this._stores[ tableName ] ) {
        var dbConnection = this.dbConnection;
        if ( dbConnection ) {
            this._stores[ tableName ] = new Evernote.LocalStore( dbConnection, tableName );
        }
    }

    return this._stores[ tableName ];
};

Evernote.DataBaseManager.prototype.getDBConnection = function() {
    Evernote.logger.debug( "DataBaseManager.getDBConnection()" );

    try {
        if ( !this._dbConnection ) {
            var file = Components.classes[ "@mozilla.org/file/directory_service;1" ].getService( Components.interfaces.nsIProperties ).
                           get( "ProfD", Components.interfaces.nsIFile );
            file.append( this.constructor.LOCAL_STORE_FILENAME );

            var storageService = Components.classes[ "@mozilla.org/storage/service;1" ].getService( Components.interfaces.mozIStorageService );
            this._dbConnection = storageService.openDatabase( file );
        }
    }
    catch ( e ) {
        Evernote.logger.error( "DataBaseManager.getDBConnection() failed: error = " + e );
    }

    return this._dbConnection;
};

Evernote.DataBaseManager.prototype.deleteUnusedKeys = function() {
    try {
        this.put( "common_store", "password", null );
        this.put( "common_store", "secureProto", null );
        this.put( "common_store", "insecureProto", null );
        this.put( "common_store", "serviceDomain", null );
    }
    catch ( e ) {
    }
};