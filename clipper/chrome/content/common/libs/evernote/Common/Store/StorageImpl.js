//"use strict";

Evernote.StorageImpl = function StorageImpl() {
};

Evernote.StorageImpl.prototype.get = function( /*key*/ ) {
};

Evernote.StorageImpl.prototype.put = function( /*key, value*/ ) {
};

Evernote.StorageImpl.prototype.remove = function( /*key*/ ) {
};

Evernote.StorageImpl.prototype.clear = function() {
};

Evernote.StorageImpl.prototype.getLength = function() {
};
