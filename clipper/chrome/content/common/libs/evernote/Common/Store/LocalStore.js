//"use strict";

Evernote.LocalStore = function LocalStore( dbConnection, tableName ) {
    this.initialize( dbConnection, tableName );
};

Evernote.LocalStore.CHAR_TRANSLATIONS = {
    "-" : "__dash_",
    "." : "__point_",
    "@" : "__commercialAT_",
    " " : "__space_"
};

Evernote.LocalStore.prototype._impl = null;

Evernote.LocalStore.prototype.initialize = function( dbConnection, tableName ) {
    Evernote.logger.debug( "LocalStore.initialize()" );
    this._impl = new Evernote.SqlLiteStorageImpl( dbConnection, this.translateTableName( tableName ) );
};

Evernote.LocalStore.prototype.translateTableName = function( tableName ) {
    Evernote.logger.debug( "LocalStore.translateTableName(): tableName = " );

    var self = this;
    var replaceFunc = function( conjunction, offset, source ) {
        return self.constructor.CHAR_TRANSLATIONS[ conjunction ];
    };

    tableName = tableName.replace( /[\.-]/i, replaceFunc ).replace( /[@ ]/i, replaceFunc );
    Evernote.logger.debug( "Translated table name = " + tableName );

    return tableName;
};

Evernote.LocalStore.prototype.get = function( key ) {
    if ( this._impl ) {
        return this._impl.get( key );
    }

    return null;
};

Evernote.LocalStore.prototype.put = function( key, value ) {
    if ( this._impl ) {
        this._impl.put( key, value );
    }
};

Evernote.LocalStore.prototype.remove = function( key ) {
    if ( this._impl ) {
        this._impl.remove( key );
    }
};

Evernote.LocalStore.prototype.clear = function() {
    if ( this._impl ) {
        this._impl.clear();
    }
};

Evernote.LocalStore.prototype.getLength = function() {
    if ( this._impl ) {
        return this._impl.length;
    }

    return 0;
};