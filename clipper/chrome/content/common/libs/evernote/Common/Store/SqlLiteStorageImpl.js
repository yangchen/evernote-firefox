//"use strict";

Evernote.SqlLiteStorageImpl = function SqlLiteStorageImpl( sdb, tableName ) {
    this.initialize( sdb, tableName );
};

Evernote.inherit( Evernote.SqlLiteStorageImpl, Evernote.StorageImpl, true );

Evernote.SqlLiteStorageImpl.DEFAULT_TABLE_NAME = "common_store";

Evernote.SqlLiteStorageImpl.prototype._cache = null;
Evernote.SqlLiteStorageImpl.prototype._keys = null;
Evernote.SqlLiteStorageImpl.prototype._tableName = null;

Evernote.SqlLiteStorageImpl.prototype.initialize = function( sdb, tableName ) {
    Evernote.logger.debug( "SqlLiteStorageImpl.initialize(): tableName = " + tableName );

    this._cache = sdb;
    this._tableName = (typeof tableName == 'string') ? tableName : this.constructor.DEFAULT_TABLE_NAME;
    this.createTable();
    this._keys = this.getKeys();
};

Evernote.SqlLiteStorageImpl.prototype.get = function( key ) {
    Evernote.logger.debug( "SqlLiteStorageImpl.get(): key = " + key + " , tableName = " + this._tableName );

    try {
        var result = null;
        var st = this._cache.createStatement( "SELECT * FROM " + this._tableName + " where key = :row_key" );
        st.params.row_key = key;

        if ( st.executeStep() ) {
            result = JSON.parse( st.row.value );
        }
        st.reset();
    }
    catch ( e ) {
        Evernote.logger.error( "SqlLiteStorageImpl.get() failed: key = " + key + " , tableName = " + this._tableName + ", error = " + e );
    }

    return result;
};

Evernote.SqlLiteStorageImpl.prototype.put = function( key, value ) {
    Evernote.logger.debug( "SqlLiteStorageImpl.put(): key = " + key + ", tableName = " + this._tableName );

    try {
        var stStr = "";
        var i = -1;
        if ( (i = this._keys.indexOf( key )) >= 0 ) {
            stStr = "UPDATE " + this._tableName + " set value = :row_value where key = :row_key";
        }
        else {
            stStr = "INSERT INTO " + this._tableName + " (key, value) VALUES(:row_key, :row_value)";
        }

        Evernote.logger.debug( "SQL: " + stStr );
        var strValue = JSON.stringify( value );
        Evernote.logger.debug( "VALUE: " + strValue );

        var st = this._cache.createStatement( stStr );
        st.params.row_key = key;
        st.params.row_value = strValue;

        if ( typeof st.executeStep() != 'undefined' ) {
            if ( i < 0 ) {
                this._keys.push( key );
            }
        }

        st.reset();
    }
    catch ( e ) {
        Evernote.logger.debug( "SqlLiteStorageImpl.put() failed: key = " + key + ", tableName = " + this._tableName + ". error = " + e );
    }
};

Evernote.SqlLiteStorageImpl.prototype.remove = function( key ) {
    Evernote.logger.debug( "SqlLiteStorageImpl.remove(): key = " + key + ", tableName = " + this._tableName );

    try {
        var i = -1;
        if ( (i = this._keys.indexOf( key )) >= 0 ) {
            var st = this._cache.createStatement( "DELETE FROM " + this._tableName + " where key = :row_key" );
            st.params.row_key = key;
            if ( typeof st.executeStep() != 'undefined' ) {
                this._keys.splice( i, 1 );
            }

            st.reset();
        }
    }
    catch ( e ) {
        Evernote.logger.error( "SqlLiteStorageImpl.remove() failed: key = " + key + ", tableName = " + this._tableName + ", error = " + e );
    }
};

Evernote.SqlLiteStorageImpl.prototype.clear = function() {
    Evernote.logger.debug( "SqlLiteStorageImpl.clear(): tableName = " + this._tableName );

    try {
        var st = this._cache.createStatement( "DELETE FROM " + this._tableName );
        if ( st.executeStep() === false ) {
            this._keys = [ ];
        }
    }
    catch ( e ) {
        Evernote.logger.error( "SqlLiteStorageImpl.clear() failed: tableName = " + this._tableName + ", error = " + e );
    }
};

Evernote.SqlLiteStorageImpl.prototype.getLength = function() {
    return this._keys.length;
};

Evernote.SqlLiteStorageImpl.prototype.getKeys = function() {
    Evernote.logger.debug( "SqlLiteStorageImpl.getKeys(): tableName = " + this._tableName );

    try {
        var st = this._cache.createStatement( "SELECT key FROM " + this._tableName );
        var keys = [ ];
        while ( st.executeStep() ) {
            keys.push( st.row.key );
        }

        return keys;
    }
    catch ( e ) {
        Evernote.logger.error( "SqlLiteStorageImpl.getKeys() failed: tableName = " + this._tableName + ", error = " + e );
    }

    return [ ];
};

Evernote.SqlLiteStorageImpl.prototype.createTable = function( drop ) {
    Evernote.logger.debug( "SqlLiteStorageImpl.createTable(): tableName = " + this._tableName );

    try {
        if ( drop ) {
            this._cache.executeSimpleSQL( "DROP TABLE " + this._tableName + " IF EXISTS" );
        }
        this._cache.executeSimpleSQL( "CREATE TABLE IF NOT EXISTS " + this._tableName + " (key TEXT PRIMARY KEY, value BLOB)" );
    }
    catch ( e ) {
        Evernote.logger.error( "SqlLiteStorageImpl.createTable() failed: tableName = " + this._tableName + ", error = " + e );
    }
};