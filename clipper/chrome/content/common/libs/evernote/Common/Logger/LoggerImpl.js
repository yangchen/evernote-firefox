//"use strict";

Evernote.LoggerImpl = function LoggerImpl() {
};

Evernote.LoggerImpl.TRACE_PREFIX = "[TRACE] ";
Evernote.LoggerImpl.DEBUG_PREFIX = "[DEBUG] ";
Evernote.LoggerImpl.INFO_PREFIX = "[INFO] ";
Evernote.LoggerImpl.WARN_PREFIX = "[WARN] ";
Evernote.LoggerImpl.ERROR_PREFIX = "[ERROR] ";

Evernote.LoggerImpl.prototype.isEnabled = function() {
    return false;
};

Evernote.LoggerImpl.prototype.trace = function( /*obj*/ ) {
};

Evernote.LoggerImpl.prototype.debug = function( /*str*/ ) {
};

Evernote.LoggerImpl.prototype.info = function( /*str*/ ) {
};

Evernote.LoggerImpl.prototype.warn = function( /*str*/ ) {
};

Evernote.LoggerImpl.prototype.error = function( /*str*/ ) {
};