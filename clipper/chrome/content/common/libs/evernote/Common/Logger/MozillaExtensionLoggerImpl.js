//"use strict";

Evernote.MozillaExtensionLoggerImpl = function MozillaExtensionLoggerImpl() {
    this.__defineGetter__( "console", this.getConsole );
};

Evernote.inherit( Evernote.MozillaExtensionLoggerImpl, Evernote.LoggerImpl, true );

Evernote.MozillaExtensionLoggerImpl.prototype._console = null;

Evernote.MozillaExtensionLoggerImpl.prototype.isEnabled = function() {
    return navigator.userAgent.match( /Firefox/i );
};

Evernote.MozillaExtensionLoggerImpl.prototype.trace = function( str ) {
    this.console.logStringMessage( this.parent.constructor.TRACE_PREFIX + str );
};

Evernote.MozillaExtensionLoggerImpl.prototype.debug = function( str ) {
    this.console.logStringMessage( this.parent.constructor.DEBUG_PREFIX + str );
};

Evernote.MozillaExtensionLoggerImpl.prototype.info = function( str ) {
    this.console.logStringMessage( this.parent.constructor.INFO_PREFIX + str );
};

Evernote.MozillaExtensionLoggerImpl.prototype.warn = function( str ) {
    this.console.logStringMessage( this.parent.constructor.WARN_PREFIX + str );
};

Evernote.MozillaExtensionLoggerImpl.prototype.error = function( str ) {
    this.console.logStringMessage( this.parent.constructor.ERROR_PREFIX + str );
};

Evernote.MozillaExtensionLoggerImpl.prototype.getConsole = function() {
    if ( !this._console ) {
        this._console = Components.classes[ "@mozilla.org/consoleservice;1" ].getService( Components.interfaces.nsIConsoleService );
    }

    return this._console;
};