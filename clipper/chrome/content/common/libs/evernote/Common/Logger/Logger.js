//"use strict";

Evernote.Logger = function Logger() {
    this.__defineGetter__( "mozillaImpl", this.getMozillaImpl );
    this.__defineGetter__( "fileImpl", this.getFileImpl );
};

Evernote.Logger.LOG_LEVEL_TRACE = 0;
Evernote.Logger.LOG_LEVEL_DEBUG = 1;
Evernote.Logger.LOG_LEVEL_INFO = 2;
Evernote.Logger.LOG_LEVEL_WARN = 3;
Evernote.Logger.LOG_LEVEL_ERROR = 4;
Evernote.Logger.LOG_LEVEL_OFF = 5;

//Should be switched to ERROR before release
Evernote.Logger.prototype._level = Evernote.Logger.LOG_LEVEL_ERROR;

Evernote.Logger.prototype._mozillaImpl = null;
Evernote.Logger.prototype._fileImpl = null;

Evernote.Logger.prototype.trace = function( str ) {
    if ( this._level <= this.constructor.LOG_LEVEL_TRACE ) {
        this.mozillaImpl.trace( str );
        this.fileImpl.trace( str );
    }
};

Evernote.Logger.prototype.debug = function( str ) {
    if ( this._level <= this.constructor.LOG_LEVEL_DEBUG ) {
        this.mozillaImpl.debug( str );
        this.fileImpl.debug( str );
    }
};

Evernote.Logger.prototype.info = function( str ) {
    if ( this._level <= this.constructor.LOG_LEVEL_INFO ) {
        this.mozillaImpl.info( str );
        this.fileImpl.info( str );
    }
};

Evernote.Logger.prototype.warn = function( str ) {
    if ( this._level <= this.constructor.LOG_LEVEL_WARN ) {
        this.mozillaImpl.warn( str );
        this.fileImpl.warn( str );
    }
};

Evernote.Logger.prototype.error = function( str ) {
    if ( this._level <= this.constructor.LOG_LEVEL_ERROR ) {
        //alert( str );
        this.mozillaImpl.error( str );
        this.fileImpl.error( str );
    }
};

Evernote.Logger.prototype.getMozillaImpl = function() {
    if ( !this._mozillaImpl ) {
        this._mozillaImpl = new Evernote.MozillaExtensionLoggerImpl();
    }

    return this._mozillaImpl;
};

Evernote.Logger.prototype.getFileImpl = function() {
    if ( !this._fileImpl ) {
        this._fileImpl = new Evernote.FileLoggerImpl();
    }

    return this._fileImpl;
};















