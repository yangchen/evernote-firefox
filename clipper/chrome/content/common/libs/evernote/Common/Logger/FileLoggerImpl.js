//"use strict";

Evernote.FileLoggerImpl = function FileLoggerImpl() {
    this.initialize();
};

Evernote.inherit( Evernote.FileLoggerImpl, Evernote.LoggerImpl, true );

Evernote.FileLoggerImpl.ENABLED = false;
Evernote.FileLoggerImpl.LOG_FILE_NAME = "ff_evernote.log";

Evernote.FileLoggerImpl.prototype._file = null;

Evernote.FileLoggerImpl.prototype.isEnabled = function() {
    return (this.constructor.ENABLED && navigator.userAgent.match( /Firefox/i )) ? true : false;
};

Evernote.FileLoggerImpl.prototype.initialize = function() {
    try {
        var profilePath = Components.classes[ "@mozilla.org/file/directory_service;1" ].getService( Components.interfaces.nsIProperties ).
            get( "ProfD", Components.interfaces.nsIFile ).path;

        var file = Components.classes[ "@mozilla.org/file/local;1" ].createInstance( Components.interfaces.nsILocalFile );
        file.initWithPath( profilePath + Evernote.SLASH + this.constructor.LOG_FILE_NAME );

        var foStream = Components.classes[ "@mozilla.org/network/file-output-stream;1" ].createInstance( Components.interfaces.nsIFileOutputStream );
        // use 0x02 | 0x20 to open file and truncate content.
        foStream.init( file, 0x02 | 0x08 | 0x10, 0666, 0 );

        var converter = Components.classes[ "@mozilla.org/intl/converter-output-stream;1" ].createInstance( Components.interfaces.nsIConverterOutputStream );
        converter.init( foStream, "UTF-8", 0, 0 );
        this._file = converter;
    }
    catch ( e ) {
        this.constructor.ENABLED = false;
    }
};

Evernote.FileLoggerImpl.prototype.trace = function( str ) {
    this.writeStr( this.parent.constructor.TRACE_PREFIX + str );
};

Evernote.FileLoggerImpl.prototype.debug = function( str ) {
    this.writeStr( this.parent.constructor.DEBUG_PREFIX + str );
};

Evernote.FileLoggerImpl.prototype.info = function( str ) {
    this.writeStr( this.parent.constructor.INFO_PREFIX + str );
};

Evernote.FileLoggerImpl.prototype.warn = function( str ) {
    this.writeStr( this.parent.constructor.WARN_PREFIX + str );
};

Evernote.FileLoggerImpl.prototype.error = function( str ) {
    this.writeStr( this.parent.constructor.ERROR_PREFIX + str );
};

Evernote.FileLoggerImpl.prototype.writeStr = function( str ) {
    if ( !this.constructor.ENABLED || !this._file ) {
        return;
    }

    var curTime = new Date();
    var msg = [ curTime.toLocaleFormat( "%d.%m.%y %H:%M:%S," ), curTime.getMilliseconds() % 1000, " ", str, "\r\n" ].join( "" );
    this._file.writeString( msg );
    this._file.flush();
};