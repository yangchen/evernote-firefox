//"use strict";

Evernote.BaseListener = function BaseListener() {
    this._listeners = [ ];
};

Evernote.BaseListener.prototype._listeners = null;

Evernote.BaseListener.prototype.addListener = function( obj ) {
    for ( var i = 0; i < this._listeners.length; ++i ) {
        if ( this._listeners[ i ] == obj ) {
            return;
        }
    }
    
    this._listeners.push( obj );
};

Evernote.BaseListener.prototype.removeListener = function( obj ) {
    for ( var i = 0; i < this._listeners.length; ++i ) {
        if ( this._listeners[ i ] == obj ) {
            this._listeners.splice( i, 1 );
            break;
        }
    }
};

Evernote.BaseListener.prototype.notify = function( eventName, args ) {
    args = Array.prototype.slice.call( arguments, 1 );
    for ( var i = 0; i < this._listeners.length; ++i ) {
        if ( typeof this._listeners[ i ][ eventName ] == "function" ) {
            try {
                this._listeners[ i ][ eventName ].apply( this._listeners[ i ], args );
            }
            catch ( e ) {
            }
        }
    }
};