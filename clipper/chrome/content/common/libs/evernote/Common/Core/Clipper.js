//"use strict";

Evernote.Clipper = function Clipper() {
};

Evernote.Clipper.prototype.startClip = function( rootElement, contextMenuClipType ) {
    Evernote.logger.debug( "Clipper.startClip()" );
    this.clipFF( content, rootElement, contextMenuClipType );
};

Evernote.Clipper.prototype.finishClip = function( clip, metadata, isSaveUrl ) {
    Evernote.logger.debug( "Clipper.finishClip()" );

    var id = Evernote.Utils.generateGuid();
    Evernote.logger.debug( "Clip length is " + clip.length );
    Evernote.logger.debug( "Clip content length is " + clip.contentLength );

    if ( clip.length >= Evernote.Limits.CLIP_NOTE_CONTENT_LEN_MAX || clip.contentLength >= Evernote.Limits.CLIP_CONTENT_LENGTH_MAX ) {
        Evernote.logger.warn( "Clip content is too big" );
        Evernote.Utils.alert( Evernote.localizer.getMessage( "BrowserActionTitle" ), Evernote.localizer.getMessage( "fullPageClipTooBig" ) );
        return;
    }

    var note = new Evernote.ClipNote( clip.toJSON() );
    note.initMetadata( JSON.parse( metadata ), isSaveUrl );

    Evernote.clipProcessor.processNote( note, id );
};

Evernote.Clipper.prototype.clipFF = function( tab, rootElement, contextMenuClipType ) {
    new Evernote.FFClipper( tab, rootElement, contextMenuClipType ).clip( this );
};