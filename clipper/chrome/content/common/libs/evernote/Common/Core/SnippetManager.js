//"use strict";

Evernote.SnippetManager = function () {
    this.initialize();
};

Evernote.SnippetManager.SNIPPET_KEY_PREFIX = "snippet_";
Evernote.SnippetManager.SNIPPET_ENTRIES_KEY = "snippetEntries";
Evernote.SnippetManager.MAX_CACHED_SNIPPETS = 200;

Evernote.SnippetManager.prototype._entries = null;

Evernote.SnippetManager.prototype.initialize = function() {
    Evernote.logger.debug( "SnippetManager.initialize()" );

    this._entries = { };
    var guids = Evernote.context.getFromUserDataBase( this.constructor.SNIPPET_ENTRIES_KEY );
    if ( guids instanceof Array ) {
        for ( var i = 0; i < guids.length; ++i ) {
            if ( guids[ i ] ) {
                this._entries[ guids[ i ] ] = null;
            }
        }
    }
};

Evernote.SnippetManager.prototype.getSnippet = function( guid ) {
    if ( typeof guid != "string" || typeof this._entries[ guid ] == 'undefined' ) {
        return;
    }

    Evernote.logger.debug( "SnippetManager.getSnippet(): guid = " + guid );
    if ( this._entries[ guid ] == null ) {
        var snippet = Evernote.context.getFromUserDataBase( this.constructor.SNIPPET_KEY_PREFIX + guid );
        if ( snippet ) {
            this._entries[ guid ] = new Evernote.Snippet( snippet );
        }
    }

    return this._entries[ guid ];
};

Evernote.SnippetManager.prototype.putSnippet = function( snippet ) {
    if ( snippet instanceof Evernote.Snippet && snippet.guid ) {
        Evernote.logger.debug( "SnippetManager.putSnippet(): guid = " + snippet.guid );
        this.reserveSpace( 1 );
        Evernote.context.putToUserDataBase( this.constructor.SNIPPET_KEY_PREFIX + snippet.guid, snippet );

        this._entries[ snippet.guid ] = snippet;
        this.updateKeyEntries();
    }
};

Evernote.SnippetManager.prototype.putSnippetList = function( snippetList ) {
    Evernote.logger.debug( "SnippetManager.putSnippetList()" );

    if ( snippetList instanceof Array ) {
        var num = Math.min( this.constructor.MAX_CACHED_SNIPPETS, snippetList.length );
        this.reserveSpace( num );

        for ( var i = 0; i < num; ++i ) {
            var snippet = snippetList[ i ];
            if ( snippet instanceof Evernote.Snippet && snippet.guid ) {
                Evernote.context.putToUserDataBase( this.constructor.SNIPPET_KEY_PREFIX + snippet.guid, snippet );
                this._entries[ snippet.guid ] = snippet;
            }
        }

        this.updateKeyEntries();
    }
};

Evernote.SnippetManager.prototype.removeSnippet = function( guid ) {
    if ( typeof guid != "string" || typeof this._entries[ guid ] == 'undefined' ) {
        return;
    }

    Evernote.logger.debug( "SnippetManager.removeSnippet(): guid = " + guid );
    Evernote.context.putToUserDataBase( this.constructor.SNIPPET_KEY_PREFIX + guid, null );
    delete this._entries[ guid ];

    this.updateKeyEntries();
 };

Evernote.SnippetManager.prototype.removeSnippetList = function( guids ) {
    Evernote.logger.debug( "SnippetManager.removeSnippetList()" );

    if ( guids instanceof Array ) {
        for ( var i = 0; i < guids.length; ++i ) {
            var guid = guids[ i ];
            if ( typeof guid == "string" && typeof this._entries[ guid ] != 'undefined' ) {
                Evernote.context.putToUserDataBase( this.constructor.SNIPPET_KEY_PREFIX + guid, null );
                delete this._entries[ guid ];
            }
        }

        this.updateKeyEntries();
    }
};

Evernote.SnippetManager.prototype.clear = function() {
    Evernote.logger.debug( "SnippetManager.clear()" );

    var guids = Evernote.context.getFromUserDataBase( this.constructor.SNIPPET_ENTRIES_KEY );
    this.removeSnippetList( guids );
    this._entries = { };
};

Evernote.SnippetManager.prototype.reserveSpace = function( count ) {
    Evernote.logger.debug( "SnippetManager.reserveSpace(): count = " + count );

    var length = this.getLength();
    if ( count <= 0 || (length + count) <= this.constructor.MAX_CACHED_SNIPPETS ) {
        return;
    }

    var delta = Math.abs( count - (this.constructor.MAX_CACHED_SNIPPETS - length) );
    var guids = Evernote.context.getFromUserDataBase( this.constructor.SNIPPET_ENTRIES_KEY );

    if ( guids instanceof Array ) {
        var toRemove = guids.slice( 0, delta );
        if ( toRemove && toRemove.length > 0 ) {
            this.removeSnippetList( toRemove );
        }
    }
};

Evernote.SnippetManager.prototype.getLength = function() {
    var num = 0;
    for ( var key in this._entries ) {
        ++num;
    }

    return num;
};

Evernote.SnippetManager.prototype.updateKeyEntries = function() {
    Evernote.logger.debug( "SnippetManager.updateKeyEntries()" );

    var guids = [ ];
    for ( var guid in this._entries ) {
        guids.push( guid );
    }

    Evernote.context.putToUserDataBase( this.constructor.SNIPPET_ENTRIES_KEY, guids );
};