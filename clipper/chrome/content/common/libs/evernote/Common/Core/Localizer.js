//"use strict";

Evernote.Localizer = function Localizer() {
    this.__defineSetter__( "stringBundle", this.setStringBundle );
};

Evernote.Localizer.MESSAGE_ATTR = "message";
Evernote.Localizer.LOCALIZED_ATTR = "localized";
Evernote.Localizer.PLACEHOLDER_ATTR = "placeholder";
Evernote.Localizer.TITLE_ATTR = "title";
Evernote.Localizer.MESSAGE_DATA_ATTR = "messagedata";

Evernote.Localizer.prototype._stringBundle = null;

Evernote.Localizer.prototype.getMessage = function( key, params ) {
    Evernote.logger.debug( "Localizer.getMessage():  key = " + key );

    if ( this._stringBundle ) {
        try {
            if ( typeof params != 'undefined' ) {
                var p = [ ].concat( params );
                return this._stringBundle.formatStringFromName( key, p, p.length );
            }
            else {
                return this._stringBundle.GetStringFromName( key );
            }
        }
        catch ( e ) {
            Evernote.logger.warn( "Localizer.getMessage() failed: key = " + key + ", error = " + e );
        }

        return;
    }

    return key;
};

Evernote.Localizer.prototype.localizeBlock = function( block ) {
    Evernote.logger.debug( "Localizer.localizeBlock()" );

    if ( block.attr( this.constructor.MESSAGE_ATTR ) ) {
        this.localizeElement( block );
    }

    var siblings = block.find( "[" + this.constructor.MESSAGE_ATTR + "], [" + this.constructor.PLACEHOLDER_ATTR + "], [" + this.constructor.TITLE_ATTR + "]" );
    for ( var i = 0; i < siblings.length; i++ ) {
        var sibling = Evernote.jQuery( siblings.get( i ) );
        this.localizeElement( sibling );
    }
};

Evernote.Localizer.prototype.localizeElement = function( element, force ) {
    Evernote.logger.debug( "Localizer.localizeElement()" );

    if ( !force && element.attr( this.constructor.LOCALIZED_ATTR ) && element.attr( this.constructor.LOCALIZED_ATTR ) == "true" ) {
        return;
    }

    var field = this.extractLocalizationField( element );
    var placeHolderField = this.extractLocalizationPlaceholderField( element );
    var titleField = this.extractLocalizationTitleField( element );
    var dataField = this.extractLocalizationDataField( element );

    if ( field ) {
        if ( dataField ) {
           var msg = this.getMessage( field, dataField );
        }
        else {
            msg = this.getMessage( field );
        }

        if ( element.attr( "tagName" ) == "INPUT" ) {
            element.val( msg );
        }
        else {
            element.html( msg );
        }

        var doneLocalize = true;
    }

    if ( placeHolderField && typeof placeHolderField == "string" ) {
        msg = this.getMessage( placeHolderField );
        element.attr( this.constructor.PLACEHOLDER_ATTR, msg );
        doneLocalize = true;
    }

    if ( titleField && typeof titleField == "string" ) {
        msg = this.getMessage( titleField );
        element.attr( this.constructor.TITLE_ATTR, msg );
        doneLocalize = true;
    }

    if ( doneLocalize ) {
        element.attr( this.constructor.LOCALIZED_ATTR, "true" );
    }
};

Evernote.Localizer.prototype.extractLocalizationField = function( element ) {
    if ( typeof element.attr == 'function' && element.attr( this.constructor.MESSAGE_ATTR ) ) {
        return element.attr( this.constructor.MESSAGE_ATTR );
    }
    else {
        return null;
    }
};

Evernote.Localizer.prototype.extractLocalizationPlaceholderField = function ( element ) {
    if ( typeof element.attr == "function" && element.attr( this.constructor.PLACEHOLDER_ATTR ) ) {
        return element.attr( this.constructor.PLACEHOLDER_ATTR );
    }
    else {
        return null;
    }
};

Evernote.Localizer.prototype.extractLocalizationTitleField = function ( element ) {
    if ( typeof element.attr == "function" && element.attr( this.constructor.TITLE_ATTR ) ) {
        return element.attr( this.constructor.TITLE_ATTR );
    }
    else {
        return null;
    }
};

Evernote.Localizer.prototype.extractLocalizationDataField = function ( element ) {
    if ( typeof element.attr == "function" && element.attr( this.constructor.MESSAGE_DATA_ATTR ) ) {
        var v = element.attr( this.constructor.MESSAGE_DATA_ATTR );
        try {
            v = JSON.parse( v );
        }
        catch ( e ) {
        }

        if ( !(v instanceof Array) ) {
            v = [ v ];
        }
        
        return v;
    }
    else {
        return null;
    }
};

Evernote.Localizer.prototype.setStringBundle = function( bundle ) {
    this._stringBundle = bundle;
};