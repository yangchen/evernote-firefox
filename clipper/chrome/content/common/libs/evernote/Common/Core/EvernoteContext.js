//"use strict";

Evernote.EvernoteContext = function EvernoteContext() {
    this.__defineGetter__( "authenticationToken", this.getAuthenticationToken );
    this.__defineGetter__( "tags", this.getTags );
    this.__defineSetter__( "tags", this.setTags );
    this.__defineGetter__( "searches", this.getSearches );
    this.__defineSetter__( "searches", this.setSearches );
    this.__defineGetter__( "notebooks", this.getNotebooks );
    this.__defineSetter__( "notebooks", this.setNotebooks );
    this.__defineGetter__( "linkedNotebooks", this.getLinkedNotebooks );
    this.__defineSetter__( "linkedNotebooks", this.setLinkedNotebooks );
    this.__defineGetter__( "defaultNotebook", this.getDefaultNotebook );
    this.__defineSetter__( "defaultNotebook", this.setDefaultNotebook );
    this.__defineGetter__( "syncState", this.getSyncState );
    this.__defineSetter__( "syncState", this.setSyncState );
    this.__defineGetter__( "user", this.getUser );
    this.__defineSetter__( "user", this.setUser );
    this.__defineGetter__( "noteFilter", this.getNoteFilter );
    this.__defineSetter__( "noteFilter", this.setNoteFilter );
    this.__defineGetter__( "options", this.getOptions );
    this.__defineSetter__( "options", this.setOptions );
    this.__defineGetter__( "state", this.getState );
    this.__defineSetter__( "state", this.setState );
    this.__defineGetter__( "autoSavedNote", this.getAutoSavedNote );
    this.__defineSetter__( "autoSavedNote", this.setAutoSavedNote );
    this.__defineSetter__( "currentUserName", this.setCurrentUserName );
    this.__defineGetter__( "currentUserName", this.getCurrentUserName );
    this.__defineSetter__( "password", this.setUserPassword );
    this.__defineGetter__( "password", this.getUserPassword );
    this.__defineGetter__( "passwordStorage", this.getPasswordStorage );
    this.__defineGetter__( "clientEnabled", this.getClientEnabled );
    this.__defineSetter__( "clientEnabled", this.setClientEnabled );
    this.__defineGetter__( "useChina", this.getUseChina );
    this.__defineSetter__( "useChina", this.setUseChina );
};

Evernote.EvernoteContext.COMMON_STORE_NAME = "common_store";

Evernote.EvernoteContext.prototype._tags = null;
Evernote.EvernoteContext.prototype._tagGuidMap = null;
Evernote.EvernoteContext.prototype._tagNameMap = null;
Evernote.EvernoteContext.prototype._searches = null;
Evernote.EvernoteContext.prototype._notebooks = null;
Evernote.EvernoteContext.prototype._notebookGuidMap = null;
Evernote.EvernoteContext.prototype._notebookNameMap = null;
Evernote.EvernoteContext.prototype._linkedNotebooks = null;
Evernote.EvernoteContext.prototype._linkedNotebookGuidMap = null;
Evernote.EvernoteContext.prototype._defaultNotebook = null;
Evernote.EvernoteContext.prototype._user = null;
Evernote.EvernoteContext.prototype._noteFilter = null;
Evernote.EvernoteContext.prototype._options = null;
Evernote.EvernoteContext.prototype._state = null;
Evernote.EvernoteContext.prototype._autoSavedNote = null;
Evernote.EvernoteContext.prototype._dataBaseManager = null;
Evernote.EvernoteContext.prototype._passwordStorage = null;

Evernote.EvernoteContext.prototype.destroy = function() {
    this._tags = null;
    this._tagGuidMap = null;
    this._tagNameMap = null;
    this._searches = null;
    this._notebooks = null;
    this._notebookGuidMap = null;
    this._notebookNameMap = null;
    this._linkedNotebooks = null;
    this._linkedNotebookGuidMap = null;
    this._defaultNotebook = null;
    this._user = null;
    this._noteFilter = null;
    this._options = null;
    this._state = null;
    this._autoSavedNote = null;
    this.currentUserName = "";
};

Evernote.EvernoteContext.prototype.isLogined = function() {
    return this.authenticationToken && this.currentUserName;
};

Evernote.EvernoteContext.prototype.getAuthenticationToken = function() {
    var auth = Evernote.cookieManager.get( Evernote.getSecureServiceUrl(), "auth" );
    if ( auth ) {
        return auth.value;
    }

    return null;
};

Evernote.EvernoteContext.prototype.getTags = function() {
    if ( this._tags == null ) {
        var tags = this.getFromUserDataBase( "tags" );
        if ( tags instanceof Array ) {
            this._tags = [ ];
            for ( var i = 0; i < tags.length; ++i ) {
                this._tags.push( new Evernote.Tag( tags[ i ] ) );
            }
        }
    }

    return this._tags;
};

Evernote.EvernoteContext.prototype.setTags = function( tags ) {
    var changed = false;
    if ( tags == null ) {
        this._tags = null;
        changed = true;
    }
    else if ( tags instanceof Evernote.Tag ) {
        this._tags = [ tags ];
        changed = true;
    }
    else if ( tags instanceof Array ) {
        this._tags = tags;
        changed = true;
    }

    if ( changed ) {
        this._tagGuidMap = null;
        this._tagNameMap = null;
        this.putToUserDataBase( "tags", this._tags );
    }
};

Evernote.EvernoteContext.prototype.getTagNames = function() {
    var tags = this.tags;
    var tagNames = [ ];
    if ( tags instanceof Array ) {
        for ( var i = 0; i < tags.length; ++i ) {
            tagNames.push( tags[ i ].name );
        }
    }

    return tagNames;
};

Evernote.EvernoteContext.prototype.getTagByGuid = function( guid ) {
    var tagMap = this.getTagGuidMap();
    if ( typeof guid == 'string' && tagMap[ guid ] ) {
        return tagMap[ guid ];
    }

    return null;
};

Evernote.EvernoteContext.prototype.getTagByName = function( name ) {
    if ( typeof name == 'string' ) {
        name = name.toLowerCase();
        var tagMap = this.getTagNameMap();
        return (tagMap[ name ]) ? tagMap[ name ] : null;
    }

    return null;
};

Evernote.EvernoteContext.prototype.getTagGuidMap = function() {
    var tags = this.tags;
    if ( this._tagGuidMap == null && tags instanceof Array ) {
        this._tagGuidMap = { };
        for ( var i = 0; i < tags.length; ++i ) {
            this._tagGuidMap[ tags[ i ].guid ] = tags[ i ];
        }
    }

    return this._tagGuidMap;
};

Evernote.EvernoteContext.prototype.getTagNameMap = function() {
    var tags = this.tags;
    if ( this._tagNameMap == null && tags instanceof Array ) {
        this._tagNameMap = { };
        for ( var i = 0; i < tags.length; ++i ) {
            this._tagNameMap[ tags[ i ].name.toLowerCase() ] = tags[ i ];
        }
    }

    return this._tagNameMap;
};

Evernote.EvernoteContext.prototype.getSearches = function() {
    if ( this._searches == null ) {
        var searches = this.getFromUserDataBase( "searches" );
        if ( searches instanceof Array ) {
            this._searches = [ ];
            for ( var i = 0; i < searches.length; ++i ) {
                this._searches.push( new Evernote.SavedSearch( searches[ i ] ) );
            }
        }
    }

    return this._searches;
};

Evernote.EvernoteContext.prototype.setSearches = function( searches ) {
    var changed = false;
    if ( searches == null ) {
        this._searches = null;
        changed = true;
    }
    else if ( searches instanceof Evernote.SavedSearch ) {
        this._searches = [ searches ];
        changed = true;
    }
    else if ( searches instanceof Array ) {
        this._searches = searches;
        changed = true;
    }

    if ( changed ) {
        this.putToUserDataBase( "searches", this._searches );
    }
};

Evernote.EvernoteContext.prototype.getNotebooks = function() {
    if ( this._notebooks == null ) {
        var notebooks = this.getFromUserDataBase( "notebooks" );
        if ( notebooks instanceof Array ) {
            this._notebooks = [ ];
            for ( var i = 0; i < notebooks.length; ++i ) {
                this._notebooks.push( new Evernote.Notebook( notebooks[ i ] ) );
            }
        }
    }

    return this._notebooks;
};

Evernote.EvernoteContext.prototype.setNotebooks = function( notebooks ) {
    var changed = false;
    if ( notebooks == null ) {
        this._notebooks = null;
        changed = true;
    }
    else if ( notebooks instanceof Evernote.Notebook ) {
        this._notebooks = [ notebooks ];
        changed = true;
    }
    else if ( notebooks instanceof Array ) {
        this._notebooks = Evernote.Utils.sortModelsByField( notebooks, "name" );
        changed = true;
    }

    if ( changed ) {
        this._defaultNotebook = null;
        this._notebookGuidMap = null;
        this._notebookNameMap = null;
        this.putToUserDataBase( "notebooks", this._notebooks );
    }
};

Evernote.EvernoteContext.prototype.getLinkedNotebooks = function() {
    if ( this._linkedNotebooks == null ) {
        var notebooks = this.getFromUserDataBase( "linkedNotebooks" );
        if ( notebooks instanceof Array ) {
            this._linkedNotebooks = [ ];
            for ( var i = 0; i < notebooks.length; ++i ) {
                this._linkedNotebooks.push( new Evernote.LinkedNotebook( notebooks[ i ] ) );
            }
        }
    }

    return this._linkedNotebooks;
};

Evernote.EvernoteContext.prototype.setLinkedNotebooks = function( notebooks ) {
    var changed = false;
    if ( notebooks == null ) {
        this._linkedNotebooks = null;
        changed = true;
    }
    else if ( notebooks instanceof Evernote.LinkedNotebook ) {
        this._linkedNotebooks = [ notebooks ];
        changed = true;
    }
    else if ( notebooks instanceof Array ) {
        this._linkedNotebooks = Evernote.Utils.sortModelsByField( notebooks, "name" );
        changed = true;
    }

    if ( changed ) {
        this._linkedNotebookGuidMap = null;
        this.putToUserDataBase( "linkedNotebooks", this._linkedNotebooks );
    }
};

Evernote.EvernoteContext.prototype.getNotebookNames = function() {
    var notebooks = this.notebooks;
    var notebookNames = [ ];
    if ( notebooks instanceof Array ) {
        for ( var i = 0; i < notebooks.length; ++i ) {
            notebookNames.push( notebooks[ i ].name );
        }
    }

    return notebookNames;
};

Evernote.EvernoteContext.prototype.getNotebookGuidMap = function() {
    var notebooks = this.notebooks;
    if ( this._notebookGuidMap == null && notebooks instanceof Array ) {
        this._notebookGuidMap = { };
        for ( var i = 0; i < notebooks.length; ++i ) {
            this._notebookGuidMap[ notebooks[ i ].guid ] = notebooks[ i ];
        }
    }

    return this._notebookGuidMap;
};

Evernote.EvernoteContext.prototype.getNotebookNameMap = function() {
    var notebooks = this.notebooks;
    if ( this._notebookNameMap == null && notebooks instanceof Array ) {
        this._notebookNameMap = { };
        for ( var i = 0; i < notebooks.length; ++i ) {
            this._notebookNameMap[ notebooks[ i ].name ] = notebooks[ i ];
        }
    }

    return this._notebookNameMap;
};

Evernote.EvernoteContext.prototype.getLinkedNotebookGuidMap = function() {
    var notebooks = this.linkedNotebooks;
    if ( this._linkedNotebookGuidMap == null && notebooks instanceof Array ) {
        this._linkedNotebookGuidMap = { };
        for ( var i = 0; i < notebooks.length; ++i ) {
            this._linkedNotebookGuidMap[ notebooks[ i ].guid ] = notebooks[ i ];
        }
    }

    return this._linkedNotebookGuidMap;
};

Evernote.EvernoteContext.prototype.getNotebookByGuid = function( guid ) {
    if ( typeof guid == 'string' ) {
        var guidMap = this.getNotebookGuidMap();
        return (guidMap && guidMap[ guid ]) ? guidMap[ guid ] : null;
    }

    return null;
};

Evernote.EvernoteContext.prototype.getNotebookByName = function( name ) {
    if ( typeof name == 'string' ) {
        var nameMap = this.getNotebookNameMap();
        return (nameMap && nameMap[ name ]) ? nameMap[ name ] : null;
    }

    return null;
};

Evernote.EvernoteContext.prototype.getLinkedNotebookByGuid = function( guid ) {
    if ( typeof guid == 'string' ) {
        var guidMap = this.getLinkedNotebookGuidMap();
        return (guidMap && guidMap[ guid ]) ? guidMap[ guid ] : null;
    }

    return null;
};

Evernote.EvernoteContext.prototype.getDefaultNotebook = function() {
    var notebooks = this.notebooks;
    if ( this._defaultNotebook == null && notebooks instanceof Array ) {
        for ( var i = 0; i < notebooks.length; ++i ) {
            if ( notebooks[ i ].defaultNotebook ) {
                this._defaultNotebook = notebooks[ i ];
                break;
            }
        }
    }

    return this._defaultNotebook;
};

Evernote.EvernoteContext.prototype.setDefaultNotebook = function( notebook ) {
    if ( notebook instanceof Evernote.Notebook ) {
        this._defaultNotebook = notebook;
    }
};

Evernote.EvernoteContext.prototype.getPreferredNotebook = function() {
    var preferredNotebook = null;
    var options = this.options;
    var state = this.state;

    if ( options ) {
        if ( options.clipNotebook == Evernote.Options.CLIP_NOTEBOOK_OPTIONS.SELECT && options.clipNotebookGuid ) {
            if ( options.clipNotebookGuid.match( /^shared_/ ) ) {
                preferredNotebook = this.getLinkedNotebookByGuid( options.clipNotebookGuid );
            }
            else {
                preferredNotebook = this.getNotebookByGuid( options.clipNotebookGuid );
            }
        }
        else if ( options.clipNotebook == Evernote.Options.CLIP_NOTEBOOK_OPTIONS.REMEMBER && state && state.notebookGuid ) {
            if ( state.notebookGuid.match( /^shared_/ ) ) {
                preferredNotebook = this.getLinkedNotebookByGuid( state.notebookGuid );
            }
            else {
                preferredNotebook = this.getNotebookByGuid( state.notebookGuid );
            }
        }
    }

    return (preferredNotebook) ? preferredNotebook : this.getDefaultNotebook();
};

Evernote.EvernoteContext.prototype.getSyncState = function() {
    var syncState = this.getFromUserDataBase( "syncState" );
    return (syncState) ? new Evernote.SyncState( syncState ) : null;
};

Evernote.EvernoteContext.prototype.setSyncState = function( syncState ) {
    if ( syncState instanceof Evernote.SyncState ) {
        this.putToUserDataBase( "syncState", syncState );
    }
};

Evernote.EvernoteContext.prototype.getUser = function() {
    if ( this._user == null ) {
        var user = this.getFromUserDataBase( "user" );
        if ( user ) {
            this._user = new Evernote.User( user );
        }
    }

    return this._user;
};

Evernote.EvernoteContext.prototype.setUser = function( user ) {
    var changed = false;
    if ( user instanceof Evernote.User ) {
        this._user = user;
        changed = true;
    }
    else if ( user == null ) {
        this._user = null;
        changed = true;
    }

    if ( user && typeof user.username == "string" ) {
        this.currentUserName = user.username;
    }
    else if ( !user ) {
        this.currentUserName = "";
    }
    
    if ( changed ) {
        this.putToUserDataBase( "user", this._user );
    }
};

Evernote.EvernoteContext.prototype.getNoteFilter = function() {
    if ( this._noteFilter == null ) {
        var noteFilter = this.getFromUserDataBase( "noteFilter" );
        if ( noteFilter ) {
            this._noteFilter = new Evernote.NoteFilter( noteFilter );
        }
    }

    if ( this._noteFilter == null ) {
        this._noteFilter = new Evernote.NoteFilter();
        this._noteFilter.fuzzy = true;
    }

    var sortOrder = this.options.noteSortOrder;
    if ( sortOrder ) {
        this._noteFilter.order = sortOrder.order;
        this._noteFilter.ascending = sortOrder.ascending;
    }

    return this._noteFilter;
};

Evernote.EvernoteContext.prototype.setNoteFilter = function( noteFilter ) {
    var changed = false;
    if ( noteFilter instanceof Evernote.NoteFilter ) {
        this._noteFilter = noteFilter;
        changed = true;
    }
    else if ( noteFilter == null ) {
        this._noteFilter = null;
        changed = true;
    }

    if ( changed ) {
        this.putToUserDataBase( "noteFilter", this._noteFilter );
    }
};

Evernote.EvernoteContext.prototype.getOptions = function() {
    var opts = this.getFromCommonDataBase( "options" );
    if ( opts ) {
        this._options = new Evernote.Options( opts );
    }

    if ( this._options == null ) {
        this._options = new Evernote.Options( Evernote.Options.DEFAULTS );
    }

    return this._options;
};

Evernote.EvernoteContext.prototype.setOptions = function( options ) {
    var changed = false;
    if ( options instanceof Evernote.Options ) {
        this._options = options;
        changed = true;
    }
    else if ( options == null ) {
        this._options = null;
        changed = true;
    }

    if ( changed ) {
        this.putToCommonDataBase( "options", this._options );
    }
};

Evernote.EvernoteContext.prototype.getState = function() {
    var state = this.getFromUserDataBase( "state" );
    if ( state ) {
        this._state = new Evernote.AppState( state );
    }

    if ( this._state == null ) {
        this._state = new Evernote.AppState();
    }

    return this._state;
};

Evernote.EvernoteContext.prototype.setState = function( state ) {
    var changed = false;
    if ( state instanceof Evernote.AppState ) {
        this._state = state;
        changed = true;
    }
    else if ( state == null ) {
        this._state = null;
        changed = true;
    }

    if ( changed ) {
        this.putToUserDataBase( "state", this._state );
    }
};

Evernote.EvernoteContext.prototype.getAutoSavedNote = function() {
    if ( this._autoSavedNote == null ) {
        var clipNote = this.getFromUserDataBase( "autoSavedNote" );
        if ( clipNote ) {
            this._autoSavedNote = new Evernote.ClipNote( clipNote );
        }
    }

    return this._autoSavedNote;
};

Evernote.EvernoteContext.prototype.setAutoSavedNote = function( clipNote ) {
    var changed = false;
    if ( clipNote instanceof Evernote.ClipNote ) {
        this._autoSavedNote = clipNote;
        changed = true;
    }
    else if ( clipNote == null ) {
        this._autoSavedNote = null;
        changed = true;
    }

    if ( changed ) {
        this.putToUserDataBase( "autoSavedNote", this._autoSavedNote );
    }
};

Evernote.EvernoteContext.prototype.getCurrentUserName = function() {
    var userName = this.getFromCommonDataBase( "userName" );
    return (userName) ? userName : "";
};

Evernote.EvernoteContext.prototype.setCurrentUserName = function( userName ) {
    if ( typeof userName == "string" ) {
        this.putToCommonDataBase( "userName", userName );
    }
};

Evernote.EvernoteContext.prototype.getUserPassword = function() {
    if ( !this.passwordStorage.hasMasterPassword() ) {
        return this.passwordStorage.loadPassword( this.currentUserName );
    }

    return "";
};

Evernote.EvernoteContext.prototype.setUserPassword = function( value ) {
    if ( typeof value == "string" && !this.passwordStorage.hasMasterPassword() ) {
        if ( value.length > 0 ) {
            this.passwordStorage.savePassword( this.currentUserName, value );
        }
        else {
            this.passwordStorage.removePassword( this.currentUserName );
        }
    }
};

Evernote.EvernoteContext.prototype.getClientEnabled = function() {
    return this.getFromCommonDataBase( "clientEnabled" );
};

Evernote.EvernoteContext.prototype.setClientEnabled = function( value ) {
    this.putToCommonDataBase( "clientEnabled", (value) ? true : false );
};

Evernote.EvernoteContext.prototype.getUseChina = function() {
    return this.getFromCommonDataBase( "useChina" );
};

Evernote.EvernoteContext.prototype.setUseChina = function( value ) {
    this.putToCommonDataBase( "useChina", (value) ? true : false );
};

Evernote.EvernoteContext.prototype.getShardId = function() {
    return (this.user instanceof Evernote.User) ? this.user.shardId : null;
};

Evernote.EvernoteContext.prototype.putToUserDataBase = function( key, value ) {
    if ( this.currentUserName == "" ) {
        return;
    }

    this.getDataBaseManager().put( this.currentUserName, key, value );
};

Evernote.EvernoteContext.prototype.getFromUserDataBase = function( key ) {
    if ( this.currentUserName == "" ) {
        return null;
    }

    return this.getDataBaseManager().get( this.currentUserName, key );
};

Evernote.EvernoteContext.prototype.putToCommonDataBase = function( key, value ) {
    this.getDataBaseManager().put( this.constructor.COMMON_STORE_NAME, key, value );
};

Evernote.EvernoteContext.prototype.getFromCommonDataBase = function( key ) {
    return this.getDataBaseManager().get( this.constructor.COMMON_STORE_NAME, key );
};

Evernote.EvernoteContext.prototype.processSyncChunk = function( syncChunk ) {
    Evernote.logger.debug( "EvernoteContext.processSyncChunk()" );

    try {
        if ( syncChunk instanceof Evernote.SyncChunk ) {
            if ( syncChunk.expungedNotebooks.length > 0 ) {
                this.notebooks = Evernote.Utils.expungeModels( this.notebooks, syncChunk.expungedNotebooks );
            }

            if ( syncChunk.notebooks.length > 0 ) {
                this.notebooks = Evernote.Utils.mergeModels( this.notebooks, syncChunk.notebooks );
            }

            if ( syncChunk.expungedTags.length > 0 ) {
                this.tags = Evernote.Utils.expungeModels( this.tags, syncChunk.expungedTags );
            }

            if ( syncChunk.tags.length > 0 ) {
                this.tags = Evernote.Utils.mergeModels( this.tags, syncChunk.tags );
            }

            var syncState = this.syncState;
            if ( syncState && syncChunk.chunkHighUSN > 0 ) {
                syncState.updateCount = syncChunk.chunkHighUSN;
                if ( syncChunk.currentTime ) {
                    syncState.currentTime = syncChunk.currentTime;
                }
                
                this.syncState = syncState;
            }
        }
    }
    catch ( e ) {
        Evernote.logger.error( "EvernoteContext.processSyncChunk() failed: error = " + e );
    }
};

Evernote.EvernoteContext.prototype.getDataBaseManager = function() {
    if ( !this._dataBaseManager ) {
        this._dataBaseManager = new Evernote.DataBaseManager();
        this._dataBaseManager.deleteUnusedKeys();
    }

    return this._dataBaseManager;
};

Evernote.EvernoteContext.prototype.getPasswordStorage = function() {
    if ( !this._passwordStorage ) {
        this._passwordStorage = new Evernote.PasswordStorage();
    }

    return this._passwordStorage;
};
