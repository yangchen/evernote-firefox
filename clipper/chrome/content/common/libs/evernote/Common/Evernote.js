//"use strict";

if ( typeof Evernote == "undefined" ) {
    Evernote = {
        _context : null,
        _remote : null,
        _logger: null,
        _jQuery : null,
        _syncManager : null,
        _cookieManager : null,
        _uninstaller : null,
        _prefsManager : null,
        _tabManager : null,
        _simSearchStorage : null,
        _clipProcessor : null,
        _fileManager : null,
        _closeListener : null,
        _localizer : null,
        _toolbarManager : null,
        _clipNotificator : null,
        _nativeController : null,
        _viewManager : null,
        _linkedNotebooksSource : null,
        _nativeAccessor : null,
        _pageInjector : null
    };

    Evernote.simulateChinese = false;
    //mike.c.yang@gmail.com: Force to using chinese
    Evernote.simulateChinese = true;

    Evernote.SECURE_PROTO = "https://";
    //mike.c.yang@gmail.com: change to new id
    //Evernote.EXTENSIOD_ID = "{E0B8C461-F8FB-49b4-8373-FE32E9252800}";
    Evernote.EXTENSIOD_ID = "clipper@yinxiang.com";

    Evernote.REGISTRATION_CODE = "clipper_ff";
}

Evernote.inherit = function( childConstructor, parentClassOrObject, includeConstructorDefs ) {
    if ( parentClassOrObject.constructor == Function ) {
        // Normal Inheritance
        childConstructor.prototype = new parentClassOrObject;
        childConstructor.prototype.constructor = childConstructor;
        childConstructor.prototype.parent = parentClassOrObject.prototype;
        childConstructor.constructor.parent = parentClassOrObject;
    }
    else {
        // Pure Virtual Inheritance
        childConstructor.prototype = parentClassOrObject;
        childConstructor.prototype.constructor = childConstructor;
        childConstructor.prototype.parent = parentClassOrObject;
        childConstructor.constructor.parent = parentClassOrObject;
    }

    if ( includeConstructorDefs ) {
        for ( var i in parentClassOrObject.prototype.constructor ) {
            if ( i != "parent" && i != "prototype" && parentClassOrObject.constructor[i] != parentClassOrObject.prototype.constructor[ i ]
                && typeof childConstructor.prototype.constructor[ i ] == 'undefined' ) {
                childConstructor.prototype.constructor[ i ] = parentClassOrObject.prototype.constructor[ i ];
            }
        }
    }

    if ( typeof childConstructor.handleInheritance == 'function' ) {
        childConstructor.handleInheritance.apply( childConstructor, arguments );
    }

    if ( typeof childConstructor.prototype.handleInheritance == 'function' ) {
        childConstructor.prototype.handleInheritance.apply( childConstructor, arguments );
    }

    return childConstructor;
};

Evernote.mixin = function( classOrObject, mixin, map ) {
    var target = (typeof classOrObject == 'function') ? classOrObject.prototype : classOrObject;
    for ( var i in mixin.prototype ) {
        var to;
        var from = to = i;
        if ( typeof map == 'object' && map && typeof map[ i ] != 'undefined' ) {
            to = map[ i ];
        }

        target[ to ] = mixin.prototype[ from ];
    }
};

Evernote.forceInit = function() {
    this._syncManager = new Evernote.SyncManager();
    this._clipProcessor = new Evernote.ClipProcessor();
    this._simSearchStorage = new Evernote.SimSearchStorage();
    this._pageInjector = new Evernote.PageInjector();
};

Evernote.getClipperName = function() {
    return Evernote.CLIPPER_NAME;
};

Evernote.getSecureServiceUrl = function() {
    return this.SECURE_PROTO + ((Evernote.simulateChinese && Evernote.context.useChina) ? "app.yinxiang.com" : "www.evernote.com");
};

Evernote.getSetAuthTokenUrl = function() {
    var authToken = this.context.authenticationToken;
    return this.getSecureServiceUrl() + "/SetAuthToken.action?auth=" + encodeURIComponent( (authToken) ? authToken : "" );
};

Evernote.getAuthenticatedUrlFor = function( targetUrl ) {
    return this.getSetAuthTokenUrl() + "&targetUrl=" + encodeURIComponent( targetUrl );
};

Evernote.getNoteUrl = function( note, searchWords ) {
    var url = null;
    if ( note instanceof Evernote.Note ) {
        url = note.getNoteUrl( this.getSecureServiceUrl(), this.context.shardId, searchWords );
    }

    return url;
};

Evernote.getStartPageUrl = function() {
    return this.getSecureServiceUrl() + Evernote.WebPaths.START_PAGE_PATH;
};

Evernote.getLoginUrl = function() {
    return this.getSecureServiceUrl() + Evernote.WebPaths.LOGIN_PATH;
};

Evernote.getLogoutUrl = function() {
    return this.getSecureServiceUrl() + Evernote.WebPaths.LOGOUT_PATH;
};

Evernote.getRegistrationUrl = function( code ) {
    return this.getSecureServiceUrl() + Evernote.WebPaths.REGISTRATION_PATH + "?code=" + this.REGISTRATION_CODE;
};

Evernote.getAccountSettingsUrl = function() {
    return this.getSecureServiceUrl() + Evernote.WebPaths.ACCOUNT_SETTINGS_PATH;
};

Evernote.getHomeUrl = function() {
    return this.getSecureServiceUrl() + Evernote.WebPaths.HOME_PATH;
};

Evernote.getClipUrl = function() {
    return this.getSecureServiceUrl() + Evernote.WebPaths.CLIP_PATH;
};

Evernote.getSyncStateUrl = function() {
    return this.getSecureServiceUrl() + Evernote.WebPaths.SYNC_STATE_PATH;
};

Evernote.getFileNoteUrl = function() {
    return this.getSecureServiceUrl() + Evernote.WebPaths.FILE_NOTE_PATH;
};

Evernote.getDeleteNoteUrl = function() {
    return this.getSecureServiceUrl() + Evernote.WebPaths.DELETE_NOTE_PATH;
};

Evernote.getFindNotesUrl = function() {
    return this.getSecureServiceUrl() + Evernote.WebPaths.FIND_NOTES_PATH;
};

Evernote.getNewNoteUrl = function() {
    return this.getSecureServiceUrl() + Evernote.WebPaths.NEW_NOTE_PATH + this.getLocale();
};

Evernote.getShardedUrl = function() {
    var shardId = this.context.getShardId();
    if ( shardId ) {
        return this.getSecureServiceUrl() + Evernote.WebPaths.SHARD_PATH + "/" + shardId;
    }
    else {
        return null;
    }
};

Evernote.getNoteViewUrl = function( noteGuid ) {
    var shardId = this.context.getShardId();
    if ( shardId ) {
        return this.getSecureServiceUrl() + Evernote.WebPaths.NOTE_VIEW_PATH + noteGuid + "?shard=" + shardId + "#v=t";
    }
    else {
        return null;
    }
};

Evernote.getSharedNoteViewUrl = function( noteGuid, shareKey, shardId ) {
    return this.getSecureServiceUrl() + "/shard/" + shardId + "/share/" + shareKey + "/#n=" + noteGuid;
};

Evernote.getNoteEditUrl = function( noteGuid ) {
    var shardId = this.context.getShardId();
    if ( shardId ) {
        return this.getSecureServiceUrl() + Evernote.WebPaths.NOTE_EDIT_PATH + noteGuid + "?shard=" + shardId + "#v=t";
    }
    else {
        return null;
    }
};

Evernote.getFindMetaNotesUrl = function() {
    return this.getSecureServiceUrl() + Evernote.WebPaths.FIND_META_NOTES_PATH;
};

Evernote.getCountNotesUrl = function() {
    return this.getSecureServiceUrl() + Evernote.WebPaths.COUNT_NOTES_PATH;
};

Evernote.getEvernoteSearchUrl = function( words, isAny ) {
    var q = (typeof words == 'string') ? words.replace( /\s+/g, " " ) : "";
    return this.getSecureServiceUrl() + Evernote.WebPaths.EVERNOTE_SEARCH_PATH + "#v=t&b=0&o=" + ((isAny) ? "n" : "l") + "&x=" + encodeURIComponent( q );
};

Evernote.getAccountUrl = function() {
    return this.getSecureServiceUrl() + Evernote.WebPaths.ACCOUNT_PATH;
};

Evernote.getNoteSnippetsUrl = function() {
    return this.getSecureServiceUrl() + Evernote.WebPaths.NOTE_SNIPPETS_PATH;
};

Evernote.getSetAuthTokenUrlForSearch = function( authToken, url ) {
    var authUrl = this.getSecureServiceUrl() + Evernote.WebPaths.SET_AUTH_TOKEN_PATH;
    authUrl += "?auth=" + encodeURIComponent( authToken ) + "&targetUrl=" + encodeURIComponent( url );
    return authUrl;
};

Evernote.getCheckVersionUrl = function() {
    return this.getSecureServiceUrl() + Evernote.WebPaths.CHECK_VERSION_PATH;
};

Evernote.getContext = function() {
    if ( !this._context ) {
        this._context = new Evernote.EvernoteContext();
    }

    return this._context;
};

Evernote.getRemote = function() {
    if ( !this._remote ) {
        this._remote = new Evernote.EvernoteRemote();
    }

    return this._remote;
};

Evernote.getLogger = function() {
    if ( !this._logger ) {
        this._logger = new Evernote.Logger();
    }

    return this._logger;
};

Evernote.getjQuery = function() {
    // for xul
    if ( this._jQuery ) {
        return this._jQuery;
    }

    // used only in html not in xul
    this._jQuery = jQuery;
    return this._jQuery;
};

Evernote.setjQuery = function( jQ ) {
    this._jQuery = jQ;
};

Evernote.getSyncManager = function() {
    if ( !this._syncManager ) {
        this._syncManager = new Evernote.SyncManager();
    }

    return this._syncManager;
};

Evernote.getCookieManager = function() {
    if ( !this._cookieManager ) {
        this._cookieManager = new Evernote.CookieManager();
    }

    return this._cookieManager;
};

Evernote.getCloseListener = function() {
    if ( !this._closeListener ) {
        this._closeListener = new Evernote.CloseListener();
    }

    return this._closeListener;
};

Evernote.getUninstaller = function() {
    if ( !this._uninstaller ) {
        Components.utils.import( "resource://gre/modules/AddonManager.jsm" );
        this._uninstaller = new Evernote.AddonListener();
    }

    return this._uninstaller;
};

Evernote.getPrefsManager = function() {
    if ( !this._prefsManager ) {
        this._prefsManager = new Evernote.PrefsManager();
        this._prefsManager.deleteUnusedPrefs();
    }

    return this._prefsManager;
};

Evernote.setTabManager = function( mgr ) {
    this._tabManager = mgr;
};

Evernote.getTabManager = function() {
    if ( !this._tabManager ) {
        this._tabManager = new Evernote.TabManager();
    }

    return this._tabManager;
};

Evernote.getSimSearchStorage = function() {
    if ( !this._simSearchStorage ) {
        this._simSearchStorage = new Evernote.SimSearchStorage();
    }

    return this._simSearchStorage;
};

Evernote.getClipProcessor = function() {
    if ( !this._clipProcessor ) {
        this._clipProcessor = new Evernote.ClipProcessor();
    }

    return this._clipProcessor;
};

Evernote.getFileManager = function() {
    if ( !this._fileManager ) {
        this._fileManager = new Evernote.FileManager();
    }

    return this._fileManager;
};

Evernote.setLocalizer = function( localizer ) {
    this._localizer = localizer;
};

Evernote.getLocalizer = function() {
    if ( !this._localizer ) {
        this._localizer = new Evernote.Localizer();
    }

    return this._localizer;
};

Evernote.getToolbarManager = function() {
    if ( !this._toolbarManager ) {
        this._toolbarManager = new Evernote.ToolbarManager();
    }

    return this._toolbarManager;
};

Evernote.getClipNotificator = function() {
    if ( !this._clipNotificator ) {
        this._clipNotificator = new Evernote.ClipNotificator();
    }

    return this._clipNotificator;
};

Evernote.getNativeController = function() {
    if ( !this._nativeController ) {
        this._nativeController = new Evernote.NativeController();
    }

    return this._nativeController;
};

Evernote.getViewManager = function() {
    if ( !this._viewManager ) {
        this._viewManager = new Evernote.ViewManager();
    }

    return this._viewManager;
};

Evernote.getLinkedNotebooksSource = function() {
    if ( !this._linkedNotebooksSource ) {
        this._linkedNotebooksSource = new Evernote.Chrome.LinkedNotebooksSource();
    }

    return this._linkedNotebooksSource;
};

Evernote.getNativeAcessor = function() {
    if ( !this._nativeAccessor ) {
        if ( Evernote.Platform.isWin() ) {
            this._nativeAccessor = new Evernote.WinNativeAccessor();
        }
        else if ( Evernote.Platform.isMacOS() ) {
            this._nativeAccessor = new Evernote.MacNativeAccessor();
        }
    }

    return this._nativeAccessor;
};

Evernote.getPageInjector = function() {
    if ( !this._pageInjector ) {
        this._pageInjector = new Evernote.PageInjector();
    }

    return this._pageInjector;
};

Evernote.getLocale = function() {
    //mike.c.yang@gmail.com: Force to using china local to invoke app.yinxiang.com bootstrap
    //return navigator.language;
    return "zh_CN";
};

Evernote.cleanUp = function() {
    if ( this._cookieManager ) {
        this._cookieManager.cleanUp();
        this._cookieManager = null;
    }

    if ( this._pageInjector ) {
        this._pageInjector.cleanUp();
        this._pageInjector = null;
    }

    if ( this._tabManager ) {
        this._tabManager.cleanUp();
        this._tabManager = null;
    }

    if ( this._prefsManager ) {
        this._prefsManager.cleanUp();
        this._prefsManager = null;
    }

    if ( this._uninstaller ) {
        this._uninstaller.cleanUp();
        this._uninstaller = null;
    }

    if ( this._closeListener ) {
        this._closeListener.cleanUp();
        this._closeListener = null;
    }
};

Evernote.__defineGetter__( "context", Evernote.getContext );
Evernote.__defineGetter__( "remote", Evernote.getRemote );
Evernote.__defineGetter__( "logger", Evernote.getLogger );
Evernote.__defineSetter__( "jQuery", Evernote.setjQuery );
Evernote.__defineGetter__( "jQuery", Evernote.getjQuery );
Evernote.__defineGetter__( "syncManager", Evernote.getSyncManager );
Evernote.__defineGetter__( "cookieManager", Evernote.getCookieManager );
Evernote.__defineGetter__( "closeListener", Evernote.getCloseListener );
Evernote.__defineGetter__( "uninstaller", Evernote.getUninstaller );
Evernote.__defineGetter__( "prefsManager", Evernote.getPrefsManager );
Evernote.__defineSetter__( "tabManager", Evernote.setTabManager );
Evernote.__defineGetter__( "tabManager", Evernote.getTabManager );
Evernote.__defineGetter__( "simSearchStorage", Evernote.getSimSearchStorage );
Evernote.__defineGetter__( "clipProcessor", Evernote.getClipProcessor );
Evernote.__defineGetter__( "fileManager", Evernote.getFileManager );
Evernote.__defineSetter__( "localizer", Evernote.setLocalizer );
Evernote.__defineGetter__( "localizer", Evernote.getLocalizer );
Evernote.__defineGetter__( "toolbarManager", Evernote.getToolbarManager );
Evernote.__defineGetter__( "clipNotificator", Evernote.getClipNotificator );
Evernote.__defineGetter__( "nativeController", Evernote.getNativeController );
Evernote.__defineGetter__( "viewManager", Evernote.getViewManager );
Evernote.__defineGetter__( "linkedNotebooksSource", Evernote.getLinkedNotebooksSource );
Evernote.__defineGetter__( "nativeAccessor", Evernote.getNativeAcessor );
Evernote.__defineGetter__( "pageInjector", Evernote.getPageInjector );




