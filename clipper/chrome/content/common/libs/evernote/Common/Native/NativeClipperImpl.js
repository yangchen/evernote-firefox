//"use strict";

Evernote.NativeClipperImpl = function NativeClipperImpl() {
};

Evernote.NativeClipperImpl.prototype._instance = null;

Evernote.NativeClipperImpl.handleInheritance = function( child/*, parent*/ ) {
    Evernote.NativeClipperImplFactory.ClassRegistry.push( child );
};

Evernote.NativeClipperImpl.isResponsibleFor = function( /*navigator*/ ) {
    return false;
};

Evernote.NativeClipperImpl.prototype.createNewNote = function() {
};

Evernote.NativeClipperImpl.prototype.clip = function( /*tab, domElement, clipType, isClipGo*/ ) {
};

Evernote.NativeClipperImpl.prototype.isEnabled = function() {
    return false;
};
