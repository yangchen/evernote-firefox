//"use strict";

Evernote.NativeClipper = function NativeClipper() {
    this.initialize();
};

Evernote.NativeClipper.prototype._impl = null;

Evernote.NativeClipper.prototype.initialize = function() {
    Evernote.logger.debug( "NativeClipper.initialize()" );

    var nativeClipperImpl = Evernote.NativeClipperImplFactory.getImplementationFor( navigator );
    if ( typeof nativeClipperImpl == "function" ) {
        var impl = new nativeClipperImpl();
        if ( impl.isEnabled() ) {
            this._impl = impl;
        }
    }
};

Evernote.NativeClipper.prototype.createNewNote = function() {
    if ( this._impl ) {
        this._impl.createNewNote();
    }
};

Evernote.NativeClipper.prototype.clip = function( tab, domElement, clipType, isClipGo ) {
    if ( this._impl ) {
        this._impl.clip( tab, domElement, clipType, isClipGo );
    }
};

Evernote.NativeClipper.prototype.isEnabled = function() {
    return (this._impl) ? this._impl.isEnabled() : false;
};