//"use strict";

Evernote.SimpleViewMessage = function SimpleViewMessage( containerSelector ) {
    this.__defineGetter__( "container", this.getContainer );
    this.__defineGetter__( "message", this.getMessage );
    this.__defineSetter__( "message", this.setMessage );
    this.initialize( containerSelector );
};

Evernote.SimpleViewMessage.MESSAGE_CLASS = "SimpleViewMessage";

Evernote.SimpleViewMessage.prototype._container = null;
Evernote.SimpleViewMessage.prototype._message = null;

Evernote.SimpleViewMessage.prototype.initialize = function( containerSelector ) {
    Evernote.logger.debug( "SimpleViewMessage.initialize()" );

    if ( containerSelector ) {
        this._container = (containerSelector instanceof Evernote.jQuery) ? containerSelector : Evernote.jQuery( containerSelector );
    }
};

Evernote.SimpleViewMessage.prototype.getContainer = function() {
    return this._container;
};

Evernote.SimpleViewMessage.prototype.setMessage = function( message ) {
    this._message = message;
};

Evernote.SimpleViewMessage.prototype.getMessage = function() {
    return this._message;
};

Evernote.SimpleViewMessage.prototype.show = function() {
    Evernote.logger.debug( "SimpleViewMessage.show()" );

    if ( this.message ) {
        var msgBlock = this.createMessageBlock();
        msgBlock.append( this.message );

        this._container.empty();
        this._container.append( msgBlock );
    }
    else {
        this._container.empty();
    }
    
    this._container.show();
};

Evernote.SimpleViewMessage.prototype.hide = function() {
    this._container.hide();
};

Evernote.SimpleViewMessage.prototype.createMessageBlock = function() {
    return Evernote.jQuery( "<div>", { className : this.getMessageClass() } );
};

Evernote.SimpleViewMessage.prototype.getMessageClass = function() {
    return this.constructor.MESSAGE_CLASS;
};
