//"use strict";

Evernote.OptionsDlg = function OptionsDlg() {
    this.initialize();
};

Evernote.OptionsDlg.SUBMIT_MESSAGE_TIMEOUT = 1000;

Evernote.OptionsDlg.prototype._optionsForm = null;
Evernote.OptionsDlg.prototype._optionsValidator = null;

Evernote.OptionsDlg.prototype.initialize = function() {
    Evernote.logger.debug( "OptionsDlg.initialize()" );

    this.bindView();
    this.bindForm();
    Evernote.localizer.localizeBlock( Evernote.jQuery( "body" ) );

    var self = this;
    Evernote.syncManager.remoteSyncState(
        function() {
            Evernote.logger.debug( "Successfully got SyncState for OptionsDlg" );
            self.populateForm();
        },
        function() {
            Evernote.logger.error( "Failed to get SyncState for OptionsDlg - work with existing user data" );
            self.populateForm();
        }
    );
};

Evernote.OptionsDlg.prototype.bindView = function() {
    Evernote.logger.debug( "OptionsDlg.bindView()" );

    if ( Evernote.Platform.isLinux() ) {
        Evernote.jQuery( "#desktop-options" ).hide();
    }

    Evernote.jQuery( "#not_installed" ).hide();
    var isDesktopEnabled = new Evernote.NativeClipper().isEnabled();
    if ( !isDesktopEnabled ) {
        Evernote.jQuery( "#useNative1" ).attr( "disabled", true );
    }

    if ( Evernote.Utils.isDesktopClipperSelected() ) {
        this.selectDesktopClip();
    }
    else {
        this.selectWebClip();
    }

    Evernote.jQuery( "#useSearchHelper" ).attr( "checked", Evernote.context.options.useSearchHelper );
    this.submitChoice();

    Evernote.jQuery( "#logo_header" ).attr( "src", (Evernote.simulateChinese && Evernote.context.useChina) ? "images/popup_logo_china.png"
                                                                                                           : "images/popup_logo.png" );
};

Evernote.OptionsDlg.prototype.bindForm = function() {
    Evernote.logger.debug( "OptionsDlg.bindForm()" );

    Evernote.jQuery.validator.addMethod( "noteList", Evernote.Options.isValidNoteListOptionValue, Evernote.localizer.getMessage( "invalidExpression" ) );

    var self = this;
    var form = Evernote.jQuery( "form[name=optionsForm]" );
    var options = {
        submitHandler: function( /*form*/ ) {
            self.submitForm();
        },

        errorClass: Evernote.ViewManager.FORM_FIELD_ERROR_MESSAGE_CLASS,
        errorElement: "div",
        onkeyup: false,
        onfocusout: false,
        onsubmit: true,

        rules: {
            noteList: {
                noteList: true
            }
        },

        messages: {
            noteList: {
                noteList: Evernote.localizer.getMessage( "invalidNoteListOptionValue" )
            }
        }
    };

    Evernote.logger.debug( "Setting up validation on login form" );
    this._optionsValidator = form.validate( options );

    form = form.observeform( { onSubmit: function() {
                                  return self._optionsValidator && self._optionsValidator.numberOfInvalids() == 0;
                              }
    } );

    this._optionsForm = Evernote.ModelForm.onForm( form );
    this.bindControls();
};

Evernote.OptionsDlg.prototype.bindControls = function() {
    Evernote.logger.debug( "OptionsDlg.bindControls()" );

    var self = this;
    Evernote.jQuery( "#useNative0" ).click( function( /*event*/ ) {
        self.selectWebClip();
        return true;
    } );

    Evernote.jQuery( "#useNative1" ).click( function( /*event*/ ) {
        self.selectDesktopClip();
        return true;
    } );

    Evernote.jQuery( "#evernoteLink" ).click( function( /*event*/ ) {
        new Evernote.WindowOpener().openUrl( Evernote.getSecureServiceUrl() + "/getit", "", "location=yes,menubar=yes,resizable=yes,scrollbars=yes" );
    } );

    Evernote.jQuery( ":button[value=reset]" ).click( function( /*event*/ ) {
        self.resetForm();
        return false;
    } );

    Evernote.jQuery( ":button[value=close]" ).click( function( /*event*/ ) {
        self.dismiss();
    } );
};

Evernote.OptionsDlg.prototype.populateForm = function () {
    Evernote.logger.debug( "OptionsDlg.populateForm()" );

    var context = Evernote.context;
    var options = context.options;

    this._optionsForm.populateWith( options );
    if ( options.noteSortOrder ) {
        this._optionsForm.noteSortOrder = JSON.stringify( options.noteSortOrder );
    }

    if ( context.isLogined() && context.notebooks instanceof Array && context.notebooks.length > 0 ) {
        var notebooks = context.notebooks.sort( function( a, b ) {
            return a.name.toLowerCase() > b.name.toLowerCase();
        } );

        var notebookGuids = Evernote.jQuery( "#clipNotebookGuid" );
        notebookGuids.empty();

        for ( var i = 0; i < notebooks.length; ++i ) {
            var notebook = notebooks[ i ];
            var notebookName = Evernote.jQuery( "<div/>" ).text( notebook.name ).html();
            notebookGuids.append( Evernote.jQuery( "<option>", { value : notebook.guid, text : notebookName } ) );
        }

        var self = this;
        var notebookSelected = false;

        var selectNotebookFn = function() {
            if ( !notebookSelected ) {
                var preferredNotebook = context.getPreferredNotebook();
                if ( preferredNotebook ) {
                   self._optionsForm.clipNotebookGuid = preferredNotebook.guid;
                }
                Evernote.jQuery( "#clipNotebookSelect" ).show();
                notebookSelected = true;
            }
        };

        Evernote.linkedNotebooksSource.addLinkedNotebooksToSelect( document, document.querySelector( "#clipNotebookGuid" ), function() {
            selectNotebookFn();
        } );

        setTimeout( function() {
            selectNotebookFn();
        }, 2000 );
    }
    else {
        Evernote.jQuery( "#clipNotebookSelect" ).hide();
    }
};

Evernote.OptionsDlg.prototype.showSubmitMessage = function ( msg ) {
    Evernote.logger.debug( "OptionsDlg.showSubmitMessage()" );

    var msgBlock = Evernote.jQuery( "#submitMessage" );

    msgBlock.html( msg );
    msgBlock.removeClass();
    msgBlock.addClass( "success" );
    msgBlock.show();

    setTimeout( function() {
        Evernote.jQuery( "#submitMessage" ).fadeOut( 300 );
    }, this.constructor.SUBMIT_MESSAGE_TIMEOUT );
};

Evernote.OptionsDlg.prototype.showSubmitError = function ( msg ) {
    Evernote.logger.debug( "OptionsDlg.showSubmitError()" );

    var msgBlock = Evernote.jQuery( "#submitMessage" );

    msgBlock.html( msg );
    msgBlock.removeClass();
    msgBlock.addClass( "error" );
    msgBlock.show();

    setTimeout( function() {
        Evernote.jQuery( "#submitMessage" ).fadeOut( 300 );
    }, this.constructor.SUBMIT_MESSAGE_TIMEOUT );
};

Evernote.OptionsDlg.prototype.submitForm = function () {
    Evernote.logger.debug( "OptionsDlg.submitForm()" );

    this.submitChoice();
    if ( this._optionsValidator.numberOfInvalids() > 0 ) {
        Evernote.logger.debug( "Not submitting options form cuz it has errors" );
        return false;
    }
    
    var obj = this._optionsForm.toStorable();
    obj.noteSortOrder = new Evernote.NoteSortOrder( JSON.parse( obj.noteSortOrder ) );

    Evernote.context.options = new Evernote.Options( obj );
    this.showSubmitMessage( Evernote.localizer.getMessage( "options_formSaved" ) );

    return false;
};

Evernote.OptionsDlg.prototype.resetForm = function () {
    Evernote.logger.debug( "OptionsDlg.resetForm()" );

    this.resetChoice();
    Evernote.context.options = new Evernote.Options( Evernote.Options.DEFAULTS );

    this.populateForm();
    this.showSubmitMessage( Evernote.localizer.getMessage( "options_formReset" ) );

    return false;
};

Evernote.OptionsDlg.prototype.dismiss = function () {
    Evernote.logger.debug( "OptionsDlg.dismiss()" );

    if ( typeof window.overlay != 'undefined' && typeof window.overlay.close == 'function' ) {
        window.overlay.close();
    }
    else {
        window.close();
    }
};

Evernote.OptionsDlg.prototype.selectWebClip = function () {
    Evernote.logger.debug( "OptionsDlg.selectWebClip()" );

    Evernote.jQuery( "#useNative0" ).attr( "checked", true );
    Evernote.jQuery( "#useNative1" ).attr( "checked", false );
    Evernote.jQuery( "#bottom_part" ).show();
    Evernote.jQuery( "#bottom_part_header" ).show();
};

Evernote.OptionsDlg.prototype.selectDesktopClip = function () {
    Evernote.logger.debug( "OptionsDlg.selectDesktopClip()" );

    Evernote.jQuery( "#useNative0" ).attr( "checked", false );
    Evernote.jQuery( "#useNative1" ).attr( "checked", true );
    Evernote.jQuery( "#bottom_part" ).hide();
    Evernote.jQuery( "#bottom_part_header" ).hide();
};

Evernote.OptionsDlg.prototype.submitChoice = function () {
    Evernote.logger.debug( "OptionsDlg.submitChoice()" );
    Evernote.prefsManager.addPref( Evernote.PrefKeys.IS_DESKTOP_SELECTED, Evernote.jQuery( "#useNative1" ).attr( "checked" ) );
};

Evernote.OptionsDlg.prototype.resetChoice = function () {
    Evernote.logger.debug( "OptionsDlg.resetChoice()" );

    var isDesktopSelected = Evernote.prefsManager.getPref( Evernote.PrefKeys.IS_DESKTOP_SELECTED, "boolean" );
    if ( isDesktopSelected ) {
        this.selectDesktopClip();
    }
    else {
        this.selectWebClip();
    }
};
