//"use strict";

Evernote.StackableViewMessage = function( containerSelector ) {
    this.initialize( containerSelector );
};

Evernote.inherit( Evernote.StackableViewMessage, Evernote.SimpleViewMessage, true );

Evernote.StackableViewMessage.MESSAGE_CLASS = "StackableViewMessage";

Evernote.StackableViewMessage.prototype._messageStack = null;

Evernote.StackableViewMessage.prototype.initialize = function( containerSelector ) {
    this.parent.initialize.apply( this, arguments );
    this._messageStack = [ ];
};

Evernote.StackableViewMessage.prototype.getMessage = function() {
    if ( this._messageStack.length > 0 ) {
        return this._messageStack[ this._messageStack.length - 1 ];
    }
    else {
        return null;
    }
};

Evernote.StackableViewMessage.prototype.setMessage = function( message ) {
    this._messageStack = [ ];
    this.addMessage( message );
};

Evernote.StackableViewMessage.prototype.addMessage = function( msg ) {
    this._messageStack.push( this.describeMessage( msg ) );
};

Evernote.StackableViewMessage.prototype.removeMessage = function( msg ) {
    var msgDescription = this.describeMessage( msg );
    for ( var i = this._messageStack.length - 1; i >= 0; i-- ) {
        if ( this._messageStack[ i ] == msgDescription ) {
            this._messageStack.splice( i, 1 );
            break;
        }
    }
};

Evernote.StackableViewMessage.prototype.removeAllMessages = function() {
    this._messageStack = [ ];
};

Evernote.StackableViewMessage.prototype.length = function() {
    return this._messageStack.length;
};

Evernote.StackableViewMessage.prototype.describeMessage = function( msg ) {
    Evernote.logger.debug( "StackableViewMessage.describeMessage()" );

    var str = "";
    if ( typeof msg == 'string' ) {
        str = msg;
    }
    else if ( msg instanceof Evernote.jQuery ) {
        msg.each( function( i, e ) {
            str += this.describeMessage( e );
        } );
    }
    else if ( msg instanceof Text ) {
        str += msg;
    }
    else if ( msg instanceof HTMLElement ) {
        str += msg.innerHTML;
    }

    return str;
};

Evernote.StackableViewMessage.prototype.getMessageClass = function() {
    return this.parent.getMessageClass() + " " + this.constructor.MESSAGE_CLASS;
};
