//"use strict";

Evernote.ViewManager = function ViewManager() {
    this.__defineGetter__( "containerResizeCallback", this.getContainerResizeCallback );
    this.__defineSetter__( "containerResizeCallback", this.setContainerResizeCallback );
    this.__defineGetter__( "globalMessage", this.getGlobalMessage );
    this.__defineSetter__( "globalMessage", this.setGlobalMessage );
    this.__defineGetter__( "globalErrorMessage", this.getGlobalErrorMessage );
    this.__defineSetter__( "globalErrorMessage", this.setGlobalErrorMessage );
    this.__defineGetter__( "quiet", this.getQuiet );
};

Evernote.ViewManager.FORM_FIELD_ERROR_CLASS = "error";
Evernote.ViewManager.FORM_FIELD_ERROR_MESSAGE_CLASS = "error";
Evernote.ViewManager.FORM_FIELD_ERROR_MESSAGE_ELEMENT = "div";
Evernote.ViewManager.CONTAINER_MIN_SIZE= 38;

Evernote.ViewManager.prototype._containerResizeCallback = null;
Evernote.ViewManager.prototype._globalMessage = null;
Evernote.ViewManager.prototype._globalErrorMessage = null;
Evernote.ViewManager.prototype._quiet = false;
Evernote.ViewManager.prototype._animateContainerResizing = true;
Evernote.ViewManager.prototype._currentView = null;

Evernote.ViewManager.prototype.getEffectiveHeight = function() {
    Evernote.logger.debug( "ViewManager.getEffectiveHeight()" );

    var h = 0;
    Evernote.jQuery( "body > div" ).each( function( i, element ) {
        var e = Evernote.jQuery( element );
        if ( e.css( "display" ) != "none" && !e.hasClass( "banner" ) && !e.hasClass( "drawer" ) && !e.hasClass( "drawerHandleTitle" ) ) {
            h += e.innerHeight();
        }
    } );

    var waitBlock = Evernote.jQuery( "#spinner" );
    if ( waitBlock && waitBlock.css( "display" ) != "none" ) {
        h += waitBlock.height();
    }

    var globalErrorMessage = this.globalErrorMessage;
    if ( globalErrorMessage && globalErrorMessage.container.css( "display" ) != "none" ) {
        h += (globalErrorMessage.container.height() + 18);
    }
    
    if ( h < this.constructor.CONTAINER_MIN_SIZE ) {
        h = this.constructor.CONTAINER_MIN_SIZE;
    }

    Evernote.logger.debug( "Effective height is " + h );
    return h;
};

Evernote.ViewManager.prototype.getWindowHeight = function() {
    Evernote.logger.debug( "ViewManager.getWindowHeight()" );

    var h = 0;
    Evernote.jQuery( "body > div" ).each( function( i, element ) {
        var e = Evernote.jQuery( element );
        if ( e.css( "display" ) != "none" && !e.hasClass( "banner" ) && !e.hasClass( "drawer" ) && !e.hasClass( "drawerHandleTitle" ) ) {
            if ( e.css( "float" ) != "none" ) {
                h = Math.max( h, e.innerHeight() );
            }
            else {
                h += e.innerHeight();
            }
        }
    } );

    var waitBlock = Evernote.jQuery( "#spinner" );
    if ( waitBlock && waitBlock.css( "display" ) != "none" ) {
        h += waitBlock.height();
    }

    var globalErrorMessage = this.globalErrorMessage;
    if ( globalErrorMessage && globalErrorMessage.container.css( "display" ) != "none" ) {
        h += (globalErrorMessage.container.height() + 18);
    }

    if ( h < this.constructor.CONTAINER_MIN_SIZE ) {
        h = this.constructor.CONTAINER_MIN_SIZE;
    }

    Evernote.logger.debug( "Window height is " + h );
    return h;
};

Evernote.ViewManager.prototype.updateBodyHeight = function( height ) {
    Evernote.logger.debug( "ViewManager.updateBodyHeight():  height = " + height );

    var h = (typeof height == 'number') ? height : this.getEffectiveHeight();
    var body = Evernote.jQuery( "body" );

    if ( this._animateContainerResizing ) {
        body.animate( { height : h + "px" }, 10 );
    }
    else {
        body.css( { height : h + "px" } );
    }

    if ( typeof this._containerResizeCallback == 'function' ) {
        this._containerResizeCallback( h );
    }
};

Evernote.ViewManager.prototype.showBlock = function( block, dataArray ) {
    block.show();
    if ( dataArray instanceof Array ) {
        block.trigger( "show", dataArray );
    }
    else {
        block.trigger( "show" );
    }

    this.updateBodyHeight();
};

Evernote.ViewManager.prototype.hideBlock = function( block, dataArray ) {
    block.hide();
    if ( dataArray instanceof Array ) {
        block.trigger( "hide", dataArray );
    }
    else {
        block.trigger( "hide" );
    }

    this.updateBodyHeight();
};

Evernote.ViewManager.prototype.showView = function( viewNameOrBlock, data ) {
    if ( viewNameOrBlock instanceof Evernote.jQuery ) {
        view = viewNameOrBlock;
    }
    else {
        Evernote.logger.debug( "ViewManager.showView() on " + viewNameOrBlock );
        var view = Evernote.jQuery( "#" + viewNameOrBlock );
    }

    if ( view.length == 0 ) {
        return null;
    }

    this.showBlock( view, [ data ] );
    return view;
};

Evernote.ViewManager.prototype.hideView = function( viewNameOrBlock ) {
    if ( viewNameOrBlock instanceof Evernote.jQuery ) {
        view = viewNameOrBlock;
    }
    else {
        Evernote.logger.debug( "ViewManager.hideView() on " + viewNameOrBlock );
        var view = Evernote.jQuery( "#" + viewNameOrBlock );
    }

    if ( view.length == 0 || view.css( "display" ) == "none" ) {
        return null;
    }

    this.hideBlock( view );
    if ( this._currentView && view.attr( "id" ) == this._currentView.attr( "id" ) ) {
        this._currentView = null;
    }

    return view;
};

Evernote.ViewManager.prototype.switchElements = function( a, b ) {
    if ( a instanceof Evernote.jQuery ) {
        a.hide();
    }

    if ( b instanceof Evernote.jQuery ) {
        b.show();
    }
};

Evernote.ViewManager.prototype.switchView = function( viewName, data ) {
    Evernote.logger.debug( "ViewManager.switchView(): viewName = " + viewName );

    var visibleView = null;
    var view = (viewName instanceof Evernote.jQuery) ? viewName : Evernote.jQuery( "#" + viewName );

    if ( this._currentView && this._currentView.attr( "id" ) == view.attr( "id" ) ) {
        Evernote.logger.debug( "Already showing..." );
        return;
    }

    if ( this._currentView ) {
        this.hideView( this._currentView.attr( "id" ) );
    }

    if ( visibleView = this.showView( viewName, data ) ) {
        this._currentView = visibleView;
    }

    return visibleView;
};

Evernote.ViewManager.prototype.wait = function( msg ) {
    Evernote.jQuery( "#spinner" ).find( "#spinnerMessage" ).html( msg ).end().show();
    this.updateBodyHeight();
};

Evernote.ViewManager.prototype.clearWait = function() {
    Evernote.jQuery( "#spinner" ).hide();
    this.updateBodyHeight();
};

Evernote.ViewManager.prototype.showMessage = function( message ) {
    Evernote.logger.debug( "ViewManager.showMessage(): message = " + message );

    var globalMessage = this.globalMessage;
    if ( globalMessage ) {
        globalMessage.addMessage( message );
        globalMessage.show();
    }
};

Evernote.ViewManager.prototype.hideMessage = function( message ) {
    Evernote.logger.debug( "ViewManager.hideMessage(): message = " + message );

    var globalMessage = this.globalMessage;
    if ( globalMessage ) {
        globalMessage.removeMessage( message );
        if ( globalMessage.length() > 0 ) {
            globalMessage.show();
        }
        else {
            globalMessage.hide();
        }
    }
};

Evernote.ViewManager.prototype.hideAllMessages = function() {
    Evernote.logger.debug( "ViewManager.hideAllMessages()" );

    var globalMessage = this.globalMessage;
    if ( globalMessage ) {
        globalMessage.removeAllMessages();
        globalMessage.hide();
    }
};

Evernote.ViewManager.prototype.extractErrorMessage = function( error, defaultMessage ) {
    Evernote.logger.debug( "ViewManager.extractErrorMessage()" );

    var msg = (typeof defaultMessage != 'undefined') ? defaultMessage : null;
    Evernote.logger.debug( "Error: " + error.toString() );

    if ( error instanceof Evernote.EvernoteError ) {
        Evernote.logger.debug( "Error is EvernoteError" );
    }

    if ( error instanceof Evernote.EvernoteError && typeof error.errorCode == 'number' && typeof error.parameter == 'string'
        && Evernote.localizer.getMessage( "EDAMError_" + error.errorCode + "_" + error.parameter.replace( /[^a-zA-Z0-9_]+/g, "_" ) ) ) {
        msg = Evernote.localizer.getMessage( "EDAMError_" + error.errorCode + "_" + error.parameter.replace( /[^a-zA-Z0-9_]+/g, "_" ) );
    }
    else if ( error instanceof Evernote.EDAMResponseException && typeof error.errorCode == 'number' && Evernote.localizer.getMessage( "EDAMResponseError_" + error.errorCode ) ) {
        Evernote.logger.debug( "Got localized message for EDAMResponseException" );
        if ( typeof error.parameter == 'string' ) {
            msg = Evernote.localizer.getMessage( "EDAMResponseError_" + error.errorCode, error.parameter );
        }
        else {
            msg = Evernote.localizer.getMessage( "EDAMResponseError_" + error.errorCode );
        }
    }
    else if ( error instanceof Evernote.EvernoteError && typeof error.errorCode == 'number' && Evernote.localizer.getMessage( "EDAMError_" + error.errorCode ) ) {
        Evernote.logger.debug( "Got localized message for EvernoteError" );
        if ( typeof error.parameter == 'string' ) {
            msg = Evernote.localizer.getMessage( "EDAMError_" + error.errorCode, error.parameter );
        }
        else {
            msg = Evernote.localizer.getMessage( "EDAMError_" + error.errorCode );
        }
    }
    else if ( error instanceof Evernote.EvernoteError && typeof error.message == 'string' ) {
        msg = error.message;
    }
    else if ( error instanceof Error && typeof error.message == 'string' ) {
        msg = error.message
    }
    else if ( typeof error == 'string' ) {
        msg = error;
    }

    Evernote.logger.debug( "Error message is " + msg );
    return msg;
};

Evernote.ViewManager.prototype.showError = function( error ) {
    Evernote.logger.debug( "ViewManager.showError()" );

    var msg = this.extractErrorMessage( error, Evernote.localizer.getMessage( "UnknownError" ) );
    if ( msg && this._globalErrorMessage ) {
        this._globalErrorMessage.message = msg;
        this._globalErrorMessage.show();
        this.updateBodyHeight();
    }
};

Evernote.ViewManager.prototype.showErrors = function( errors ) {
    Evernote.logger.debug( "ViewManager.showErrors()" );

    if ( this._quiet ) {
        return;
    }

    var errs = (errors instanceof Array) ? errors : [ errors ];
    if ( errs.length == 1 ) {
        this.showError( errs[ 0 ] );
        return;
    }

    var errorTitle = Evernote.localizer.getMessage( "multipleErrorsTitle" );
    var messageList = Evernote.jQuery( "<ul>" );

    for ( var i = 0; i < errs.length; i++ ) {
        var msg = this.extractErrorMessage( errors[ i ] );
        if ( msg ) {
            messageList.append( Evernote.jQuery( "<li>", { text : msg } ) );
        }
    }

    if ( messageList.children().length > 0 ) {
        var errorBlock = Evernote.jQuery( "<div>", { className : "multiErrorTitle" } );
        errorBlock.append( errorTitle );
        errorBlock.append( messageList );

        if ( this._globalErrorMessage ) {
            this._globalErrorMessage.message = errorBlock;
            this._globalErrorMessage.show();
        }
    }

    this.updateBodyHeight();
};

Evernote.ViewManager.prototype.hideErrors = function() {
    if ( this._globalErrorMessage ) {
        this._globalErrorMessage.hide();
    }

    this.updateBodyHeight();
};

Evernote.ViewManager.prototype.showHttpError = function( xhr, textStatus, error ) {
    Evernote.logger.debug( "ViewManager.showHttpError()" );
    this.showError( new Error( this.getLocalizedHttpErrorMessage( xhr, textStatus, error ) ) );
};

Evernote.ViewManager.prototype.showFormErrors = function( form, errors, callback ) {
    Evernote.logger.debug( "ViewManager.showFormErrors(" + (typeof form) + ", " + (typeof errors) + ")" );

    if ( this._quiet ) {
        return;
    }

    var f = (form instanceof Evernote.jQuery) ? form : Evernote.jQuery( form );
    for ( var i = 0; i < errors.length; i++ ) {
        var error = errors[ i ];
        var msg = this.extractErrorMessage( error );

        Evernote.logger.debug( error.toString() + " => " + msg );

        if ( typeof callback == 'function' ) {
            callback( ((typeof error.parameter == 'string') ? error.parameter : null), msg );
        }
        else {
            var field = null;
            if ( typeof error.parameter == 'string' && msg != null ) {
                field = f.find( "[name=" + error.parameter + "]" );
                if ( field.length == 0 ) {
                    field = null;
                }
            }

            if ( field ) {
                this.showFormFieldErrors( field, msg );
            }
            else {
                this.showError( msg );
            }
        }
    }

    this.updateBodyHeight();
};

Evernote.ViewManager.prototype.showFormFieldErrors = function( field, errorMessage ) {
    Evernote.logger.debug( "ViewManager.showFormFieldError(" + field + ", " + errorMessage + ")" );

    if ( this._quiet || typeof field == 'undefined'  ) {
        return;
    }

    if ( !(field instanceof Evernote.jQuery) ) {
        field = Evernote.jQuery( field );
    }

    if ( !field.hasClass( this.constructor.FORM_FIELD_ERROR_CLASS ) ) {
        field.addClass( this.constructor.FORM_FIELD_ERROR_CLASS );
    }

    if ( field.next( "." + this.constructor.FORM_FIELD_ERROR_MESSAGE_CLASS ).length == 0 ) {
        field.after( Evernote.jQuery( "<" + this.constructor.FORM_FIELD_ERROR_MESSAGE_ELEMENT + ">",
                                      { className : this.constructor.FORM_FIELD_ERROR_MESSAGE_CLASS, text : errorMessage } ) );
    }
    else {
        field.next( "." + this.constructor.FORM_FIELD_ERROR_MESSAGE_CLASS ).html( errorMessage );
    }
};

Evernote.ViewManager.prototype.clearFormArtifacts = function( form ) {
    Evernote.logger.debug( "ViewManager.clearFormArtifacts()" );

    if ( typeof form == 'undefined' ) {
        return;
    }
    
    if ( !(form instanceof Evernote.jQuery) ) {
        form = Evernote.jQuery( form );
    }
    
    Evernote.logger.debug( "Removing error messages..." );
    form.find( "." + this.constructor.FORM_FIELD_ERROR_MESSAGE_CLASS ).remove();

    Evernote.logger.debug( "Removing error classes from fields" );
    form.find( "." + this.constructor.FORM_FIELD_ERROR_CLASS ).removeClass( this.constructor.FORM_FIELD_ERROR_CLASS );

    Evernote.logger.debug( "Done..." );
    this.updateBodyHeight();
};

Evernote.ViewManager.prototype.getLocalizedHttpErrorMessage = function( xhr, textStatus, error ) {
    return Evernote.localizer.getMessage( "Error_HTTP_Transport", [ ("" + xhr.status), ((typeof error == 'string') ? error : "") ] );
};

Evernote.ViewManager.prototype.getContainerResizeCallback = function() {
    return this._containerResizeCallback;
};

Evernote.ViewManager.prototype.setContainerResizeCallback = function( fn ) {
    if ( typeof fn == "function" ) {
        this._containerResizeCallback = fn;
    }
};

Evernote.ViewManager.prototype.getGlobalMessage = function() {
    return this._globalMessage;
};

Evernote.ViewManager.prototype.setGlobalMessage = function( msg ) {
    this._globalMessage = msg;
};

Evernote.ViewManager.prototype.getGlobalErrorMessage = function() {
    return this._globalErrorMessage;
};

Evernote.ViewManager.prototype.setGlobalErrorMessage = function( msg ) {
    this._globalErrorMessage = msg;
};

Evernote.ViewManager.prototype.getQuiet = function() {
    return this._quiet;
};