//"use strict";

(function() {
    if ( typeof Evernote != "undefined" && typeof Evernote.ScriptLoader != "undefined" ) {
        Evernote.ScriptLoader.load( [
            "chrome://webclipper3-common/content/libs/evernote/Common/Errors/EDAMConstants.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Errors/Error.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Errors/Exception.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Errors/EDAMResponseException.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Errors/EDAMSystemException.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Errors/EDAMUserException.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Errors/ValidationError.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Errors/SimpleError.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Errors/ScopedLocalizableError.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Errors/EDAMScopedError.js"
        ] );
    }
})();
