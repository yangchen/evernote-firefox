//"use strict";

(function() {
    if ( typeof Evernote != "undefined" && typeof Evernote.ScriptLoader != "undefined" ) {
        Evernote.ScriptLoader.load( [
            "chrome://webclipper3-common/content/libs/evernote/Common/Platform/Platform.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Platform/JQueryLoader.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Platform/PasswordStorage.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Platform/CloseListener.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Platform/PrefsManager.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Platform/TabManager.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Platform/AddonListener.js"
        ] );
    }
})();
