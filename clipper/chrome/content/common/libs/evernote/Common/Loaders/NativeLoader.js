//"use strict";

(function() {
    if ( typeof Evernote != "undefined" && typeof Evernote.ScriptLoader != "undefined" ) {
        Evernote.ScriptLoader.load( [
            "chrome://webclipper3-common/content/libs/evernote/Common/Native/NativeClipper.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Native/NativeClipperImpl.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Native/NativeClipperImplFactory.js"
        ] );
    }
})();

