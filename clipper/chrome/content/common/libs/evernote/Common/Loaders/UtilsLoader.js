//"use strict";

(function() {
    if ( typeof Evernote != "undefined" && typeof Evernote.ScriptLoader != "undefined" ) {
        Evernote.ScriptLoader.load( [
            "chrome://webclipper3-common/content/libs/evernote/Common/Utils/SimpleDateFormat.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Utils/DOMHelpers.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Utils/UrlHelpers.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Utils/Utils.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Utils/CachedOption.js"
        ] );
    }
})();

