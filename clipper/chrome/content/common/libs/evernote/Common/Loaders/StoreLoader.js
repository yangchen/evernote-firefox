//"use strict";

(function() {
    if ( typeof Evernote != "undefined" && typeof Evernote.ScriptLoader != "undefined" ) {
        Evernote.ScriptLoader.load( [
            "chrome://webclipper3-common/content/libs/evernote/Common/Store/StorageImpl.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Store/LocalStore.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Store/SqlLiteStorageImpl.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Store/DataBaseManager.js"
        ] );
    }
})();

