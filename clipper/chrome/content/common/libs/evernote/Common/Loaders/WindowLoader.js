//"use strict";

(function() {
    if ( typeof Evernote != "undefined" && typeof Evernote.ScriptLoader != "undefined" ) {
        Evernote.ScriptLoader.load( [
            "chrome://webclipper3-common/content/libs/evernote/Common/Window/WindowOpener.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Window/WindowOpenerImpl.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Window/WindowOpenerImplFactory.js"
        ] );
    }
})();
