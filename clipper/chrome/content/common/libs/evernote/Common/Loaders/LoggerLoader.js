//"use strict";

(function() {
    if ( typeof Evernote != "undefined" && typeof Evernote.ScriptLoader != "undefined" ) {
        Evernote.ScriptLoader.load( [
            "chrome://webclipper3-common/content/libs/evernote/Common/Logger/LoggerImpl.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Logger/Logger.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Logger/FileLoggerImpl.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Logger/MozillaExtensionLoggerImpl.js"
        ] );
    }
})();
