//"use strict";

(function() {
    if ( typeof Evernote != "undefined" && typeof Evernote.ScriptLoader != "undefined" ) {
        Evernote.ScriptLoader.load( [
            "chrome://webclipper3-common/content/libs/evernote/Common/Cookies/Cookie.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Cookies/CookieManagerImpl.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Cookies/CookieManagerImplFactory.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Cookies/MozillaCookieManagerImpl.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Cookies/CookieManager.js"
        ] );
    }
})();

