//"use strict";

(function() {
    if ( typeof Evernote != "undefined" && typeof Evernote.ScriptLoader != "undefined" ) {
        Evernote.ScriptLoader.load( [
            "chrome://webclipper3-common/content/libs/evernote/Common/Serialize/ClipRules.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Serialize/StylesCollection.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Serialize/ClipStyle.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Serialize/ClipStyleProperty.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Serialize/ClipStylingStrategy.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Serialize/ClipTextStylingStrategy.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Serialize/ClipFullStylingStrategy.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Serialize/Clip.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Serialize/AbstractElementSerializer.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Serialize/ElementSerializerFactory.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Serialize/SelectionFinder.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Serialize/NodeSerializer.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Serialize/SerializedNode.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Serialize/DomParser.js"
        ] );
    }
})();
