//"use strict";

(function() {
    if ( typeof Evernote != "undefined" && typeof Evernote.ScriptLoader != "undefined" ) {
        Evernote.ScriptLoader.load( [
            "chrome://webclipper3-common/content/libs/evernote/Common/Core/Localizer.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Core/BaseListener.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Core/EvernoteContext.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Core/SnippetManager.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Core/Clipper.js"
        ] ); }
})();

