//"use strict";

(function() {
    if ( typeof Evernote != "undefined" && typeof Evernote.ScriptLoader != "undefined" ) {
        Evernote.ScriptLoader.load( [
            "chrome://webclipper3-common/content/libs/evernote/Common/UI/SimpleViewMessage.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/UI/StackableViewMessage.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/UI/OptionsDlg.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/UI/ViewManager.js"
        ] );
    }
})();
