//"use strict";

(function() {
    if ( typeof Evernote != "undefined" && typeof Evernote.ScriptLoader != "undefined" ) {
        Evernote.ScriptLoader.load( [
            "chrome://webclipper3-common/content/libs/evernote/Common/Remote/EDAMResponse.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Remote/EvernoteFormPart.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Remote/EvernoteMultiPartForm.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Remote/EvernoteRemote.js",
            "chrome://webclipper3-common/content/libs/evernote/Common/Remote/SyncManager.js"
        ] );
    }
})();
