//"use strict";

var evernote_uploadNotifier = null;

function evernote_initNotification() {
    Evernote.logger.debug( "evernote_initNotification()" );

    try {
        var params = window.overlay.arguments[ 0 ];
        Evernote.localizer = params.localizer;

        var notifyManager = params.notifyManager;
        Evernote.viewManager.containerResizeCallback = function( h ) {
            var frame = window.overlay.document.getElementById( "popupContent" );
            if ( frame ) {
                frame.style.height = (h + 2) + "px";
            }

            window.sizeToContent();
            window.overlay.sizeToContent();
            notifyManager.onNotificationSizeUpdate();
        };

        evernote_uploadNotifier = new Evernote.UploadNotifier( params );
        evernote_uploadNotifier.notifyWithPayload();
    }
    catch ( e ) {
        Evernote.logger.error( "evernote_initNotification() failed: error = " + e );
    }
}

Evernote.jQuery( document ).ready( function() {
    var initTimeout = 5000;
    var initPeriod = 100;
    var initStartTime = new Date().getTime();

    var initProc = setInterval( function() {
        if ( window.overlay && window.overlayContentContainer ) {
            clearInterval( initProc );
            initProc = null;

            evernote_initNotification();
        }
        else if ( initStartTime + initTimeout < new Date().getTime() ) {
            Evernote.logger.error( "Notification initialization timeout exceeding" );

            clearInterval( initProc );
            initProc = null;
        }
    }, initPeriod );
} );
