//"use strict";

var evernote_simulateVersionConflict = false;

function evernote_doAction( popupSrc, clipType ) {
    new Evernote.Clipper().startClip( popupSrc, clipType );
    return true;
}

function evernote_contextMenuShow( event ) {
    Evernote.ContextMenuController.onContextMenuShow( event );
}

function evernote_start() {
    Evernote.JQueryLoader.load();

    var i18n = document.getElementById( "evernote_i18n" );
    if ( i18n ) {
        Evernote.localizer.stringBundle = i18n.stringBundle;
    }

    // hack to fix FF bug with undeleted session cookie
    if ( !Evernote.context.isLogined() ) {
        Evernote.context.destroy();
    }

    if ( !Evernote.simulateChinese ) {
        Evernote.context.useChina = false;
    }

    //mike.c.yang@gmail.com: Force to using china
    Evernote.simulateChinese = true;
    Evernote.context.useChina = true;

    Evernote.context.clientEnabled = !evernote_simulateVersionConflict;
    Evernote.forceInit();
    
    if ( !Evernote.prefsManager.getPref( Evernote.PrefKeys.IS_START_PAGE_SHOWED, "boolean" ) ) {
        setTimeout( function() {
            Evernote.tabManager.create( { url : Evernote.getStartPageUrl() } );
        }, 0 );
        Evernote.prefsManager.addPref( Evernote.PrefKeys.IS_START_PAGE_SHOWED, true );
    }

    Evernote.jQuery( "#contentAreaContextMenu" ).bind( "popupshowing", evernote_contextMenuShow );
    new Evernote.ButtonPositionSetter().setUp();
}

function evernote_finish() {
    Evernote.jQuery( "#contentAreaContextMenu" ).unbind( "popupshowing", evernote_contextMenuShow );
    Evernote.cleanUp();

    window.removeEventListener( "load", evernote_start, false );
}

window.addEventListener( "load", evernote_start, false );

