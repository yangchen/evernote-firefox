//"use strict";

function evernote_onNativePanelIFrameLoad( /*event*/ ) {
    var popupContent = document.getElementById( "webclipper3_popupContent" );
    if ( popupContent ) {
        var data = window.nativeClipperdata;
        popupContent.contentWindow.document.setUserData( "data", data, null );
        popupContent.contentWindow.document.setUserData( "overlayContentContainer", popupContent, null );
        window.nativeClipperdata = null;
    }
}

function evernote_nativePanelInit() {
    var popupContent = document.getElementById( "webclipper3_popupContent" );
    if ( !popupContent ) {
        return;
    }

    if ( popupContent.getAttribute( "src" ) == "chrome://webclipper3-common/content/nativePopup.html" ) {
        evernote_onNativePanelIFrameLoad();
    }

    popupContent.addEventListener( "load", evernote_onNativePanelIFrameLoad, true );
    popupContent.setAttribute( "src", "chrome://webclipper3-common/content/nativePopup.html" );
}

function evernote_nativePanelClose() {
    var popupContent = document.getElementById( "webclipper3_popupContent" );
    if ( popupContent ) {
        popupContent.removeEventListener( "load", evernote_onNativePanelIFrameLoad, true );
        popupContent.style.display = "none";
    }
}

