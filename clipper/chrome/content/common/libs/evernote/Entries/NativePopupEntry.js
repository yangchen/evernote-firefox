//"use strict";

var evernote_nativePopup = null;

function evernote_localizeNativePopup() {
    Evernote.logger.debug( "evernote_localizeNativePopup()" );

    var allViews = Evernote.jQuery( "div[type='submit']" );
    for ( var i = 0; i < allViews.length; i++ ) {
        Evernote.localizer.localizeBlock( Evernote.jQuery( allViews.get( i ) ) );
    }
}

function evernote_initNativePopup() {
    Evernote.logger.debug( "evernote_initNativePopup()" );

    try {
        var panelData = document.getUserData( "data" );
        window.overlayContentContainer = document.getUserData( "overlayContentContainer" );

        Evernote.localizer = panelData.localizer;
        Evernote.tabManager = panelData.tabManager;

        evernote_nativePopup = new Evernote.NativePopup( panelData );
        evernote_localizeNativePopup();
        panelData.controller.popup = evernote_nativePopup;
    }
    catch ( e ) {
        Evernote.logger.error( "evernote_initNativePopup() failed: error = " + e );
    }
}

Evernote.jQuery( document ).ready( function() {
    var initTimeout = 5000;
    var initPeriod = 100;
    var initStartTime = new Date().getTime();

    var initProc = setInterval( function() {
        if ( document.getUserData( "data" ) != null && document.getUserData( "overlayContentContainer" ) != null ) {
            clearInterval( initProc );
            initProc = null;

            evernote_initNativePopup();
        }
        else if ( initStartTime + initTimeout < new Date().getTime() ) {
            Evernote.logger.error( "NativePopup initialization timeout exceeding" );

            clearInterval( initProc );
            initProc = null;
        }
    }, initPeriod );
} );