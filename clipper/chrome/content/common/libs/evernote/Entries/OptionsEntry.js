//"use strict";

var evernote_optionsDlg = null;

function evernote_initOptionsDlg() {
    Evernote.logger.debug( "evernote_initOptionsDlg()" );

    try {
        if ( window.overlay && window.overlay.i18n && window.overlay.i18n.stringBundle ) {
            Evernote.localizer.stringBundle = window.overlay.i18n.stringBundle;
            evernote_optionsDlg = new Evernote.OptionsDlg();
        }
    }
    catch ( e ) {
        Evernote.logger.error( "evernote_initOptionsDlg() failed: error = " + e );
    }
}

Evernote.jQuery( document ).ready( function() {
    evernote_initOptionsDlg();
} );
