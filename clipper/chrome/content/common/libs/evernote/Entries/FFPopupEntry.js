//"use strict";

var evernote_FFPopup = null;
var evernote_simulateVersionConflict = false;

function evernote_localizeFFPopup() {
    Evernote.logger.debug( "evernote_localizeFFPopup()" );

    var allViews = Evernote.jQuery( "body" );
    for ( var i = 0; i < allViews.length; i++ ) {
        Evernote.localizer.localizeBlock( Evernote.jQuery( allViews.get( i ) ) );
    }
}

function evernote_initFFPopup() {
    Evernote.logger.debug( "evernote_initFFPopup()" );

    try {
        var args = window.overlay.arguments[ 0 ];
        Evernote.localizer = args.localizer;
        Evernote.tabManager = args.tabManager;

        var popupPosition = args.popupPosition;
        Evernote.viewManager.containerResizeCallback = function( h ) {
            var frame = window.overlay.document.getElementById( "popupContent" );
            if ( frame ) {
                frame.style.height = (h + 36) + "px";
                frame.style.width = (frame.style.width + 10) + "px";
            }

            window.sizeToContent();
            window.overlay.sizeToContent();

            if ( !popupPosition.isTopAlign ) {
                if ( window.innerHeight + window.screenY > popupPosition.top + popupPosition.height ) {
                    window.moveTo( popupPosition.left, (popupPosition.top + popupPosition.height) - window.innerHeight )
                }

                if ( window.innerHeight + window.screenY < popupPosition.top + popupPosition.height ) {
                    window.moveBy( 0, ((popupPosition.top + popupPosition.height) - (window.innerHeight + window.screenY) ) )
                }
            }

            if ( evernote_FFPopup ) {
                evernote_FFPopup.updateNotebookArrowPosition();
            }
        };

        Evernote.viewManager.globalErrorMessage = new Evernote.SimpleViewMessage( Evernote.jQuery( "#globalErrorMessage" ) );
        Evernote.viewManager.globalMessage = new Evernote.StackableViewMessage( Evernote.jQuery( "#globalMessage" ) );

        Evernote.jQuery( window ).bind( Evernote.AppState.CHANGE_EVENT_NAME, function( event, newState ) {
            if ( newState instanceof Evernote.AppState ) {
                Evernote.context.state = newState;
            }
        } );

        evernote_localizeFFPopup();
        evernote_FFPopup = new Evernote.FFPopup( args );
        if ( args.tab ) {
            args.tab = null;
        }

        Evernote.viewManager.wait( Evernote.localizer.getMessage( "loading" ) );

        Evernote.jQuery( window ).blur( function () {
            evernote_FFPopup.dismissPopup( true );
        } );

        Evernote.context.clientEnabled = true;
        var versionConflictBodyHeight = 75;
        var initTimeout = 300;

        setTimeout( function() {
            evernote_FFPopup.remoteCheckVersion( function( result ) {
                result = (!evernote_simulateVersionConflict && result) ? true : false;
                Evernote.context.clientEnabled = result;

                if ( !result ) {
                    Evernote.logger.error( "Unsupported plugin version" );

                    Evernote.viewManager.clearWait();
                    Evernote.viewManager.showError( new Evernote.EDAMUserException( Evernote.EDAMErrorCode.VERSION_CONFLICT ) );
                    Evernote.viewManager.updateBodyHeight( versionConflictBodyHeight );
                    return;
                }

                evernote_FFPopup.remoteSyncState( function( /*response*/ ) {
                                                      evernote_FFPopup.startUp();
                                                  },
                                                  function( /*xhr, textStatus, response*/ ) {
                                                      if ( Evernote.context.isLogined() ) {
                                                          evernote_FFPopup.startUp();
                                                      }
                                                      else {
                                                          Evernote.viewManager.switchView( "loginView" );
                                                          Evernote.jQuery( "#headerUser" ).hide();
                                                          Evernote.jQuery( "#headerNoUser" ).show();

                                                          Evernote.viewManager.clearWait();
                                                      }
                                                  } );
            } );
        }, initTimeout );

        window.focus();
    }
    catch ( e ) {
        Evernote.logger.error( "evernote_initFFPopup() failed: error = " + e );
    }
}

Evernote.jQuery( document ).ready( function() {
    var initTimeout = 5000;
    var initPeriod = 100;
    var initStartTime = new Date().getTime();

    var initProc = setInterval( function() {
        if ( window.overlay && window.overlayContentContainer ) {
            clearInterval( initProc );
            initProc = null;

            evernote_initFFPopup();
        }
        else if ( initStartTime + initTimeout < new Date().getTime() ) {
            Evernote.logger.error( "FFPopup initialization timeout exceeding" );

            clearInterval( initProc );
            initProc = null;
        }
    }, initPeriod );
} );