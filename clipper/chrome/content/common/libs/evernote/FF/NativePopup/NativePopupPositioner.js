//"use strict";

Evernote.NativePopupPositioner = function NativePopupPositioner() {
};

Evernote.NativePopupPositioner.MINIMAL_WIDTH = 150;

Evernote.NativePopupPositioner.prototype.getAnchorElement = function() {
    Evernote.logger.debug( "NativePopupPositioner.getAnchorElement()" );

    var anchor = document.getElementById( "webclipper3-button" );
    if ( !anchor ) {
        var navPanelBoxObject = document.getElementById( "navigator-toolbox" );
        var tabContainer = gBrowser.tabContainer;
        if ( !navPanelBoxObject ) {
            return tabContainer;
        }

        if ( tabContainer.boxObject.screenY + tabContainer.boxObject.height >= navPanelBoxObject.boxObject.screenY + navPanelBoxObject.boxObject.height ) {
            anchor = tabContainer;
        }
        else {
            anchor = navPanelBoxObject
        }
    }

    return anchor;
};

Evernote.NativePopupPositioner.prototype.getPositionRelativeToAnchor = function( panel, anchor ) {
     Evernote.logger.debug( "NativePopupPositioner.getPositionRelativeToAnchor()" );

    var buttonBoxObject = anchor.boxObject;
    var verticalAlign = "after_";
    var displayArrowClass = ".clip-arrow";
    var hideArrowClass = ".bottom-clip-arrow";

    if ( 100 + buttonBoxObject.screenY > window.screenY + window.innerHeight ) {
        displayArrowClass = ".bottom-clip-arrow";
        hideArrowClass = ".clip-arrow";
        verticalAlign = "before_";
    }
    else {
        displayArrowClass = ".clip-arrow";
        hideArrowClass = ".bottom-clip-arrow";
        verticalAlign = "after_";
    }

    Evernote.jQuery( panel ).children( displayArrowClass ).css( "display", "" );
    Evernote.jQuery( panel ).children( hideArrowClass ).css( "display", "none" );

    if ( this.constructor.MINIMAL_WIDTH + buttonBoxObject.screenX > window.screenX + window.innerWidth ) {
        Evernote.jQuery( panel ).children( displayArrowClass ).attr( "pack", "end" );
        return verticalAlign + "end";
    }

    Evernote.jQuery( panel ).children( displayArrowClass ).removeAttr( "pack" );

    Evernote.logger.debug( "Native popup position is " + verticalAlign + "start" );
    return verticalAlign + "start";
};


