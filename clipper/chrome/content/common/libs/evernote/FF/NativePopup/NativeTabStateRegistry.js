//"use strict";

Evernote.NativeTabStateRegistry = function NativeTabStateRegistry() {
    this._states = { };
};

Evernote.NativeTabStateRegistry.prototype._states = null;

Evernote.NativeTabStateRegistry.prototype.register = function( tabId, state ) {
    Evernote.logger.debug( "NativeTabStateRegistry.register(): tabId = " + tabId );

    this.unregister( tabId );
    if ( state instanceof Evernote.NativeTabState ) {
        this._states[ tabId ] = state;
    }
};

Evernote.NativeTabStateRegistry.prototype.unregister = function( tabId ) {
    Evernote.logger.debug( "NativeTabStateRegistry.unregister(): tabId = " + tabId );

    if ( typeof this._states[ tabId ] != "undefined" ) {
        this._states[ tabId ].cleanUp();
        delete this._states[ tabId ];
    }
};

Evernote.NativeTabStateRegistry.prototype.getTabState = function( tabId ) {
    return this._states[ tabId ];
};

Evernote.NativeTabStateRegistry.prototype.cleanUp = function() {
    Evernote.logger.debug( "NativeTabStateRegistry.cleanUp()" );

    for ( var key in this._states ) {
        this.unregister( key );
    }
    this._states = { };
};

Evernote.NativeTabStateRegistry.prototype.isEmpty = function() {
    for ( var key in this._states ) {
        return false;
    }
    
    return true;
};
