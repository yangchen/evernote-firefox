//"use strict";

Evernote.NativeTabState = function( tab, clipper, clipType, position, isArticleAvailable, isFullPageAvailable ) {
    this.__defineGetter__( "tab", this.getTab );
    this.__defineGetter__( "clipper", this.getClipper );
    this.__defineGetter__( "clipType", this.getClipType );
    this.__defineSetter__( "clipType", this.setClipType );
    this.__defineGetter__( "position", this.getPosition );
    this.__defineGetter__( "articleAvailable", this.getArticleAvailable );
    this.__defineGetter__( "fullPageAvailable", this.getFullPageAvailable );
    this.__defineGetter__( "existingArticle", this.getExistingArticle );
    this.__defineSetter__( "existingArticle", this.setExistingArticle );
    this.initialize( tab, clipper, clipType, position, isArticleAvailable, isFullPageAvailable );
};

Evernote.NativeTabState.prototype._tab = null;
Evernote.NativeTabState.prototype._clipper = null;
Evernote.NativeTabState.prototype._clipType = null;
Evernote.NativeTabState.prototype._position = null;
Evernote.NativeTabState.prototype._isArticleAvailable = false;
Evernote.NativeTabState.prototype._isFullPageAvailable = false;
Evernote.NativeTabState.prototype._existingArticle = null;
Evernote.NativeTabState.prototype._listeners = null;

Evernote.NativeTabState.prototype.initialize = function( tab, clipper, clipType, position, isArticleAvailable, isFullPageAvailable ) {
    Evernote.logger.debug( "NativeTabState.initialize()" );

    this._tab = tab;
    this._clipper = clipper;
    this._clipType = clipType;
    this._position = position;
    this._isArticleAvailable = isArticleAvailable;
    this._isFullPageAvailable = isFullPageAvailable;
    this._listeners = { };
};

Evernote.NativeTabState.prototype.addListener = function( eventName, fn ) {
    Evernote.logger.debug( "NativeTabState.addListener(): eventName = " + eventName );

    if ( typeof fn == "function" ) {
        this.removeListener( eventName );
        this._tab.addEventListener( eventName, fn, true );
        this._listeners[ eventName ] = fn;
    }
};

Evernote.NativeTabState.prototype.cleanUp = function() {
    Evernote.logger.debug( "NativeTabState.cleanUp()" );
    
    for ( var key in this._listeners ) {
        this.removeListener( key );
    }
    this._tab = null;
};

Evernote.NativeTabState.prototype.removeListener = function( eventName ) {
    Evernote.logger.debug( "NativeTabState.removeListener(): eventName = " + eventName );

    if ( typeof this._listeners[ eventName ] == "function" ) {
        try {
            this._tab.removeEventListener( eventName, this._listeners[ eventName ], true );
        }
        catch ( e ) {
            // FF3.6
        }
        delete this._listeners[ eventName ];
    }
};

Evernote.NativeTabState.prototype.getTab = function() {
    return this._tab;
};

Evernote.NativeTabState.prototype.getClipper = function() {
    return this._clipper;
};

Evernote.NativeTabState.prototype.getClipType = function() {
    return this._clipType;
};

Evernote.NativeTabState.prototype.setClipType = function( clipType ) {
    this._clipType = clipType;
};

Evernote.NativeTabState.prototype.getPosition = function() {
    return this._position;
};

Evernote.NativeTabState.prototype.getArticleAvailable = function() {
    return this._isArticleAvailable;
};

Evernote.NativeTabState.prototype.getFullPageAvailable = function() {
    return this._isFullPageAvailable;
};

Evernote.NativeTabState.prototype.getExistingArticle = function() {
    return this._existingArticle;
};

Evernote.NativeTabState.prototype.setExistingArticle = function( element ) {
    this._existingArticle = element;
};



