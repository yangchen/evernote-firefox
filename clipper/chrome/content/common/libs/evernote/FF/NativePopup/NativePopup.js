//"use strict";

Evernote.NativePopup = function NativePopup( panelData ) {
    this.initialize( panelData );
};

Evernote.NativePopup.prototype._controller = null;
Evernote.NativePopup.prototype._registry = null;
Evernote.NativePopup.prototype._width = 0;

Evernote.NativePopup.prototype.initialize = function( panelData ) {
    Evernote.logger.debug( "NativePopup.initialize()" );

    this._controller = panelData.controller;
    this._registry = panelData.registry;
    this.bindControls();
};

Evernote.NativePopup.prototype.bindControls = function() {
    Evernote.logger.debug( "NativePopup.bindControls()" );

    var self = this;
    Evernote.jQuery( "#clipNoteArticle, #clipNoteFullpage, #clipNoteUrl" ).click( function() {
        self._controller.onMouseClicked( Evernote.tabManager.getSelectedTabId() );
    } );
    
    Evernote.jQuery( "div[type='submit']" ).mouseenter( function() {
        var allButtons = Evernote.jQuery( "div[type='submit']" );
        for ( var i = 0; i < allButtons.length; ++i ) {
            Evernote.jQuery( allButtons.get( i ) ).removeClass( "ui-state-hover" );
        }
        Evernote.jQuery( this ).addClass( "ui-state-hover" );

        var tabId = Evernote.tabManager.getSelectedTabId();
        var tabState = self._registry.getTabState( tabId );

        if ( tabState ) {
            tabState.clipType = Evernote.RequestType[ "PREVIEW_" + Evernote.jQuery( this ).attr( "name" ) ];
            self._controller.onClipTypeChanged( tabId );
        }
    } );
};

Evernote.NativePopup.prototype.highlightSelectedButton = function() {
    Evernote.logger.debug( "NativePopup.highlightSelectedButton()" );

    var tabState = this._registry.getTabState( Evernote.tabManager.getSelectedTabId() );
    if ( !tabState ) {
        return;
    }

    var allButtons = Evernote.jQuery( "div[type='submit']" );
    for ( var i = 0; i < allButtons.length; ++i ) {
        Evernote.jQuery( allButtons[ i ] ).removeClass( "ui-state-hover" );
    }

    var clipType = tabState.clipType;
    if ( clipType == Evernote.RequestType.PREVIEW_CLIP_ACTION_ARTICLE ) {
        var button = Evernote.jQuery( "#clipNoteArticle" );
    }
    else if ( clipType == Evernote.RequestType.PREVIEW_CLIP_ACTION_URL ) {
        button = Evernote.jQuery( "#clipNoteUrl" );
    }
    else {
        button = Evernote.jQuery( "#clipNoteFullpage" );
    }

    button.addClass( "ui-state-hover" );
};

Evernote.NativePopup.prototype.show = function( showArticleBtn, showFullPageBtn ) {
    Evernote.logger.debug( "NativePopup.show()" );

    Evernote.jQuery( "#clipNoteArticle" ).css( "display", (showArticleBtn) ? "block" : "none" );
    Evernote.jQuery( "#clipNoteFullpage" ).css( "display", (showFullPageBtn) ? "block" : "none" );
    window.overlayContentContainer.style.display = "block";

    var container = Evernote.jQuery( ".submitWithOptions" ).get( 0 );
    if ( !this._width ) {
        this._width = container.offsetWidth + 12 + Evernote.jQuery( "#clipNoteUrl .keypress-text" ).width();
    }
    
    var height = container.offsetHeight;
    Evernote.jQuery( window.overlayContentContainer ).css( "height", (height + 12) + "px" );

    this.highlightSelectedButton();
    this._controller.onPopupShowed( this._width, height );
};

Evernote.NativePopup.prototype.hide = function() {
    Evernote.logger.debug( "NativePopup.hide()" );
    
    window.overlayContentContainer.style.display = "none";
    this._controller.onPopupHidden();
};

Evernote.NativePopup.prototype.close = function() {
    Evernote.logger.debug( "NativePopup.close()" );

    window.overlayContentContainer.setAttribute( "src", "" );
    this._controller.onPopupClosed();
};