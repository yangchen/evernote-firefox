//"use strict";

Evernote.NativeController = function NativeController() {
    this.__defineSetter__( "popup", this.setPopup );
    this.__defineGetter__( "stateRegistry", this.getStateRegistry );
};

Evernote.NativeController.prototype._stateRegistry = null;
Evernote.NativeController.prototype._panel = null;
Evernote.NativeController.prototype._popup = null;
Evernote.NativeController.prototype._contentPreviewer = null;
Evernote.NativeController.prototype._enterPressFn = null;
Evernote.NativeController.prototype._nudgeFn = null;

Evernote.NativeController.prototype.initialize = function( tab, tabId, clipper, clipType, position ) {
    Evernote.logger.debug( "NativeController.initialize(): clipType = " + clipType + ",  tabId = " + tabId );

    try {
        window.nativeClipperdata = {
            controller : this,
            registry : this.stateRegistry,
            localizer : Evernote.localizer,
            tabManager : Evernote.tabManager
        };

        this._panel = this.getPanel();
        if ( !this._panel ){
            Evernote.logger.error( "Null panel in NativeController" );
            return;
        }

        Evernote.prefsManager.addListener( this );
        Evernote.tabManager.addListener( this );
        Evernote.closeListener.addListener( this );

        var self = this;
        var tabState = this.resetTabState( tab, tabId, clipper, clipType, position );
        tabState.addListener( "click", function() {
            self.closePopup( tabId );
        } );

        //Wait initialization of native popup dialog, and show it
        Evernote.logger.debug( "Wait until native popup initialization will be finished" );
        var initTimeout = 5000;
        var initPeriod = 100;
        var initStartTime = new Date().getTime();

        var initProc = setInterval( function() {
            if ( self._popup ) {
                Evernote.logger.debug( "Native popup was created" );

                clearInterval( initProc );
                initProc = null;
                self.showPopup( tabId );
            }
            else if ( initStartTime + initTimeout < new Date().getTime() ) {
                Evernote.logger.error( "Native popup was not created, timeout has exceeded" );

                clearInterval( initProc );
                initProc = null;
                self.closePopup( tabId );
            }
        }, initPeriod );
    }
    catch ( e ) {
        Evernote.logger.error( "NativeController.initialize() failed: clipType = " + clipType + ",  tabId = " + tabId + ", error = " + e );
    }
};

Evernote.NativeController.prototype.showPopup = function( tabId ) {
    Evernote.logger.debug( "NativeController.showPopup(): tabId = " + tabId );

    try {
        var activeTabId = Evernote.tabManager.getSelectedTabId();
        var tabState = this.stateRegistry.getTabState( tabId );

        //do not any actions if  native clip dialog was closed/or tab changed
        if ( activeTabId != tabId || !tabState || !this._panel || !this._popup ) {
            this.closePopup( tabId );
            return;
        }

        this._popup.show( tabState.articleAvailable, tabState.fullPageAvailable );
        this._panel.focus();

        this.showPreview( tabState );
    }
    catch ( e ) {
        Evernote.logger.error( "NativeController.showPopup() failed: tabId = " + tabId + ", error = " + e );
    }
};

Evernote.NativeController.prototype.closePopup = function( tabId ) {
    Evernote.logger.debug( "NativeController.closePopup(): tabId = " + tabId );

    this.stateRegistry.unregister( tabId );
    if ( this.stateRegistry.isEmpty() ) {
        this.closePanel();
    }
    else if ( Evernote.tabManager.getSelectedTabId() == tabId ) {
        this.hidePanel();
    }
};

Evernote.NativeController.prototype.onPopupShowed = function( popupWidth, popupHeight ) {
    Evernote.logger.debug( "NativeController.onPopupShowed()" );

    this._panel.sizeTo( popupWidth, popupHeight );
    Evernote.jQuery( this._panel ).children( "box" ).children( "image" ).css( "display", "" );

    window.addEventListener( "keypress", this.getEnterPressFn(), false );
    window.addEventListener( "keypress", this.getNudgeFn(), false );
};

Evernote.NativeController.prototype.onPopupHidden = function() {
    Evernote.logger.debug( "NativeController.onPopupHidden()" );
    window.removeEventListener( "keypress", this.getEnterPressFn(), false );
    window.removeEventListener( "keypress", this.getNudgeFn(), false );
};

Evernote.NativeController.prototype.onPopupClosed = function() {
    Evernote.logger.debug( "NativeController.onPopupClosed()" );
    window.removeEventListener( "keypress", this.getEnterPressFn(), false );
    window.removeEventListener( "keypress", this.getNudgeFn(), false );
};

Evernote.NativeController.prototype.onClipTypeChanged = function( tabId ) {
    Evernote.logger.debug( "NativeController.onClipTypeChanged(): tabId = " + tabId );

    var tabState = this.stateRegistry.getTabState( tabId );
    if ( tabState ) {
        this.showPreview( tabState );
    }
};

Evernote.NativeController.prototype.onMouseClicked = function( tabId ) {
    Evernote.logger.debug( "NativeController.onMouseClicked(): tabId = " + tabId );
    this.clip( tabId );
};

Evernote.NativeController.prototype.onEnterPressed = function( tabId ) {
    Evernote.logger.debug( "NativeController.onEnterPressed(): tabId = " + tabId );
    this.clip( tabId );
};

Evernote.NativeController.prototype.closePanel = function() {
    Evernote.logger.debug( "NativeController.closePanel()" );

    this.hidePreview();
    this.stateRegistry.cleanUp();

    Evernote.closeListener.removeListener( this );
    Evernote.tabManager.removeListener( this );
    Evernote.prefsManager.removeListener( this );

    if ( this._popup ) {
        this._popup.close();
        this._popup = null;
    }

    //MAC OSX: fix for panel size if we close it from other tab (closed tab is not selected)
    if ( this._panel ) {
        this._panel.sizeTo( 1, 1 );
        this._panel.hidePopup();
        this._panel = null;
    }
};

Evernote.NativeController.prototype.hidePanel = function() {
    Evernote.logger.debug( "NativeController.hidePanel()" );

    if ( this._popup ) {
        this._popup.hide();
    }

    if ( this._panel ) {
        Evernote.jQuery( this._panel ).children( "box" ).children( "image" ).css( "display", "none" );
        this._panel.sizeTo( 1, 1 );
    }
    
    this.hidePreview();
};

Evernote.NativeController.prototype.clip = function( tabId ) {
    Evernote.logger.debug( "NativeController.clip(): tabId = " + tabId );

    try {
        var state = this.stateRegistry.getTabState( tabId );
        if ( state )  {
            state.tab.focus();

            var clipper = state.clipper;
            var clipType = state.clipType;
            var articleElement = this._contentPreviewer.getArticleElement();

            this.closePopup( tabId );
            if ( clipType == Evernote.RequestType.PREVIEW_CLIP_ACTION_ARTICLE ) {
                clipper.clipArticle( articleElement );
            }
            else if ( clipType == Evernote.RequestType.PREVIEW_CLIP_ACTION_FULL_PAGE ) {
                clipper.clipFullPage();
            }
            else if ( clipType == Evernote.RequestType.PREVIEW_CLIP_ACTION_URL ) {
                clipper.clipUrl();
            }
        }
    }
    catch ( e ) {
        Evernote.logger.error( "NativeController.clip() failed: error = " + e );
    }
};

Evernote.NativeController.prototype.showPreview = function( tabState ) {
    Evernote.logger.debug( "NativeController.showPreview()" );

    try {
        if ( Evernote.Utils.isForbiddenUrl( tabState.tab.location.href ) ) {
            return;
        }

        var contentPreviwer = this.getContentPreviewer( tabState );
        if ( tabState.clipType == Evernote.RequestType.PREVIEW_CLIP_ACTION_FULL_PAGE ) {
            contentPreviwer.previewFullPage();
        }
        else if ( tabState.clipType == Evernote.RequestType.PREVIEW_CLIP_ACTION_URL ) {
            contentPreviwer.previewURL();
        }
        else if ( tabState.clipType == Evernote.RequestType.PREVIEW_CLIP_ACTION_ARTICLE ) {
            if ( tabState.existingArticle ) {
                contentPreviwer.previewExistingArticle( tabState.existingArticle, true );
            }
            else {
                contentPreviwer.previewArticle( true );
            }
        }
    }
    catch ( e ) {
        Evernote.logger.error( "NativeController.showPreview() failed: error = " + e );
    }
};

Evernote.NativeController.prototype.hidePreview = function() {
    Evernote.logger.debug( "NativeController.hidePreview()" );

    if ( this._contentPreviewer ) {
        this._contentPreviewer.clear();
        this._contentPreviewer = null;
    }
};

Evernote.NativeController.prototype.resetTabState = function( tab, tabId, clipper, clipType, position ) {
    Evernote.logger.debug( "NativeController.resetTabState(): tabId = " + tabId );

    var isForbiddenUrl = Evernote.Utils.isForbiddenUrl( tab.location.href );
    var isArticleSane = Evernote.Utils.isArticleSane( new Evernote.PageInfo( tab ), tab );

    var tabState = new Evernote.NativeTabState( tab, clipper, clipType, position, (!isForbiddenUrl && isArticleSane), !isForbiddenUrl );
    this.stateRegistry.register( tabId, tabState );

    return tabState;
};

Evernote.NativeController.prototype.onPrefChanged = function( prefName ) {
    Evernote.logger.debug( "NativeController.onPrefChanged(): prefName = " + prefName );

    if ( prefName != Evernote.PrefKeys.IS_DESKTOP_SELECTED ) {
        return;
    }

    if ( !Evernote.Utils.isDesktopClipperSelected() ) {
        this.closePanel();
    }
};

Evernote.NativeController.prototype.onTabSelect = function( tabId ) {
    Evernote.logger.debug( "NativeController.onTabSelect(): tabId = " + tabId );

    if ( this._panel ) {
        var tabState = this.stateRegistry.getTabState( tabId );
        if ( tabState ) {
            if ( !Evernote.Utils.isDesktopClipperSelected() ) {
                this.closePopup( tabId );
            }
            else {
                this.showPopup( tabId );
            }
        }
        else {
            this.hidePanel();
        }
    }
};

Evernote.NativeController.prototype.onTabClose = function( tabId ) {
    Evernote.logger.debug( "NativeController.onTabClose(): tabId = " + tabId );
    this.closePopup( tabId );
};

Evernote.NativeController.prototype.onLocationChange = function( browser ) {
    Evernote.logger.debug( "NativeController.onLocationChange()" );

    var tabId = Evernote.Utils.getBrowserTab( browser ).linkedPanel;
    var state = this.stateRegistry.getTabState( tabId );
    if ( state ) {
        this.closePopup( tabId );
    }
};

Evernote.NativeController.prototype.onWindowClose = function( /*tabId*/ ) {
    Evernote.logger.debug( "NativeController.onWindowClose()" );
    this.closePanel();
};

Evernote.NativeController.prototype.getPanel = function() {
    Evernote.logger.debug( "NativeController.getPanel()" );

    var panel = document.getElementById( "webclipperNativePopup" );
    if ( panel ) {
        var positioner = new Evernote.NativePopupPositioner();
        var anchor = positioner.getAnchorElement();
        panel.openPopup( anchor, positioner.getPositionRelativeToAnchor( panel, anchor ), 0, 1, false, false );
    }

    return panel;
};

Evernote.NativeController.prototype.getEnterPressFn = function() {
    if ( !this._enterPressFn ) {
        var self = this;
        this._enterPressFn = function( event ) {
            if ( event.which == 13 ) {
                self.onEnterPressed( Evernote.tabManager.getSelectedTabId() );
                event.stopPropagation();
                return false;
            }

            return true;
        };
    }

    return this._enterPressFn;
};

Evernote.NativeController.prototype.getNudgeFn = function() {
    if ( !this._nudgeFn ) {
        var self = this;
        this._nudgeFn = function( event ) {
            if ( event.keyCode >= 37 && event.keyCode <= 40 ) {
                self.nudge( event );
                event.stopPropagation();
                return false;
            }

            return true;
        };
    }

    return this._nudgeFn;
};

Evernote.NativeController.prototype.getStateRegistry = function() {
    if ( !this._stateRegistry ) {
        this._stateRegistry = new Evernote.NativeTabStateRegistry();
    }

    return this._stateRegistry;
};

Evernote.NativeController.prototype.getContentPreviewer = function( tabState ) {
    if ( !this._contentPreviewer || tabState.tab != this._contentPreviewer.tab ) {
        this.hidePreview();
        this._contentPreviewer = new Evernote.Chrome.ContentPreview( tabState.tab );
    }

    return this._contentPreviewer;
};

Evernote.NativeController.prototype.setPopup = function( popup ) {
    this._popup = popup;
};

Evernote.NativeController.prototype.nudge = function ( event ) {
    if ( Evernote.context.options.selectionNudging == Evernote.Options.SELECTION_NUDGING_OPTIONS.DISABLED ) {
        return;
    }

    var key = event.keyCode;
    var keyMap = {
        37: "left",
        38: "up",
        39: "right",
        40: "down"
    };

    this._contentPreviewer.nudgePreview( keyMap[ key ] );
    // saving nudge
    var tabState = this.stateRegistry.getTabState( Evernote.tabManager.getSelectedTabId() );
    if ( tabState ) {
        tabState.existingArticle = this._contentPreviewer.getArticleElement();
    }
};