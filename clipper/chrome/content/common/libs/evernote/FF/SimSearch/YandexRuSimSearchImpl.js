//"use strict";

Evernote.YandexRuSimSearchImpl = function YandexRuSimSearchImpl( doc ) {
    this.parent.initialize.call( this, doc );
};

Evernote.inherit( Evernote.YandexRuSimSearchImpl, Evernote.SimSearchImpl, true );

Evernote.YandexRuSimSearchImpl.isResponsibleFor = function( url ) {
    return (this.queryFromUrl( url )) ? true : false;
};

Evernote.YandexRuSimSearchImpl.queryFromUrl = function( url ) {
    Evernote.logger.debug( "YandexRuSimSearchImpl.queryFromUrl()" );

    if ( typeof url == "string" ) {
        var domain = Evernote.UrlHelpers.urlDomain( url );
        var path = Evernote.UrlHelpers.urlPath( url );

        if ( typeof domain == "string" && typeof path == "string" ) {
            if ( this.isYandexSearch( domain, path ) || this.isYandexImagesSearch( domain, path ) || this.isYandexVideosSearch( domain, path ) ) {
                var res = Evernote.UrlHelpers.urlQueryValue( this.getQueryKey(), url );
                return (typeof res == "string" && res.length > 0) ? res : null;
            }
        }
    }
    
    return null;
};

Evernote.YandexRuSimSearchImpl.isYandexSearch = function( domain, path ) {
    return domain.toLowerCase().indexOf( "yandex.ru" ) == 0 && path.toLowerCase().indexOf( "/yandsearch" ) == 0;
};

Evernote.YandexRuSimSearchImpl.isYandexImagesSearch = function( domain, path ) {
    return domain.toLowerCase().indexOf( "images.yandex.ru" ) >= 0 && path.toLowerCase().indexOf( "/yandsearch" ) == 0;
};

Evernote.YandexRuSimSearchImpl.isYandexVideosSearch = function( domain, path ) {
    return domain.toLowerCase().indexOf( "video.yandex.ru" ) >= 0 && path.toLowerCase().indexOf( "/" ) == 0;
};

Evernote.YandexRuSimSearchImpl.getQueryKey = function() {
    return "text";
};

Evernote.YandexRuSimSearchImpl.prototype.getAnchor = function() {
    if ( this.constructor.isYandexSearch( this._domain, this._path ) ) {
        return ".b-page-layout__column b-page-layout__column_type_left";
    }
    else if ( this.constructor.isYandexImagesSearch( this._domain, this._path ) ) {
        return "#result";
    }
    else if ( this.constructor.isYandexVideosSearch( this._domain, this._path ) ) {
        return ".g-line";
    }

    return "";
};

Evernote.YandexRuSimSearchImpl.prototype.getQuery = function() {
    return this.constructor.queryFromUrl( this._doc.location.href );
};

Evernote.YandexRuSimSearchImpl.prototype.getContentStyles = function() {
    if ( this.constructor.isYandexSearch( this._domain, this._path ) ) {
        return { marginLeft : "125px", marginBottom : "-25px" };
    }
    else if ( this.constructor.isYandexImagesSearch( this._domain, this._path ) ) {
        return { marginLeft : "0px" };
    }
    else if ( this.constructor.isYandexVideosSearch( this._domain, this._path ) ) {
        return { marginLeft : "0px", marginTop : "-15px", marginRight : "5px", marginBottom : "10px" };
    }

    return { };
};