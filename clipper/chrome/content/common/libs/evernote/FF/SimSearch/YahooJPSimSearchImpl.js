//"use strict";

Evernote.YahooJPSimSearchImpl = function YahooJPSimSearchImpl( doc ) {
    this.parent.initialize.call( this, doc );
};

Evernote.inherit( Evernote.YahooJPSimSearchImpl, Evernote.SimSearchImpl, true );

Evernote.YahooJPSimSearchImpl.isResponsibleFor = function( url ) {
    return (this.queryFromUrl( url )) ? true : false;
};

Evernote.YahooJPSimSearchImpl.queryFromUrl = function( url ) {
    Evernote.logger.debug( "YahooJPSimSearchImpl.queryFromUrl()" );

    if ( typeof url == "string" ) {
        var domain = Evernote.UrlHelpers.urlDomain( url );
        var path = Evernote.UrlHelpers.urlPath( url );

        if ( typeof domain == "string" && typeof path == "string" ) {
            if ( this.isYahooJP( domain, path ) ) {
                var res = Evernote.UrlHelpers.urlQueryValue( this.getQueryKey(), url );
                return (typeof res == "string" && res.length > 0) ? res : null;
            }
        }
    }

    return null;
};

Evernote.YahooJPSimSearchImpl.isYahooJP = function( domain, path ) {
    return domain.toLowerCase().indexOf( "search.yahoo.co.jp" ) >= 0 && path.toLowerCase().indexOf( "/search" ) == 0;
};

Evernote.YahooJPSimSearchImpl.getQueryKey = function() {
    return "p";
};

Evernote.YahooJPSimSearchImpl.prototype.getAnchor = function() {
    return "#contents";
};

Evernote.YahooJPSimSearchImpl.prototype.getQuery = function() {
    return this.constructor.queryFromUrl( this._doc.location.href );
};