//"use strict";

Evernote.SimSearchStorage = function SimSearchStorage() {
    this.initialize();
};

Evernote.SimSearchStorage.DOM_LOAD_TIMEOUT = 60000;

Evernote.SimSearchStorage.prototype._simSearches = null;
Evernote.SimSearchStorage.prototype._simTimers = null;

Evernote.SimSearchStorage.prototype.initialize = function() {
    Evernote.logger.debug( "SimSearchStorage.initialize()" );

    this._simSearches = { };
    this._simTimers = { };

    Evernote.prefsManager.addListener( this );
    Evernote.tabManager.addListener( this );
    Evernote.closeListener.addListener( this );
};

Evernote.SimSearchStorage.prototype.onLocationChange = function( browser ) {
    Evernote.logger.debug( "SimSearchStorage.onLocationChange()" );

    var doc = browser.contentDocument;
    var tab = Evernote.Utils.getBrowserTab( browser );
    var tabId = tab.linkedPanel;

    this.stopTimer( tabId );
    if ( tabId == Evernote.tabManager.getSelectedTabId() ) {
        Evernote.toolbarManager.updateSimSearchBadge( 0 );
    }

    if ( !Evernote.context.options.useSearchHelper ) {
        this.abortAll();
        return;
    }

    var self = this;
    var domLoadStartTime = new Date().getTime();
    var domLoadPeriod = 100;

    this._simTimers[ tabId ] = setInterval( function() {
        if ( doc.readyState == "complete" ) {
            self.stopTimer( tabId );
            self.onDOMLoaded( tab, doc );
        }
        else if ( domLoadStartTime + self.constructor.DOM_LOAD_TIMEOUT < new Date().getTime() ) {
            Evernote.logger.error( "SimSearchStorage.onLocationChange(): DOM load timeout" );
            self.stopTimer( tabId );
        }

    }, domLoadPeriod );
};

Evernote.SimSearchStorage.prototype.onTabSelect = function( tabId ) {
    Evernote.logger.debug( "SimSearchStorage.onTabSelect(): tabId = " + tabId );
    this.updateToolbar( tabId );
};

Evernote.SimSearchStorage.prototype.onTabClose = function( tabId ) {
    Evernote.logger.debug( "SimSearchStorage.onTabClose(): tabId = " + tabId );

    this.stopTimer( tabId );
    this.clearSimSearch( tabId );

    this.updateToolbar( Evernote.tabManager.getSelectedTabId() );
};

Evernote.SimSearchStorage.prototype.onDOMLoaded = function( tab, doc ) {
    Evernote.logger.debug( "SimSearchStorage.onDOMLoaded()" );

    if ( !doc.location ) {
        return;
    }

    var tabId = tab.linkedPanel;
    if ( this._simSearches[ tabId ] ) {
        this._simSearches[ tabId ].reset( tab, doc );
    }
    else {
        this._simSearches[ tabId ] = new Evernote.SimSearcher( tab, doc );
    }
};

Evernote.SimSearchStorage.prototype.abortAll = function() {
    Evernote.logger.debug( "SimSearchStorage.abortAll()" );

    for ( var key in this._simTimers ) {
        this.stopTimer( key );
    }

    for ( key in this._simSearches ) {
        this.clearSimSearch( key );
    }

    this._simTimers = { };
    this._simSearches = { };

    Evernote.toolbarManager.updateSimSearchBadge( 0 );
};

Evernote.SimSearchStorage.prototype.getResultNotes = function( tabId ) {
    if ( this._simSearches[ tabId ] ) {
        return this._simSearches[ tabId ].getResultNotes();
    }

    return 0;
};

Evernote.SimSearchStorage.prototype.disableSearchHelper = function() {
    Evernote.logger.debug( "SimSearchStorage.disableSearchHelper()" );

    var options = Evernote.context.options;
    options.useSearchHelper = false;
    Evernote.context.options = options;
};

Evernote.SimSearchStorage.prototype.onPrefChanged = function( prefName ) {
    Evernote.logger.debug( "SimSearchStorage.onPrefChanged(): prefName = " + prefName );

    if ( prefName != Evernote.PrefKeys.IS_DESKTOP_SELECTED ) {
        return;
    }

    this.updateToolbar( Evernote.tabManager.getSelectedTabId() );
};

Evernote.SimSearchStorage.prototype.getSimSearcher = function( tabId ) {
    return (Evernote.context.options.useSearchHelper) ? this._simSearches[ tabId ] : null;
};

Evernote.SimSearchStorage.prototype.onWindowClose = function() {
    Evernote.logger.debug( "SimSearchStorage.onWindowClose()" );
    this.cleanUp();
};

Evernote.SimSearchStorage.prototype.updateToolbar = function( tabId ) {
    Evernote.logger.debug( "SimSearchStorage.updateToolbar()" );

    var notesCount = 0;
    if ( Evernote.context.options.useSearchHelper && !Evernote.Utils.isDesktopClipperSelected() && this._simSearches[ tabId ] ) {
        notesCount = this._simSearches[ tabId ].getResultNotes();
    }

    Evernote.toolbarManager.updateSimSearchBadge( notesCount );
};

Evernote.SimSearchStorage.prototype.cleanUp = function() {
    Evernote.logger.debug( "SimSearchStorage.cleanUp()" );

    this.abortAll();
    Evernote.tabManager.removeListener( this );
    Evernote.prefsManager.removeListener( this );
};

Evernote.SimSearchStorage.prototype.stopTimer = function( tabId ) {
    if ( this._simTimers[ tabId ] ) {
        clearInterval( this._simTimers[ tabId ] );
        delete this._simTimers[ tabId ];
    }
};

Evernote.SimSearchStorage.prototype.clearSimSearch = function( tabId ) {
    if ( this._simSearches[ tabId ] ) {
        this._simSearches[ tabId ].cleanUp();
        delete this._simSearches[ tabId ];
    }
};