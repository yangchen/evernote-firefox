//"use strict";

Evernote.SimSearchContentScript = function SimSearchContentScript( doc ) {
    this._doc = doc;
};

Evernote.SimSearchContentScript.EVERNOTE_RESULT_STATS_ID = "evernoteResultStats";
Evernote.SimSearchContentScript.EVERNOTE_RESULT_STATS_MESSAGE_ID = "evernoteResultStatsMessage";
Evernote.SimSearchContentScript.EVERNOTE_RESULT_STATS_ERROR_MESSAGE_ID = "evernoteResultStatsErrorMessage";
Evernote.SimSearchContentScript.WAIT_ANCHOR_TIMEOUT = 60000;

Evernote.SimSearchContentScript.prototype._doc = null;
Evernote.SimSearchContentScript.prototype._waitTimer = null;

Evernote.SimSearchContentScript.prototype.prependMessageBlock = function( params, message, id ) {
    Evernote.logger.debug( "SimSearchContentScript.prependMessageBlock(): id = " + id + ", message = " + message );

    this.waitAnchorAppear( params.anchor, function( elem ) {
        Evernote.logger.debug( "SimSearchContentScript: anchor element has appeared" );
        // context must be SimSearchContentScript

        try {
            this.removeResultStats();
            var doc = this._doc;

            if ( elem && elem.childNodes.length > 0 ) {
                var stats = doc.getElementById( this.constructor.EVERNOTE_RESULT_STATS_ID );
                if ( !stats ) {
                    stats = doc.createElement( "div" );
                    stats.setAttribute( "id", this.constructor.EVERNOTE_RESULT_STATS_ID );
                    elem.parentNode.insertBefore( stats, elem );
                }

                if ( params.styles ) {
                    for ( var key in params.styles ) {
                        stats.style[ key ] = params.styles[ key ];
                    }
                }

                if ( message ) {
                    var block = doc.createElement( "div" );
                    block.id = id;
                    block.innerHTML = message;

                    stats.appendChild( block );
                }
            }

            this.bindDisableSearchHelper();
        }
        catch ( e ) {
            Evernote.logger.error( "SimSearchContentScript.prependMessageBlock() failed: error = " + e );
        }
    } );

};

Evernote.SimSearchContentScript.prototype.prependMessage = function( params, message ) {
    this.prependMessageBlock( params, message, this.constructor.EVERNOTE_RESULT_STATS_MESSAGE_ID );
};

Evernote.SimSearchContentScript.prototype.prependErrorMessage = function( params, message ) {
    this.prependMessageBlock( params, message, this.constructor.EVERNOTE_RESULT_STATS_ERROR_MESSAGE_ID );
};

Evernote.SimSearchContentScript.prototype.removeResultStats = function() {
    Evernote.DOMHelpers.removeNode( this._doc, this.constructor.EVERNOTE_RESULT_STATS_ID );
};

Evernote.SimSearchContentScript.prototype.waitAnchorAppear = function( anchor, callback ) {
    Evernote.logger.debug( "SimSearchContentScript.waitAnchorAppear(): anchor = "  + anchor );

    if ( this._waitTimer ) {
        clearInterval( this._waitTimer );
        this._waitTimer = null;
    }

    var waitInitTime = new Date().getTime();
    var waitInitPeriod = 100;

    var self = this;
    this._waitTimer = setInterval( function() {
        var targetElem = self.findTargetElement( anchor );
        if ( targetElem ) {
            clearInterval( self._waitTimer );
            self._waitTimer = null;

            callback.call( self, targetElem );
        }
        else if ( waitInitTime + self.constructor.WAIT_ANCHOR_TIMEOUT < new Date().getTime() ) {
            clearInterval( self._waitTimer );
            self._waitTimer = null;
            
            Evernote.logger.error( "SimSearchContentScript.waitAnchorAppear(): anchor load timeout" );
        }
    }, waitInitPeriod );
};

Evernote.SimSearchContentScript.prototype.findTargetElement = function( element ) {
    Evernote.logger.debug( "SimSearchContentScript.findTargetElement()" );

    try {
        if ( typeof element == "string" ) {
            var x = element.charAt( 0 );
            if ( x == "#" ) {
                return this._doc.getElementById( element.substring( 1 ) );
            }
            else if ( x == "." ) {
                return this._doc.getElementsByClassName( element.substring( 1 ) )[ 0 ];
            }
        }
        else if ( element instanceof Element ) {
            return element;
        }
    }
    catch ( e ) {
    }
    
    return null;
};

Evernote.SimSearchContentScript.prototype.disableSearchHelper = function() {
    Evernote.logger.debug( "SimSearchContentScript.disableSearchHelper()" );

    if ( Evernote.Utils.confirm( Evernote.localizer.getMessage( "searchHelperConfirmDisableHeader" ),
                                 Evernote.localizer.getMessage( "searchHelperConfirmDisableMessage" ) ) ) {
        Evernote.simSearchStorage.disableSearchHelper();
        this.removeResultStats();
    }

    return false;
};

Evernote.SimSearchContentScript.prototype.bindDisableSearchHelper = function() {
    Evernote.logger.debug( "SimSearchContentScript.bindDisableSearchHelper()" );

    var elems = this._doc.getElementsByTagName( "a" );
    var self = this;
    for ( var i = 0; i < elems.length; ++i ) {
        var elem = elems[ i ];
        if ( elem && elem.getAttribute( "en-bind-click" ) == "disableSearchHelper" ) {
            elem.addEventListener( "click", function() {
                return self.disableSearchHelper();
            }, false );
        }
    }
};

Evernote.SimSearchContentScript.prototype.cleanUp = function() {
    Evernote.logger.debug( "SimSearchContentScript.cleanUp()" );

    if ( this._waitTimer ) {
        clearInterval( this._waitTimer );
        this._waitTimer = null;
    }

    this._doc = null;
};