//"use strict";

Evernote.SimSearcher = function SimSearcher( tab, doc ) {
    this.__defineGetter__( "result", this.getResult );
    this.__defineGetter__( "query", this.getQuery );
    this.__defineGetter__( "noteFilter", this.getNoteFilter );
    this.__defineSetter__( "forceReload", this.setForceReload );
    this.reset( tab, doc );
};

Evernote.SimSearcher.SEARCHHELPER_CSS = "resource://evernote_styles/searchhelper.css";
Evernote.SimSearcher.SAME_URL_MIN_SEARCH_INTERVAL = 60000;

Evernote.SimSearcher.prototype._tab = null;
Evernote.SimSearcher.prototype._doc = null;
Evernote.SimSearcher.prototype._url = "";
Evernote.SimSearcher.prototype._impl = null;
Evernote.SimSearcher.prototype._request = null;
Evernote.SimSearcher.prototype._response = null;
Evernote.SimSearcher.prototype._result = null;
Evernote.SimSearcher.prototype._proc = null;
Evernote.SimSearcher.prototype._query = "";
Evernote.SimSearcher.prototype._noteFilter = null;
Evernote.SimSearcher.prototype._contentScript = null;
Evernote.SimSearcher.prototype._forceReload = false;

Evernote.SimSearcher.prototype.reset = function( tab, doc ) {
    Evernote.logger.debug( "SimSearcher.reset()" );

    try {
        if ( !tab || !doc ) {
            this.cleanUp();
            return;
        }

        var url = doc.location.href;
        var factoryFunc = Evernote.SimSearchImplFactory.getImplementationFor( url );
        if ( typeof factoryFunc != "function" ) {
            this.cleanUp();
            return;
        }

        var impl = new factoryFunc( doc );
        var query = impl.getQuery();
        if ( !query ) {
            Evernote.logger.error( "SimSearcher.reset(): null query" );
            this.cleanUp();
            return;
        }

        if ( query == this._query && this._url ) {
            if ( !doc.getElementById( Evernote.SimSearchContentScript.EVERNOTE_RESULT_STATS_ID ) ) {
                this._forceReload = true;
            }

            if ( !impl.isReloadRequired( this._url, url ) && !this._forceReload ) {
                Evernote.toolbarManager.updateSimSearchBadge( this.getResultNotes() );
                return;
            }
        }

        this.cleanUp();
        new Evernote.AbstractContentScript().injectStyleSheets( [ this.constructor.SEARCHHELPER_CSS ], doc );

        this._tab = tab;
        this._doc = doc;
        this._url = doc.location.href;
        this._query = query;
        this._impl = impl;

        this._contentScript = new Evernote.SimSearchContentScript( this._doc );
        this.noteFilter.words = this._query;
        this._forceReload = false;
        
        this.doSearch();
    }
    catch ( e ) {
        Evernote.logger.error( "SimSearcher.reset() failed: error = " + e );
        this.cleanUp();
    }
};

Evernote.SimSearcher.prototype.cleanUp = function() {
    Evernote.logger.debug( "SimSearcher.cleanUp()" );

    this._tab = null;
    this._doc = null;
    this._url = "";
    this._query = "";
    this._impl = null;

    this._request = null;
    this._response = null;
    this._result = null;

    if ( this._proc ) {
        this._proc.abort();
        this._proc = null;
    }

    if ( this._contentScript ) {
        this._contentScript.cleanUp();
        this._contentScript = null;
    }

    this.noteFilter.words = "";
};

Evernote.SimSearcher.prototype.doSearch = function() {
    Evernote.logger.debug( "SimSearcher.doSearch(): query = " + this._query );

    try {
        this.handleActive();
        var self = this;

        setTimeout( function() {
            self._forceReload = true;
        }, this.constructor.SAME_URL_MIN_SEARCH_INTERVAL );

        this._proc = Evernote.remote.countNotes( this.noteFilter,
            function( response, textStatus, xhr ) {
                self._request = xhr;
                self._response = response;

                if ( response.isResult() && typeof response.result.noteList == "object" ) {
                    Evernote.logger.debug( "Retreived search results" );
                    self._result = response.result.noteList;
                }
                else if ( response.isError() ) {
                    Evernote.logger.warn( "Error retreiving results: " + textStatus );
                }

                self.onAfterSearch();
            },
            function( xhr, textStatus, error ) {
                self._request = xhr;
                self._response = error;

                Evernote.Utils.logHttpErrorMessage( xhr, "count Notes" );
                self.onAfterSearch();
            },
        true );
    }
    catch ( e ) {
        Evernote.logger.error( "SimSearcher.doSearch() failed: error = " + e );
    }
};

Evernote.SimSearcher.prototype.onAfterSearch = function() {
    Evernote.logger.debug( "SimSearcher.onAfterSearch()" );

    try {
        if ( this.isActive() ) {
            this.handleActive();
        }
        else if ( this.isComplete() && this.hasResult() ) {
            this.handleResult();
        }
        else if ( this.hasErrors() ) {
            if ( this._response.isMissingAuthTokenError() ) {
                this.handleMissingAuthTokenError();
            }
            else if ( this._response.hasAuthenticationError() ) {
                this.handleAuthenticationError();
            }
            else if ( this._response.isVersionConflictError() ) {
                this.handleVersionConflictError();
            }
             else {
                this.handleErrors();
            }
        }
        else if ( !this.isTransportError() ) {
            this.handleNoResult();
        }
        else {
            this.handleTransportError();
        }
    }
    catch ( e ) {
        Evernote.logger.error( "SimSearcher.onAfterSearch() failed: error = " + e );
    }
};

Evernote.SimSearcher.prototype.handleActive = function() {
    Evernote.logger.debug( "SimSearcher.handleActive()" );
    this._contentScript.prependMessage( { anchor : this._impl.getAnchor(), styles : this._impl.getContentStyles() },
                                        Evernote.localizer.getMessage( "searchHelperSearching" ) );
};

Evernote.SimSearcher.prototype.handleResult = function() {
    Evernote.logger.debug( "SimSearcher.handleResult()" );

    var totalNotes = this._result.totalNotes;
    var authToken = Evernote.context.authenticationToken;

    if ( authToken ) {
        var resultUrl = Evernote.getEvernoteSearchUrl( this._query ).replace( /^https?:\/\/[^\/]+/, "" );
        resultUrl = Evernote.getSetAuthTokenUrlForSearch( authToken, resultUrl );

        this._contentScript.prependMessage( { anchor : this._impl.getAnchor(), styles : this._impl.getContentStyles() },
                                            Evernote.localizer.getMessage( "searchHelperFoundNotes", [ resultUrl, totalNotes ] ) );

        if ( Evernote.tabManager.getSelectedTab() == this._tab ) {
            Evernote.simSearchStorage.updateToolbar( this._tab.linkedPanel );
        }
    }
};

Evernote.SimSearcher.prototype.handleMissingAuthTokenError = function() {
    Evernote.logger.debug( "SimSearcher.handleMissingAuthTokenError()" );
    this._contentScript.prependErrorMessage( { anchor : this._impl.getAnchor(), styles : this._impl.getContentStyles() },
                                             Evernote.localizer.getMessage( "authPersistenceError" ) );
};

Evernote.SimSearcher.prototype.handleAuthenticationError = function() {
    Evernote.logger.debug( "SimSearcher.handleAuthenticationError()" );
    this._contentScript.prependErrorMessage( { anchor : this._impl.getAnchor(), styles : this._impl.getContentStyles() },
                                             Evernote.localizer.getMessage( "searchHelperLoginMessage", [ Evernote.getAccountUrl(), "javascript:" ] ) );
};

Evernote.SimSearcher.prototype.handleErrors = function() {
    Evernote.logger.debug( "SimSearcher.handleErrors()" );
    this._contentScript.prependMessage( { anchor : this._impl.getAnchor(), styles : this._impl.getContentStyles() },
                                        Evernote.localizer.getMessage( "searchHelperNotesNotFound" ) );
};

Evernote.SimSearcher.prototype.handleTransportError = function() {
    Evernote.logger.debug( "SimSearcher.handleTransportError()" );
    this._contentScript.prependErrorMessage( { anchor : this._impl.getAnchor(), styles : this._impl.getContentStyles() },
                                             Evernote.localizer.getMessage( "searchHelperNoTransportError" ) );
};

Evernote.SimSearcher.prototype.handleNoResult = function() {
    Evernote.logger.debug( "SimSearcher.handleNoResult()" );

    this._contentScript.prependMessage( { anchor : this._impl.getAnchor(), styles : this._impl.getContentStyles() },
                                        Evernote.localizer.getMessage( "searchHelperNotesNotFound" ) );

    if ( Evernote.tabManager.getSelectedTab() == this._tab ) {
        Evernote.toolbarManager.updateSimSearchBadge( 0 );
    }
};

Evernote.SimSearcher.prototype.handleVersionConflictError = function() {
    Evernote.logger.debug( "SimSearcher.handleVersionConflictError()" );
    this._contentScript.prependErrorMessage( { anchor : this._impl.getAnchor(), styles : this._impl.getContentStyles() },
                                             Evernote.localizer.getMessage( "checkVersionWarning" ) );
};

Evernote.SimSearcher.prototype.isActive = function() {
    return this._proc && this._proc.readyState > 1 && this._proc.readyState < 4;
};

Evernote.SimSearcher.prototype.hasResult = function() {
    return this._result && typeof this._result.totalNotes == "number" && this._result.totalNotes > 0;
};

Evernote.SimSearcher.prototype.hasErrors = function() {
    return this._response && this._response.isError();
};

Evernote.SimSearcher.prototype.isTransportError = function() {
    return this._request && this._request.readyState == 0 || ( this._request.readyState == 4 && ( this._request.status < 200 || this._request.status >= 400 ) );
};

Evernote.SimSearcher.prototype.isComplete = function() {
    return this._proc && this._proc.readyState == 4;
};

Evernote.SimSearcher.prototype.getResult = function() {
    return this._result;
};

Evernote.SimSearcher.prototype.getQuery = function() {
    return this._query;
};

Evernote.SimSearcher.prototype.getNoteFilter = function() {
    if ( !this._noteFilter ) {
        this._noteFilter = new Evernote.NoteFilter( { _order : Evernote.NoteSortOrderTypes.UPDATED,
                                                      _ascending : true,
                                                      _fuzzy : true } );
    }
    return this._noteFilter;
};

Evernote.SimSearcher.prototype.setForceReload = function( val ) {
    this._forceReload = (val) ? true : false;
};

Evernote.SimSearcher.prototype.getResultNotes = function() {
    if ( this._result ) {
         return this._result.totalNotes;
    }

    return 0;
};