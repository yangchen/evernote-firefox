//"use strict";

Evernote.YandexComSimSearchImpl = function YandexComSimSearchImpl( doc ) {
    this.parent.initialize.call( this, doc );
};

Evernote.inherit( Evernote.YandexComSimSearchImpl, Evernote.SimSearchImpl, true );

Evernote.YandexComSimSearchImpl.isResponsibleFor = function( url ) {
    return (this.queryFromUrl( url )) ? true : false;
};

Evernote.YandexComSimSearchImpl.queryFromUrl = function( url ) {
    Evernote.logger.debug( "YandexComSimSearchImpl.queryFromUrl()" );

    if ( typeof url == "string" ) {
        var domain = Evernote.UrlHelpers.urlDomain( url );
        var path = Evernote.UrlHelpers.urlPath( url );

        if ( typeof domain == "string" && typeof path == "string" ) {
            if ( this.isSearchYandex( domain, path ) || this.isImagesSearchYandex( domain, path ) || this.isVideosSearchYandex( domain, path ) ) {
                var res = Evernote.UrlHelpers.urlQueryValue( "text", url );
                return (typeof res == "string" && res.length > 0) ? res : null;
            }
        }
    }
    
    return null;
};

Evernote.YandexComSimSearchImpl.isSearchYandex = function( domain, path ) {
    return (domain.toLowerCase().indexOf( "www.yandex.com" ) == 0 || domain.toLowerCase().indexOf( "yandex.com" ) == 0) && path.toLowerCase().indexOf( "/yandsearch" ) == 0;
};

Evernote.YandexComSimSearchImpl.isImagesSearchYandex = function( domain, path ) {
    return domain.toLowerCase().indexOf( "images.yandex.com" ) >= 0 && path.toLowerCase().indexOf( "/yandsearch" ) == 0;
};

Evernote.YandexComSimSearchImpl.isVideosSearchYandex = function( domain, path ) {
    return domain.toLowerCase().indexOf( "video.yandex.com" ) >= 0 && path.toLowerCase().indexOf( "/search" ) == 0;
};

Evernote.YandexComSimSearchImpl.getQueryKey = function() {
    return "text";
};

Evernote.YandexComSimSearchImpl.prototype.getAnchor = function() {
    return ".b-page-layout";
};

Evernote.YandexComSimSearchImpl.prototype.getQuery = function() {
    return this.constructor.queryFromUrl( this._doc.location.href );
};

Evernote.YandexComSimSearchImpl.prototype.getContentStyles = function() {
    if ( this.constructor.isSearchYandex( this._domain, this._path ) ) {
        return { marginLeft: "10px", marginTop: "20px", marginRight: "10px", marginBottom: "-20px" };
    }
    else if ( this.constructor.isImagesSearchYandex( this._domain, this._path ) ) {
        return { marginLeft: "10px", marginTop: "0px", marginRight: "10px", marginBottom: "-10px" };
    }
    else if ( this.constructor.isVideosSearchYandex( this._domain, this._path ) ) {
        return { marginLeft: "10px", marginTop: "20px", marginRight: "10px", marginBottom: "-10px" };
    }

    return { };
};