//"use strict";

Evernote.SimSearchImplFactory = {
    getImplementationFor : function( url ) {
        var reg = this.ClassRegistry;
        for ( var i = 0; i < reg.length; i++ ) {
            if ( typeof reg[ i ] == "function" && typeof reg[ i ].isResponsibleFor == "function"
                 && reg[ i ].isResponsibleFor( url ) ) {
                return reg[ i ];
            }
        }

        return null;
    }
};

Evernote.SimSearchImplFactory.ClassRegistry = [ ];