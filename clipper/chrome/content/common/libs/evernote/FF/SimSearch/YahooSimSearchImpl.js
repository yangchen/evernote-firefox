//"use strict";

Evernote.YahooSimSearchImpl = function YahooSimSearchImpl( doc ) {
    this.parent.initialize.call( this, doc );
};

Evernote.inherit( Evernote.YahooSimSearchImpl, Evernote.SimSearchImpl, true );

Evernote.YahooSimSearchImpl.isResponsibleFor = function( url ) {
    return (this.queryFromUrl( url )) ? true : false;
};

Evernote.YahooSimSearchImpl.queryFromUrl = function( url ) {
    Evernote.logger.debug( "YahooSimSearchImpl.queryFromUrl()" );

    if ( typeof url == "string" ) {
        var domain = Evernote.UrlHelpers.urlDomain( url );
        var path = Evernote.UrlHelpers.urlPath( url );

        if ( typeof domain == "string" && typeof path == "string" ) {
            if ( this.isYahooSearch( domain, path ) || this.isYahooImagesSearch( domain, path ) || this.isYahooVideosSearch( domain, path ) ) {
                var res = Evernote.UrlHelpers.urlQueryValue( "p", url );
                return (typeof res == "string" && res.length > 0) ? res : null;
            }
        }
    }
    
    return null;
};

Evernote.YahooSimSearchImpl.isYahooSearch = function( domain, path ) {
    var domainLC = domain.toLowerCase();
    return domainLC.indexOf( "search.yahoo.com" ) >= 0 && domainLC.indexOf( "images" ) < 0
           && domainLC.indexOf( "video" ) < 0 && path.toLowerCase().indexOf( "/search" ) == 0;
};

Evernote.YahooSimSearchImpl.isYahooImagesSearch = function( domain, path ) {
    return domain.toLowerCase().indexOf( "images.search.yahoo.com" ) >= 0 && path.toLowerCase().indexOf( "/search" ) == 0;
};

Evernote.YahooSimSearchImpl.isYahooVideosSearch = function( domain, path ) {
    return domain.toLowerCase().indexOf( "video.search.yahoo.com" ) >= 0 && path.toLowerCase().indexOf( "/search" ) == 0;
};

Evernote.YahooSimSearchImpl.getQueryKey = function() {
    return "p";
};

Evernote.YahooSimSearchImpl.prototype.getAnchor = function() {
    if ( this.constructor.isYahooSearch( this._domain, this._path ) || this.constructor.isYahooImagesSearch( this._domain, this._path ) ) {
        return "#results";
    }
    else if ( this.constructor.isYahooVideosSearch( this._domain, this._path ) ) {
        return "#r-m";
    }

    return "";
};

Evernote.YahooSimSearchImpl.prototype.getQuery = function() {
    return this.constructor.queryFromUrl( this._doc.location.href );
};

Evernote.YahooSimSearchImpl.prototype.getContentStyles = function() {
    if ( this.constructor.isYahooSearch( this._domain, this._path ) ) {
        return { marginLeft : "195px", marginTop : "-10px", marginRight : "5px" };
    }
    else if ( this.constructor.isYahooImagesSearch( this._domain, this._path ) ) {
        return { marginLeft : "0px", marginBottom : "-5px" };
    }
    else if ( this.constructor.isYahooVideosSearch( this._domain, this._path ) ) {
        return { marginLeft : "205px", marginTop : "-10px" };
    }

    return { };
};