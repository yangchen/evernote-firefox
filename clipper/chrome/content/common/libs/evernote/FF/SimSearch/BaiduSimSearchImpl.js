//"use strict";

Evernote.BaiduSimSearchImpl = function BaiduSimSearchImpl( doc ) {
    this.parent.initialize.call( this, doc );
};

Evernote.inherit( Evernote.BaiduSimSearchImpl, Evernote.SimSearchImpl, true );

Evernote.BaiduSimSearchImpl.isResponsibleFor = function( url ) {
    return (this.queryFromUrl( url )) ? true : false;
};

Evernote.BaiduSimSearchImpl.queryFromUrl = function( url ) {
    Evernote.logger.debug( "BaiduSimSearchImpl.queryFromUrl()" );

    if ( typeof url == "string" ) {
        var domain = Evernote.UrlHelpers.urlDomain( url );
        var path = Evernote.UrlHelpers.urlPath( url );

        if ( typeof domain == "string" && typeof path == "string" ) {
            if ( this.isBaiduDomain( domain ) &&
                 (this.isBaiduSearchPath( path ) || this.isBaiduImagesSearchPath( path ) || this.isBaiduVideosSearchPath( path )) ) {
                try {
                    var utf8Converter = Components.classes[ "@mozilla.org/intl/utf8converterservice;1" ].
                        getService( Components.interfaces.nsIUTF8ConverterService );
                    var convertedUrl = utf8Converter.convertURISpecToUTF8( url, "gb2312" );
                }
                catch ( e ) {
                    convertedUrl = url;
                }

                var res = Evernote.UrlHelpers.urlQueryValue( this.getQueryKey( path ), convertedUrl );
                return (typeof res == "string" && res.length > 0) ? res : null;
            }
        }
    }

    return null;
};

Evernote.BaiduSimSearchImpl.isBaiduDomain = function( domain ) {
    return domain.toLowerCase().indexOf( "baidu.com" ) >= 0;
};

Evernote.BaiduSimSearchImpl.isBaiduSearchPath = function( path ) {
    return path.toLowerCase().indexOf( "/s" ) == 0;
};

Evernote.BaiduSimSearchImpl.isBaiduImagesSearchPath = function( path ) {
    return path.toLowerCase().indexOf( "/i" ) == 0;
};

Evernote.BaiduSimSearchImpl.isBaiduVideosSearchPath = function( path ) {
    return path.toLowerCase().indexOf( "/v" ) == 0;
};

Evernote.BaiduSimSearchImpl.getQueryKey = function( path ) {
    if ( this.isBaiduSearchPath( path ) ) {
        return "wd";
    }
    else if ( this.isBaiduImagesSearchPath( path ) || this.isBaiduVideosSearchPath( path ) ) {
        return "word";
    }

    return "";
};

Evernote.BaiduSimSearchImpl.prototype.getAnchor = function() {
    if ( this.constructor.isBaiduSearchPath( this._path ) ) {
        return "#container";
    }
    else if ( this.constructor.isBaiduImagesSearchPath( this._path ) ) {
        return "#imgContainer";
    }
    else if ( this.constructor.isBaiduVideosSearchPath( this._path ) ) {
        return "#results";
    }

    return "";
};

Evernote.BaiduSimSearchImpl.prototype.getQuery = function() {
    return this.constructor.queryFromUrl( this._doc.location.href );
};

Evernote.BaiduSimSearchImpl.prototype.getContentStyles = function() {
    if ( this.constructor.isBaiduSearchPath( this._path ) ) {
        return { marginLeft : "15px", marginTop : "-20px", marginBottom : "10px", marginRight : "5px" };
    }
    else if ( this.constructor.isBaiduImagesSearchPath( this._path ) ) {
        return { marginLeft : "20px", marginTop : "-15px", marginBottom : "-10px", marginRight : "5px" };
    }
    else if ( this.constructor.isBaiduVideosSearchPath( this._path ) ) {
        return { marginLeft : "0px", marginBottom : "-10px" };
    }

    return { };
};