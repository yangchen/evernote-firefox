//"use strict";

Evernote.YahooCNSimSearchImpl = function YahooCNSimSearchImpl( doc ) {
    this.parent.initialize.call( this, doc );
};

Evernote.inherit( Evernote.YahooCNSimSearchImpl, Evernote.SimSearchImpl, true );

Evernote.YahooCNSimSearchImpl.isResponsibleFor = function( url ) {
    return (this.queryFromUrl( url )) ? true : false;
};

Evernote.YahooCNSimSearchImpl.queryFromUrl = function( url ) {
    Evernote.logger.debug( "YahooCNSimSearchImpl.queryFromUrl()" );

    if ( typeof url == "string" ) {
        var domain = Evernote.UrlHelpers.urlDomain( url );
        var path = Evernote.UrlHelpers.urlPath( url );

        if ( typeof domain == "string" && typeof path == "string" ) {
            if ( this.isYahooCN( domain, path ) ) {
                var res = Evernote.UrlHelpers.urlQueryValue( this.getQueryKey(), url );
                return (typeof res == "string" && res.length > 0) ? res : null;
            }
        }
    }

    return null;
};

Evernote.YahooCNSimSearchImpl.isYahooCN = function( domain, path ) {
    return domain.toLowerCase().indexOf( "yahoo.cn" ) >= 0 && path.toLowerCase().indexOf( "/s" ) == 0;
};

Evernote.YahooCNSimSearchImpl.getQueryKey = function() {
    return "q";
};

Evernote.YahooCNSimSearchImpl.prototype.getAnchor = function() {
    return ".content";
};

Evernote.YahooCNSimSearchImpl.prototype.getQuery = function() {
    return this.constructor.queryFromUrl( this._doc.location.href );
};

Evernote.YahooCNSimSearchImpl.prototype.getContentStyles = function() {
    return { marginLeft: "50px", marginTop : "-15px", marginRight: "10px", marginBottom: "-20px" };
};