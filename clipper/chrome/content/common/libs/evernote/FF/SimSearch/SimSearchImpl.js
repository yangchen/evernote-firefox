//"use strict";

Evernote.SimSearchImpl = function SimSearchImpl( /*doc*/ ) {
};

Evernote.SimSearchImpl.isResponsibleFor = function( /*url*/ ) {
    return false;
};

Evernote.SimSearchImpl.prototype._doc = null;
Evernote.SimSearchImpl.prototype._domain = "";
Evernote.SimSearchImpl.prototype._path = "";

Evernote.SimSearchImpl.prototype.handleInheritance = function( child/*, parent*/ ) {
    Evernote.SimSearchImplFactory.ClassRegistry.push( child );
};

Evernote.SimSearchImpl.prototype.initialize = function( doc ) {
    this._doc = doc;
    this._domain = Evernote.UrlHelpers.urlDomain( doc.location.href );
    this._path = Evernote.UrlHelpers.urlPath( doc.location.href );
};

Evernote.SimSearchImpl.prototype.getAnchor = function() {
    return "";
};

Evernote.SimSearchImpl.prototype.getQuery = function() {
    return "";
};

Evernote.SimSearchImpl.prototype.getContentStyles = function() {
    return { };
};

Evernote.SimSearchImpl.prototype.isReloadRequired = function( /*currUrl, url*/ ) {
    return true;
};