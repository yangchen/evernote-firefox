//"use strict";

Evernote.BingSimSearchImpl = function BingSimSearchImpl( doc ) {
    this.parent.initialize.call( this, doc );
};

Evernote.inherit( Evernote.BingSimSearchImpl, Evernote.SimSearchImpl, true );

Evernote.BingSimSearchImpl.isResponsibleFor = function( url ) {
    return (this.queryFromUrl( url )) ? true : false;
};

Evernote.BingSimSearchImpl.queryFromUrl = function( url ) {
    Evernote.logger.debug( "BingSimSearchImpl.queryFromUrl()" );

    if ( typeof url == "string" ) {
        var domain = Evernote.UrlHelpers.urlDomain( url );
        if ( typeof domain == "string" ) {
             if ( this.isBingDomain( domain ) ) {
                var res = Evernote.UrlHelpers.urlQueryValue( this.getQueryKey(), url );
                return (typeof res == "string" && res.length > 0) ? res : null;
            }
        }
    }

    return null;
};

Evernote.BingSimSearchImpl.isBingDomain = function( domain ) {
    return domain.toLowerCase().indexOf( "www.bing." ) == 0
};

Evernote.BingSimSearchImpl.isBingSearchPath = function( path ) {
    return path.toLowerCase().indexOf( "/search" ) == 0;
};

Evernote.BingSimSearchImpl.isBingImagesSearchPath = function( path ) {
    return path.toLowerCase().indexOf( "/images/search" ) == 0;
};

Evernote.BingSimSearchImpl.isBingVideosSearchPath = function( path ) {
    return path.toLowerCase().indexOf( "/videos/search" ) == 0;
};

Evernote.BingSimSearchImpl.isBingNewsSearchPath = function( path ) {
    return path.toLowerCase().indexOf( "/news/search" ) == 0;
};

Evernote.BingSimSearchImpl.getQueryKey = function() {
    return "q";
};

Evernote.BingSimSearchImpl.prototype.getAnchor = function() {
    if ( this.constructor.isBingSearchPath( this._path ) ) {
         return "#results_area";
    }
    else if ( this.constructor.isBingImagesSearchPath( this._path ) ) {
        return "#vm_c";
    }
    else if ( this.constructor.isBingVideosSearchPath( this._path ) ) {
        return "#vm_c";
    }
    else if ( this.constructor.isBingNewsSearchPath( this._path ) ) {
        return ".NewsResultSet";
    }

    return "";
};

Evernote.BingSimSearchImpl.prototype.getQuery = function() {
    return this.constructor.queryFromUrl( this._doc.location.href );
};

Evernote.BingSimSearchImpl.prototype.getContentStyles = function() {
    if ( this.constructor.isBingSearchPath( this._path ) ) {
        return { marginLeft : "10px", marginTop : "-10px" };
    }
    else if ( this.constructor.isBingImagesSearchPath( this._path ) ) {
        return { marginLeft : "0px", marginBottom : "10px", marginRight : "5px" };
    }
    else if ( this.constructor.isBingVideosSearchPath( this._path ) ) {
        return { marginLeft : "0px", marginBottom : "5px" };
    }
    else if ( this.constructor.isBingNewsSearchPath( this._path ) ) {
        return { marginLeft : "0px", marginTop : "20px" };
    }

    return { };
};

Evernote.BingSimSearchImpl.prototype.isReloadRequired = function( currUrl, url ) {
    return Evernote.UrlHelpers.urlWithoutHash( currUrl ) != Evernote.UrlHelpers.urlWithoutHash( url )
};