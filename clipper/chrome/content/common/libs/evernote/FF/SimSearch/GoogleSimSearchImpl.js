//"use strict";

Evernote.GoogleSimSearchImpl = function GoogleSimSearchImpl( doc ) {
    this.parent.initialize.call( this, doc );
};

Evernote.inherit( Evernote.GoogleSimSearchImpl, Evernote.SimSearchImpl, true );

Evernote.GoogleSimSearchImpl.isResponsibleFor = function( url ) {
    return (this.queryFromUrl( url )) ? true : false;
};

Evernote.GoogleSimSearchImpl.queryFromUrl = function( url ) {
    Evernote.logger.debug( "GoogleSimSearchImpl.queryFromUrl()" );

    if ( typeof url == "string" ) {
        var domain = Evernote.UrlHelpers.urlDomain( url );
        if ( typeof domain == "string" ) {
            if ( this.isGoogleDomain( domain ) ) {
                url = ( url.indexOf( "#" ) > 0 ) ? url.replace( /(\?.*)?\#/, "?" ) : url;
                var res = Evernote.UrlHelpers.urlQueryValue( this.getQueryKey(), url );
                return (typeof res == "string" && res.length > 0) ? res : null;
            }
        }
    }
    
    return null;
};

Evernote.GoogleSimSearchImpl.isGoogleDomain = function( domain ) {
    return domain.toLowerCase().indexOf( "www.google." ) == 0;
};

Evernote.GoogleSimSearchImpl.getQueryKey = function() {
    return "q";
};

Evernote.GoogleSimSearchImpl.prototype.getAnchor = function() {
    return "#res";
};

Evernote.GoogleSimSearchImpl.prototype.getQuery = function() {
    return this.constructor.queryFromUrl( this._doc.location.href );
};

Evernote.GoogleSimSearchImpl.prototype.getContentStyles = function() {
    return { marginLeft : "0px", marginTop : "-10px", marginBottom : "-5px" };
};