//"use strict";

Evernote.ClipStorer = function ClipStorer( processor ) {
    this._processor = processor;
};

Evernote.ClipStorer.CLIPPER_FOLDER_NAME = "evernoteclips";
Evernote.ClipStorer.NOTE_MATCHER = new RegExp( "^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$" );

Evernote.ClipStorer.prototype._processor = null;

Evernote.ClipStorer.prototype.enumDirNotes = function() {
    try {
        var dirName = Evernote.fileManager.getProfileDirPath() + Evernote.SLASH + this.constructor.CLIPPER_FOLDER_NAME;
        Evernote.fileManager.createDir( dirName );

        var notesNames = Evernote.fileManager.enumerateDirFiles( dirName );
        var notes = [ ];

        for ( var i = 0; i < notesNames.length; ++i ) {
            if ( notesNames[ i ].match( this.constructor.NOTE_MATCHER ) ) {
                notes.push( notesNames[ i ] );
            }
        }

        if ( notes.length > 0 ) {
            this._processor.processDirNotes( notes );
        }
    }
    catch ( e ) {
        Evernote.logger.error( "ClipStorer.enumDirNotes() failed: error = " + e );
    }
};

Evernote.ClipStorer.prototype.saveNote = function( clipNote, noteId ) {
    Evernote.fileManager.writeFile( this.getNotePath( noteId ), clipNote.toStorable(), true );
};

Evernote.ClipStorer.prototype.loadNote = function( noteId ) {
    var fileContent = Evernote.fileManager.readFile( this.getNotePath( noteId ) );
    return (fileContent) ? (new Evernote.ClipNote( fileContent )) : null;
};

Evernote.ClipStorer.prototype.deleteNote = function( noteId ) {
    Evernote.fileManager.deleteFile( this.getNotePath( noteId ) );
};

Evernote.ClipStorer.prototype.getNotePath = function( noteId ) {
    return Evernote.fileManager.getProfileDirPath() + Evernote.SLASH + this.constructor.CLIPPER_FOLDER_NAME + Evernote.SLASH + noteId;
};

