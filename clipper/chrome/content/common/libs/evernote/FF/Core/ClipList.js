//"use strict";

Evernote.ProcessingClipList = function ProcessingClipList() {
};

Evernote.ProcessingClipList.prototype.addNote = function( noteId ) {
    Evernote.logger.debug( "ProcessingClipList.addNote(): noteId = " + noteId );

    var noteList = Evernote.prefsManager.getPref( Evernote.PrefKeys.PROCESSING_CLIPLIST, "string" );
    if ( noteList == null ) {
        noteList = noteId + ";";
    }
    else {
        noteList += noteId + ";";
    }

    Evernote.prefsManager.addPref( Evernote.PrefKeys.PROCESSING_CLIPLIST, noteList );
};

Evernote.ProcessingClipList.prototype.removeNote = function( noteId ) {
    Evernote.logger.debug( "ProcessingClipList.removeNote(): noteId = " + noteId );

    var noteList = Evernote.prefsManager.getPref( Evernote.PrefKeys.PROCESSING_CLIPLIST, "string" );
    if ( noteList == null ) {
        return;
    }

    noteList = noteList.replace( noteId + ";", "" );
    Evernote.prefsManager.addPref( Evernote.PrefKeys.PROCESSING_CLIPLIST, noteList );
};

Evernote.ProcessingClipList.prototype.hasNote = function( noteId ) {
    Evernote.logger.debug( "ProcessingClipList.hasNote(): noteId = " + noteId );

    var noteList = Evernote.prefsManager.getPref( Evernote.PrefKeys.PROCESSING_CLIPLIST, "string" );
    if ( noteList == null ) {
        return false;
    }

    return noteList.indexOf( noteId ) != -1;
};

Evernote.ProcessingClipList.prototype.getSize = function() {
    var noteList = Evernote.prefsManager.getPref( Evernote.PrefKeys.PROCESSING_CLIPLIST, "string" );
    if ( noteList == null ) {
        return 0;
    }
    
    return noteList.split( ";" ).length - 1;
};