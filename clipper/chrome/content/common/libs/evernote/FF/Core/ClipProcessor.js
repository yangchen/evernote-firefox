//"use strict";

Evernote.ClipProcessor = function ClipProcessor() {
    this.__defineGetter__( "sender", this.getSender );
    this.__defineGetter__( "storer", this.getStorer );
    this.__defineGetter__( "clipList", this.getClipList );
    this.__defineGetter__( "connObserver", this.getConnectionObserver );
    this.initialize();
};

Evernote.ClipProcessor.UNPROCESSED_NOTE = 0;
Evernote.ClipProcessor.PROCESSING_NOTE = 1;
Evernote.ClipProcessor.WAITING_NETWORK_NOTE = 2;
Evernote.ClipProcessor.ENUM_DIR_TIMEOUT = 1000;
Evernote.ClipProcessor.UPDATE_TOOLBAR_TIMEOUT = 1000;
Evernote.ClipProcessor.PROCESS_QUEUE_TIMEOUT = 1000;

Evernote.ClipProcessor.prototype._tasks = null;
Evernote.ClipProcessor.prototype._enumDirTimer = null;
Evernote.ClipProcessor.prototype._updateToolbarTimer = null;
Evernote.ClipProcessor.prototype._processQueueTimer = null;

Evernote.ClipProcessor.prototype.initialize = function() {
    Evernote.logger.debug( "ClipProcessor.initialize()" );

    this._tasks = { };
    this.startProcessing();

    var self = this;
    this._enumDirTimer = setInterval( function() {
        self.storer.enumDirNotes();
    }, this.constructor.ENUM_DIR_TIMEOUT );

    this._updateToolbarTimer = setInterval( function() {
        self.updateToolbar();
    }, this.constructor.UPDATE_TOOLBAR_TIMEOUT );

    Evernote.closeListener.addListener( this );
};

Evernote.ClipProcessor.prototype.processNote = function( clipNote, noteId ) {
    Evernote.logger.debug( "ClipProcessor.processNote(): noteId = " + noteId );

    this.clipList.addNote( noteId );
    this.storer.saveNote( clipNote, noteId );
        
    var self = this;
    this.addClipTask( noteId, function() {
            self.sender.send( clipNote, noteId );
        }
    );
};

Evernote.ClipProcessor.prototype.processDirNotes = function( notesId ) {
    for ( var i in notesId ) {
        this.processDirNote( notesId[ i ] );
    }
};

Evernote.ClipProcessor.prototype.processDirNote = function( noteId ) {
    if ( this.clipList.hasNote( noteId ) ) {
        return;
    }

    Evernote.logger.debug( "ClipProcessor.processDirNote(): noteId = " + noteId );

    var self = this;
    this.clipList.addNote( noteId );
    this.addClipTask( noteId, function() {
            var note = self.storer.loadNote( noteId );
            if ( note ) {
                self.sender.send( note, noteId );
            }
            else {
                self.clipList.removeNote( noteId );
            }
        }
    );
};

Evernote.ClipProcessor.prototype.startProcessing = function() {
    Evernote.logger.debug( "ClipProcessor.startProcessing()" );
    
    var self = this;
    this._processQueueTimer = setInterval( function() {
         for ( var i in self._tasks ) {
            var task = self._tasks[ i ];

            if ( task && task.state == self.constructor.UNPROCESSED_NOTE ) {
                task.state = self.constructor.PROCESSING_NOTE;
                task.action();
                break;
            }
        }
    }, this.constructor.PROCESS_QUEUE_TIMEOUT );
};

Evernote.ClipProcessor.prototype.onSuccess = function( noteId ) {
    this.storer.deleteNote( noteId );
    this.clipList.removeNote( noteId );
    delete this._tasks[ noteId ];
};

Evernote.ClipProcessor.prototype.onCancel = function( noteId ) {
    this.sender.onCancel( noteId );
    this.onSuccess( noteId );
};

Evernote.ClipProcessor.prototype.onRetry = function( noteId ) {
    this.sender.onRetry( noteId );
    if ( typeof this._tasks[ noteId ] != "undefined" ) {
        this._tasks[ noteId ].state = this.constructor.UNPROCESSED_NOTE;
    }
};

Evernote.ClipProcessor.prototype.updateToolbar = function() {
    Evernote.toolbarManager.updateProcessingBadge( this.clipList.getSize() );
};

Evernote.ClipProcessor.prototype.addClipTask = function( id, fn ) {
    this._tasks[ id ] = { id : id,
                          action : fn,
                          state : this.constructor.UNPROCESSED_NOTE };
};

Evernote.ClipProcessor.prototype.onConnectionAppear = function() {
    Evernote.logger.debug( "ClipProcessor.onConnectionAppear()" );

    for ( var id in this._tasks ) {
        if ( this._tasks[ id ].state == this.constructor.WAITING_NETWORK_NOTE ) {
            this._tasks[ id ].state = this.constructor.UNPROCESSED_NOTE;
        }
    }
};

Evernote.ClipProcessor.prototype.onConnectionDisappear = function( id ) {
    Evernote.logger.debug( "ClipProcessor.onConnectionDisappear(): id = " + id );

    if ( typeof this._tasks[ id ] != "undefined" ) {
        this._tasks[ id ].state = this.constructor.WAITING_NETWORK_NOTE;
    }

    this.connObserver.startObserve();
};

Evernote.ClipProcessor.prototype.stopTimers = function() {
    Evernote.logger.debug( "ClipProcessor.stopTimers()" );

    if ( this._enumDirTimer ) {
        clearInterval( this._enumDirTimer );
        this._enumDirTimer = null;
    }

    if ( this._updateToolbarTimer ) {
        clearInterval( this._updateToolbarTimer );
        this._updateToolbarTimer = null;
    }

    if ( this._processQueueTimer ) {
        clearInterval( this._processQueueTimer );
        this._processQueueTimer = null;
    }
};

Evernote.ClipProcessor.prototype.getSender = function() {
    if ( !this._sender ) {
        this._sender = new Evernote.ClipSender( this );
    }

    return this._sender;
};

Evernote.ClipProcessor.prototype.getStorer = function() {
    if ( !this._storer ) {
        this._storer = new Evernote.ClipStorer( this );
    }

    return this._storer;
};

Evernote.ClipProcessor.prototype.getClipList = function() {
    if ( !this._clipList ) {
        this._clipList = new Evernote.ProcessingClipList();
    }

    return this._clipList;
};

Evernote.ClipProcessor.prototype.getConnectionObserver = function() {
    if ( !this._connObserver ) {
        this._connObserver = new Evernote.ConnectionObserver( this );
    }
    
    return this._connObserver;
};

Evernote.ClipProcessor.prototype.onWindowClose = function() {
    Evernote.logger.debug( "ClipProcessor.onWindowClose()" );

    this.stopTimers();
    this.sender.onWindowClose();
    for ( var id in this._tasks ) {
        this.clipList.removeNote( id );
    }
};