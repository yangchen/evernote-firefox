//"use strict";

Evernote.ConnectionObserver = function ConnectionObserver( listener ) {
    this._listener = listener;
};

Evernote.ConnectionObserver.prototype._listener = null;
Evernote.ConnectionObserver.prototype._timerId = null;

Evernote.ConnectionObserver.prototype.startObserve = function() {
    // already observing
    if ( this._timerId != null ) {
        return;
    }
    
    this.observerTick();
};

Evernote.ConnectionObserver.prototype.doRequest = function() {
   Evernote.logger.debug( "ConnectionObserver.doRequest()" );

   var self = this;
   Evernote.remote.postJson( Evernote.getCountNotesUrl(), "",
                             function() {
                                 Evernote.logger.debug( "Connection appeared" );
                                 if ( self._listener && typeof self._listener.onConnectionAppear == "function" ) {
                                     self._listener.onConnectionAppear();
                                 }

                                 self._timerId = null;
                             },
                             function( xhr ) {
                                 // HTTP0 error
                                 if ( xhr.status == 0 ) {
                                     self.observerTick();
                                 }
                             },
                             true, false );
};

Evernote.ConnectionObserver.prototype.observerTick = function() {
    var self = this;
    this._timerId = setTimeout( function() {
        self.doRequest();
    }, 1000 );
};






