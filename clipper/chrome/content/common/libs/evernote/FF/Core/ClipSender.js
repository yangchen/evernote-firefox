//"use strict";

Evernote.ClipSender = function ClipSender( processor ) {
    this.initialize( processor );
};

Evernote.ClipSender.prototype._processor = null;
Evernote.ClipSender.prototype._procs = null;

Evernote.ClipSender.prototype.initialize = function( processor ) {
    Evernote.logger.debug( "ClipSender.initialize()" );

    this._processor = processor;
    this._procs = { };
};

Evernote.ClipSender.prototype.send = function( clipNote, noteId ) {
    Evernote.logger.debug( "ClipSender.send(): noteId = " + noteId );

    var notificator = Evernote.clipNotificator;
    notificator.showCopyNotification( content.document );
    var self = this;

    this._procs[ noteId ] = this.remoteClipNote( clipNote,
        function( response, textStatus ) {
            Evernote.logger.debug( "Note with id = " + noteId + " clipped successfully" );
            Evernote.logger.debug( response.toSource() );
            Evernote.logger.debug( textStatus.toSource() );

            notificator.addSuccessNotification( clipNote, response, noteId );
            self._processor.onSuccess( noteId );
            self.onSuccess( noteId );
        },
        function( response/*, textStatus*/ ) {
            Evernote.logger.error( "Error occured during clipping note " + noteId );

            // HTTP0 error
            if ( response.status == 0 ) {
                self._processor.onConnectionDisappear( noteId );
            }
            notificator.addFailureNotification( clipNote, response, noteId );
        }
    );
};

Evernote.ClipSender.prototype.remoteClipNote = function( clipNote, success, failure ) {
    Evernote.logger.debug( "ClipSender.remoteClipNote()" );

    if ( !(clipNote instanceof Evernote.ClipNote) ) {
        Evernote.logger.warn( "Tried to clip invalid object" );
        return;
    }

    return Evernote.remote.clip( clipNote,
        function( response, textStatus ) {
            Evernote.logger.debug( "Popup got response from the server regarding clipped note" );

            if ( response.isResult() ) {
                Evernote.logger.debug( "Successfully sent clip to the server" );
                if ( typeof success == 'function' ) {
                    success( response, textStatus );
                }
            }
            else if ( response.isError() ) {
                Evernote.logger.error( "Failed to send clip to the server" );

                var userErrors = response.selectErrors( function( error ) {
                                    if ( error instanceof Evernote.EvernoteError && (error.errorCode == Evernote.EDAMErrorCode.INVALID_AUTH
                                         || error.errorCode == Evernote.EDAMErrorCode.PERMISSION_DENIED || error.errorCode == Evernote.EDAMErrorCode.AUTH_EXPIRED ) ) {
                                        return true;
                                    }
                                 }
                );

                if ( userErrors instanceof Array && userErrors.length > 0 ) {
                    Evernote.logger.debug( "Authentication invalid" );
                    if ( typeof failure == "function" ) {
                        failure( response, textStatus );
                    }
                }
                else {
                    Evernote.logger.error( "Unexpected response when tried to clip" );
                    if ( typeof failure == 'function' ) {
                        failure( response, textStatus );
                    }
                }
            }
            else {
                Evernote.logger.error( "Unrecognized response when tried to send clip to the server" );
            }
        },
        function( xhr, textStatus, error ) {
            Evernote.logger.error( "Failed to send clip to server due to transport errors (" + ( (xhr && typeof xhr.status != 'undefined') ? xhr.status : 'UNKNOWN') + ")");
            if ( typeof failure == "function" ) {
                failure( xhr, error );
            }
    }, true );
};

Evernote.ClipSender.prototype.onWindowClose = function() {
    for ( var id in this._procs ) {
        if ( this._procs[ id ] ) {
            this._procs[ id ].abort();
        }
    }
};

Evernote.ClipSender.prototype.onSuccess = function( noteId ) {
     delete this._procs[ noteId ];
};

Evernote.ClipSender.prototype.onCancel = function( noteId ) {
    if ( this._procs[ noteId ] ) {
        this._procs[ noteId ].abort();
    }
    delete this._procs[ noteId ];
};

Evernote.ClipSender.prototype.onRetry = function( noteId ) {
    this.onCancel( noteId );
};