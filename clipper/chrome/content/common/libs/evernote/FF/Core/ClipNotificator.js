//"use strict";

Evernote.ClipNotificator = function ClipNotificator() {
    this.__defineGetter__( "notifyManager", this.getNotifyManager );
    this._notifyQueue = { };
};

Evernote.ClipNotificator.WAIT_CONTAINER_ID = "evernoteContentClipperWait";
Evernote.ClipNotificator.SHOW_WAIT_MIN_TIME = 2000;

Evernote.ClipNotificator.prototype._waitFlag = false;
Evernote.ClipNotificator.prototype._notifyQueue = null;
Evernote.ClipNotificator.prototype._notifyManager = null;

Evernote.ClipNotificator.prototype.showCopyNotification = function( doc ) {
    Evernote.logger.debug( "ClipNotificator.showCopyNotification()" );

    try {
        new Evernote.AbstractContentScript().injectStyleSheets( [ "resource://evernote_styles/contentclipper.css" ], doc );

        var wait = this.getWaitContainer( doc, Evernote.localizer.getMessage( "contentclipper_clipping" ) );
        wait.style.opacity = "1";

        if ( doc.body ) {
            doc.body.appendChild( wait );
        }

        var self = this;
        setTimeout( function() {
            self.clearWait( doc );
            }, this.constructor.SHOW_WAIT_MIN_TIME );

        this._waitFlag = true;
    }
    catch ( e ) {
        Evernote.logger.error( "ClipNotificator.showCopyNotification() failed: error = " + e );
        this._waitFlag = false;
    }
};

Evernote.ClipNotificator.prototype.addSuccessNotification = function( clipNote, response, noteId ) {
    Evernote.logger.debug( "ClipNotificator.addSuccessNotification(): noteId = " + noteId );

    var self = this;
    var successFn = function() {
          if ( Evernote.context.options.clipNotificationEnabled ) {
              Evernote.logger.debug( "Submit success notification for note with id " + noteId );
              self.notifyManager.notifySucess( response.result.note, noteId );
          }
    };

    if ( this._waitFlag ) {
        this._notifyQueue[ noteId ] = successFn;
    }
    else {
        successFn();
    }
};

Evernote.ClipNotificator.prototype.addFailureNotification = function( clipNote, response, noteId ) {
    Evernote.logger.debug( "ClipNotificator.addFailureNotification(): noteId = " + noteId );

    var self = this;
    var failureFn = function() {
        Evernote.logger.debug( "Submit failure notification for note with id " + noteId );
        self.notifyManager.notifyFailure( clipNote, response, noteId );
    };

   if ( this._waitFlag ) {
        this._notifyQueue[ noteId ] = failureFn;
   }
    else {
        failureFn();
   }
};

Evernote.ClipNotificator.prototype.getWaitContainer = function( doc, msg ) {
    Evernote.logger.debug( "ClipNotificator.getWaitContainer()" );

    var container = doc.getElementById( this.constructor.WAIT_CONTAINER_ID );
    if ( !container ) {
        container = doc.createElement( "evernotediv" );
        container.id = this.constructor.WAIT_CONTAINER_ID;

        var wait = doc.createElement( "div" );
        wait.id = this.constructor.WAIT_CONTAINER_ID + "Content";
        container.appendChild( wait );

        var center = doc.createElement( "center" );
        wait.appendChild( center );

        var spinner = doc.createElement( "img" );
        spinner.setAttribute( "src", "resource://evernote_images/icon_scissors.png" );
        center.appendChild( spinner );

        var text = doc.createElement( "span" );
        text.id = this.constructor.WAIT_CONTAINER_ID + "Text";
        center.appendChild( text );

        container._waitMsgBlock = text;
        container._waitMsgBlock.appendChild( doc.createTextNode( msg ) );
    }

    return container;
};

Evernote.ClipNotificator.prototype.clearWait = function( doc ) {
    Evernote.logger.debug( "ClipNotificator.clearWait()" );

    var wait = doc.getElementById( this.constructor.WAIT_CONTAINER_ID );
    if ( wait ) {
        wait.style.opacity = "0";
        setTimeout( function() {
            if ( wait.parentNode ) {
                wait.parentNode.removeChild( wait );
            }
        }, 1000 );
    }

    this._waitFlag = false;
    this.processNotifications();
};

Evernote.ClipNotificator.prototype.processNotifications = function() {
    Evernote.logger.debug( "ClipNotificator.processNotifications()" );

    for ( var key in this._notifyQueue ) {
        if ( typeof this._notifyQueue[ key ] == "function" ) {
            this._notifyQueue[ key ]();
        }
        delete this._notifyQueue[ key ];
    }
};

Evernote.ClipNotificator.prototype.getNotifyManager = function() {
    if ( !this._notifyManager ) {
        this._notifyManager = new Evernote.NotificationManager();
    }

    return this._notifyManager;
};

