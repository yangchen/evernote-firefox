//"use strict";

Evernote.UploadNotifier = function ( params ) {
    this.initialize( params );
};

Evernote.UploadNotifier.NOTIFICATION_CLOSE_TIMEOUT = 6000;

Evernote.UploadNotifier.prototype._note = null;
Evernote.UploadNotifier.prototype._response = null;
Evernote.UploadNotifier.prototype._noteId = null;
Evernote.UploadNotifier.prototype._clipProcessor = null;
Evernote.UploadNotifier.prototype._notifyManager = null;

Evernote.UploadNotifier.prototype.initialize = function( params ) {
    Evernote.logger.debug( "UploadNotifier.initialize()" );

    this._note = params.note;
    this._response = params.response;
    this._noteId = params.id;
    this._clipProcessor = params.clipProcessor;
    this._notifyManager = params.notifyManager;

    this.bindClose();
};

Evernote.UploadNotifier.prototype.setHeadline = function( str ) {
    this.getNotificationHeadline().html( Evernote.Utils.escapeXML( str ) );
};

Evernote.UploadNotifier.prototype.setDetails = function( str ) {
    this.getNotificationDetails().html( Evernote.Utils.escapeXML( str ) );
};

Evernote.UploadNotifier.prototype.notifyWithPayload = function() {
    Evernote.logger.debug( "UploadNotifier.notifyWithPayload()" );

    Evernote.jQuery( ".title" ).html( Evernote.localizer.getMessage( "BrowserActionTitle" ) );
    var error = Evernote.localizer.getMessage( "EDAMResponseError_1" );

    if ( this._note ) {
        if ( !this._response ) {
            this.showClipSuccess();
            return;
        }

        var status;
        try {
            status = this._response.status;
        }
        catch ( e ) {
            Evernote.logger.warn( "UploadNotifier.notifyWithPayload(): XHR status is unavailable due to connection errors" );
        }

        if ( typeof status == "undefined" ) {
            if ( typeof this._response.hasAuthenticationError == "function" && this._response.hasAuthenticationError() ) {
                Evernote.logger.debug( "Response has authentication error" );
                this.showAuthenticationError();
                return;
            }
            else if ( typeof this._response.isError == "function" && this._response.isError() ) {
                Evernote.logger.debug( "Response has errors" );

                var responseErrors = this._response.errors;
                var firstError = Evernote.EvernoteError.fromObject( responseErrors[ 0 ] );
                error = Evernote.viewManager.extractErrorMessage( firstError, error );

                this.showClipError( error );
                return;
            }
        }
        else if ( typeof status == "number" && status == 0 ) {
            this.showClipError( Evernote.localizer.getMessage( "Error_HTTP_Transport_duringNoteSending" ) );
            return;
        }
    }

    this.showUnknownError();
};

Evernote.UploadNotifier.prototype.showClipSuccess = function() {
    Evernote.logger.debug( "UploadNotifier.showClipSuccess()" );

    this.clear();

    this.showSuccessIcon();
    this.setHeadline( Evernote.localizer.getMessage( "desktopNotification_clipUploaded", [ ((this._note.title) ? this._note.title : "") ] ) );
    this.showSuccessActions( true, (this._note.shareKey == null) );

    Evernote.viewManager.updateBodyHeight( Evernote.viewManager.getWindowHeight() );

    var self = this;
    setTimeout( function() {
        self.close();
    }, this.constructor.NOTIFICATION_CLOSE_TIMEOUT );
};

Evernote.UploadNotifier.prototype.showAuthenticationError = function() {
    Evernote.logger.debug( "UploadNotifier.showAuthenticationError()" );

    this.clear();

    this.showErrorIcon();
    this.setHeadline( Evernote.localizer.getMessage( "desktopNotification_clipProcessorSignInTitle", [ ((this._note.title) ? this._note.title : "") ] ) );
    this.setDetails( Evernote.localizer.getMessage( "desktopNotification_clipProcessorSignInMessage" ) );
    this.showErrorActions( true, true, true );

    Evernote.viewManager.updateBodyHeight( Evernote.viewManager.getWindowHeight() );
};

Evernote.UploadNotifier.prototype.showUnknownError = function() {
    Evernote.logger.debug( "UploadNotifier.showUnknownError()" );

    this.clear();

    this.showErrorIcon();
    this.setHeadline( Evernote.localizer.getMessage( "desktopNotification_unableToSaveClipUnknown" ) );
    this.setDetails( Evernote.localizer.getMessage( "desktopNotification_unableToSaveClipUnknownMessage" ) );

    Evernote.viewManager.updateBodyHeight( Evernote.viewManager.getWindowHeight() );
};

Evernote.UploadNotifier.prototype.showClipError = function( error ) {
    Evernote.logger.debug( "UploadNotifier.showClipError()" );

    this.clear();

    this.showErrorIcon();
    this.setHeadline( Evernote.localizer.getMessage( "desktopNotification_unableToSaveClip", [ ((this._note.title) ? this._note.title : "") ] ) );
    this.setDetails( error );
    this.showErrorActions( true, true, true );
    
    Evernote.viewManager.updateBodyHeight( Evernote.viewManager.getWindowHeight() );
};

Evernote.UploadNotifier.prototype.showSuccessActions = function( showView, showEdit ) {
    Evernote.logger.debug( "UploadNotifier.showSuccessActions()" );

    var actions = this.getNotificationActions();
    var list = [ ];
    var windowOpener = new Evernote.WindowOpener();

    var self = this;
    if ( showView ) {
        var viewAction = Evernote.jQuery( "<a>", { href : "javascript:" } );
        viewAction.text( Evernote.localizer.getMessage( "desktopNotification_viewSuccessClip" ) );

        viewAction.bind( "click", function() {
            var viewUrl = (self._note.shareKey) ? Evernote.getSharedNoteViewUrl( self._note.guid, self._note.shareKey, self._note.shardId )
                                                : Evernote.getNoteViewUrl( self._note.guid );
            windowOpener.openUrl( viewUrl, "", "location=yes,menubar=yes,resizable=yes,scrollbars=yes" );
        } );

        list.push( viewAction );
    }

    if ( showEdit ) {
        var editAction = Evernote.jQuery( "<a>", { href : "javascript:" } );
        editAction.text( Evernote.localizer.getMessage( "desktopNotification_editSuccessClip" ) );

        editAction.bind( "click", function() {
            windowOpener.openUrl( Evernote.getNoteEditUrl( self._note.guid ), "", "location=yes,menubar=yes,resizable=yes,scrollbars=yes" );
        } );

        list.push( editAction );
    }

    this.appendList( actions, list, Evernote.jQuery( "<span>", { className : "separator", text : "-" } ) );
};

Evernote.UploadNotifier.prototype.showErrorActions = function( showView, showRetry, showCancel ) {
    Evernote.logger.debug( "UploadNotifier.showErrorActions()" );

    var actions = this.getNotificationActions();
    var list = [ ];
    var windowOpener = new Evernote.WindowOpener();

    var self = this;
    if ( showView ) {
        var viewAction = Evernote.jQuery( "<a>", { href : "javascript:" } );
        viewAction.text( Evernote.localizer.getMessage( "desktopNotification_viewFailedClip" ) );

        viewAction.bind( "click", function() {
            windowOpener.openUrl( self._note.url, "", "location=yes,menubar=yes,resizable=yes,scrollbars=yes" );
        } );
        
        list.push( viewAction );
    }

    if ( showRetry ) {
        var retryAction = Evernote.jQuery( "<a>", { href : "javascript:" } );
        retryAction.text( Evernote.localizer.getMessage( "desktopNotification_retryFailedClip" ) );

        retryAction.bind( "click", function() {
            self._clipProcessor.onRetry( self._noteId );
            self.close();
        } );
        
        list.push( retryAction );
    }

    if ( showCancel ) {
        var cancelAction = Evernote.jQuery( "<a>", { href : "javascript:" } );
        cancelAction.text( Evernote.localizer.getMessage( "desktopNotification_cancelFailedClip" ) );

        cancelAction.bind( "click", function() {
            self._clipProcessor.onCancel( self._noteId );
            self.close();
        } );

        list.push( cancelAction );
    }

    this.appendList( actions, list, Evernote.jQuery( "<span>", { className : "separator", text : "-" } ) );
};

Evernote.UploadNotifier.prototype.appendList = function( container, list, separator ) {
    Evernote.logger.debug( "UploadNotifier.appendList()" );

    for ( var i = 0; i < list.length; ++i ) {
        container.append( list[ i ] );
        if ( i + 1 < list.length && separator ) {
            container.append( separator.clone() );
        }
    }
};

Evernote.UploadNotifier.prototype.clear = function() {
    this.getNotificationHeadline().empty();
    this.getNotificationDetails().empty();
    this.getNotificationActions().empty();
};

Evernote.UploadNotifier.prototype.showSuccessIcon = function() {
    this.getErrorIcon().hide();
    this.getSuccessIcon().show();
};

Evernote.UploadNotifier.prototype.showErrorIcon = function() {
    this.getSuccessIcon().hide();
    this.getErrorIcon().show();
};

Evernote.UploadNotifier.prototype.getNotificationHeadline = function() {
    return Evernote.jQuery( "#notificationHeadline" );
};

Evernote.UploadNotifier.prototype.getNotificationDetails = function() {
    return Evernote.jQuery( "#notificationDetails" );
};

Evernote.UploadNotifier.prototype.getNotificationActions = function() {
    return Evernote.jQuery( "#notificationActions" );
};

Evernote.UploadNotifier.prototype.getSuccessIcon = function() {
    return Evernote.jQuery( "#successIcon" );
};

Evernote.UploadNotifier.prototype.getErrorIcon = function() {
    return Evernote.jQuery( "#errorIcon" );
};

Evernote.UploadNotifier.prototype.close = function() {
    try {
        this._notifyManager.onClose();
    }
    catch( e ) {
        Evernote.logger.error( "UploadNotifier.close() failed: error =  " + e );
    }
    finally {
        window.close();
    }
};

Evernote.UploadNotifier.prototype.bindClose = function() {
    var self = this;
    Evernote.jQuery( "#close-image" ).click( function() {
        self.close();
    } );
};