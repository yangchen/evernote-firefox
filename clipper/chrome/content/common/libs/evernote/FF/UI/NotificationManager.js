//"use strict";

Evernote.NotificationManager = function NotificationManager() {
    this.initialize();
};

Evernote.NotificationManager.NOTIFY_TIMEOUT = 300;

Evernote.NotificationManager.prototype._isShowing = false;
Evernote.NotificationManager.prototype._notifications = null;

Evernote.NotificationManager.prototype.initialize = function() {
    Evernote.logger.debug( "NotificationManager.initialize()" );

    this._notifications = [ ];
    Evernote.closeListener.addListener( this );
};

Evernote.NotificationManager.prototype.notifySucess = function( resultNote, id ) {
    Evernote.logger.debug( "NotificationManager.notifySucess(): id = " + id );

    this.showNotification( { note : resultNote,
                             id : id,
                             clipProcessor : Evernote.clipProcessor } );
};

Evernote.NotificationManager.prototype.notifyFailure = function( note, response, id ) {
    Evernote.logger.debug( "NotificationManager.notifyFailure(): id = " + id );

    this.showNotification( { response : response,
                             note : note,
                             id : id,
                             clipProcessor : Evernote.clipProcessor } );
};

Evernote.NotificationManager.prototype.showNotification = function( data ) {
     Evernote.logger.debug( "NotificationManager.showNotification()" );

    if ( this._isShowing ) {
        this._notifications.push( data );
        return;
    }

    this._isShowing = true;

    if ( !data ) {
        data = { };
    }
    data.localizer = Evernote.localizer;
    data.notifyManager = this;

    var position = this.getPosition();
    this._dlg = window.openDialog( "chrome://webclipper3-common/content/notification.xul", "",
                                   "chrome, popup, left=" + position.left + ", top=" + position.top + ", titlebar=no, resizable=no", data );
};

Evernote.NotificationManager.prototype.onNotificationSizeUpdate = function() {
    Evernote.logger.debug( "NotificationManager.onNotificationSizeUpdate()" );

    if ( this._dlg ) {
        var popupContent = this._dlg.document.getElementById( "popupContent" );
        if ( popupContent ) {
            var messageHeight = popupContent.style.height.replace( "px", '' );
            var messageWidth = popupContent.style.width.replace( "px", '' );

            var position = this.getPosition();
            position.top = screen.height - messageHeight - 5;
            position.left = screen.width - messageWidth - 5;

            this._dlg.moveTo( position.left, position.top );
        }
    }
};

Evernote.NotificationManager.prototype.getPosition = function() {
    return { left : ( Evernote.Platform.isMacOS() ) ? (screen.width - 150) : (screen.width - 300),
             top : screen.height - 150 };
};

Evernote.NotificationManager.prototype.onClose = function() {
    this._isShowing = false;
    var self = this;

    setTimeout( function() {
        self.showNext();
    }, this.constructor.NOTIFY_TIMEOUT );
};

Evernote.NotificationManager.prototype.showNext = function() {
    Evernote.logger.debug( "NotificationManager.showNext()" );
    
    if ( this._notifications.length > 0 ) {
        var data = this._notifications.splice( 0, 1 )[ 0 ];
        this.showNotification( data );
    }
};

Evernote.NotificationManager.prototype.onWindowClose = function() {
    Evernote.logger.debug( "NotificationManager.onWindowClose()" );

    if ( this._dlg ) {
        this._dlg.close();
    }
};