//"use strict";

Evernote.ToolbarManager = function ToolbarManager() {
};

Evernote.ToolbarManager.BIG_LABEL_CLASS = "big";
Evernote.ToolbarManager.SMALL_LABEL_CLASS = "small";
Evernote.ToolbarManager.PROCESSING_LABEL_CLASS = "processing";

Evernote.ToolbarManager.prototype._simSearchCount = 0;
Evernote.ToolbarManager.prototype._processingCount = 0;

Evernote.ToolbarManager.prototype.updateSimSearchBadge = function( count ) {
    Evernote.logger.debug( "ToolbarManager.updateSimSearchBadge(): count = " + count );

    this._simSearchCount = count;
    this.updateBadge();
};

Evernote.ToolbarManager.prototype.updateProcessingBadge = function( count ) {
    Evernote.logger.debug( "ToolbarManager.updateProcessingBadge(): count = " + count );

    this._processingCount = count;
    this.updateBadge();
};

Evernote.ToolbarManager.prototype.updateBadge = function() {
    //do not update badge if the evernote toolbar icon is disabled (do not exist on toolbar)
    var button = document.getElementById( "webclipper3-button" );
    if ( !button ) {
        return;
    }

    Evernote.logger.debug( "ToolbarManager.updateBadge()" );
    this.setTooltipText( button );

    var count = (this._processingCount > 0) ? this._processingCount : this._simSearchCount;
    if ( count == 0 ) {
        this.clearBadge();
        return;
    }

    var labels = document.getElementsByClassName( "toolbar-value" );
    var value = ( count > 10 ) ? count : (" " + count);

    for ( var i in labels ) {
        labels[ i ].value = value;
        Evernote.DOMHelpers.removeClassName( labels[ i ], this.constructor.PROCESSING_LABEL_CLASS );
        if ( this._processingCount > 0 ) {
            Evernote.DOMHelpers.addClassName( labels[ i ], this.constructor.PROCESSING_LABEL_CLASS );
        }
    }

    var iconsize = document.getElementById( "nav-bar" ).getAttribute( "iconsize" );
    if ( !Evernote.Platform.isMacOS() ) {
        if ( iconsize == "large" ) {
            var elementClass = this.constructor.BIG_LABEL_CLASS;
        }
        else {
            elementClass = this.constructor.SMALL_LABEL_CLASS;
        }
    }

    var currentLabel = document.getElementsByClassName( elementClass )[ 0 ];
    if ( Evernote.Platform.isMacOS() ) {
        currentLabel = document.getElementById("webclipper3-button-label");
    }
    currentLabel.style.display = "block";
};

Evernote.ToolbarManager.prototype.clearBadge = function() {
    Evernote.logger.debug( "ToolbarManager.clearBadge()" );

    var labels = document.getElementsByClassName( "toolbar-value" );
    for ( var i in labels ) {
        if ( labels[ i ].style ) {
            labels[ i ].style.display = "none";
        }
    }
};

Evernote.ToolbarManager.prototype.setTooltipText = function( button ) {
    var tooltipMessage = (this._processingCount > 0) ? "BrowserActionTitlePending" : "add_to_evernote_tooltip";
    if ( button ) {
        button.tooltipText = Evernote.localizer.getMessage( tooltipMessage );
    }
};
