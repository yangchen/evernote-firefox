Evernote.PageInjector = function PageInjector() {
    this.initialize();
};

Evernote.PageInjector.META = "evernote-webclipper-extension";
Evernote.PageInjector.prototype._injectTimers = null;

Evernote.PageInjector.prototype.initialize = function() {
    this._injectTimers = { };

    Evernote.tabManager.addListener( this );
    Evernote.closeListener.addListener( this );
};

Evernote.PageInjector.prototype.onLocationChange = function ( browser ) {
    Evernote.logger.debug( "PageInjector.onLocationChange()" );

    try {
        var doc = browser.contentDocument;
        var tab = Evernote.Utils.getBrowserTab( browser );
        var tabId = tab.linkedPanel;
        var url = doc.location.href;

        if ( url.match( /^https?:\/\/[a-z0-9-+\.]*(evernote|yinxiang)\.com\//i ) ) {
            if ( this._injectTimers[ tabId ] ) {
                clearInterval( this._injectTimers[ tabId ] );
            }

            var self = this;
            this._injectTimers[ tabId ] = setInterval( function() {
                if ( doc.readyState == "complete" ) {
                    self.stopTimer( tabId );

                    try {
                        var metas = doc.getElementsByTagName( "meta" );
                        for ( var i = 0; i < metas.length; ++i ) {
                            if ( metas[i].name == self.constructor.META ) {
                                return;
                            }
                        }

                        var meta = doc.createElement( "meta" );
                        meta.name = self.constructor.META;
                        meta.content = "installed";

                        var head = Evernote.DOMHelpers.getHead( doc );
                        if ( head ) {
                            head.appendChild( meta );
                        }

                        if ( doc.body ) {
                            Evernote.DOMHelpers.addClassName( doc.body, self.constructor.META );
                        }
                    }
                    catch ( e ) {
                        Evernote.logger.error( "PageInjector.onLocationChange() failed: " + e );
                    }
                }
            }, 100 );
        }
    }
    catch ( e ) {
        Evernote.logger.error( "PageInjector.onLocationChange() failed: " + e );
    }
};

Evernote.PageInjector.prototype.onTabClose = function( tabId ) {
    this.stopTimer( tabId );
};

Evernote.PageInjector.prototype.stopTimer = function( tabId ) {
    if ( this._injectTimers[ tabId ] ) {
        clearInterval( this._injectTimers[ tabId ] );
        delete this._injectTimers[ tabId ];
    }
};

Evernote.PageInjector.prototype.abortAll = function() {
    Evernote.logger.debug( "PageInjector.abortAll()" );

    for ( var key in this._injectTimers ) {
        this.stopTimer( key );
    }
    this._injectTimers = { };
};

Evernote.PageInjector.prototype.onWindowClose = function() {
    Evernote.logger.debug( "PageInjector.onWindowClose()" );
    this.cleanUp();
};

Evernote.PageInjector.prototype.cleanUp = function () {
    Evernote.logger.debug( "PageInjector.cleanUp()" );

    this.abortAll();
    Evernote.tabManager.removeListener( this );
};
