//"use strict";

Evernote.PopupPositioner = function PopupPositioner( anchorId, popupWidth, popupHeight ) {
    this.initialize( anchorId, popupWidth, popupHeight );
};

Evernote.PopupPositioner.prototype._anchor = null;
Evernote.PopupPositioner.prototype._popupWidth = 0;
Evernote.PopupPositioner.prototype._popupHeight = 0;

Evernote.PopupPositioner.prototype.initialize = function( anchorId, popupWidth, popupHeight ) {
   Evernote.logger.debug( "PopupPositioner.initialize()" );

   this._anchor = document.getElementById( anchorId );
   this._popupWidth = popupWidth;
   this._popupHeight = popupHeight;
};

/**
 * Returns position of the popup window { left : left_value, right : right_value }.
 * Position of popup will be under toolbar button.
 * If toolbar button does not exist, than position will be at the right top corner
 */
Evernote.PopupPositioner.prototype.getPosition = function() {
    Evernote.logger.debug( "PopupPositioner.getPosition()" );

    var popupLeft = window.innerWidth - this._popupWidth;
    if ( !this._anchor || this._anchor.boxObject.width == 0 ) {
        var navPanelBoxObject = document.getElementById( "navigator-toolbox" ).boxObject;
        return {
            left: window.screenX + popupLeft,
            top: navPanelBoxObject.screenY + navPanelBoxObject.height
        }
    }

    var buttonBoxObject = this._anchor.boxObject;
    var anchorHeight = buttonBoxObject.height;
    var anchorWidth = buttonBoxObject.width;
    var winHeight = screen.height;

    popupLeft = buttonBoxObject.screenX - this._popupWidth + anchorWidth;
    var popupTop = buttonBoxObject.screenY + anchorHeight;

    if ( popupLeft < 0 ) {
        popupLeft = buttonBoxObject.screenX;
    }

    if ( popupTop + this._popupHeight > winHeight ) {
        popupTop = buttonBoxObject.screenY - this._popupHeight - anchorWidth;
    }

    var isTopAlign = true;
    if ( buttonBoxObject.screenY > popupTop ) {
        isTopAlign = false;
    }

    Evernote.logger.debug( "Popup position is left:" + popupLeft + ", top: " + popupTop );
    return {
        left : popupLeft,
        top : popupTop,
        width : this._popupWidth,
        height : this._popupHeight,
        isTopAlign : isTopAlign
    }
};