//"use strict";

Evernote.DataImageSerializer = function DataImageSerializer( node, nodeStyle ) {
    if ( !nodeStyle ) {
        nodeStyle = new Evernote.ClipStyle();
    }

    this.initialize( node, nodeStyle );
};

Evernote.inherit( Evernote.DataImageSerializer, Evernote.AbstractElementSerializer, true );

Evernote.DataImageSerializer.isResponsibleFor = function( node ) {
    if ( node && node.nodeType == Node.ELEMENT_NODE && node.nodeName.toLowerCase() == "img" ) {
        var src = node.getAttribute( "src" );
        if ( src && src.indexOf( "data:image" ) != -1 ) {
            return true;
        }
    }

    return false;
};

Evernote.DataImageSerializer.prototype.serialize = function( /*docBase*/ ) {
    Evernote.logger.debug( "DataImageSerializer.serialize()" );

    try {
        this._nodeStyle.addStyle( { "background-image" : "url('" + this._node.getAttribute( "src" ) + "')",
                                    "width" : this._node.offsetWidth + "px",
                                    "height" : this._node.offsetHeight + "px",
                                    "display" : "block" } );

        return "<span style=\"" + this._nodeStyle.toString().replace( /\"/g, "" ) + "\"" + ">&nbsp;</span>";
    }
    catch ( e ) {
        Evernote.logger.error( "DataImageSerializer.serialize() failed: error = " + e );
    }

    return "";
};