//"use strict";

(function() {
    if ( typeof Evernote != "undefined" && typeof Evernote.ScriptLoader != "undefined" ) {
        Evernote.ScriptLoader.load( [
            "chrome://webclipper3-common/content/libs/evernote/FF/Core/ClipList.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Core/ClipNotificator.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Core/ClipProcessor.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Core/ClipSender.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Core/ClipStorer.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Core/ConnectionObserver.js"
        ] );
    }
})();
