//"use strict";

(function() {
    if ( typeof Evernote != "undefined" && typeof Evernote.ScriptLoader != "undefined" ) {
        Evernote.ScriptLoader.load( [
            "chrome://webclipper3-common/content/libs/evernote/FF/Utils/PopupPositioner.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Utils/PageInjector.js"
        ] );
    }
})();
