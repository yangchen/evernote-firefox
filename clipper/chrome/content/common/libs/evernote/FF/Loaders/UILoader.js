//"use strict";

(function() {
    if ( typeof Evernote != "undefined" && typeof Evernote.ScriptLoader != "undefined" ) {
        Evernote.ScriptLoader.load( [
            "chrome://webclipper3-common/content/libs/evernote/FF/UI/NotificationManager.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/UI/ToolbarManager.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/UI/UploadNotifier.js"
        ] );
    }
})();
