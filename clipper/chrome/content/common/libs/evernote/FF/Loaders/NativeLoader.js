//"use strict";

(function() {
    if ( typeof Evernote != "undefined" && typeof Evernote.ScriptLoader != "undefined" ) {
        Evernote.ScriptLoader.load( [
            "chrome://webclipper3-common/content/libs/evernote/FF/Native/StreamListener.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Native/ImageLoader.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Native/WinNativeAccessor.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Native/WinNativeClipperImpl.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Native/MacNativeAccessor.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Native/MacNativeClipperImpl.js"
        ] );
    }
})();

