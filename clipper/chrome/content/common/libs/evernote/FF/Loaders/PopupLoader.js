//"use strict";

(function() {
    if ( typeof Evernote != "undefined" && typeof Evernote.ScriptLoader != "undefined" ) {
        Evernote.ScriptLoader.load( [
            "chrome://webclipper3-common/content/libs/evernote/FF/Popup/FFLoginController.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Popup/FFNotesSearchController.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Popup/FFNotesSearcher.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Popup/FFPopup.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Popup/FFQuickNoteController.js"
        ] );
    }
})();
