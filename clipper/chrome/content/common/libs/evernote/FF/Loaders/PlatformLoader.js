//"use strict";

(function() {
    if ( typeof Evernote != "undefined" && typeof Evernote.ScriptLoader != "undefined" ) {
        Evernote.ScriptLoader.load( [
            "chrome://webclipper3-common/content/libs/evernote/FF/Platform/ButtonPositionSetter.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Platform/ContextMenuController.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Platform/FileManager.js"
        ] );
    }
})();
