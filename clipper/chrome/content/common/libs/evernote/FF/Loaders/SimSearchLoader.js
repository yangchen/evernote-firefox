//"use strict";

(function() {
    if ( typeof Evernote != "undefined" && typeof Evernote.ScriptLoader != "undefined" ) {
        Evernote.ScriptLoader.load( [
            "chrome://webclipper3-common/content/libs/evernote/FF/SimSearch/SimSearchImpl.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/SimSearch/SimSearchImplFactory.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/SimSearch/SimSearchStorage.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/SimSearch/SimSearchContentScript.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/SimSearch/SimSearcher.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/SimSearch/BaiduSimSearchImpl.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/SimSearch/BingSimSearchImpl.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/SimSearch/GoogleSimSearchImpl.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/SimSearch/YandexRuSimSearchImpl.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/SimSearch/YandexComSimSearchImpl.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/SimSearch/YahooSimSearchImpl.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/SimSearch/YahooJPSimSearchImpl.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/SimSearch/YahooCNSimSearchImpl.js"
        ] );
    }
})();

