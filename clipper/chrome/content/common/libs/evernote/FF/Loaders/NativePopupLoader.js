//"use strict";

(function() {
    if ( typeof Evernote != "undefined" && typeof Evernote.ScriptLoader != "undefined" ) {
        Evernote.ScriptLoader.load( [
            "chrome://webclipper3-common/content/libs/evernote/FF/NativePopup/NativeTabState.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/NativePopup/NativeTabStateRegistry.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/NativePopup/NativePopupPositioner.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/NativePopup/NativePopup.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/NativePopup/NativeController.js"
        ] );
    }
})();

