//"use strict";

(function() {
    if ( typeof Evernote != "undefined" && typeof Evernote.ScriptLoader != "undefined" ) {
        Evernote.ScriptLoader.load( [
            "chrome://webclipper3-common/content/libs/evernote/hatena/extract-content-all.js",
            "chrome://webclipper3-common/content/libs/evernote/hatena/extract-content.js",
            "chrome://webclipper3-common/content/libs/evernote/hatena/lib.js",
            "chrome://webclipper3-common/content/libs/evernote/hatena/scoring-words.js",
            "chrome://webclipper3-common/content/libs/evernote/jabsorb/jsonrpc.js",
            "chrome://webclipper3-common/content/libs/evernote/Chrome/ContentVeil.js",
            "chrome://webclipper3-common/content/libs/evernote/Chrome/PageInfo.js",
            "chrome://webclipper3-common/content/libs/evernote/Chrome/Preview.js",
            "chrome://webclipper3-common/content/libs/evernote/Chrome/Scroller.js",
            "chrome://webclipper3-common/content/libs/evernote/Chrome/JsonRpc.js",
            "chrome://webclipper3-common/content/libs/evernote/Chrome/LinkedNotebooks.js",
            "chrome://webclipper3-common/content/libs/evernote/Chrome/LinkedNotebooksSource.js",
            "chrome://webclipper3-common/content/libs/evernote/Chrome/TagEntryBox.js"
        ] );
    }
})();
