//"use strict";

(function() {
    if ( typeof Evernote != "undefined" && typeof Evernote.ScriptLoader != "undefined" ) {
        Evernote.ScriptLoader.load( [
            "chrome://webclipper3-common/content/libs/evernote/FF/Clipper/FFClipperImplFactory.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Clipper/FFClipperImpl.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Clipper/FFDesktopContextClipperImpl.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Clipper/FFDesktopToolbarClipperImpl.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Clipper/FFWebContextClipperImpl.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Clipper/FFWebToolbarClipperImpl.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Clipper/FFClipper.js"
        ] );
    }
})();
