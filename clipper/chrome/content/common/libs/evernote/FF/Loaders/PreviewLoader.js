//"use strict";

(function() {
    if ( typeof Evernote != "undefined" && typeof Evernote.ScriptLoader != "undefined" ) {
        Evernote.ScriptLoader.load( [
            "chrome://webclipper3-common/content/libs/evernote/FF/Preview/AbstractContentScript.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Preview/ExtractContentJS.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Preview/PageInfo.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Preview/UrlPreviewer.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Preview/ContentVeil.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Preview/ContentPreviewScript.js"
        ] );
    }
})();
