//"use strict";

(function() {
    if ( typeof Evernote != "undefined" && typeof Evernote.ScriptLoader != "undefined" ) {
        Evernote.ScriptLoader.load( [
            "chrome://webclipper3-common/content/libs/evernote/FF/Serialize/DataImageSerializer.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Serialize/VideoElementSerializer.js",
            "chrome://webclipper3-common/content/libs/evernote/FF/Serialize/YoutubeElementSerializer.js"
        ] );
    }
})();


