//"use strict";

Evernote.ButtonPositionSetter = function ButtonPositionSetter() {
};

/**
 * Position button at the top right corner in the navigation bar.
 * This action is performed on first start only (when the property extensions.evernote.webclipper3.is_first_start does not exist or false)
 */
Evernote.ButtonPositionSetter.prototype.setUp = function() {
    Evernote.logger.debug( "ButtonPositionSetter.setUp()" );

    try {
        var isFirstStart = Evernote.prefsManager.getPref( Evernote.PrefKeys.IS_FIRST_START, "boolean" );
        if ( null == isFirstStart || isFirstStart ) {
            var navBar = document.getElementById( "nav-bar" );
            if ( !navBar ) {
                return;
            }

            Evernote.prefsManager.addPref( Evernote.PrefKeys.IS_FIRST_START, false );

            var currentset = navBar.currentSet;
            currentset = ( (currentset > "") ? currentset : navBar.getAttribute( "defaultset" ) );

            Evernote.logger.debug( "Initial currentset " + currentset );
            if ( !document.getElementById( "webclipper3-button" ) ) {
                navBar.insertItem( "webclipper3-button", null, null, false );
            }

            currentset = currentset.replace( ",webclipper3-button", "" );
            currentset = currentset + ",webclipper3-button";

            navBar.setAttribute( "currentset", currentset );
            navBar.currentSet = currentset;

            document.persist( navBar.id, "currentset" );
            Evernote.logger.debug( "Currentset after persist " + navBar.currentSet );

            try {
                BrowserToolboxCustomizeDone( true );
            }
            catch ( e ) {
            }
        }
    }
    catch ( e ) {
        Evernote.logger.error( "ButtonPositionSetter.setUp() failed: error =  " + e );
    }
};
