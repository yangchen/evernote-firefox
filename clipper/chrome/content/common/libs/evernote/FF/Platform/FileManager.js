//"use strict";

Evernote.FileManager = function FileManager() {
};

Evernote.FileManager.prototype.getProfileDir = function() {
    return Components.classes[ "@mozilla.org/file/directory_service;1" ].getService( Components.interfaces.nsIProperties ).get( "ProfD", Components.interfaces.nsIFile );
};

Evernote.FileManager.prototype.getProfileDirPath = function() {
    return this.getProfileDir().path;
};

Evernote.FileManager.prototype.createDir = function( dirPath ) {
    Evernote.logger.debug( "FileManager.createDir(): dirPath = " + dirPath );

    try {
        var dir = Components.classes[ "@mozilla.org/file/local;1" ].createInstance( Components.interfaces.nsILocalFile );
        dir.initWithPath( dirPath );

        dir.QueryInterface( Components.interfaces.nsIFile );
        if ( !dir.exists() ) {
            dir.create( Components.interfaces.nsIFile.DIRECTORY_TYPE, 0777 );
        }
    }
    catch ( e ) {
        Evernote.logger.error( "FileManager.createDir() failed: dirPath = " + dirPath + ", error = " + e );
    }
};

Evernote.FileManager.prototype.deleteDir = function( dirPath ) {
    Evernote.logger.debug( "FileManager.deleteDir(): dirPath = " + dirPath );

    try {
        var dir = Components.classes[ "@mozilla.org/file/local;1" ].createInstance( Components.interfaces.nsILocalFile );
        dir.initWithPath( dirPath );

        dir.QueryInterface( Components.interfaces.nsIFile );
        if ( dir.exists() ) {
            dir.remove( true );
        }
    }
    catch ( e ) {
        Evernote.logger.error( "FileManager.deleteDir() failed: dirPath = " + dirPath + ", error = " + e );
    }
};

Evernote.FileManager.prototype.doesDirExist = function( dirPath ) {
    Evernote.logger.debug( "FileManager.doesDirExist(): dirPath = " + dirPath );

    try {
        var dir = Components.classes[ "@mozilla.org/file/local;1" ].createInstance( Components.interfaces.nsILocalFile );
        dir.initWithPath( dirPath );

        dir.QueryInterface( Components.interfaces.nsIFile );
        return dir.exists();
    }
    catch ( e ) {
        Evernote.logger.error( "FileManager.doesDirExist() failed: dirPath = " + dirPath + ", error = " + e );
    }

    return false;
};

Evernote.FileManager.prototype.enumerateDirFiles = function( dirPath ) {
    Evernote.logger.debug( "FileManager.enumerateDirFiles(): dirPath = " + dirPath );

    try{
        var dir = Components.classes[ "@mozilla.org/file/local;1" ].createInstance( Components.interfaces.nsILocalFile );
        dir.initWithPath( dirPath );

        dir.QueryInterface( Components.interfaces.nsIFile );
        if ( !dir.exists() ) {
            return [ ];
        }

        var entries = dir.directoryEntries;
        var array = [ ];

        while ( entries.hasMoreElements() ) {
            var entry = entries.getNext();
            entry.QueryInterface( Components.interfaces.nsIFile );
            array.push( entry.leafName );
        }
    }
    catch ( e ) {
        Evernote.logger.error( "FileManager.enumerateDirFiles() failed: dirPath = " + dirPath + ", error = " + e );
    }

    return array;
};

Evernote.FileManager.prototype.writeFile = function( filePath, data, isJSONformat ) {
    Evernote.logger.debug( "FileManager.writeFile(): filePath = " + filePath );

    try{
        var file = Components.classes[ "@mozilla.org/file/local;1" ].createInstance( Components.interfaces.nsILocalFile );
        file.initWithPath( filePath );

        var foStream = Components.classes[ "@mozilla.org/network/file-output-stream;1" ].
                           createInstance( Components.interfaces.nsIFileOutputStream );
        // use 0x02 | 0x20 to open file and truncate content.
        foStream.init( file, 0x02 | 0x08 | 0x10, 0666, 0 );

        // write, create, truncate
        var converter = Components.classes[ "@mozilla.org/intl/converter-output-stream;1" ].
                            createInstance( Components.interfaces.nsIConverterOutputStream );
        converter.init( foStream, "UTF-8", 0, 0 );

        if ( isJSONformat ) {
            converter.writeString( JSON.stringify( data ) );
        }
        else {
            converter.writeString( data );
        }

        converter.flush();
        converter.close();
    }
    catch ( e ) {
        Evernote.logger.error( "FileManager.writeFile(): filePath = " + filePath + ", error = " + e );
    }
};

Evernote.FileManager.prototype.readFile = function( filePath ) {
    Evernote.logger.debug( "FileManager.readFile(): filePath = " + filePath );

    try {
        var file = Components.classes[ "@mozilla.org/file/local;1" ].createInstance( Components.interfaces.nsILocalFile );
        file.initWithPath( filePath );

        var fiStream = Components.classes[ "@mozilla.org/network/file-input-stream;1" ].
                           createInstance( Components.interfaces.nsIFileInputStream );
        fiStream.init( file, -1, 0, 0 );

        var converter = Components.classes[ "@mozilla.org/intl/converter-input-stream;1" ].
                            createInstance( Components.interfaces.nsIConverterInputStream );
        converter.init( fiStream, "UTF-8", 0, 0 );

        var data = "";
        let (str = { }) {
            let read = 0;

            do {
                read = converter.readString( 0xffffffff, str ); // read as much as we can and put it in str.value
                data += str.value;
            }
            while ( read != 0 );
        }

        var obj = JSON.parse( data );
        converter.close();

        return obj;
    }
    catch ( e ) {
        Evernote.logger.error( "FileManager.readFile() failed: filePath = " + filePath + ", error = " + e );
    }

    return null;
};

Evernote.FileManager.prototype.renameFile = function( oldPath, newName ) {
    Evernote.logger.debug( "FileManager.renameFile(): oldPath = " + oldPath + ", newName = " + newName );

    try {
        var file = Components.classes[ "@mozilla.org/file/local;1" ].createInstance( Components.interfaces.nsILocalFile );
        file.initWithPath( oldPath );

        file.QueryInterface( Components.interfaces.nsIFile );
        if ( file.exists() ) {
            file.moveTo( null, newName );
        }
    }
    catch ( e ) {
        Evernote.logger.error( "FileManager.renameFile() failed: oldPath = " + oldPath + ", newName = " + newName + ", error = " + e );
    }
};

Evernote.FileManager.prototype.deleteFile = function( filePath ) {
    Evernote.logger.debug( "FileManager.deleteFile(): filePath = " + filePath );

    try {
        var file = Components.classes[ "@mozilla.org/file/local;1" ].createInstance( Components.interfaces.nsILocalFile );
        file.initWithPath( filePath );

        file.QueryInterface( Components.interfaces.nsIFile );
        if ( file.exists() ) {
            file.remove( false );
        }
    }
    catch ( e ) {
        Evernote.logger.error( "FileManager.deleteFile() failed: filePath = " + filePath + ", error = " + e );
    }
};

