//"use strict";

Evernote.ContextMenuController = {
    onContextMenuShow : function( /*event*/ ) {
        Evernote.logger.debug( "ContextMenuController().onContextMenuShow()" );

        try {
            Evernote.jQuery( "#webclipper3-contextmenu-item" ).hide();
            Evernote.jQuery( "#webclipper3-contextmenu" ).show();

            if ( Evernote.Utils.isForbiddenUrl( content.document.location.href ) ) {
                Evernote.jQuery( "#webclipper3-contextmenu-popup-sep" ).hide();
                Evernote.jQuery( "#webclipper3-contextmenu-popup-clippage" ).hide();
                Evernote.jQuery( "#webclipper3-contextmenu-popup-clipimg" ).hide();
                Evernote.jQuery( "#webclipper3-contextmenu-popup-clipsel" ).hide();
                Evernote.jQuery( "#webclipper3-contextmenu-popup-clipurl" ).hide();
            }
            else {
                Evernote.jQuery( "#webclipper3-contextmenu-popup-sep" ).show();
                Evernote.jQuery( "#webclipper3-contextmenu-popup-clipurl" ).show();

                var hasSelection = gContextMenu.isContentSelected;
                var clickOnImage = gContextMenu.onImage;

                if ( hasSelection ) {
                    if ( clickOnImage ) {
                        Evernote.jQuery( "#webclipper3-contextmenu-popup-clippage" ).show();
                        Evernote.jQuery( "#webclipper3-contextmenu-popup-clipimg" ).show();
                        Evernote.jQuery( "#webclipper3-contextmenu-popup-clipsel" ).show();
                    }
                    else {
                        Evernote.jQuery( "#webclipper3-contextmenu-popup-clippage" ).hide();
                        Evernote.jQuery( "#webclipper3-contextmenu-popup-clipimg" ).hide();
                        Evernote.jQuery( "#webclipper3-contextmenu-popup-clipsel" ).show();
                    }
                }
                else {
                    if ( clickOnImage ) {
                        Evernote.jQuery( "#webclipper3-contextmenu-popup-clippage" ).show();
                        Evernote.jQuery( "#webclipper3-contextmenu-popup-clipimg" ).show();
                        Evernote.jQuery( "#webclipper3-contextmenu-popup-clipsel" ).hide();
                    }
                    else {
                        Evernote.jQuery( "#webclipper3-contextmenu-popup-clippage" ).show();
                        Evernote.jQuery( "#webclipper3-contextmenu-popup-clipimg" ).hide();
                        Evernote.jQuery( "#webclipper3-contextmenu-popup-clipsel" ).hide();
                    }
                }
            }
        }
        catch ( e ) {
            Evernote.logger.error( "ContextMenuController.onContextMenuShow() failed: error = " + e );
        }

        return true;
    }
};