//"use strict";

Evernote.FFWebContextClipperImpl = function( tab, rootElement, contextMenuClipType ) {
    this.initialize( tab, rootElement, contextMenuClipType );
};

Evernote.inherit( Evernote.FFWebContextClipperImpl, Evernote.FFClipperImpl, true );

Evernote.FFWebContextClipperImpl.isResponsibleFor = function( contextMenuClipType ) {
    if ( contextMenuClipType ) {
        return !Evernote.Utils.isDesktopClipperSelected();
    }

    return false;
};

Evernote.FFWebContextClipperImpl.prototype.initialize = function( tab, rootElement, contextMenuClipType ) {
    Evernote.logger.debug( "FFWebContextClipperImpl.initialize()" );

    this._tab = tab;
    this._rootElement = rootElement;
    this._contextMenuClipType = contextMenuClipType;
};

Evernote.FFWebContextClipperImpl.prototype.clip = function( clipNotifier ) {
    Evernote.logger.debug( "FFWebContextClipperImpl.clip()" );

    try {
        if ( !Evernote.context.clientEnabled ) {
            Evernote.Utils.alert( Evernote.localizer.getMessage( "checkVersionHeader" ), Evernote.localizer.getMessage( "checkVersionWarning" ) );
            return;
        }

        if ( this._contextMenuClipType == "NEW_NOTE_ACTION" ) {
           this.createNewNote();
           return;
        }

        if ( !Evernote.context.isLogined() ) {
            this.parent.openFFPopup( { clip : null, tab : this._tab, clipGoMode : true, clipper : this, notifier : clipNotifier } );
            return;
        }

        var clip = new Evernote.Clip( this._tab, this.parent.getStyleStrategy( this._tab ) );
        var notebook = Evernote.context.getPreferredNotebook();

        var metaJSON = { notebookGuid : notebook.guid,
                         title : this._tab.document.title,
                         tagNames : "",
                         comment : "" };

        if ( this._contextMenuClipType == "CLIP_ACTION_IMAGE" ) {
            var altAttr = this._rootElement.getAttribute( "alt" );
            var srcAttr = this._rootElement.getAttribute( "src" );
            if ( altAttr || (srcAttr && srcAttr.indexOf( "data:image" ) < 0) ) {
                metaJSON.title = (altAttr) ? altAttr : srcAttr;
            }
        }

        var metaData = JSON.stringify( metaJSON );

        switch ( this._contextMenuClipType )
        {
        case "CLIP_ACTION_FULL_PAGE":
            this.clipFullPage( clip, metaData, clipNotifier );
            break;

        case "CLIP_ACTION_IMAGE":
            this.clipImage( clip, metaData, clipNotifier );
            break;

        case "CLIP_ACTION_SELECTION":
            this.clipSelection( clip, metaData, clipNotifier );
            break;

        case "CLIP_ACTION_URL":
            this.clipUrl( clip, metaData, clipNotifier );
            break;
        }
    }
    catch ( e ) {
        Evernote.logger.error( "FFWebContextClipperImpl.clip() failed: error = " + e );
    }
};

Evernote.FFWebContextClipperImpl.prototype.clipFullPage = function( clip, metaData, clipNotifier ) {
    Evernote.logger.debug( "FFWebContextClipperImpl.clipFullPage()" );
    clip.clipFullPage();
    clipNotifier.finishClip( clip, metaData, false );
};

Evernote.FFWebContextClipperImpl.prototype.clipImage = function( clip, metaData, clipNotifier ) {
    Evernote.logger.debug( "FFWebContextClipperImpl.clipImage()" );
    clip.clipImage( this._rootElement );
    clipNotifier.finishClip( clip, metaData, false );
};

Evernote.FFWebContextClipperImpl.prototype.clipSelection = function( clip, metaData, clipNotifier ) {
    Evernote.logger.debug( "FFWebContextClipperImpl.clipSelection()" );
    clip.clipSelection();
    clipNotifier.finishClip( clip, metaData, false );
};

Evernote.FFWebContextClipperImpl.prototype.clipUrl = function( clip, metaData, clipNotifier ) {
    Evernote.logger.debug( "FFWebContextClipperImpl.clipUrl()" );
    clip.clipUrl();
    clipNotifier.finishClip( clip, metaData, true );
};

Evernote.FFWebContextClipperImpl.prototype.createNewNote = function() {
    Evernote.logger.debug( "FFWebContextClipperImpl.createNewNote()" );
    new Evernote.WindowOpener().openUrl( Evernote.getNewNoteUrl(), "Evernote", "location=yes,menubar=yes,resizable=yes,scrollbars=yes" );
};