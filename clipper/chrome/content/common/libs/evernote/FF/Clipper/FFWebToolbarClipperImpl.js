//"use strict";

Evernote.FFWebToolbarClipperImpl = function( tab/*, rootElement, contextMenuClipType*/ ) {
    this._tab = tab;
};

Evernote.inherit( Evernote.FFWebToolbarClipperImpl, Evernote.FFClipperImpl, true );

Evernote.FFWebToolbarClipperImpl.isResponsibleFor = function( contextMenuClipType ) {
    if ( !contextMenuClipType ) {
        return !Evernote.Utils.isDesktopClipperSelected();
    }

    return false;
};

Evernote.FFWebToolbarClipperImpl.prototype.clip = function( clipNotifier ) {
    Evernote.logger.debug( "FFWebToolbarClipperImpl.clip()" );

    try {
        var clip = new Evernote.Clip( this._tab, this.parent.getStyleStrategy( this._tab ) );
        this.parent.openFFPopup( { clip : clip, tab : this._tab, clipGoMode : false, clipper : null, notifier : clipNotifier } );
    }
    catch ( e ) {
        Evernote.logger.error( "FFWebToolbarClipperImpl.clip() failed: error = " + e );
    }
};