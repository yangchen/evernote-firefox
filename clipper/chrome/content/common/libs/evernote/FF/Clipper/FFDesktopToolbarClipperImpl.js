//"use strict";

Evernote.FFDesktopToolbarClipperImpl = function( tab, rootElement, contextMenuClipType ) {
    this.initialize( tab, rootElement, contextMenuClipType );
};

Evernote.inherit( Evernote.FFDesktopToolbarClipperImpl, Evernote.FFClipperImpl, true );

Evernote.FFDesktopToolbarClipperImpl.isResponsibleFor = function( contextMenuClipType ) {
    if ( !contextMenuClipType ) {
        return Evernote.Utils.isDesktopClipperSelected();
    }

    return false;
};

Evernote.FFDesktopToolbarClipperImpl.prototype.initialize = function( tab, rootElement/*, contextMenuClipType*/ ) {
    Evernote.logger.debug( "FFDesktopToolbarClipperImpl.initialize()" );

    this._tab = tab;
    this._rootElement = rootElement;
};

Evernote.FFDesktopToolbarClipperImpl.prototype.clip = function( /*clipNotifier*/ ) {
    Evernote.logger.debug( "FFDesktopToolbarClipperImpl.clip()" );

    try {
        var selectionFinder = new Evernote.SelectionFinder( this._tab.document );
		selectionFinder.find( true );

        if ( !Evernote.Utils.isForbiddenUrl( this._tab.location.href ) && selectionFinder.hasSelection() ) {
            this.clipSelection();
        }
        else {
            new Evernote.AbstractContentScript().injectStyleSheets( [ "resource://evernote_styles/contentpreview.css" ], this._tab.document );
            this.openNativePopup();
        }
    }
    catch ( e ) {
        Evernote.logger.error( "FFDesktopToolbarClipperImpl.clip() failed: error = " + e );
    }
};

Evernote.FFDesktopToolbarClipperImpl.prototype.clipArticle = function( element ) {
    Evernote.logger.debug( "FFDesktopToolbarClipperImpl.clipArticle()" );

    var articleNode = element || Evernote.DOMHelpers.getArticleNode( this._tab.document );
    if ( articleNode ) {
        Evernote.clipNotificator.showCopyNotification( this._tab.document );
        new Evernote.NativeClipper().clip( this._tab, articleNode, Evernote.FFClipper.CLIP_ARTICLE, false );
    }
};

Evernote.FFDesktopToolbarClipperImpl.prototype.clipFullPage = function() {
    Evernote.logger.debug( "FFDesktopToolbarClipperImpl.clipFullPage()" );
    Evernote.clipNotificator.showCopyNotification( this._tab.document );
    new Evernote.NativeClipper().clip( this._tab, null, Evernote.FFClipper.CLIP_FULL_PAGE, false );
};

Evernote.FFDesktopToolbarClipperImpl.prototype.clipSelection = function() {
    Evernote.logger.debug( "FFDesktopToolbarClipperImpl.clipSelection()" );
    Evernote.clipNotificator.showCopyNotification( this._tab.document );
    new Evernote.NativeClipper().clip( this._tab, null, Evernote.FFClipper.CLIP_SELECTION, false );
};

Evernote.FFDesktopToolbarClipperImpl.prototype.clipUrl = function() {
    Evernote.logger.debug( "FFDesktopToolbarClipperImpl.clipUrl()" );
    Evernote.clipNotificator.showCopyNotification( this._tab.document );
    this.parent.clipUrl( this._tab, false );
};

Evernote.FFDesktopToolbarClipperImpl.prototype.openNativePopup = function() {
    Evernote.logger.debug( "FFDesktopToolbarClipperImpl.openNativePopup()" );

    var clipType = this.getDefaultNativeClipType();
    var navPanelBoxObject = document.getElementById( "navigator-toolbox" ).boxObject;
    var tabId = Evernote.tabManager.getSelectedTabId();

    Evernote.nativeController.initialize( this._tab, tabId, this, clipType,
                                          { x : navPanelBoxObject.screenX, y : navPanelBoxObject.screenY + navPanelBoxObject.height } );
};

Evernote.FFDesktopToolbarClipperImpl.prototype.getDefaultNativeClipType = function() {
    Evernote.logger.debug( "FFDesktopToolbarClipperImpl.getDefaultNativeClipType()" );

	var pageInfo = new Evernote.Chrome.PageInfo( this._tab );
    var clipAction = Evernote.context.options.clipAction;

    if ( Evernote.Utils.isForbiddenUrl( this._tab.document.location.href ) ) {
        var clipType = Evernote.RequestType.PREVIEW_CLIP_ACTION_URL;
    }
    else if ( clipAction == Evernote.Options.CLIP_ACTION_OPTIONS.ARTICLE ) {
        clipType = (pageInfo.getDefaultArticle()) ? Evernote.RequestType.PREVIEW_CLIP_ACTION_ARTICLE
                                                  : Evernote.RequestType.PREVIEW_CLIP_ACTION_FULL_PAGE;
    }
    else if ( clipAction == Evernote.Options.CLIP_ACTION_OPTIONS.FULL_PAGE ) {
        clipType = Evernote.RequestType.PREVIEW_CLIP_ACTION_FULL_PAGE;
    }
    else if ( clipAction == Evernote.Options.CLIP_ACTION_OPTIONS.URL ) {
        clipType = Evernote.RequestType.PREVIEW_CLIP_ACTION_URL;
    }

    return clipType;
};