//"use strict";

Evernote.FFDesktopContextClipperImpl = function( tab, rootElement, contextMenuClipType ) {
    this.initialize( tab, rootElement, contextMenuClipType );
};

Evernote.inherit( Evernote.FFDesktopContextClipperImpl, Evernote.FFClipperImpl, true );

Evernote.FFDesktopContextClipperImpl.isResponsibleFor = function( contextMenuClipType ) {
    if ( contextMenuClipType ) {
        return Evernote.Utils.isDesktopClipperSelected();
    }

    return false;
};

Evernote.FFDesktopContextClipperImpl.prototype.initialize = function( tab, rootElement, contextMenuClipType ) {
    Evernote.logger.debug( "FFDesktopContextClipperImpl.initialize()" );

    this._tab = tab;
    this._rootElement = rootElement;
    this._contextMenuClipType = contextMenuClipType;
};

Evernote.FFDesktopContextClipperImpl.prototype.clip = function( /*clipNotifier*/ ) {
    Evernote.logger.debug( "FFDesktopContextClipperImpl.clip()" );

    try {
        if ( this._contextMenuClipType == "NEW_NOTE_ACTION" ) {
            this.createNewNote();
            return;
        }

        Evernote.clipNotificator.showCopyNotification( this._tab.document );

        if ( this._contextMenuClipType == "CLIP_ACTION_FULL_PAGE" ) {
            this.clipFullPage();
        }
        else if ( this._contextMenuClipType == "CLIP_ACTION_IMAGE" ) {
            this.clipImage();
        }
        else if ( this._contextMenuClipType == "CLIP_ACTION_SELECTION" ) {
            this.clipSelection();
        }
        else if ( this._contextMenuClipType == "CLIP_ACTION_URL" ) {
            this.clipUrl();
        }
    }
    catch ( e ) {
        Evernote.logger.error( "FFDesktopContextClipperImpl.clip() failed: error = " + e );
    }
};

Evernote.FFDesktopContextClipperImpl.prototype.clipFullPage = function() {
    Evernote.logger.debug( "FFDesktopContextClipperImpl.clipFullPage()" );
    new Evernote.NativeClipper().clip( this._tab, null, Evernote.FFClipper.CLIP_FULL_PAGE, true );
};

Evernote.FFDesktopContextClipperImpl.prototype.clipImage = function() {
    Evernote.logger.debug( "FFDesktopContextClipperImpl.clipImage()" );
    new Evernote.NativeClipper().clip( this._tab, this._rootElement, Evernote.FFClipper.CLIP_IMAGE, true );
};

Evernote.FFDesktopContextClipperImpl.prototype.clipSelection = function() {
    Evernote.logger.debug( "FFDesktopContextClipperImpl.clipSelection()" );
    new Evernote.NativeClipper().clip( this._tab, null, Evernote.FFClipper.CLIP_SELECTION, true );
};

Evernote.FFDesktopContextClipperImpl.prototype.clipUrl = function() {
    Evernote.logger.debug( "FFDesktopContextClipperImpl.clipUrl()" );
    this.parent.clipUrl( this._tab, true );
};

Evernote.FFDesktopContextClipperImpl.prototype.createNewNote = function() {
    Evernote.logger.debug( "FFDesktopContextClipperImpl.createNewNote()" );
    new Evernote.NativeClipper().createNewNote();
};