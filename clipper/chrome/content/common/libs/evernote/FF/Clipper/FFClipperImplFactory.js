//"use strict";

Evernote.FFClipperImplFactory = {
    getImplementationFor : function( contextMenuClipType ) {
        var reg = this.ClassRegistry;
        for ( var i = 0; i < reg.length; ++i ) {
            if ( typeof reg[ i ] == 'function' && typeof reg[ i ].isResponsibleFor == 'function'
                 && reg[ i ].isResponsibleFor( contextMenuClipType ) ) {
                return reg[ i ];
            }
        }

        return null;
    }
};

Evernote.FFClipperImplFactory.ClassRegistry = [ ];