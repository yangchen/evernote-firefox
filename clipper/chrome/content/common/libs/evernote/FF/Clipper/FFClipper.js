//"use strict";

Evernote.FFClipper = function( tab, rootElement, contextMenuClipType ) {
    this.initialize( tab, rootElement, contextMenuClipType );
};

Evernote.FFClipper.CLIP_FULL_PAGE = 0;
Evernote.FFClipper.CLIP_IMAGE = 1;
Evernote.FFClipper.CLIP_SELECTION = 2;
Evernote.FFClipper.CLIP_URL = 3;
Evernote.FFClipper.CLIP_ARTICLE = 4;

Evernote.FFClipper.prototype._tab = null;
Evernote.FFClipper.prototype._rootElement = null;
Evernote.FFClipper.prototype._contextMenuClipType = null;

Evernote.FFClipper.prototype.initialize = function( tab, rootElement, contextMenuClipType ) {
    Evernote.logger.debug( "FFClipper.initialize()" );

    this._tab = tab;
    this._rootElement = rootElement;
    this._contextMenuClipType = contextMenuClipType;
};

Evernote.FFClipper.prototype.clip = function( clipNotifier ) {
    Evernote.logger.debug( "FFClipper.clip()" );

    try {
        var implClass = Evernote.FFClipperImplFactory.getImplementationFor( this._contextMenuClipType );
        if ( typeof implClass == "function" ) {
            var impl = new implClass( this._tab, this._rootElement, this._contextMenuClipType );
            impl.clip( clipNotifier );
        }
    }
    catch ( e ) {
        Evernote.logger.error( "FFClipper.clip() failed: error = " + e );
    }
};