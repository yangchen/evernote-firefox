//"use strict";

Evernote.FFClipperImpl = function FFClipperImpl() {
};

Evernote.FFClipperImpl.handleInheritance = function( child/*, parent*/ ) {
    Evernote.FFClipperImplFactory.ClassRegistry.push( child );
};

Evernote.FFClipperImpl.isResponsibleFor = function( /*contextMenuClipType*/ ) {
    return false;
};

Evernote.FFClipperImpl.prototype._tab = null;
Evernote.FFClipperImpl.prototype._rootElement = null;
Evernote.FFClipperImpl.prototype._contextMenuClipType = null;

Evernote.FFClipperImpl.prototype.clip = function( /*clipNotifier*/ ) {
};

Evernote.FFClipperImpl.prototype.clipArticle = function() {
};

Evernote.FFClipperImpl.prototype.clipFullPage = function() {
};

Evernote.FFClipperImpl.prototype.clipImage = function() {
};

Evernote.FFClipperImpl.prototype.clipSelection = function() {
};

Evernote.FFClipperImpl.prototype.clipUrl = function() {
};

Evernote.FFClipperImpl.prototype.getStyleStrategy = function( tab ) {
    Evernote.logger.debug( "FFClipperImpl.getStyleStrategy()" );

    var options = Evernote.context.options;
    if ( options.clipStyle == Evernote.Options.CLIP_STYLE_OPTIONS.FULL ) {
        var styleStrategy = Evernote.ClipFullStylingStrategy;
    }
    else if ( options.clipStyle == Evernote.Options.CLIP_STYLE_OPTIONS.TEXT ) {
        styleStrategy = Evernote.ClipTextStylingStrategy;
    }

    return (styleStrategy) ? new styleStrategy( tab ) : null;
};

Evernote.FFClipperImpl.prototype.openFFPopup = function( params ) {
    Evernote.logger.debug( "FFClipperImpl.openFFPopup()" );

    if ( !params ) {
        params = { };
    }

    params.popupPosition = new Evernote.PopupPositioner( "webclipper3-button", 587, 250 ).getPosition();
    params.simSearcher = Evernote.simSearchStorage.getSimSearcher( Evernote.tabManager.getSelectedTabId() );
    params.localizer = Evernote.localizer;
    params.tabManager = Evernote.tabManager;

    window.openDialog( "chrome://webclipper3-common/content/FFPopup.xul", "",
                       "chrome, titlebar=no, left=" + params.popupPosition.left + ", top=" + params.popupPosition.top + ", resizable=no", params );
};

Evernote.FFClipperImpl.prototype.clipUrl = function( tab, isClipGo ) {
    Evernote.logger.debug( "FFClipperImpl.clipUrl()" );

    var doc = tab.document;
    var favIconUrl = "http://www.google.com/s2/favicons?domain=" + Evernote.UrlHelpers.urlDomain( tab.location.href ).toLowerCase();
    var elem = Evernote.DOMHelpers.createUrlClipContent( doc, tab.title, tab.location.href, favIconUrl );

    doc.body.appendChild( elem );
    new Evernote.NativeClipper().clip( tab, elem, Evernote.FFClipper.CLIP_URL, isClipGo );
    doc.body.removeChild( elem );
};