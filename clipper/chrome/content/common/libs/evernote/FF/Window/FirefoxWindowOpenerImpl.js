//"use strict";

Evernote.FirefoxWindowOpenerImpl = function FirefoxWindowOpenerImpl() {
    this.__defineGetter__( "mainWindow", this.getMainWindow );
};

Evernote.inherit( Evernote.FirefoxWindowOpenerImpl, Evernote.WindowOpenerImpl, true );

Evernote.FirefoxWindowOpenerImpl.isResponsibleFor = function( navigator ) {
    var ua = navigator.userAgent.toLowerCase();
    return ua.indexOf( "firefox" ) >= 0;
};

Evernote.FirefoxWindowOpenerImpl.prototype._mainWindow = null;

Evernote.FirefoxWindowOpenerImpl.prototype.openUrl = function( url, winName, winOpts ) {
    Evernote.logger.debug( "FirefoxWindowOpenerImpl.openUrl(): url = " + url );

    var wnd = this.mainWindow.open( url, winName, winOpts );
    wnd.focus();
    return wnd;
};

Evernote.FirefoxWindowOpenerImpl.prototype.getMainWindow = function() {
    if ( !this._mainWindow ) {
        var mediator = Components.classes[ "@mozilla.org/appshell/window-mediator;1" ].getService( Components.interfaces.nsIWindowMediator );
        this._mainWindow = mediator.getMostRecentWindow( "navigator:browser" );
    }

    return this._mainWindow;
};
