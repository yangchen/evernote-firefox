//"use strict";

Evernote.ContentPreviewScript = function ContentPreviewScript( tab ) {
    this.__defineGetter__( "contentVeil", this.getContentVeil );
    this.__defineGetter__( "urlPreviewer", this.getUrlPreviewer );
    this.__defineGetter__( "tab", this.getTab );
    this.initialize( tab );
};

Evernote.ContentPreviewScript.prototype._tab = null;
Evernote.ContentPreviewScript.prototype._contentVeil = null;
Evernote.ContentPreviewScript.prototype._urlPreviewer = null;

Evernote.ContentPreviewScript.SELECTION_CTX_ATTRS = {
    fillStyle: "rgba(255, 255, 0, 0.3)"
};

Evernote.ContentPreviewScript.PREVIEW_CTX_ATTRS = {
    fillStyle: "rgba(255, 255, 255, 0.3)"
};

Evernote.ContentPreviewScript.PREVIEW_ARTICLE_CLASS = "evernotePreviewArticleContainer";

Evernote.ContentPreviewScript.INITIAL_STYLE_SHEETS = [
    "resource://evernote_styles/contentpreview.css",
    "resource://evernote_styles/contentclipper.css",
    "resource://evernote_styles/contentveil.css"
];

Evernote.ContentPreviewScript.prototype.initialize = function( tab ) {
    Evernote.logger.debug( "ContentPreviewScript.initialize()" );

    this._tab = (tab) ? tab : window;
    new Evernote.AbstractContentScript().injectStyleSheets( this.constructor.INITIAL_STYLE_SHEETS, this._tab.document );
};

Evernote.ContentPreviewScript.prototype.previewArticle = function () {
    Evernote.logger.debug( "ContentPreviewScript.previewArticle()" );

    this.clear();
    try {
        var extractor = new Evernote.ExtractContentJS.LayeredExtractor();
        extractor.addHandler( extractor.factory.getHandler( "Heuristics" ) );
        var result = extractor.extract( this._tab.document );

        if ( result.isSuccess ) {
            var root = result.content.asNode();
            root = Evernote.DOMHelpers.getElementForNode( root );
            Evernote.DOMHelpers.addElementClass( root, this.constructor.PREVIEW_ARTICLE_CLASS );

            this.contentVeil.makeShadow();
            this.contentVeil.revealRect( Evernote.DOMHelpers.getAbsoluteBoundingClientRect( root ), true, this.constructor.PREVIEW_CTX_ATTRS );
        }
    }
    catch ( e ) {
        Evernote.logger.error( "ContentPreviewScript.previewArticle() failed: error = " + e );
        this.clear();
    }
};

Evernote.ContentPreviewScript.prototype.previewFullPage = function () {
    Evernote.logger.debug( "ContentPreviewScript.previewFullPage()" );
    this.clear();
};

Evernote.ContentPreviewScript.prototype.previewSelection = function () {
    Evernote.logger.debug( "ContentPreviewScript.previewSelection()" );

    this.clear();
    try {
        var finder = new Evernote.SelectionFinder( this._tab.document );
        finder.find( true );
        if ( !finder.hasSelection() ) {
            this.previewFullPage();
            return;
        }

        var selRange = finder.getRange();
        var clientSelRects = selRange.getClientRects();

        var activeElem = this._tab.document.activeElement;
        if ( activeElem instanceof HTMLTextAreaElement || (activeElem instanceof HTMLInputElement && activeElem.type == "text") ) {
            clientSelRects = this.getInputSelectedRects( activeElem, selRange );
            var visibleRect = activeElem.getBoundingClientRect();
        }

        this.contentVeil.makeShadow();
        for ( var i in clientSelRects ) {
            var clientSelRect = {
                top : clientSelRects[ i ].top,
                left: clientSelRects[ i ].left,
                right: clientSelRects[ i ].right,
                bottom: clientSelRects[ i ].bottom,
                width : clientSelRects[ i ].right - clientSelRects[ i ].left,
                height : clientSelRects[ i ].bottom - clientSelRects[ i ].top
            };

            //skip unvisible selected rectangles
            if ( visibleRect && ( visibleRect.bottom < clientSelRect.top || visibleRect.top > clientSelRect.bottom ) ) {
                continue;
            }

            //cut top unvisible part of selected rectangles
            if ( visibleRect && (visibleRect.bottom - clientSelRect.top) <= clientSelRect.height ) {
                clientSelRect.bottom = visibleRect.bottom;
                clientSelRect.height = clientSelRect.bottom - clientSelRect.top;
            }

            //cut bottom unvisible part of selected rectangles
            if ( visibleRect && (clientSelRect.bottom - visibleRect.top) <= clientSelRect.height ) {
                clientSelRect.top = visibleRect.top;
                clientSelRect.height = clientSelRect.bottom - clientSelRect.top;
            }

            Evernote.logger.debug( "preview " + clientSelRect.top + " " + clientSelRect.bottom + " " + clientSelRect.left + " " + clientSelRect.right );
            this.contentVeil.revealRect( Evernote.DOMHelpers.makeAbsoluteClientRect( clientSelRect, this._tab ), false, this.constructor.SELECTION_CTX_ATTRS );
        }
    }
    catch ( e ) {
        Evernote.logger.error( "ContentPreviewScript.previewSelection() failed: error = "  + e );
        this.clear();
    }
};

Evernote.ContentPreviewScript.prototype.previewURL = function () {
    Evernote.logger.debug( "ContentPreviewScript.previewURL()" );

    this.clear();
    try {
        this.contentVeil.makeShadow();
        this.urlPreviewer.createContent();

        var self = this;
        setTimeout( function () {
            self.urlPreviewer.showContent();
        }, 100 );
    }
    catch ( e ) {
        Evernote.logger.error( "ContentPreviewScript.previewURL() failed: error = " + e );
        this.clear();
    }
};

Evernote.ContentPreviewScript.prototype.clear = function () {
    Evernote.logger.debug( "ContentPreviewScript.clear()" );

    this.urlPreviewer.destroyContent();
    this._urlPreviewer = null;

    this.contentVeil.clear();
    this._contentVeil = null;
};

Evernote.ContentPreviewScript.prototype.getInputSelectedRects = function( activeElem, selRange ) {
    Evernote.logger.debug( "ContentPreviewScript.getInputSelectedRects()" );

    try {
        //Temporary replace TextArea element tor <PRE> and try to safe location of text/words/latter
        var newNode = this._tab.document.createElement( "pre" );
        var textNode = this._tab.document.createTextNode( activeElem.value );
        var styles = this._tab.getComputedStyle( activeElem, null );
        var prevDisplay = styles.getPropertyValue( "display" );

        //Hide new element and insert in dom
        newNode.style.visibility = "hidden";
        newNode.appendChild( textNode );
        activeElem.parentNode.appendChild( newNode );

        //Set styles for Input/TextArea fields
        newNode.style.wordWrap = "break-word";
        newNode.style.whiteSpace = "pre-wrap";
        newNode.style.letterSpacing = "0.1px";
        newNode.style.padding = styles.getPropertyValue( "padding-top" ) + " " + styles.getPropertyValue( "padding-right" ) + " "
                                + styles.getPropertyValue( "padding-bottom" ) + " " + styles.getPropertyValue( "padding-left" );
        newNode.style.margin = styles.getPropertyValue( "margin-top" ) + " " + styles.getPropertyValue( "margin-right" ) + " "
                               + styles.getPropertyValue( "margin-bottom" ) + " " + styles.getPropertyValue( "margin-left" );

        if ( activeElem instanceof HTMLTextAreaElement ) {
            newNode.style.width = styles.getPropertyValue( "width" );
            newNode.style.height = (activeElem.scrollHeight > parseInt( styles.getPropertyValue( "height" ) )) ? activeElem.scrollHeight : parseInt( styles.getPropertyValue( "height" ) ) + "px";
            newNode.style.overflow = "auto";
            newNode.style[ "line-height" ] = styles.getPropertyValue( "line-height" );
        }
        else if ( activeElem instanceof HTMLInputElement && activeElem.type == "text" ) {
            for ( var i = 0; i < styles.length; ++i ) {
                //copy all styles except that was predefined
                var prop = styles[ i ];
                if ( newNode.style[ prop ] ) {
                    continue;
                }

                newNode.style[ prop ] = styles.getPropertyValue( prop );
            }
        }

        if ( activeElem.id || activeElem.className ) {
            newNode.id = activeElem.id;
            newNode.className = activeElem.className;
        }
        //hide input/textarea element
        activeElem.style.display = "none";

        //evaluate Range for temporary node
        var range = this._tab.document.createRange();
        range.setStart( textNode, selRange.startOffset );
        range.setEnd( textNode, selRange.endOffset );
        var clientRects = range.getClientRects();

        activeElem.parentNode.removeChild( newNode );
        activeElem.style.display = prevDisplay;
        var topScrollDiff = activeElem.scrollTop;
        var selRects = [ ];

        for ( var key in clientRects ) {
            var rect = {
                top : clientRects[ key ].top - topScrollDiff,
                left: clientRects[ key ].left,
                right: clientRects[ key ].right,
                bottom: clientRects[ key ].bottom - topScrollDiff
            };

            selRects.push( rect );
        }

        return selRects;
    }
    catch ( e ) {
        Evernote.logger.error( "ContentPreviewScript.getInputSelectedRects() failed: error = " + e );
    }

    return [ ];
};

Evernote.ContentPreviewScript.prototype.getContentVeil = function () {
    if ( !this._contentVeil ) {
        this._contentVeil = new Evernote.ContentVeil( this._tab );
    }

    return this._contentVeil;
};

Evernote.ContentPreviewScript.prototype.getUrlPreviewer = function() {
    if ( !this._urlPreviewer ) {
        this._urlPreviewer = new Evernote.UrlPreviewer( this._tab );
    }

    return this._urlPreviewer;
};

Evernote.ContentPreviewScript.prototype.getTab = function() {
    return this._tab;
};