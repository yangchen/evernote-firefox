//"use strict";

Evernote.PageInfo = function PageInfo( tab ) {
    this.__defineGetter__( "articleNode", this.getArticleNode );
    this.__defineGetter__( "articleBoundingClientRect", this.getArticleBoundingClientRect );
    this.__defineGetter__( "documentWidth", this.getDocumentWidth );
    this.__defineGetter__( "documentHeight", this.getDocumentHeight );
    this.initialize( tab );
};

Evernote.PageInfo.prototype._articleNode = null;
Evernote.PageInfo.prototype._articleBoundingClientRect = null;
Evernote.PageInfo.prototype._documentWidth = 0;
Evernote.PageInfo.prototype._documentHeight = 0;

Evernote.PageInfo.prototype.initialize = function( tab ) {
    Evernote.logger.debug( "PageInfo.initialize()" );

    try {
        this.initArticle( tab );
        this.initDocument( tab );
    }
    catch ( e ) {
        Evernote.logger.error( "PageInfo.initialize() failed: error = " + e );
    }
};

Evernote.PageInfo.prototype.initArticle = function( tab ) {
    Evernote.logger.debug( "PageInfo.initArticle()" );

    var extractor = new Evernote.ExtractContentJS.LayeredExtractor();
    extractor.addHandler( extractor.factory.getHandler( "Heuristics" ) );
    var result = extractor.extract( tab.document );

    if ( result.isSuccess ) {
        var articleNode = result.content.asNode();
        if ( articleNode ) {
            var clientRect = Evernote.DOMHelpers.getAbsoluteBoundingClientRect( articleNode.parentNode );
            if ( clientRect ) {
                this._articleNode = articleNode;
                this._articleBoundingClientRect = clientRect;
            }
        }
    }
};

Evernote.PageInfo.prototype.initDocument = function( tab ) {
    this._documentWidth = tab.document.width;
    this._documentHeight = tab.document.height;
};

Evernote.PageInfo.prototype.getArticleNode = function() {
    return this._articleNode;
};

Evernote.PageInfo.prototype.getArticleBoundingClientRect = function() {
    return this._articleBoundingClientRect;
};

Evernote.PageInfo.prototype.getDocumentWidth = function() {
    return this._documentWidth;
};

Evernote.PageInfo.prototype.getDocumentHeight = function() {
    return this._documentHeight;
};
