//"use strict";

Evernote.ContentVeil = function ( tab ) {
    this.initialize( tab );
};

Evernote.ContentVeil.VEIL_ID = "evernoteContentVeil";
Evernote.ContentVeil.VEIL_CLASS = "evernoteContentVeil";
Evernote.ContentVeil.FILL_STYLE = "rgba(0, 0, 0, 0.7)";
Evernote.ContentVeil.FILL_HIGHLIGHT_STYLE = "rgba(255, 255, 255, 0.3)";
Evernote.ContentVeil.STROKE_STYLE = "rgba(255, 255, 255, 0.9)";
Evernote.ContentVeil.STROKE_WIDTH = 6;
Evernote.ContentVeil.STROKE_CAP = "round";
Evernote.ContentVeil.STROKE_JOIN = "round";
Evernote.ContentVeil.MAX_CANVAS_HEIGHT = 1000;

Evernote.ContentVeil.VEIL_STYLES = {
    position: "absolute",
    top: "0px",
    left: "0px",
    zIndex: "999999990"
};

Evernote.ContentVeil.prototype._tab = null;
Evernote.ContentVeil.prototype._canvases = null;

Evernote.ContentVeil.prototype.initialize = function( tab ) {
    Evernote.logger.debug( "ContentVeil.initialize()" );

    this._tab = (tab) ? tab : window;
    this._canvases = [ ];
};

Evernote.ContentVeil.prototype.makeShadow = function () {
    Evernote.logger.debug( "ContentVeil.makeShadow()" );

    try {
        this.clear();
        this.createCanvases();

        for ( var i = 0; i < this._canvases.length; ++i ) {
            var canvas = this._canvases[ i ];
            var context = canvas.getContext( "2d" );

            context.clearRect( 0, 0, canvas.width, canvas.height );
            context.fillStyle = this.constructor.FILL_STYLE;
            context.lineWidth = 0;

            context.moveTo( 0, 0 );
            context.beginPath();
            context.lineTo( canvas.width, 0 );
            context.lineTo( canvas.width, canvas.height );
            context.lineTo( 0, canvas.height );
            context.lineTo( 0, 0 );
            context.closePath();
            context.fill();
        }
    }
    catch ( e ) {
        Evernote.logger.error( "ContentVeil.makeShadow() failed: error = " + e );
    }
};

Evernote.ContentVeil.prototype.revealRect = function ( highlightRect, isStrokeEnabled, contextAttr ) {
    Evernote.logger.debug( "ContentVeil.revealRect(): [" + highlightRect.top + ", " + highlightRect.left + ", " + highlightRect.right + ", " + highlightRect.bottom + "]" );

    try {
        for ( var i = 0; i < this._canvases.length; ++i ) {
            var canvas = this._canvases[ i ];
            var canvasRect = { left : 0,
                               top : i * this.constructor.MAX_CANVAS_HEIGHT,
                               right : Evernote.DOMHelpers.getBodyWidth( this._tab ),
                               bottom : i * this.constructor.MAX_CANVAS_HEIGHT + canvas.height };

            var resultRect = Evernote.Utils.rectIntersection( highlightRect, canvasRect );
            if ( !resultRect ) {
                continue;
            }

            var context = canvas.getContext( "2d" );
            var strokeWidth = 0;

            if ( isStrokeEnabled ) {
                strokeWidth = Math.max( 2, (Math.min( (resultRect.bottom - resultRect.top), (resultRect.right - resultRect.left) ) / 8) );
                strokeWidth = Math.min( (contextAttr && contextAttr.lineWidth) ? contextAttr.lineWidth : this.constructor.STROKE_WIDTH, strokeWidth );
            }

            context.lineWidth = strokeWidth;

            var left = resultRect.left - strokeWidth;
            var top = resultRect.top - strokeWidth - i * this.constructor.MAX_CANVAS_HEIGHT;
            var width = (resultRect.right - resultRect.left) + strokeWidth * 2;
            var height = (resultRect.bottom - resultRect.top) + strokeWidth * 2;

            context.clearRect( left, top, width, height );
            context.fillStyle = (contextAttr && contextAttr.fillStyle) ? contextAttr.fillStyle : this.constructor.FILL_HIGHLIGHT_STYLE;
            context.fillRect( left, top, width, height );

            if ( isStrokeEnabled ) {
                context.strokeStyle = (contextAttr && contextAttr.strokeStyle) ? contextAttr.strokeStyle : this.constructor.STROKE_STYLE;
                context.lineCap = (contextAttr && contextAttr.lineCap) ? contextAttr.lineCap : this.constructor.STROKE_CAP;
                context.lineJoin = (contextAttr && contextAttr.lineJoin) ? contextAttr.lineJoin : this.constructor.STROKE_JOIN;
                context.strokeRect( left, top, width, height );
            }

            if ( highlightRect.bottom < i * this.constructor.MAX_CANVAS_HEIGHT + canvas.height ) {
                break;
            }
        }
    }
    catch ( e ) {
        Evernote.logger.error( "ContentVeil.revealRect() failed: error = " + e );
    }
};

Evernote.ContentVeil.prototype.clear = function() {
    Evernote.logger.debug( "ContentVeil.clear()" );

    try {
        for ( var i = 0; i < this._canvases.length; ++i ) {
            var canvas = this._canvases[ i ];
            if ( canvas && canvas.parentNode ) {
                canvas.parentNode.removeChild( canvas );
            }

            this._canvases[ i ] = null;
        }

        this._canvases = [ ];
    }
    catch ( e ) {
        Evernote.logger.error( "ContentVeil.clear() failed: error = " + e );
    }
};

Evernote.ContentVeil.prototype.createCanvases = function () {
    Evernote.logger.debug( "ContentVeil.createCanvases()" );

    try {
        var canvases = [ ];
        var overallVeilHeight = 0;
        var bodyHeight = Evernote.DOMHelpers.getBodyHeight( this._tab );
        var bodyWidth = Evernote.DOMHelpers.getBodyWidth( this._tab );
        var index = 0;

        while ( overallVeilHeight < bodyHeight ) {
            var canvas = this._tab.document.createElement( "CANVAS" );
            if ( !(canvas instanceof HTMLCanvasElement) ) {
                var doc = document.implementation.createHTMLDocument( "temp" );
                canvas = doc.createElement( "CANVAS" );
            }

            canvas.setAttribute( "id", this.constructor.VEIL_ID + "_" + index );
            canvas.setAttribute( "class", this.constructor.VEIL_CLASS );

            canvas.height = ((bodyHeight - overallVeilHeight) > this.constructor.MAX_CANVAS_HEIGHT) ? this.constructor.MAX_CANVAS_HEIGHT
                                                                                                    : (bodyHeight - overallVeilHeight);
            canvas.width = bodyWidth;

            for ( var key in this.constructor.VEIL_STYLES ) {
                canvas.style[ key ] = this.constructor.VEIL_STYLES[ key ];
            }

            if ( canvases.length != 0 ) {
                canvas.style.top = overallVeilHeight + "px";
            }

            if ( !canvas.parentElement ) {
                var parentElem = (this._tab.document.body.parentElement) ? this._tab.document.body.parentElement : this._tab.document.body;
                parentElem.appendChild( canvas );
            }

            canvas.style.display = "";

            canvases.push( canvas );
            ++index;
            overallVeilHeight += canvas.height;
        }

        this._canvases = canvases;
    }
    catch ( e ) {
        Evernote.logger.error( "ContentVeil.createCanvases() failed: error = " + e );
    }
};