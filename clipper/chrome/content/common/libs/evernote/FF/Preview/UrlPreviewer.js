//"use strict";

Evernote.UrlPreviewer = function UrlPreviewer( tab ) {
    this._tab = (tab) ? tab : window;
};

Evernote.UrlPreviewer.PREVIEW_CONTENT_ID = "evernotePreviewContainerID";
Evernote.UrlPreviewer.PREVIEW_CONTENT_CLASS = "evernotePreviewContainer";
Evernote.UrlPreviewer.PREVIEW_CONTENT_URL_CLASS = "evernotePreviewUrlContainer";
Evernote.UrlPreviewer.URL_MAX_LENGTH = 100;

Evernote.UrlPreviewer.prototype._tab = null;
Evernote.UrlPreviewer.prototype._elem = null;

Evernote.UrlPreviewer.prototype.createContent = function() {
    Evernote.logger.debug( "ContentPreviewScript.createContent()" );

    try {
        var doc = this._tab.document;
        var elem = doc.getElementById( this.constructor.PREVIEW_CONTENT_ID );

        if ( !elem ) {
            var favIconUrl = "http://www.google.com/s2/favicons?domain=" + Evernote.UrlHelpers.urlDomain( this._tab.location.href ).toLowerCase();
            elem = Evernote.DOMHelpers.createUrlClipContent( doc, doc.title, Evernote.Utils.shortWord( this._tab.location.href, this.constructor.URL_MAX_LENGTH ), favIconUrl );

            elem.setAttribute( "id", this.constructor.PREVIEW_CONTENT_ID );
            elem.setAttribute( "class", this.constructor.PREVIEW_CONTENT_CLASS + " " + this.constructor.PREVIEW_CONTENT_URL_CLASS );

            elem.style.display = "none";
            elem.style.position = "absolute";

            var parentElem = doc.body.parentElement || doc.body;
            parentElem.appendChild( elem );
        }

        this._elem = elem;
    }
    catch ( e ) {
        Evernote.logger.error( "ContentPreviewScript.createContent() failed: error = " + e );
    }
};

Evernote.UrlPreviewer.prototype.destroyContent = function () {
    Evernote.logger.debug( "ContentPreviewScript.destroyContent()" );

    var elem = this._elem;
    if ( elem && elem.parentNode ) {
        elem.parentNode.removeChild( elem );
        this._elem = null;
    }
};

Evernote.UrlPreviewer.prototype.showContent = function () {
    Evernote.logger.debug( "ContentPreviewScript.showContent()" );

    try {
        var elem = this._elem;
        if ( elem ) {
            elem.style.display = "";

            var compStyles = this._tab.getComputedStyle( elem, "" );
            var width = parseInt( compStyles.getPropertyValue( "width" ) );
            var height = parseInt( compStyles.getPropertyValue( "height" ) );

            if ( width && height ) {
                elem.style.marginLeft = (-width / 2) + "px";
                elem.style.marginTop = (-height / 2) + "px";
                //Position element to the center of the screen (including scroll position)
                elem.style.setProperty( "top", ( parseInt( compStyles.getPropertyValue( "top" ) ) + this._tab.document.documentElement.scrollTop ) + "px", "important" );
            }
        }
    }
    catch ( e ) {
        Evernote.logger.error( "ContentPreviewScript.showContent() failed: error = " + e );
    }
};






