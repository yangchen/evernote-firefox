//"use strict";

Evernote.AbstractContentScript = function () {
};

Evernote.AbstractContentScript.prototype.injectStyleSheets = function ( styleSheets, doc ) {
    Evernote.logger.debug( "AbstractContentScript.injectStyleSheets()" );

    try {
        for ( var i = 0; i < styleSheets.length; ++i ) {
            var css = styleSheets[ i ];
            var links = doc.getElementsByTagName( "link" );
            var isDuplicate = false;

            for ( var j = 0; j < links.length; ++j ) {
                if ( links[ j ].getAttribute( "href" ).toLowerCase() == css.toLowerCase() ) {
                    isDuplicate = true;
                    break;
                }
            }

            if ( isDuplicate ) {
                continue;
            }

            var link = doc.createElement( "link" );
            link.setAttribute( "rel", "stylesheet" );
            link.setAttribute( "type", "text/css" );
            link.setAttribute( "href", css );

            var head = Evernote.DOMHelpers.getHead( doc );
            if ( head ) {
                head.appendChild( link );
            }
        }
    }
    catch ( e ) {
        Evernote.logger.error( "AbstractContentScript.injectStyleSheets() failed: error = " + e );
    }
};