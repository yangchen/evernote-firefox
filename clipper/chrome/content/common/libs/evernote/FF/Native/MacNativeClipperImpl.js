//"use strict";

Evernote.MacNativeClipperImpl = function MacNativeClipperImpl() {
};

Evernote.inherit( Evernote.MacNativeClipperImpl, Evernote.NativeClipperImpl, true );

Evernote.MacNativeClipperImpl.CONTAINER_PATH = "~/Library/Containers/com.evernote.Evernote";

Evernote.MacNativeClipperImpl.isResponsibleFor = function() {
    return Evernote.Platform.isMacOS();
};

Evernote.MacNativeClipperImpl.prototype._tempDir = null;

Evernote.MacNativeClipperImpl.prototype.isEnabled = function() {
    Evernote.logger.debug( "MacNativeClipperImpl.isEnabled()" );

    try {
        return Evernote.nativeAccessor.isEvernoteInstalled();
    }
    catch ( e ) {
        Evernote.logger.error( "MacNativeClipperImpl.isEnabled() failed: error = " + e );
    }

    return false;
};

Evernote.MacNativeClipperImpl.prototype.createNewNote = function() {
    Evernote.logger.debug( "MacNativeClipperImpl.createNewNote()" );

    try {
        Evernote.nativeAccessor.createNewNote();
    }
    catch( e ) {
        Evernote.logger.error( "MacNativeClipperImpl.createNewNote() failed: error = " + e );
    }
};

Evernote.MacNativeClipperImpl.prototype.clip = function( tab, domElement, clipType, isClipGo ) {
    Evernote.logger.debug( "MacNativeClipperImpl.clip()" );

    try {
        var clip = new Evernote.Clip( tab, new Evernote.ClipFullStylingStrategy( tab ) );
        if ( clipType == Evernote.FFClipper.CLIP_FULL_PAGE ) {
            clip.clipFullPage();
        }
        else if ( clipType == Evernote.FFClipper.CLIP_IMAGE ) {
            clip.clipImage( domElement );
        }
        else if ( clipType == Evernote.FFClipper.CLIP_SELECTION ) {
            clip.clipSelection();
        }
        else if ( clipType == Evernote.FFClipper.CLIP_ARTICLE ) {
            clip.clipArticle( domElement );
        }
        else if ( clipType == Evernote.FFClipper.CLIP_URL ) {
            clip.clipUrl();
        }

        if ( !clip.content ) {
            Evernote.logger.warn( "MacNativeClipperImpl.clip(): empty clip" );
            return;
        }

        this._tempDir = this.getClipDir();
        var filePath = this._tempDir.path + "/firefox-clip.html";

        Evernote.logger.debug( "saving clip to file " + filePath );
        Evernote.fileManager.writeFile( filePath, clip.content, false );

        Evernote.nativeAccessor.addToEn( filePath,
                                         tab.document.title,
                                         tab.document.location.href,
                                         "UTF-8",
                                         isClipGo );
    }
    catch ( e ) {
        Evernote.logger.debug( "MacNativeClipperImpl.clip() failed: error = " + e );
    }
};

Evernote.MacNativeClipperImpl.prototype.getEvernoteDir = function () {
    var fileMgr = new Evernote.FileManager();
    // check if this is MacOS 10.8 with sandboxing
    if ( fileMgr.doesDirExist( this.constructor.CONTAINER_PATH ) ) {
        var dir = Components.classes[ "@mozilla.org/file/local;1" ].createInstance( Components.interfaces.nsILocalFile );
        dir.initWithPath( this.constructor.CONTAINER_PATH );
        dir.QueryInterface( Components.interfaces.nsIFile );

        dir.append( "Data" );
        dir.append( "Library" );
        dir.append( "Caches" );
        dir.append( "TemporaryItems" );
    }
    else {
        dir = Components.classes[ "@mozilla.org/file/directory_service;1" ].getService( Components.interfaces.nsIProperties ).
            get( "TmpD", Components.interfaces.nsIFile );
    }

    dir.append( "evernote-mac-clips" );
    return dir;
};

Evernote.MacNativeClipperImpl.prototype.getClipDir = function() {
    var dir = this.getEvernoteDir();
    dir.append( "clip-tmp" );
    
    try {
        dir.createUnique( Components.interfaces.nsIFile.DIRECTORY_TYPE, 0744 );
    }
    catch ( e ) {
        dir = this.getEvernoteDir();
        try {
            dir.remove( true );
        }
        catch ( e ) {
        }

        dir = this.getEvernoteDir();
        dir.append( "clip-tmp" );
        dir.createUnique( Components.interfaces.nsIFile.DIRECTORY_TYPE, 0744 );
    }

    return dir;
};