//"use strict";

Evernote.MacNativeAccessor = function MacNativeAccessor() {
    this.__defineGetter__( "addToEnFn", this.getAddToEnFn );
    this.__defineGetter__( "isEvernoteInstalledFn", this.getIsEvernoteInstalledFn );
    this.__defineGetter__( "createNewNoteFn", this.getCreateNewNoteFn );
    this.initialize();
};

Evernote.MacNativeAccessor.prototype._accessor = null;
Evernote.MacNativeAccessor.prototype._addToEnFn = null;
Evernote.MacNativeAccessor.prototype._isEvernoteInstalledFn = null;
Evernote.MacNativeAccessor.prototype._createNewNoteFn = null;

Evernote.MacNativeAccessor.prototype.initialize = function() {
    Evernote.logger.debug( "MacNativeAccessor.initialize()" );

    if ( !this._accessor ) {
        Components.utils.import( "resource://gre/modules/ctypes.jsm" );
        var Types = ctypes.types;

        try {
            this._accessor = ctypes.open( this.getLibraryPathX64() );
        }
        catch ( e ) {
            // FF 32-bit mode
            this._accessor = ctypes.open( this.getLibraryPathX32() );
        }

        if ( !this._accessor ) {
            Evernote.logger.error( "Failed to load Mac native accessor" );
        }
    }
};

Evernote.MacNativeAccessor.prototype.cleanUp = function() {
    if ( this._accessor ) {
        this._accessor.close();
        this._accessor = null;

        this._addToEnFn = null;
        this._isEvernoteInstalledFn = null;
        this._createNewNoteFn = null;
    }
};

Evernote.MacNativeAccessor.prototype.addToEn = function( fileName, title, sourceUrl, charset, isClipGo ) {
    Evernote.logger.debug( "MacNativeAccessor.addToEn()" );

    if ( this.addToEnFn ) {
        if ( typeof fileName == "string" && typeof title == "string" && typeof sourceUrl == "string" &&
             typeof charset == "string" && typeof isClipGo == "boolean" ) {
           this.addToEnFn( fileName,
                           fileName.length,
                           title,
                           title.length,
                           sourceUrl,
                           sourceUrl.length,
                           charset,
                           charset.length,
                           isClipGo );
        }
    }
};

Evernote.MacNativeAccessor.prototype.isEvernoteInstalled = function() {
    Evernote.logger.debug( "MacNativeAccessor.isEvernoteInstalled()" );
    if ( this.isEvernoteInstalledFn ) {
        return this.isEvernoteInstalledFn();
    }
};

Evernote.MacNativeAccessor.prototype.createNewNote = function() {
    Evernote.logger.debug( "MacNativeAccessor.createNewNote()" );
    if ( this.createNewNoteFn ) {
        this.createNewNoteFn();
    }
};

Evernote.MacNativeAccessor.prototype.getAddToEnFn = function() {
    if ( !this._addToEnFn ) {
        this._addToEnFn = this._accessor.declare( "AddToEn",
                                                  ctypes.default_abi,
                                                  ctypes.void_t,
                                                  ctypes.jschar.ptr,
                                                  ctypes.unsigned_int,
                                                  ctypes.jschar.ptr,
                                                  ctypes.unsigned_int,
                                                  ctypes.jschar.ptr,
                                                  ctypes.unsigned_int,
                                                  ctypes.jschar.ptr,
                                                  ctypes.unsigned_int,
                                                  ctypes.bool );
        if ( !this._addToEnFn ) {
            Evernote.logger.error( "Failed to load Mac native method AddToEn" );
        }
    }

    return this._addToEnFn;
};

Evernote.MacNativeAccessor.prototype.getIsEvernoteInstalledFn = function() {
    if ( !this._isEvernoteInstalledFn ) {
        this._isEvernoteInstalledFn = this._accessor.declare( "IsEvernoteInstalled",
                                                              ctypes.default_abi,
                                                              ctypes.bool );
        if ( !this._isEvernoteInstalledFn ) {
            Evernote.logger.error( "Failed to load Mac native method IsEvernoteInstalled" );
        }
    }

    return this._isEvernoteInstalledFn;
};

Evernote.MacNativeAccessor.prototype.getCreateNewNoteFn = function() {
    if ( !this._createNewNoteFn ) {
        this._createNewNoteFn = this._accessor.declare( "CreateNewNote",
                                                        ctypes.default_abi,
                                                        ctypes.void_t );
        if ( !this._createNewNoteFn ) {
            Evernote.logger.error( "Failed to load Mac native method CreateNewNote" );
        }
    }

    return this._createNewNoteFn;
};

Evernote.MacNativeAccessor.prototype.getLibraryPathX64 = function() {
    return Evernote.Utils.getExtensionDir() + "/platform/Darwin_x86_64-gcc3/components/libenbar_x64.dylib";
};

Evernote.MacNativeAccessor.prototype.getLibraryPathX32 = function() {
    return Evernote.Utils.getExtensionDir() + "/platform/Darwin_x86-gcc3/components/libenbar_x32.dylib";
};