//"use strict";

Evernote.ImageLoader = function ImageLoader() {
    this.__defineGetter__( "ioService", this.getIOService );
};

Evernote.ImageLoader.prototype._ioService = null;

Evernote.ImageLoader.prototype.startLoad = function( urls, callback, exceedTimeout ) {
    Evernote.logger.debug( "ImageLoader.startLoad()" );

    if ( urls.length > 0 ) {
        var ioService = this.ioService;
        var results = [ ];

        var channelOpenFunc = function( url ) {
            if ( typeof url == "string" ) {
                var uri = ioService.newURI( url, null, null );
                var channel = ioService.newChannelFromURI( uri );

                var listener = new Evernote.StreamListener( url, channel, function( result ) {
                    results.push( result );
                    if ( results.length >= urls.length && typeof callback == "function" ) {
                        callback( results );
                     }
                } );

                channel.notificationCallbacks = listener;
                channel.asyncOpen( listener, null );

                setTimeout( function() {
                    channel.cancel( -1 );
                }, exceedTimeout );
            }
        };

        for ( var i = 0, len = urls.length; i < len; ++i ) {
            channelOpenFunc( urls[ i ] );
        }
    }
    else if ( typeof callback == "function" ) {
        callback( [ ] );
    }
};

Evernote.ImageLoader.prototype.getIOService = function() {
    if ( !this._ioService ) {
        this._ioService = Components.classes[ "@mozilla.org/network/io-service;1" ].getService( Components.interfaces.nsIIOService );
    }

    return this._ioService;
};