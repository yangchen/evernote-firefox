Evernote.StreamListener = function StreamListener( url, channel, callback ) {
    this.__defineGetter__( "binaryStream", this.getBinaryStream );
    this.initialize( url, channel, callback );
};

Evernote.StreamListener.prototype._url = null;
Evernote.StreamListener.prototype._data = null;
Evernote.StreamListener.prototype._channel = null;
Evernote.StreamListener.prototype._callback = null;
Evernote.StreamListener.prototype._binaryStream = null;

Evernote.StreamListener.prototype.initialize = function( url, channel, callback ) {
    if ( typeof url == "string" ) {
        this._url = url;
    }
    if ( channel && typeof channel == "object" ) {
        this._channel = channel;
    }
    if ( typeof callback == "function" ) {
        this._callback = callback;
    }
};

Evernote.StreamListener.prototype.onStartRequest = function ( /*request, context*/ ) {
    this._data = [ ];
};

Evernote.StreamListener.prototype.onDataAvailable = function( request, context, stream, sourceOffset, length ) {
    this.binaryStream.setInputStream( stream );
    this._data = this._data.concat( this.binaryStream.readByteArray( length ) );
};

Evernote.StreamListener.prototype.onStopRequest = function ( request, context, status ) {
    Evernote.logger.debug( "StreamListener.onStopRequest()" );

    if ( typeof this._callback == "function" ) {
        if ( Components.isSuccessCode( status ) ) {
            //Evernote.Utils.errorConsole( "Finished: " + this._url + " " + this._data.length + " " + this._channel.contentType );
            Evernote.logger.debug( "Finished load image: " + this._url );

            this._callback( {
                url: this._url,
                mime: this._channel.contentType,
                data: this._data
            } );
        }
        else {
            Evernote.logger.error( "Failed load image: " + this._url + ", status = " + status );

            this._callback( {
                url: this._url,
                mime: "",
                data: [ ]
            } );
        }
    }

    this._channel = null;
};

Evernote.StreamListener.prototype.getInterface = function( iid ) {
    try {
        return this.QueryInterface( iid );
    }
    catch ( e ) {
        throw Components.results.NS_NOINTERFACE;
    }
};

Evernote.StreamListener.prototype.QueryInterface = function( iid ) {
    if ( iid.equals( Components.interfaces.nsISupports ) ||
         iid.equals( Components.interfaces.nsIInterfaceRequestor ) ||
         iid.equals( Components.interfaces.nsIStreamListener ) ) {
        return this;
    }

    throw Components.results.NS_NOINTERFACE;
};

Evernote.StreamListener.prototype.getBinaryStream = function() {
    if ( !this._binaryStream ) {
        this._binaryStream = Components.classes[ "@mozilla.org/binaryinputstream;1" ].createInstance( Components.interfaces.nsIBinaryInputStream );
    }

    return this._binaryStream;
};