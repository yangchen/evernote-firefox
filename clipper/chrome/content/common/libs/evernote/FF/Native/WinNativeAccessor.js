//"use strict";

Evernote.WinNativeAccessor = function WinNativeAccessor() {
    this.__defineGetter__( "addToEnFn", this.getAddToEnFn );
    this.__defineGetter__( "isEvernoteInstalledFn", this.getIsEvernoteInstalledFn );
    this.__defineGetter__( "createNewNoteFn", this.getCreateNewNoteFn );
    this.initialize();
};

Evernote.WinNativeAccessor.prototype._accessor = null;
Evernote.WinNativeAccessor.prototype._addToEnFn = null;
Evernote.WinNativeAccessor.prototype._isEvernoteInstalledFn = null;
Evernote.WinNativeAccessor.prototype._createNewNoteFn = null;
Evernote.WinNativeAccessor.prototype._imageInfoType = null;

Evernote.WinNativeAccessor.prototype.initialize = function() {
    Evernote.logger.debug( "WinNativeAccessor.initialize()" );

    if ( !this._accessor ) {
        Components.utils.import( "resource://gre/modules/ctypes.jsm" );
        var Types = ctypes.types;

        this._accessor = ctypes.open( this.getLibraryPathX32() );
        if ( !this._accessor ) {
            Evernote.logger.error( "Failed to load Win native accessor" );
        }
    }
};

Evernote.WinNativeAccessor.prototype.cleanUp = function() {
    if ( this._accessor ) {
        this._accessor.close();
        this._accessor = null;

        this._addToEnFn = null;
        this._isEvernoteInstalledFn = null;
        this._createNewNoteFn = null;
        this._imageInfoType = null;
    }
};

Evernote.WinNativeAccessor.prototype.addToEn = function( title, content, sourceUrl, images, isClipGo ) {
    Evernote.logger.debug( "WinNativeAccessor.addToEn()" );

    if ( this.addToEnFn ) {
        if ( typeof title == "string" && typeof content == "string" && typeof sourceUrl == "string" &&
             images instanceof Array && typeof isClipGo == "boolean" ) {
            var imageInfos = [ ];
            for ( var i = 0, len = images.length; i < len; ++i ) {
                var image = images[ i ];
                var imageInfo = new this._imageInfoType( ctypes.jschar.array()( image.url ),
                                                         image.url.length,
                                                         ctypes.jschar.array()( image.mime ),
                                                         image.mime.length,
                                                         ctypes.uint8_t.array()( image.data ),
                                                         image.data.length );
                imageInfos.push( imageInfo );
            }

            var ImageInfoArrayType = this._imageInfoType.array( imageInfos.length );
            var imagesData = new ImageInfoArrayType( imageInfos );

            this.addToEnFn( title, content, sourceUrl, imagesData, imageInfos.length, isClipGo );
        }
    }
};

Evernote.WinNativeAccessor.prototype.isEvernoteInstalled = function() {
    Evernote.logger.debug( "WinNativeAccessor.isEvernoteInstalled()" );
    if ( this.isEvernoteInstalledFn ) {
        return this.isEvernoteInstalledFn();
    }
};

Evernote.WinNativeAccessor.prototype.createNewNote = function() {
    Evernote.logger.debug( "WinNativeAccessor.createNewNote()" );
    if ( this.createNewNoteFn ) {
        this.createNewNoteFn();
    }
};

Evernote.WinNativeAccessor.prototype.getAddToEnFn = function() {
    if ( !this._addToEnFn ) {
        this._imageInfoType = new ctypes.StructType( "ImageInfo", [
            { url: ctypes.jschar.ptr },
            { urlLength: ctypes.unsigned_int },
            { mime: ctypes.jschar.ptr },
            { mimeLength: ctypes.unsigned_int },
            { data: ctypes.uint8_t.ptr },
            { dataLength: ctypes.unsigned_int }
        ] );

        this._addToEnFn = this._accessor.declare( "AddToEn",
                                                  ctypes.winapi_abi,
                                                  ctypes.void_t,
                                                  ctypes.jschar.ptr,
                                                  ctypes.jschar.ptr,
                                                  ctypes.jschar.ptr,
                                                  this._imageInfoType.ptr,
                                                  ctypes.unsigned_int,
                                                  ctypes.bool );
        if ( !this._addToEnFn ) {
            Evernote.logger.error( "Failed to load Win native method AddToEn" );
        }
    }

    return this._addToEnFn;
};

Evernote.WinNativeAccessor.prototype.getIsEvernoteInstalledFn = function() {
    if ( !this._isEvernoteInstalledFn ) {
        this._isEvernoteInstalledFn = this._accessor.declare( "IsEvernoteInstalled",
                                                              ctypes.winapi_abi,
                                                              ctypes.bool );
        if ( !this._isEvernoteInstalledFn ) {
            Evernote.logger.error( "Failed to load Win native method IsEvernoteInstalled" );
        }
    }

    return this._isEvernoteInstalledFn;
};

Evernote.WinNativeAccessor.prototype.getCreateNewNoteFn = function() {
    if ( !this._createNewNoteFn ) {
        this._createNewNoteFn = this._accessor.declare( "CreateNewNote",
                                                        ctypes.winapi_abi,
                                                        ctypes.void_t );
        if ( !this._createNewNoteFn ) {
            Evernote.logger.error( "Failed to load Win native method CreateNewNote" );
        }
    }

    return this._createNewNoteFn;
};

Evernote.WinNativeAccessor.prototype.getLibraryPathX32 = function() {
    //return "C:/Users/paklin/AppData/Roaming/Mozilla/Firefox/Profiles/6kutbd50.Mikel/extensions/mozclip.dll";
    return Evernote.Utils.getExtensionDir() + "/platform/WINNT_x86-msvc/components/mozclip.dll";
};