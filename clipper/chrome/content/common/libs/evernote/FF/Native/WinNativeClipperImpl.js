//"use strict";

Evernote.WinNativeClipperImpl = function WinNativeClipperImpl() {
    this.__defineGetter__( "imageLoader", this.getImageLoader );
};

Evernote.inherit( Evernote.WinNativeClipperImpl, Evernote.NativeClipperImpl, true );

Evernote.WinNativeClipperImpl.EXCEED_TIMEOUT = 5000;
Evernote.WinNativeClipperImpl.isResponsibleFor = function() {
    return Evernote.Platform.isWin();
};

Evernote.WinNativeClipperImpl.prototype._imageLoader = null;

Evernote.WinNativeClipperImpl.prototype.isEnabled = function() {
    Evernote.logger.debug( "WinNativeClipperImpl.isEnabled()" );

    try {
        return Evernote.nativeAccessor.isEvernoteInstalled();
    }
    catch ( e ) {
        Evernote.logger.error( "WinNativeClipperImpl.isEnabled() failed: error = " + e );
    }

    return false;
};

Evernote.WinNativeClipperImpl.prototype.createNewNote = function() {
    Evernote.logger.debug( "WinNativeClipperImpl.createNewNote()" );

    try {
        Evernote.nativeAccessor.createNewNote();
    }
    catch( e ) {
        Evernote.logger.error( "WinNativeClipperImpl.createNewNote() failed: error = " + e );
    }
};

Evernote.WinNativeClipperImpl.prototype.clip = function( tab, domElement, clipType, isClipGo ) {
    Evernote.logger.debug( "WinNativeClipperImpl.clip()" );

    try {
        var clip = new Evernote.Clip( tab, new Evernote.ClipFullStylingStrategy( tab ) );
        if ( clipType == Evernote.FFClipper.CLIP_FULL_PAGE ) {
            clip.clipFullPage();
        }
        else if ( clipType == Evernote.FFClipper.CLIP_SELECTION ) {
            clip.clipSelection();
        }
        else if ( clipType == Evernote.FFClipper.CLIP_ARTICLE ) {
            clip.clipArticle( domElement );
        }
        else if ( clipType == Evernote.FFClipper.CLIP_URL ) {
            clip.clipUrl();
        }
        else if ( clipType == Evernote.FFClipper.CLIP_IMAGE ) {
            clip.clipImage( domElement );
        }

        if ( !clip.content ) {
            Evernote.logger.warn( "WinNativeClipperImpl.clip(): empty clip" );
            return;
        }

        var urls = [ ];
        for ( var i = 0, len = clip.imagesUrls.length; i < len; ++i ) {
            urls.push( Evernote.UrlHelpers.makeAbsolutePath( clip.docBase, clip.imagesUrls[ i ] ) );
        }

        var self = this;
        this.imageLoader.startLoad( urls, function( images ) {
            if ( images instanceof Array ) {
                images = images.filter( function( element/*, index, array*/ ) {
                    return element.data && element.data.length > 0;
                } );

                Evernote.nativeAccessor.addToEn( tab.document.title,
                                                 clip.content,
                                                 tab.document.location.href,
                                                 images,
                                                 isClipGo );
            }
        }, this.constructor.EXCEED_TIMEOUT );
    }
    catch ( e ) {
        Evernote.logger.error( "WinNativeClipperImpl.clip() failed: error = " + e );
    }
};

Evernote.WinNativeClipperImpl.prototype.getImageLoader = function() {
    if ( !this._imageLoader ) {
        this._imageLoader = new Evernote.ImageLoader();
    }

    return this._imageLoader;
};