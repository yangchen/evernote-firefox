//"use strict";

Evernote.FFNotesSearcher = function FFNotesSearcher( popup ) {
    this._popup = popup;
};

Evernote.FFNotesSearcher.THUMBNAIL_MIN_SIZE = 50;
Evernote.FFNotesSearcher.NOTELIST_PAYLOAD_SIZE = 20;
Evernote.FFNotesSearcher.SOURCE_URL_MAX_DISPLAY_LENGTH = 32;
Evernote.FFNotesSearcher.NOTELIST_PAGE_SIZE = 6;
Evernote.FFNotesSearcher.NOTELIST_ITEM_HEIGHT = 102;
Evernote.FFNotesSearcher.NOTELIST_FETCH_TIMEOUT = 400;
Evernote.FFNotesSearcher.THUMBNAIL_SIZE = 100;
Evernote.FFNotesSearcher.NOTELIST_SCROLLTO_DELAY = 200;
Evernote.FFNotesSearcher.SNIPPET_MAX_LENGTH = 150;
Evernote.FFNotesSearcher.SIMPLE_DATE_FORMAT = "M/d/yy";

Evernote.FFNotesSearcher.prototype._popup = null;
Evernote.FFNotesSearcher.prototype._findProc = null;
Evernote.FFNotesSearcher.prototype._findContextProc = null;
Evernote.FFNotesSearcher.prototype._notesSnippetsProc = null;
Evernote.FFNotesSearcher.prototype._noteListScrollTimer = null;

Evernote.FFNotesSearcher.prototype.doNotesSearch = function( filter ) {
    Evernote.logger.debug( "FFNotesSearcher.doNotesSearch()" );

    this.abortFindProc();
    var noteFilter = (filter instanceof Evernote.NoteFilter) ? filter : Evernote.context.noteFilter;
    if ( noteFilter.words ) {
        Evernote.jQuery( "#notesSearchQuery" ).val( noteFilter.words );
    }

    var self = this;
    var noteList = null;

    Evernote.jQuery( "#notesList" ).ascrollable( { debug : false,
            pageSize : this.constructor.NOTELIST_PAYLOAD_SIZE,
            pageBuffer : this.constructor.NOTELIST_PAGE_SIZE,
            itemHeight : this.constructor.NOTELIST_ITEM_HEIGHT,
            loadProcTimeout : this.constructor.NOTELIST_FETCH_TIMEOUT,
            placeholderItem : this.createPlaceholderNoteListItem(),
            loadingContainer : Evernote.jQuery( "#notesListLoadingContainer" ),
            emptyContainer : Evernote.jQuery( "#notesListEmptyContainer" ),

            onScroll : function( event ) {
                if ( self._noteListScrollTimer ) {
                    clearTimeout( self._noteListScrollTimer );
                }

                self._noteListScrollTimer = setTimeout( function() {
                    var pos = event.target.scrollTop;
                    Evernote.logger.debug( "Remembering scrollTop: " + pos );

                    var state = Evernote.context.state;
                    state.noteListScrollTop = pos;
                    Evernote.context.state = state;

                    self._noteListScrollTimer = null;
                }, 600 );
            },

            onEmpty : function() {
                Evernote.jQuery( "#notesList > *" ).hide();
                Evernote.jQuery( "#notesListEmptyContainer" ).show();
            },

            totalItems : function() {
                return (noteList) ? noteList.totalNotes : 0;
            },

            onLoadPage : function( pageIndex, pageSize, totalPages ) {
                self._findProc = self._popup.remoteFindNotes( noteFilter, pageIndex * pageSize, pageSize * totalPages,
                    function( response/*, textStatus */) {
                        Evernote.logger.debug( "findProc success" );

                        self.processNoteList( response.result.noteList.notes );
                        noteList = response.result.noteList;

                        if ( noteFilter.words ) {
                            Evernote.jQuery( "#myNotesTabButton" ).text( Evernote.localizer.getMessage( "notes_titleWithCount", [ noteList.totalNotes ] ) );
                        }
                        else {
                            Evernote.jQuery( "#myNotesTabButton" ).text( Evernote.localizer.getMessage( "notes_titleAllWithCount", [ noteList.totalNotes ] ) );
                        }

                        self._findProc = null;
                        Evernote.jQuery( "#notesList" ).trigger( "afterLoadPage", [ pageIndex, pageSize, totalPages ] );
                    },
                    function( response, textStatus, xhr ) {
                        Evernote.logger.debug( "findProc failed" );

                        if ( response && response.errors ) {
                            if ( response.errors.length == 1 ) {
                                Evernote.viewManager.showError( response.errors[ 0 ] );
                            }
                            else {
                                Evernote.viewManager.showErrors( response.errors );
                            }
                        }
                        else {
                            Evernote.jQuery( "#notesList .notelistMessageContainer" ).hide();

                            var msg = null;
                            if ( xhr.status != 200 ) {
                                msg = Evernote.viewManager.getLocalizedHttpErrorMessage( xhr, textStatus, response );
                            }

                            if ( !msg ) {
                                msg = Evernote.localizer.getMessage( "UnknownError" );
                            }
                            Evernote.jQuery( "#notesListErrorContainer" ).text( msg ).show();
                        }

                        self._findProc = null;
                    } );

                return false;
            },

            onAfterLoadPage : function( pageIndex/*, pageSize, totalPages*/ ) {
                var state = Evernote.context.state;
                if ( pageIndex == 0 && state.noteListScrollTop ) {
                    setTimeout( function() {
                        Evernote.jQuery( "#notesList" ).scrollTo( state.noteListScrollTop );
                    }, self.constructor.NOTELIST_SCROLLTO_DELAY );
                }
            },

            items : function() {
                var items = [ ];
                if ( noteList ) {
                    for ( var i = 0; i < noteList.notes.length; ++i ) {
                        items[ noteList.startIndex + i ] = self.createNoteListItem( noteList.notes[ i ], (noteFilter) ? noteFilter.words : null );
                    }
                }

                return items;
            }
        } );
};

Evernote.FFNotesSearcher.prototype.doNotesContextSearch = function( url ) {
    Evernote.logger.debug( "FFNotesSearcher.doNotesContextSearch(): " + url  );

    this.abortFindContextProc();
    var domainStr = Evernote.UrlHelpers.urlTopDomain( url );
    Evernote.jQuery( "#notesContextListTitle" ).html( domainStr );

    if ( typeof url != 'string' || url.length == 0 ) {
        Evernote.logger.warn( "Tried to find notes relative to empty URL... Ignoring!" );
        return;
    }

    var noteContextFilter = new Evernote.NoteFilter();
    noteContextFilter.words = Evernote.UrlHelpers.urlToSearchQuery( url, "sourceUrl:" );

    var self = this;
    var noteContextList = null;

    Evernote.jQuery( "#notesContextList" ).ascrollable( { debug : false,
            pageSize : this.constructor.NOTELIST_PAYLOAD_SIZE,
            pageBuffer : this.constructor.NOTELIST_PAGE_SIZE,
            itemHeight : this.constructor.NOTELIST_ITEM_HEIGHT,
            loadProcTimeout : this.constructor.NOTELIST_FETCH_TIMEOUT,
            placeholderItem : this.createPlaceholderNoteListItem(),
            loadingContainer : Evernote.jQuery( "#notesContextListLoadingContainer" ),
            emptyContainer : Evernote.jQuery( "#notesContextListEmptyContainer" ),

            onScroll : function( /*event*/ ) {
                if ( self._noteListScrollTimer ) {
                    clearTimeout( self._noteListScrollTimer );
                }
                self._noteListScrollTimer = setTimeout( function() {
                    self._noteListScrollTimer = null;
                }, 600 );
            },

            onEmpty : function() {
                Evernote.jQuery( "#notesContextList > *" ).hide();
                if ( !noteContextFilter.isEmpty() ) {
                    Evernote.jQuery( "#notesContextListEmptyContainer" ).show();
                }
                else {
                    Evernote.jQuery( "#notesContextList" ).addClass( "notesListEmpty" );
                }
                Evernote.viewManager.updateBodyHeight();
            },

            totalItems : function() {
                return (noteContextList) ? noteContextList.totalNotes : 0;
            },

            onLoadPage : function( pageIndex, pageSize, totalPages ) {
                self._findContextProc = self._popup.remoteFindNotes( noteContextFilter, pageIndex * pageSize, pageSize * totalPages,
                    function( response/*, textStatus*/ ) {
                        Evernote.logger.debug( "findContextProc success" );

                        self.processNoteList( response.result.noteList.notes );
                        noteContextList = response.result.noteList;

                        Evernote.logger.debug( "Found " + noteContextList.totalNotes + " notes relative to this domain..." );

                        var domainProto = Evernote.UrlHelpers.urlProto( url );
                        var tabButton = Evernote.jQuery( "#siteMemoryTabButton" );

                        if ( domainProto && domainProto == "file" ) {
                            tabButton.text( Evernote.localizer.getMessage( "notes_titleLocalFilesAndCount", [ noteContextList.totalNotes ] ) );
                            Evernote.jQuery( "#notesContextListTitle" ).html( Evernote.localizer.getMessage( "notes_titleForLocalFiles" ) );
                        }
                        else {
                            var domainStr = Evernote.UrlHelpers.urlTopDomain( url );
                            tabButton.text( Evernote.localizer.getMessage( "notes_titleWithDomainAndCount", [ domainStr, noteContextList.totalNotes ] ) );
                            Evernote.jQuery( "#notesContextListTitle" ).html( Evernote.localizer.getMessage( "notes_titleForDomain", [ domainStr ] ) );
                        }

                        self._findContextProc = null;
                        Evernote.jQuery( "#notesContextList" ).trigger( "afterLoadPage", [ pageIndex, pageSize, totalPages ] );
                    },
                    function( response, textStatus, xhr ) {
                        Evernote.logger.debug( "findContextProc failed" );

                        if ( response && response.errors ) {
                            if ( response.errors.length == 1 ) {
                                Evernote.viewManager.showError( response.errors[ 0 ] );
                            }
                            else {
                                Evernote.viewManager.showErrors( response.errors );
                            }
                        }
                        else {
                            Evernote.jQuery( "#notesContextList .notelistMessageContainer" ).hide();

                            var msg = null;
                            if ( xhr.status != 200 ) {
                                msg = Evernote.viewManager.getLocalizedHttpErrorMessage( xhr, textStatus, response );
                            }

                            if ( !msg ) {
                                msg = Evernote.localizer.getMessage( "UnknownError" );
                            }
                            Evernote.jQuery( "#notesContextListErrorContainer" ).text( msg ).show();
                        }

                        self._findContextProc = null;
                    } );

                return false;
            },

            onAfterLoadPage : function( pageIndex/*, pageSize, totalPages*/ ) {
                if ( pageIndex == 0 ) {
                    setTimeout( function() {
                        Evernote.jQuery( "#notesContextList" ).scrollTo( 0 );
                    }, self.constructor.NOTELIST_SCROLLTO_DELAY );
                }
            },

           items : function() {
                var items = [ ];
                if ( noteContextList ) {
                    for ( var i = 0; i < noteContextList.notes.length; ++i ) {
                        items[ noteContextList.startIndex + i ] = self.createNoteListItem( noteContextList.notes[ i ], null );
                    }
                }

                return items;
            }
        } );
};

Evernote.FFNotesSearcher.prototype.createPlaceholderNoteListItem = function() {
    Evernote.logger.debug( "FFNotesSearcher.createPlaceholderNoteListItem()" );

    var noteItem = Evernote.jQuery( "<div>", { className : "noteListItem noteListItemPlaceholder" } );
    noteItem.css( { width : "100%" } );

    var noteInfo = Evernote.jQuery( "<div>", { className : "noteListItemInfo" } );
    noteItem.append( noteInfo );

    var imgItem = Evernote.jQuery( "<img>", { src : "/images/spinner.gif" } );
    noteInfo.append( Evernote.jQuery( "<div>" ).append( imgItem ) );

    return noteItem;
};

Evernote.FFNotesSearcher.prototype.createNoteListItem = function( note, searchWords ) {
    Evernote.logger.debug( "FFNotesSearcher.createNoteListItem()" );

    if ( note.guid && note.guid.length > 0 ) {
        var noteItem = Evernote.jQuery( "<div>", { id : "noteListItem_" + note.guid, className : "noteListItem" } );
    }
    else {
        noteItem = Evernote.jQuery( "<div>", { className : "noteListItem noteListItemPlaceholder" } );
    }

    noteItem.css( { width : "100%" } );

    var self = this;
    var thumbUrl = null;
    Evernote.logger.debug( "note instanceof SnippetNote = " + (note instanceof Evernote.SnippetNote) );

    if ( note instanceof Evernote.SnippetNote && note.resources && note.resourcesSize == 1 && note.resources[ 0 ].mime ) {
        var res = note.resources[ 0 ];
        var mime = res.mime.toLowerCase();

        if ( mime.indexOf( "image" ) >= 0 && (res.width >= this.constructor.THUMBNAIL_MIN_SIZE || res.height >= this.constructor.THUMBNAIL_MIN_SIZE) ) {
            thumbUrl = res.getThumbnailUrl( Evernote.getShardedUrl(), this.constructor.THUMBNAIL_SIZE );
        }
        else if ( mime.indexOf( "pdf" ) >= 0 ) {
            thumbUrl = res.getThumbnailUrl( Evernote.getShardedUrl(), this.constructor.THUMBNAIL_SIZE );
        }
    }
    else if ( note instanceof Evernote.NoteMetadata && note.largestResourceSize ) {
        thumbUrl = note.getThumbnailUrl( Evernote.getShardedUrl(), this.constructor.THUMBNAIL_SIZE );
    }

    if ( thumbUrl ) {
        Evernote.logger.debug( "Thumbnail url is " + thumbUrl );

        var thumbItem = Evernote.jQuery( "<div>", { className : "noteListItemThumb" } );
        var imgItem = Evernote.jQuery( "<img>", { src : thumbUrl, className : "noteThumb" } );
        imgItem.css( { "max-width" : this.constructor.THUMBNAIL_SIZE, "max-height" : this.constructor.THUMBNAIL_SIZE } );
        thumbItem.append( imgItem );

        noteItem.append( thumbItem );
    }

    // note info
    var noteInfo = Evernote.jQuery( "<div>", { className : "noteListItemInfo" } );
    noteItem.append( noteInfo );

    // note title
    var noteTitle = (note.title) ? Evernote.jQuery( "<div>" ).text( note.title ).html() : "";
    noteInfo.append( Evernote.jQuery( "<div>", { className : "noteListItemTitle", text : noteTitle } ) );

    var sortField = Evernote.context.options.noteSortOrder.type.toLowerCase();

    // note date
    var noteDate = null;
    if ( typeof note[sortField] == 'number' ) {
        var f = new Evernote.SimpleDateFormat( this.constructor.SIMPLE_DATE_FORMAT );
        noteDate = f.format( new Date( note[sortField] ) );
    }

    var itemContent = Evernote.jQuery( "<div>", { className : "noteListItemContent" } );
    noteInfo.append( itemContent );
    if ( noteDate ) {
        itemContent.append( Evernote.jQuery( "<div>", { className : "noteListItemDate", text : noteDate } ) );
    }

    if ( note.attachmentFileName ) {
        itemContent.append( Evernote.jQuery( "<div>", { className : "noteItemAttachmentFileName", text : note.attachmentFileName } ) );
    }
    else {
        var snippetContent = Evernote.jQuery( "<div>", { className : "noteListItemSnippet" } );
        if ( note.snippet ) {
            snippetContent.text( note.snippet );
        }

        itemContent.append( snippetContent );
    }

    // note source url
    if ( note.attributes && typeof note.attributes.sourceURL == 'string' && note.attributes.sourceURL.length > 0 ) {
        var urlStr = Evernote.UrlHelpers.shortUrl( note.attributes.sourceURL, this.constructor.SOURCE_URL_MAX_DISPLAY_LENGTH );

        Evernote.logger.debug( "Note url is " + note.attributes.sourceURL );
        var urlAnchor = Evernote.jQuery( "<a>", { href : "#", title : note.attributes.sourceURL, text : urlStr } );

        urlAnchor.click( function( event ) {
            event.preventDefault();
            event.stopPropagation();
            Evernote.tabManager.create( { url : note.attributes.sourceURL } );
            self._popup.dismissPopup();
        } );

        var urlWrapper = Evernote.jQuery( "<div>", { className : "noteListItemUrl" } );

        urlWrapper.append( urlAnchor );
        noteInfo.append( urlWrapper );
    }

    if ( note.guid ) {
        noteItem.bind( "click", { note : note, searchWords : searchWords },
            function( event ) {
                self.openNoteWindow( event.data.note.getNoteUrl( Evernote.getShardedUrl(), Evernote.context.getShardId(),
                                     event.data.searchWords, Evernote.getLocale() ) );
        } );
    }

    return noteItem;
};

Evernote.FFNotesSearcher.prototype.processNoteList = function( notes ) {
    Evernote.logger.debug( "FFNotesSearcher.processNoteList()" );

    var fetchGuids = [ ];
    var obsolete = [ ];
    var snippetManager = new Evernote.SnippetManager();

    for ( var i = 0; i < notes.length; ++i ) {
        var note = notes[ i ];
        if ( note instanceof Evernote.NoteMetadata ) {
            var snippet = snippetManager.getSnippet( note.guid );
            if ( snippet && snippet.updateSequenceNum != note.updateSequenceNum ) {
                obsolete.push( note.guid );
            }
            else if ( snippet ) {
                notes[ i ] = Evernote.SnippetNoteMetadata.fromObject( note );
                notes[ i ].snippet = snippet.snippet;
            }
            else {
                fetchGuids.push( note.guid );
            }
        }
    }

    snippetManager.removeSnippetList( obsolete );
    if ( fetchGuids.length > 0 ) {
        this.doNotesSnippetsSearch( fetchGuids );
    }
};

Evernote.FFNotesSearcher.prototype.doNotesSnippetsSearch = function( guids ) {
    Evernote.logger.debug( "FFNotesSearcher.doNotesSnippetsSearch()" );

    this.abortNotesSnippetsProc();

    var self = this;
    var guidsArray = [ ].concat( guids );

    Evernote.logger.debug( "Fetching snippets for guids: [" + guidsArray.length + "]" );

    this._notesSnippetsProc = Evernote.remote.noteSnippets( guidsArray,
        this.constructor.SNIPPET_MAX_LENGTH,
        true,
        function( response/*, status, xhr*/ ) {
            if ( response && response.isResult() ) {
                var snippets = response.result.snippets;
                self.populateNoteSnippets( snippets );
            }
            else if ( response && response.isError() ) {
                Evernote.logger.error( "Could not retrieve snippets" );
                Evernote.logger.debug( response.errors );
            }

            self._notesSnippetsProc = null;
        },
        function( xhr/*, status, error*/ ) {
            Evernote.Utils.logHttpErrorMessage( xhr, "fetch snippets" );
            self._notesSnippetsProc = null;
        },
    true );
};

Evernote.FFNotesSearcher.prototype.populateNoteSnippets = function( snippets ) {
    Evernote.logger.debug( "FFNotesSearcher.populateNoteSnippets()" );

    var snippetsArray = [ ].concat( snippets );
    for ( var i = 0; i < snippetsArray.length; ++i ) {
        var snippet = new Evernote.Snippet( snippetsArray[ i ] );
        if ( snippet instanceof Evernote.Snippet ) {
            Evernote.jQuery( "#noteListItem_" + snippet.guid + " .noteListItemSnippet" ).text( snippet.snippet );
        }
    }
};

Evernote.FFNotesSearcher.prototype.abortFindProc = function() {
    Evernote.logger.debug( "FFNotesSearcher.abortFindProc()" );

    if ( this._findProc ) {
        this._findProc.abort();
        this._findProc = null;
    }
};

Evernote.FFNotesSearcher.prototype.abortFindContextProc = function() {
    Evernote.logger.debug( "FFNotesSearcher.abortFindContextProc()" );

    if ( this._findContextProc ) {
        this._findContextProc.abort();
        this._findContextProc = null;
    }
};

Evernote.FFNotesSearcher.prototype.abortNotesSnippetsProc = function() {
    Evernote.logger.debug( "FFNotesSearcher.abortNotesSnippetsProc()" );

    if ( this._notesSnippetsProc ) {
        this._notesSnippetsProc.abort();
        this._notesSnippetsProc = null;
    }
};

Evernote.FFNotesSearcher.prototype.abortProcs = function() {
    this.abortFindProc();
    this.abortFindContextProc();
    this.abortNotesSnippetsProc();
};

Evernote.FFNotesSearcher.prototype.openNoteWindow = function( noteUrl ) {
    new Evernote.WindowOpener().openUrl( noteUrl, "", "width=800,height=550,location=yes,menubar=yes,resizable=yes,scrollbars=yes" );
};