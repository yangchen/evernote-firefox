//"use strict";

Evernote.FFLoginController = function FFLoginController( popup ) {
    this._popup = popup;

    this.bindView();
    this.bindForm();
};

Evernote.FFLoginController.prototype._popup = null;
Evernote.FFLoginController.prototype._loginValidator = null;
Evernote.FFLoginController.prototype._loginProc = null;
Evernote.FFLoginController.prototype._logoutProc = null;
Evernote.FFLoginController.prototype._quiet = false;

Evernote.FFLoginController.prototype.login = function( username, password, rememberMe ) {
    Evernote.logger.debug( "FFLoginController.login(): username = " + username + ", rememberMe = " + rememberMe );

    this.abortLoginProc();

    var form = Evernote.jQuery( "form[name=loginForm]" );
    Evernote.viewManager.wait( Evernote.localizer.getMessage( "loggingIn" ) );
    var self = this;

    this._loginProc = Evernote.remote.authenticate( username, password, rememberMe,
        function( response/*, textStatus, xhr */) {
            Evernote.logger.debug( "Authentication response received" );

            Evernote.viewManager.clearWait();
            if ( response.isError() ) {
                var userErrors = response.selectErrors( function( error ) {
                    if ( (error instanceof Evernote.EDAMUserException || error instanceof Evernote.ValidationError || error instanceof Evernote.EvernoteError)
                         && (error.errorCode == Evernote.EDAMErrorCode.INVALID_AUTH || error.errorCode == Evernote.EDAMErrorCode.DATA_REQUIRED
                         || error.errorCode == Evernote.EDAMErrorCode.BAD_DATA_FORMAT) ) {
                        return true;
                    }
                } );

                if ( userErrors instanceof Array && userErrors.length > 0 ) {
                    Evernote.logger.error( "Authentication invalid" );

                    self._loginValidator.resetForm();
                    Evernote.viewManager.showFormErrors( form, userErrors, function( fieldName, errorMessage ) {
                        self._loginValidator.invalidate( fieldName, errorMessage );
                    } );
                }
                else {
                    Evernote.logger.error( "Unexpected response during login" );
                    Evernote.viewManager.showErrors( response.errors );
                }

                Evernote.viewManager.switchView( "loginView", { username : username, password : "" } );

                Evernote.jQuery( "#headerUser" ).hide();
                Evernote.jQuery( "#headerNoUser" ).show();
                self._loginProc = null;
            }
            else if ( response.isResult() ) {
                Evernote.logger.debug( "Successfully logged in" );

                Evernote.viewManager.hideView( "loginView" );
                if ( rememberMe ) {
                    Evernote.logger.debug( "Updating stored password" );
                    Evernote.context.password = password;
                }

                self._loginProc = null;
                self._popup.onLogin();
            }
        },
        function( xhr, textStatus, error ) {
            Evernote.Utils.logHttpErrorMessage( xhr, "authenticate" );

            Evernote.viewManager.clearWait();
            if ( !self._quiet ) {
                Evernote.viewManager.showHttpError( xhr, textStatus, error );
            }
            else {
                self._quiet = false;
            }

            self._loginProc = null;
        }, true );
};

Evernote.FFLoginController.prototype.logout = function() {
    Evernote.logger.debug( "FFLoginController.logout()" );

    this.abortLogoutProc();
    Evernote.viewManager.wait( Evernote.localizer.getMessage( "loggingOut" ) );

    var self = this;
    this._logoutProc = Evernote.remote.logout(
        function( response/*, textStatus*/ ) {
            if ( response.isResult() ) {
                Evernote.viewManager.clearWait();
                self._popup.onLogout();
                
                Evernote.logger.debug( "Successfully logged out" );
            }
            else if ( result.isError() ) {
                Evernote.logger.error( "Soft error logging out" );
            }
            else {
                Evernote.logger.error( "Got garbage response when tried to logout" );
            }

            Evernote.viewManager.clearWait();
            self._logoutProc = null;
        },
        function( xhr/*, textStatus, error*/ ) {
            Evernote.Utils.logHttpErrorMessage( xhr, "log out" );

            Evernote.viewManager.clearWait();
            self._logoutProc = null;
        },
    true );
};

Evernote.FFLoginController.prototype.bindView = function() {
    Evernote.logger.debug( "FFLoginController.bindView()" );

    var self = this;
    Evernote.jQuery( "#loginView" ).bind( "show", function( event, data ) {
        Evernote.logger.debug( "loginView.onShow()" );

        Evernote.viewManager.showBlock( Evernote.jQuery( "#header" ) );
        var view = Evernote.jQuery( "#loginView" );
        var loginForm = view.find( "form[name=loginForm]" );

        if ( typeof data == 'object' && data != null ) {
            if ( typeof data.username != 'undefined' ) {
                loginForm.find( "input[name=username]" ).val( data.username );
            }

            if ( typeof data.password != 'undefined' ) {
                loginForm.find( "input[name=password]" ).val( data.password );
            }
        }
        else {
            loginForm.find( "input[type=password]" ).val( "" );
        }

        Evernote.jQuery( "#useSearchHelper" ).attr( "checked", Evernote.context.options.useSearchHelper );
        loginForm.find( "input[name=username]" ).focus();
        self._loginValidator.focusInvalid();
    } );

    Evernote.jQuery( "#loginView" ).bind( "hide", function( /*event*/ ) {
        Evernote.viewManager.hideBlock( Evernote.jQuery( "#header" ) );
    } );

    Evernote.jQuery( "#login_searchHelperShortDescription a" ).click( function( /*event*/ ) {
        Evernote.viewManager.switchElements( Evernote.jQuery( "#login_searchHelperShortDescription" ),
                                             Evernote.jQuery( "#login_searchHelperLongDescription" ) );
        Evernote.viewManager.updateBodyHeight();
    } );

    Evernote.jQuery( "#login_storedCredentialsShortDescription a" ).click( function( /*event*/ ) {
        Evernote.viewManager.switchElements( Evernote.jQuery( "#login_storedCredentialsShortDescription" ),
                                             Evernote.jQuery( "#login_storedCredentialsDescription" ) );
        Evernote.viewManager.updateBodyHeight();
    } );

    Evernote.jQuery( "#headerSignout" ).click( function( /*event*/ ) {
        self.logout();
    } );

    document.querySelector( "#switchText" ).addEventListener( "click", function() {
        self.switchService();
    }, false );

    this.updateServiceView();
};

Evernote.FFLoginController.prototype.bindForm = function() {
    Evernote.logger.debug( "FFLoginController.bindForm()" );

    Evernote.jQuery( "#useSearchHelper" ).bind( "change", function( /*event*/ ) {
        var options = Evernote.context.options;
        options.useSearchHelper = this.checked;
        Evernote.context.options = options;
    } );

    var form = Evernote.jQuery( "form[name=loginForm]" );
    var self = this;

    var options = {
        submitHandler : function( /*form*/ ) {
            self.submitForm();
        },

        errorClass : Evernote.ViewManager.FORM_FIELD_ERROR_MESSAGE_CLASS,
        errorElement : "div",
        onkeyup : false,
        onfocusout : false,

        invalidHandler : function() {
            Evernote.viewManager.hideErrors();
            Evernote.viewManager.updateBodyHeight();

            setTimeout( function() {
                Evernote.viewManager.updateBodyHeight();
            }, 50 );
        },

        success : function() {
            Evernote.viewManager.updateBodyHeight();
        },

        rules : {
            username : {
                required : true,
                toLowerCase : true,
                minlength : Evernote.Limits.EDAM_USER_USERNAME_LEN_MIN,
                maxlength : Evernote.Limits.EDAM_USER_USERNAME_LEN_MAX,

                email : {
                    depends : function( element ) {
                        return !element.value.match( new RegExp( Evernote.Limits.EDAM_USER_USERNAME_REGEX ) );
                    }
                }
            },

            password : {
                required : true,
                minlength : Evernote.Limits.EDAM_USER_PASSWORD_LEN_MIN,
                maxlength : Evernote.Limits.EDAM_USER_PASSWORD_LEN_MAX,
                mask : Evernote.Limits.EDAM_USER_PASSWORD_REGEX
            }
        },

        messages : {
            username : {
                required : Evernote.localizer.getMessage( "valueNotPresent", Evernote.localizer.getMessage( "loginForm_username_useremail" ) ),
                minlength : Evernote.localizer.getMessage( "valueTooShort", [ Evernote.localizer.getMessage( "loginForm_username_useremail" ),
                                                                              Evernote.Limits.EDAM_USER_USERNAME_LEN_MIN ] ),
                maxlength : Evernote.localizer.getMessage( "valueTooLong", [ Evernote.localizer.getMessage( "loginForm_username_useremail" ),
                                                                             Evernote.Limits.EDAM_USER_USERNAME_LEN_MAX ] ),
                email : ((Evernote.simulateChinese && Evernote.useChina) ? Evernote.localizer.getMessage( "loginForm_usernameInvalidChinaError" ) :  Evernote.localizer.getMessage( "invalidUsername_Email" ))
            },

            password : {
                required : Evernote.localizer.getMessage( "valueNotPresent", Evernote.localizer.getMessage( "loginForm_password" ) ),
                minlength : Evernote.localizer.getMessage( "valueTooShort", [ Evernote.localizer.getMessage( "loginForm_password" ),
                                                                              Evernote.Limits.EDAM_USER_PASSWORD_LEN_MIN ] ),
                maxlength : Evernote.localizer.getMessage( "valueTooLong", [ Evernote.localizer.getMessage( "loginForm_password" ),
                                                                             Evernote.Limits.EDAM_USER_PASSWORD_LEN_MAX ] ),
                mask : Evernote.localizer.getMessage( "invalidPassword" )
            }
        }
    };

    Evernote.logger.debug( "Setting up validation on login form" );
    this._loginValidator = form.validate( options );
};

Evernote.FFLoginController.prototype.submitForm = function() {
    Evernote.logger.debug( "FFLoginController.submitForm()" );

    var form = Evernote.jQuery( "form[name=loginForm]" );
    Evernote.viewManager.hideErrors();
    Evernote.viewManager.clearFormArtifacts( form );

    var username = form.find( "[name=username]" ).val().toLowerCase();
    var password = form.find( "[name=password]" ).val();
    var rememberMe = form.find( "[name=rememberMe]" ).attr( "checked" );

    Evernote.viewManager.wait( Evernote.localizer.getMessage( "loggingIn" ) );
    this.login( username, password, rememberMe );
};

Evernote.FFLoginController.prototype.abortProcs = function() {
    this.abortLoginProc();
    this.abortLogoutProc();
};

Evernote.FFLoginController.prototype.abortLoginProc = function() {
    Evernote.logger.debug( "FFLoginController.abortLoginProc()" );

    if ( this._loginProc ) {
        this._quiet = true;

        this._loginProc.abort();
        this._loginProc = null;
    }
 };

Evernote.FFLoginController.prototype.abortLogoutProc = function() {
    Evernote.logger.debug( "FFLoginController.abortLogoutProc()" );

    if ( this._logoutProc ) {
        this._logoutProc.abort();
        this._logoutProc = null;
    }
};

Evernote.FFLoginController.prototype.updateServiceView = function() {
    if ( Evernote.simulateChinese ) {
        var text = document.querySelector( "#switchText" );
        if ( text.firstChild && text.firstChild.nodeType == Node.TEXT_NODE ) {
            text.removeChild( text.firstChild );
        }
        var img = document.querySelector( "#switchImg" );
        if ( Evernote.context.useChina ) {
            Evernote.jQuery( "#logo_header" ).attr( "src", "images/popup_logo_china.png" );
            text.insertBefore( document.createTextNode( "For users of Evernote International" ), img );
        }
        else {
            Evernote.jQuery( "#logo_header" ).attr( "src", "images/popup_logo.png" );
            text.insertBefore( document.createTextNode( "\u6211\u662F\u5370\u8C61\u7B14\u8BB0\u7528\u6237" ), img );
        }
        document.querySelector( "#switchService" ).style.display = "";
    }
    else {
        document.querySelector( "#switchService" ).style.display = "none";
    }
};

Evernote.FFLoginController.prototype.switchService = function () {
    Evernote.context.useChina = !Evernote.context.useChina;
    this.updateServiceView();
};


