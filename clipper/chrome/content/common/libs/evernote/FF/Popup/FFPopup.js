//"use strict";

Evernote.FFPopup = function FFPopup( params ) {
    this.__defineGetter__( "pageInfo", this.getPageInfo );
    this.__defineGetter__( "simSearcher", this.getSimSearcher );
    this.__defineGetter__( "notesMetadataResultSpec", this.getNotesMetadataResultSpec );
    this.__defineGetter__( "contentPreviewer", this.getContentPreviewer );
    this.__defineGetter__( "jsonRpc", this.getJsonRpc );
    this.__defineGetter__( "arrowHideMode", this.getArrowHideMode );
    this.__defineSetter__( "arrowHideMode", this.setArrowHideMode );
    this.__defineGetter__( "tab", this.getTab );
    this.initialize( params );
};

Evernote.FFPopup.DEFAULT_TIMEOUT = 300;
Evernote.FFPopup.ARROW_HIDE_MODE_NOT_VISIBLE = 0;
Evernote.FFPopup.ARROW_HIDE_MODE_USE_SMART_FILLING = 1;
Evernote.FFPopup.ARROW_HIDE_MODE_DEFAULT = 2;

Evernote.FFPopup.prototype._tab = null;
Evernote.FFPopup.prototype._clip = null;
Evernote.FFPopup.prototype._clipGoMode = false;
Evernote.FFPopup.prototype._clipper = null;
Evernote.FFPopup.prototype._notifier = null;
Evernote.FFPopup.prototype._simSearcher = null;
Evernote.FFPopup.prototype._contentPreviewer = null;
Evernote.FFPopup.prototype._pageInfo = null;
Evernote.FFPopup.prototype._reloginAttempted = null;
Evernote.FFPopup.prototype._syncProc = null;
Evernote.FFPopup.prototype._checkVersionProc = null;
Evernote.FFPopup.prototype._loginCtrl = null;
Evernote.FFPopup.prototype._quickNoteCtrl = null;
Evernote.FFPopup.prototype._notesSearchCtrl = null;
Evernote.FFPopup.prototype._searchResultSpec = null;
Evernote.FFPopup.prototype._jsonRpc = null;
Evernote.FFPopup.prototype._arrowHideMode = Evernote.FFPopup.ARROW_HIDE_MODE_NOT_VISIBLE;
Evernote.FFPopup.prototype._onKeyPressedFn = null;
Evernote.FFPopup.prototype._updateArrowTimerId = null;

Evernote.FFPopup.prototype.initialize = function( params ) {
    Evernote.logger.debug( "FFPopup.initialize()" );

    this._tab = params.tab;
    this._clip = params.clip;
    this._clipGoMode = params.clipGoMode;
    this._clipper = params.clipper;
    this._notifier = params.notifier;
    this._simSearcher = params.simSearcher;
    this._reloginAttempted = false;

    this._loginCtrl = new Evernote.FFLoginController( this );
    this._quickNoteCtrl = new Evernote.FFQuickNoteController( this );
    this._notesSearchCtrl = new Evernote.FFNotesSearchController( this );

    this.bindHeader();
    this.initValidator();

    Evernote.tabManager.addListener( this );
    new Evernote.AbstractContentScript().injectStyleSheets( [ "resource://evernote_styles/contentpreview.css" ], this._tab.document );

    var self = this;
    this._onKeyPressedFn = function( event ) {
       self.onKeyPressed( event );
    };
    window.addEventListener( "keypress", this._onKeyPressedFn, false );

    this._updateArrowTimerId = setInterval( function() {
        self.updateNotebookArrowPosition();
    }, 10 );
};

Evernote.FFPopup.prototype.onLogin = function() {
    var self = this;
    this.remoteSyncState( function() {
        Evernote.logger.debug( "Got syncState after login, continuing with clipping" );
        self.startUp();
    } );
};

Evernote.FFPopup.prototype.onLogout = function() {
    this.dismissPopup();
};

Evernote.FFPopup.prototype.startUp = function() {
    Evernote.logger.debug( "FFPopup.startUp()" );

    this.jsonRpc.initWithAuthToken( Evernote.context.authenticationToken );

    if ( this._clipGoMode && this._clipper && this._notifier ) {
        Evernote.logger.debug( "Clipping in clip and go mode" );

        this._clipper.clip( this._notifier );
        this.dismissPopup();
        return;
    }

    Evernote.viewManager.clearWait();
    var clipNote = new Evernote.ClipNote( this._clip.toJSON() );

    Evernote.logger.debug( "Popup received request from extension with clipNote: " + clipNote.toString() );

    Evernote.viewManager.switchView( "quickNoteView", { clipNote : clipNote } );
    Evernote.jQuery( "#headerUser" ).show();
    Evernote.jQuery( "#headerNoUser" ).hide();
};

Evernote.FFPopup.prototype.remoteSyncState = function( success, failure ) {
    Evernote.logger.debug( "FFPopup.remoteSyncState()" );

    this.abortSyncProc();

    var self = this;
    this._syncProc = Evernote.syncManager.remoteSyncState(
        function( response, textStatus, xhr ) {
            Evernote.viewManager.clearWait();
            Evernote.logger.debug( "response.isResult() " + response.isResult() );

            if ( response.isResult() ) {
                if ( typeof success == 'function' ) {
                    success( response, textStatus );
                }
            }
            else if ( response.isError() ) {
                var result = self.handleResponseError( response, true );
                if ( !result.handled ) {
                    Evernote.viewManager.showErrors( response.errors );
                }

                if ( result.relogin && !self._reloginAttempted ) {
                    var userName = Evernote.context.currentUserName;
                    if ( typeof userName == "string" && userName.length > 0 ) {
                        var password = Evernote.context.password;
                        if ( typeof password == "string" && password.length > 0 ) {
                            Evernote.logger.debug( "Password found. Try to relogin" );

                            self._reloginAttempted = true;
                            self._syncProc = null;
                            self._loginCtrl.login( userName, password, true );
                            return;
                        }
                    }
                }

                if ( result.type == "missingToken" || result.type == "tokenExpired" ) {
                    Evernote.cookieManager.remove( Evernote.getSecureServiceUrl(), "auth" );
                    Evernote.context.destroy();
                }

                if ( typeof failure == 'function' ) {
                    failure( xhr, textStatus, response );
                }
            }
            else {
                var err = new Evernote.Exception( Evernote.EDAMErrorCode.UNKNOWN, Evernote.localizer.getMessage( "Error_" + Evernote.EDAMErrorCode.UNKNOWN ) );
                Evernote.viewManager.showError( err );

                if ( typeof failure == 'function' ) {
                    failure( xhr, textStatus, response );
                }
            }

            self._syncProc = null;
        },
        function( xhr, textStatus, error ) {
            Evernote.Utils.logHttpErrorMessage( xhr, "get SyncState" );

            if ( xhr && xhr.readyState == 4 ) {
                Evernote.viewManager.showHttpError( xhr, textStatus, error );
                if ( typeof failure == 'function' ) {
                    failure( xhr, textStatus, error );
                }
            }

            self._syncProc = null;
        }
    );
};

Evernote.FFPopup.prototype.remoteFindNotes = function( filter, offset, maxNotes, success, failure ) {
    Evernote.logger.debug( "FFPopup.remoteFindNotes()" );
    
    if ( !(filter instanceof Evernote.NoteFilter) ) {
        Evernote.logger.error( "Tried to find notes without valid NoteFilter" );
        return;
    }

    var self = this;
    return Evernote.remote.findMetaNotes( filter, self.notesMetadataResultSpec, offset, maxNotes,
        function( response, textStatus, xhr ) {
            Evernote.logger.debug( "Popup got response from the server regarding note search" );

            if ( response.isResult() ) {
                Evernote.logger.debug( "Successfully retreived note search results" );
                if ( typeof success == 'function' ) {
                    success( response, textStatus, xhr );
                }
            }
            else if ( response.isError() ) {
                if ( !self.handleResponseError( response ).handled ) {
                    Evernote.logger.error( "Failed to find notes" );
                    if ( typeof failure == 'function' ) {
                        failure( response, textStatus, xhr );
                    }
                }
            }
            else {
                Evernote.logger.error( "Unrecognized response when tried to find notes on the server" );
            }
        },
        function( xhr, textStatus, error ) {
            Evernote.Utils.logHttpErrorMessage( xhr, "find Notes" );
            if ( typeof failure == 'function' ) {
                failure( error, textStatus, xhr );
            }
        },
    true );
};

Evernote.FFPopup.prototype.remoteCheckVersion = function( callback ) {
    Evernote.logger.debug( "FFPopup.remoteCheckVersion()" );

    var self = this;
    Evernote.uninstaller.getExtensionVersion( function( version ) {
        Evernote.logger.debug( "Got clipper version: version = " + version ) ;

        self.abortCheckVersionProc();
        var ffPopup = self;
        self._checkVersionProc = Evernote.remote.checkVersion( Evernote.getClipperName(), version,
            function( response/*, status, xhr*/ ) {
                ffPopup._checkVersionProc = null;

                if ( response.isResult() ) {
                    if ( !response.result.checkVersion ) {
                        if ( typeof callback == 'function' ) {
                            callback( false );
                            return;
                        }
                    }
                }
                else {
                    var responseStr = null;
                    try {
                        responseStr = JSON.stringify( response );
                    }
                    catch( e ) {
                    }

                    Evernote.logger.error( "Unexpected response from checkVersion: " + responseStr );
                }
                if ( typeof callback == 'function' ) {
                    callback( true );
                }
            },
            function( xhr/*, status, error*/ ) {
                Evernote.Utils.logHttpErrorMessage( xhr, "check Version" );

                ffPopup._checkVersionProc = null;
                if ( typeof callback == 'function' ) {
                    callback( true );
                }
            },
        true );
    } );
};

Evernote.FFPopup.prototype.handleResponseError = function( response, quiet ) {
    Evernote.logger.debug( "FFPopup.handleResponseError()" );

    try {
        if ( response && (response.isMissingAuthTokenError() || response.isPermissionDeniedError()) ) {
            Evernote.logger.warn( "Response indicates a problem with retaining authentication token between requests" );

            if ( !quiet ) {
                Evernote.viewManager.showError( Evernote.localizer.getMessage( "authPersistenceError" ) );
            }
            return { handled : true, relogin : true, type : "missingToken" };
        }
        else if ( response && response.isAuthTokenExpired() ) {
            Evernote.logger.warn( "Response indicates expired authentication" );

            if ( !quiet ) {
                Evernote.viewManager.showError( Evernote.localizer.getMessage( "authExpiredError" ) );
            }
            return { handled : true, relogin : true, type : "tokenExpired" };
        }
        else if ( response && response.isInvalidAuthentication() ) {
            Evernote.logger.warn( "Response indicates invalid authentication" );

            var username = Evernote.context.currentUserName || "";
            Evernote.viewManager.switchView( "loginView", { username : username, password : "" } );

            Evernote.jQuery( "#headerUser" ).hide();
            Evernote.jQuery( "#headerNoUser" ).show();
            Evernote.viewManager.showErrors( response.errors );

            return { handled : true, relogin : false, type : "invalidAuth" };
        }
    }
    catch( e ) {
        Evernote.logger.error( "FFPopup.handleResponseError() failed: " + e );
    }

    return { handled : false, relogin : false };
};

Evernote.FFPopup.prototype.initValidator = function() {
    Evernote.logger.debug( "FFPopup.initValidator()" );

    Evernote.jQuery.validator.addMethod( "mask", Evernote.jQuery.fn.validate.methods.mask, Evernote.localizer.getMessage( "invalidMask" ) );
    // trim function - replaces value with trimmed value
    Evernote.jQuery.validator.addMethod( "trim", Evernote.jQuery.fn.validate.methods.trim, "trim error" );
    // separates value into parts and checks if total number of parts is within given range
    Evernote.jQuery.validator.addMethod( "totalPartsRange", Evernote.jQuery.fn.validate.methods.totalPartsRange, Evernote.localizer.getMessage( "totalPartsNotInRange" ) );
    // separates value into parts and checks whether individual parts are within given range in length
    Evernote.jQuery.validator.addMethod( "partLengthRange", Evernote.jQuery.fn.validate.methods.partLengthRange, Evernote.localizer.getMessage( "partLengthNotInRange" ) );
    // separates value into parts and checks whether individual parts match given mask
    Evernote.jQuery.validator.addMethod( "partMask", Evernote.jQuery.fn.validate.methods.partMask );
    // custom callback validator. Useful for transforms...
    Evernote.jQuery.validator.addMethod( "toLowerCase", Evernote.jQuery.fn.validate.methods.toLowerCase );

    Evernote.jQuery.validator.prototype.invalidate = function( fieldName, errorMessage ) {
        var thisErr = { };
        thisErr[ fieldName ] = errorMessage;

        this.showErrors( thisErr );
        Evernote.viewManager.updateBodyHeight();
    };
};

Evernote.FFPopup.prototype.bindHeader = function() {
    Evernote.logger.debug( "FFPopup.bindHeader()" );

    var self = this;
    Evernote.jQuery( "#header" ).bind( "show", function( /*event, data*/ ) {
        Evernote.logger.debug( "header.onShow" );

        var username = Evernote.context.currentUserName;
        if ( username ) {
            Evernote.jQuery( "#headerUsername" ).text( (username.length <= 48) ? username : (username.substring( 0, 45 ) + "...") );
            Evernote.jQuery( "#headerUser" ).show();
            Evernote.jQuery( "#headerNoUser" ).hide();
        }
        else {
            Evernote.jQuery( "#headerUser" ).hide();
            Evernote.jQuery( "#headerNoUser" ).show();
        }
    } );

    Evernote.jQuery( "#headerGoHome" ).click( function( /*event*/ ) {
        self.goHome();
    } );

    Evernote.jQuery( "#headerRegister" ).click( function( /*event*/ ) {
        new Evernote.WindowOpener().openUrl( Evernote.getRegistrationUrl(), "", "location=yes,menubar=yes,resizable=yes,scrollbars=yes" );
    } );
};

Evernote.FFPopup.prototype.updateNotebookArrowPosition = function() {
    Evernote.logger.debug( "FFPopup.updateNotebookArrowPosition()" );

    var form = Evernote.jQuery( "form[name=quickNoteForm]" );
    var notebookElem = form.find( "select[name=notebookGuid]" );

    if ( notebookElem ) {
        Evernote.logger.debug( "Find element " + notebookElem.attr( "title" ) );

        var notebookElemOffset = notebookElem.offset();
        if ( notebookElemOffset ) {
            Evernote.logger.debug( "Current position " + notebookElemOffset.toSource() );

            var hider = Evernote.jQuery( "#arrow-hide" );
            if ( this._arrowHideMode == Evernote.FFPopup.ARROW_HIDE_MODE_NOT_VISIBLE ) {
                var left = -20;
                var top = -20;
            }
            else if ( this._arrowHideMode == Evernote.FFPopup.ARROW_HIDE_MODE_USE_SMART_FILLING ) {
                left = notebookElemOffset.left + notebookElem.width() - 15;
                top = notebookElemOffset.top + 3;
                hider.css( "background-color", "#ebf5e2" );
            }
            else if ( this._arrowHideMode == Evernote.FFPopup.ARROW_HIDE_MODE_DEFAULT ) {
                left = notebookElemOffset.left + notebookElem.width() - 13;
                top = notebookElemOffset.top - 1
                hider.css( "background-color", "#FFF" );
            }

            hider.height(notebookElem.height() + 2);
            hider.css( "left", left ).css( "top", top );
        }
    }
};

Evernote.FFPopup.prototype.setQuickNoteAction = function() {
    Evernote.logger.debug( "FFPopup.setQuickNoteAction()" );

    try {
        if ( Evernote.Utils.isForbiddenUrl( this._tab.document.location.href ) ) {
            this.selectQuickNoteAction( "CLIP_ACTION_URL" );
        }
        else if ( this._clip && this._clip.hasSelection() ) {
            this.selectQuickNoteAction( "CLIP_ACTION_SELECTION" );
        }
        else {
            var clipAction = Evernote.context.options.clipAction;
            if ( clipAction == Evernote.Options.CLIP_ACTION_OPTIONS.ARTICLE ) {
                if ( !this.pageInfo.getDefaultArticle() ||
                      (this._simSearcher && this._simSearcher.result && this._simSearcher.result.totalNotes > 0) ) {
                    clipAction = Evernote.Options.CLIP_ACTION_OPTIONS.FULL_PAGE;
                }
            }

            if ( clipAction == Evernote.Options.CLIP_ACTION_OPTIONS.ARTICLE ) {
                this.selectQuickNoteAction( "CLIP_ACTION_ARTICLE" );
            }
            else if ( clipAction ==  Evernote.Options.CLIP_ACTION_OPTIONS.FULL_PAGE ) {
                this.selectQuickNoteAction( "CLIP_ACTION_FULL_PAGE" );
            }
            else if ( clipAction == Evernote.Options.CLIP_ACTION_OPTIONS.URL ) {
                this.selectQuickNoteAction( "CLIP_ACTION_URL" );
            }
        }

        this.previewClipAction( Evernote.jQuery( "form[name=quickNoteForm] #clipAction" ).val() );
    }
    catch ( e ) {
        Evernote.logger.error( "FFPopup.setQuickNoteAction() failed " + e );
    }
};

Evernote.FFPopup.prototype.selectQuickNoteAction = function( actionName ) {
    Evernote.logger.debug( "FFPopup.selectQuickNoteAction()" );
    
    if ( actionName ) {
        var clipActionElement = Evernote.jQuery( "form[name=quickNoteForm] #clipAction" );
        if ( clipActionElement.find( "option[value=" + actionName + "]" ).length > 0 ) {
            clipActionElement.val( actionName );
            this._quickNoteCtrl.updateQuickNoteActions();
        }
    }
};

Evernote.FFPopup.prototype.removeNonapplicableActions = function() {
    Evernote.logger.debug( "FFPopup.removeNonapplicableActions()" );

    var clipAction = Evernote.jQuery( "#clipAction" );
    if ( !(this._clip && this._clip.hasSelection()) ) {
        clipAction.find( "option[value=CLIP_ACTION_SELECTION]" ).remove();
    }

    if ( Evernote.Utils.isForbiddenUrl( this._tab.document.location.href ) ) {
        clipAction.find( "option" ).each( function( i, e ) {
            var $e = Evernote.jQuery( e );
            if ( $e.val() != "CLIP_ACTION_URL" ) {
                $e.remove();
            }
        } );
    }
    else if ( this._simSearcher && this._simSearcher.result && this._simSearcher.result.totalNotes > 0 ) {
        clipAction.find( "option[value=CLIP_ACTION_ARTICLE]" ).remove();
    }
    else {
        if ( !this.pageInfo.getDefaultArticle() ) {
            clipAction.find( "option[value=CLIP_ACTION_ARTICLE]" ).remove();
        }
    }
};

Evernote.FFPopup.prototype.abortProcs = function() {
    this._loginCtrl.abortProcs();
    this._notesSearchCtrl.abortProcs();
    this.abortSyncProc();
    this.abortCheckVersionProc();
};

Evernote.FFPopup.prototype.abortSyncProc = function() {
    Evernote.logger.debug( "FFPopup.abortSyncProc()" );

    if ( this._syncProc ) {
        this._syncProc.abort();
        this._syncProc = null;
    }
};

Evernote.FFPopup.prototype.abortCheckVersionProc = function() {
    Evernote.logger.debug( "FFPopup.abortCheckVersionProc()" );

    if ( this._checkVersionProc ) {
        this._checkVersionProc.abort();
        this._checkVersionProc = null;
    }
};

Evernote.FFPopup.prototype.dismissPopup = function( instant ) {
    Evernote.logger.debug( "FFPopup.dismissPopup()" );

    Evernote.tabManager.removeListener( this );
    this.abortProcs();

    this._tab = null;
    this._clip = null;
    this._clipper = null;
    this._notifier = null;
    this._simSearcher = null;
    this._pageInfo = null;
    this._jsonRpc = null;

    window.removeEventListener( "keypress", this._onKeyPressedFn, false );
    if ( this._updateArrowTimerId ) {
        clearInterval( this._updateArrowTimerId );
        this._updateArrowTimerId = null;
    }

    this._quickNoteCtrl.cleanUp();

    var self = this;
    var callback = function() {
        if ( self._contentPreviewer ) {
             self._contentPreviewer.cleanUp();
             self._contentPreviewer = null;
        }

        window.close();
    };

    if ( typeof instant != 'undefined' && instant ) {
        callback();
    }
    else {
        setTimeout( callback, this.constructor.DEFAULT_TIMEOUT );
    }
};

Evernote.FFPopup.prototype.goHome = function() {
    Evernote.logger.debug( "FFPopup.goHome()" );

    Evernote.tabManager.create( { url : Evernote.getHomeUrl() } );
    this.dismissPopup();
};

Evernote.FFPopup.prototype.previewClipAction = function( clipAction ) {
    Evernote.logger.debug( "FFPopup.previewClipAction()" );

    if ( Evernote.Utils.isForbiddenUrl( this._tab.document.location.href ) ) {
        Evernote.logger.debug( "Not previewing quickNoteAction on forbiddenUrl" );
        return;
    }

    if ( clipAction && typeof Evernote.RequestType[ "PREVIEW_" + clipAction ] != 'undefined' ) {
        Evernote.logger.debug( "Selected action is " + Evernote.RequestType[ "PREVIEW_" + clipAction ] );

        if ( Evernote.RequestType[ "PREVIEW_" + clipAction ] == Evernote.RequestType.PREVIEW_CLIP_ACTION_FULL_PAGE ) {
            this.contentPreviewer.previewFullPage();
        }
        else if ( Evernote.RequestType[ "PREVIEW_" + clipAction ] == Evernote.RequestType.PREVIEW_CLIP_ACTION_URL ) {
            this.contentPreviewer.previewURL();
        }
        else if ( Evernote.RequestType[ "PREVIEW_" + clipAction ] == Evernote.RequestType.PREVIEW_CLIP_ACTION_ARTICLE ) {
            this.contentPreviewer.previewArticle();
        }
        else if ( Evernote.RequestType[ "PREVIEW_" + clipAction ] == Evernote.RequestType.PREVIEW_CLIP_ACTION_SELECTION ) {
            this.contentPreviewer.previewSelection();
        }
    }
};

Evernote.FFPopup.prototype.onSubmitQuickNoteForm = function( clipMetadata ) {
    Evernote.logger.debug( "FFPopup.onSubmitQuickNoteForm() " );

    var clipAction = Evernote.jQuery( "form[name=quickNoteForm] #clipAction" ).val();
    if ( clipAction == "CLIP_ACTION_FULL_PAGE" ) {
        this._clip.clipFullPage();
        this._notifier.finishClip( this._clip, clipMetadata, false );
    }
    else if ( clipAction == "CLIP_ACTION_SELECTION" ) {
        this._clip.clipSelection();
        this._notifier.finishClip( this._clip, clipMetadata, false );
    }
    else if ( clipAction == "CLIP_ACTION_ARTICLE" ) {
        this._clip.clipArticle( this.contentPreviewer.getArticleElement() );
        this._notifier.finishClip( this._clip, clipMetadata, false );
    }
    else if ( clipAction == "CLIP_ACTION_URL" ) {
        this._clip.clipUrl();
        this._notifier.finishClip( this._clip, clipMetadata, true );
    }
    else {
        Evernote.viewManager.showError( "Invalid Clip selection" );
        return;
    }

    this.dismissPopup();
};

Evernote.FFPopup.prototype.onTabClose = function( /*tabId*/ ) {
    Evernote.logger.debug( "FFPopup.onTabClose()" );
    // this method is using in MacOS only when user closes the tab by clicking scroll btn
    this.dismissPopup( true );
};

Evernote.FFPopup.prototype.getTabUrl = function() {
    return this._tab.document.location.href;
};

Evernote.FFPopup.prototype.getPageInfo = function() {
    if ( !this._pageInfo ) {
        this._pageInfo = new Evernote.Chrome.PageInfo( this._tab );
    }

    return this._pageInfo;
};

Evernote.FFPopup.prototype.getNotesMetadataResultSpec = function() {
    if ( !this._searchResultSpec ) {
        this._searchResultSpec = new Evernote.NotesMetadataResultSpec( { includeTitle : true,
            includeCreated : true,
            includeUpdated : true,
            includeUpdateSequenceNum : true,
            includeNotebookGuid : true,
            includeAttributes : true,
            includeLargestResourceMime : true,
            includeLargestResourceSize : true
        } );
    }

    return this._searchResultSpec;
};

Evernote.FFPopup.prototype.getContentPreviewer = function() {
    if ( !this._contentPreviewer ) {
        this._contentPreviewer = new Evernote.Chrome.ContentPreview( this._tab );
    }

    return this._contentPreviewer;
};

Evernote.FFPopup.prototype.getSimSearcher = function() {
    return this._simSearcher;
};

Evernote.FFPopup.prototype.getJsonRpc = function() {
    if ( !this._jsonRpc ) {
        this._jsonRpc = new Evernote.Chrome.JsonRpc( null, ["NoteStoreExtra.getFilingRecommendations",
                                                            "NoteStore.listTags",
                                                            "NoteStore.listNotebooks",
                                                            "NoteStoreExtra.findNotesWithSnippet"] );
    }

    return this._jsonRpc;
};

Evernote.FFPopup.prototype.onKeyPressed = function( event ) {
    if ( !Evernote.context.isLogined() ) {
        return;
    }

    if ( event.keyCode >= 37 && event.keyCode <= 40 ) {
        this.nudge( event );
    }
    else if ( event.keyCode == 13 ) {
        this._quickNoteCtrl.tryClipArticle();
    }
};

Evernote.FFPopup.prototype.nudge = function ( event ) {
    if ( Evernote.context.options.selectionNudging == Evernote.Options.SELECTION_NUDGING_OPTIONS.DISABLED ) {
        return;
    }

    var clipAction = Evernote.jQuery( "form[name=quickNoteForm] #clipAction" );
    if ( clipAction.val() != "CLIP_ACTION_ARTICLE" ) {
        return;
    }

    var activeElement = document.activeElement;
    if ( Evernote.jQuery( "#popup_body" ).get( 0 ) != activeElement ) {
        return;
    }

    var key = event.keyCode;
    var keyMap = {
        37: "left",
        38: "up",
        39: "right",
        40: "down"
    };

    this.contentPreviewer.nudgePreview( keyMap[ key ] );
};

Evernote.FFPopup.prototype.getArrowHideMode = function() {
    return this._arrowHideMode;
};

Evernote.FFPopup.prototype.setArrowHideMode = function( val ) {
    this._arrowHideMode = val;
    this.updateNotebookArrowPosition();
};

Evernote.FFPopup.prototype.getTab = function() {
    return this._tab;
};