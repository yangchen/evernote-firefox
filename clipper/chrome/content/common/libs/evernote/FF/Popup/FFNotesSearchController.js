//"use strict";

Evernote.FFNotesSearchController = function FFNotesSearchController( popup ) {
    this.__defineGetter__( "searcher", this.getNotesSearcher );
    this._popup = popup;
    this._searchQueryPopulated = false;

    this.bindView();
    this.bindForm();
    this.bindSimSearch();
};

Evernote.FFNotesSearchController.ALL_NOTES_VIEW_NAME = "allNotes";
Evernote.FFNotesSearchController.SITE_MEMORY_VIEW_NAME = "siteMemory";
Evernote.FFNotesSearchController.NOTELIST_AC_MULTIPLE = false;

Evernote.FFNotesSearchController.prototype._popup = null;
Evernote.FFNotesSearchController.prototype._notesSearchForm = null;
Evernote.FFNotesSearchController.prototype._searchQueryPopulated = null;
Evernote.FFNotesSearchController.prototype._searcher = null;

Evernote.FFNotesSearchController.prototype.bindView = function() {
    Evernote.logger.debug( "FFNotesSearchController.bindView()" );

    var self = this;
    var notesTabbedView = Evernote.jQuery( "#notesTabbedView" );

    notesTabbedView.tabbedView( {
         onshowview : function( viewName, viewContainer ) {
             Evernote.logger.debug( "notesTabbedView.onshowview(): " + viewName );

             var viewData = (viewContainer && viewContainer.data()) ? viewContainer.data() : { };
             var complete = function() {
                 viewData.searchStarted = true;
                 viewContainer.data( viewData );
                 Evernote.viewManager.updateBodyHeight();
             };

             if ( !self._searchQueryPopulated ) {
                 self.populateSearchQueries( Evernote.jQuery( ".notelistSearchQuery" ) );
                 self._searchQueryPopulated = true;
             }

             if ( viewName == self.constructor.ALL_NOTES_VIEW_NAME && !viewData.searchStarted ) {
                 Evernote.logger.debug( "Starting all-notes search because the view is shown wihtout prior search" );

                 self.searcher.doNotesSearch();
                 complete();
             }
             else if ( viewName == self.constructor.SITE_MEMORY_VIEW_NAME && !viewData.searchStarted ) {
                 Evernote.logger.debug( "Starting site-memory search because the view is shown without prior search" );

                 var tabUrl = self._popup.getTabUrl();
                 if ( tabUrl ) {
                     self.searcher.doNotesContextSearch( tabUrl );
                     complete();
                 }
                 else {
                     Evernote.logger.warn( "Could not determine URL of selected tab" );
                 }
             }

             Evernote.viewManager.updateBodyHeight();
         },

         onhideview : function( /*viewName*/ ) {
             Evernote.viewManager.updateBodyHeight();
         }
     } );
};

Evernote.FFNotesSearchController.prototype.bindForm = function() {
    Evernote.logger.debug( "FFNotesSearchController.bindForm()" );

    this._notesSearchForm = Evernote.ModelForm.onForm( Evernote.jQuery( "form[name=notesSearchForm]" ) );
    var form = Evernote.jQuery( "form[name=notesSearchForm]" );

    var self = this;
    form.submit( function() {
        self.submitForm();
        return false;
    } );

    var searchQueryField = Evernote.jQuery( "#notesSearchQuery" );
    // make sure to set NoteFilter to fuzzy mode when first focused.
    searchQueryField.focus( function( /*event*/ ) {
        Evernote.context.noteFilter.fuzzy = true;
    } );

    // make sure we hide autocomplete popup on ENTER
    searchQueryField.keyup( function( event ) {
        if ( event.keyCode == 13 ) {
            Evernote.jQuery( ".ac_results" ).hide();
            if ( !self.constructor.NOTELIST_AC_MULTIPLE ) {
                self.submitForm();
            }
        }
    } );
};

Evernote.FFNotesSearchController.prototype.bindSimSearch = function() {
    Evernote.logger.debug( "FFNotesSearchController.bindSimSearch()" );

    try {
        var notesTabbedView = Evernote.jQuery( "#notesTabbedView" );
        var simSearcher = this._popup.simSearcher;

        if ( simSearcher && simSearcher.result && simSearcher.result.totalNotes > 0 && simSearcher.query ) {
            this._notesSearchForm.notesSearchQuery = simSearcher.query;
            this.submitForm();

            notesTabbedView.getTabbedView()[ 0 ].showView( "allNotes" );
            Evernote.viewManager.updateBodyHeight();
        }
    }
    catch ( e ) {
        Evernote.logger.error( "FFNotesSearchController.bindSimSearch() failed: " + e );
    }
};

Evernote.FFNotesSearchController.prototype.submitForm = function() {
    Evernote.logger.debug( "FFNotesSearchController.submitForm()" );

    if ( this._notesSearchForm ) {
        var query = this._notesSearchForm.notesSearchQuery;
        var noteFilter = Evernote.context.noteFilter;
        noteFilter.resetQuery();

        noteFilter.fuzzy = true;
        noteFilter.words = query;

        Evernote.context.noteFilter = noteFilter;

        Evernote.logger.debug( "Searching for: " + JSON.stringify( noteFilter.toStorable() ) );
        Evernote.jQuery( "#notesListItems" ).empty();
        Evernote.jQuery( "#notesListEmptyContainer" ).hide();

        var state = Evernote.context.state;
        state.noteListScrollTop = 0;
        Evernote.context.state = state;

        this.searcher.doNotesSearch();
    }
    else {
        Evernote.logger.debug( "No search query..." );
    }
};

Evernote.FFNotesSearchController.prototype.populateSearchQueries = function( sel ) {
    Evernote.logger.debug( "FFNotesSearchController.populateSearchQueries()" );

    var context = Evernote.context;
    if ( sel instanceof Evernote.jQuery ) {
        var searches = [ ];
        if ( context.tags instanceof Array ) {
            Evernote.logger.debug( "Adding " + context.tags.length + " tags to searches" );
            searches = searches.concat( context.tags );
        }

        if ( context.searches instanceof Array ) {
            Evernote.logger.debug( "Adding " + context.searches.length + " saved searches to searches" );
            searches = searches.concat( context.searches );
        }

        if ( context.notebooks instanceof Array ) {
            Evernote.logger.debug( "Adding " + context.notebooks.length + " notebooks to searches" );
            searches = searches.concat( context.notebooks );
        }

        Evernote.logger.debug( "There are now " + searches.length + " searches" );
        var noteFilter = Evernote.context.noteFilter;

        sel.autocomplete( searches, {
            formatItem : function( item/*, row, total*/ ) {
                var iconSrc = null;
                if ( item instanceof Evernote.Tag ) {
                    iconSrc = "images/browse_tags_icon.png";
                }
                else if ( item instanceof Evernote.Notebook ) {
                    iconSrc = "images/browse_books_icon.png";
                }
                else if ( item instanceof Evernote.SavedSearch ) {
                    iconSrc = "images/browse_search_icon.png";
                }

                if ( iconSrc ) {
                    return Evernote.jQuery( "<div>" ).text( item.name ).html() + "<img src='" + iconSrc + "' class='listIcons'/>";
                }
                else {
                    return Evernote.jQuery( "<div>" ).text( item.name ).html();
                }
            },

            formatResult : function( item/*, row, total*/ ) {
                if ( item instanceof Evernote.SavedSearch  && noteFilter instanceof Evernote.NoteFilter ) {
                    noteFilter.fuzzy = false;
                }
                else if ( noteFilter instanceof Evernote.NoteFilter ) {
                    noteFilter.fuzzy = true;
                }

                if ( item instanceof Evernote.Tag ) {
                    return "tag:" + Evernote.NoteFilter.formatWord( item.name );
                }
                else if ( item instanceof Evernote.Notebook ) {
                    return "notebook:" + Evernote.NoteFilter.formatWord( item.name );
                }
                else if ( item instanceof Evernote.SavedSearch ) {
                    return item.query;
                }
                else {
                    return Evernote.NoteFilter.formatWord( item.name );
                }
            },

            selectFirst : false,
            multiple : this.constructor.NOTELIST_AC_MULTIPLE,
            multipleSeparator : Evernote.NoteFilter.WORD_SEPARATOR
        } );
    }
};

Evernote.FFNotesSearchController.prototype.abortProcs = function() {
    this.searcher.abortProcs();
};

Evernote.FFNotesSearchController.prototype.getNotesSearcher = function() {
    if ( !this._searcher ) {
        this._searcher = new Evernote.FFNotesSearcher( this._popup );
    }

    return this._searcher;
};