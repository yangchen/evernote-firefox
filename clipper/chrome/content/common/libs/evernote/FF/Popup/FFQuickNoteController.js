//"use strict";

Evernote.FFQuickNoteController = function FFQuickNoteController( popup ) {
    this._popup = popup;
    this.initialize();
    this.__defineSetter__( "smartFillingLoaded", this.setSmartFillingLoaded );
    this.__defineSetter__( "linkedNotebooksLoaded", this.setLinkedNotebooksLoaded );
};

Evernote.FFQuickNoteController.QUICK_NOTE_VIEW_DEFAULTS = {
    notebookGuid : null
};

Evernote.FFQuickNoteController.VIEW_UPDATE_TIMEOUT = 100;
Evernote.FFQuickNoteController.AUTO_SAVE_DELAY = 600;

Evernote.FFQuickNoteController.prototype._popup = null;
Evernote.FFQuickNoteController.prototype._quickNoteForm = null;
Evernote.FFQuickNoteController.prototype._quickNoteValidator = null;
Evernote.FFQuickNoteController.prototype._notebookLoadingInterval = null;
Evernote.FFQuickNoteController.prototype._tagControl = null;
Evernote.FFQuickNoteController.prototype._smartFillingLoaded = false;
Evernote.FFQuickNoteController.prototype._linkedNotebooksLoaded = false;
Evernote.FFQuickNoteController.prototype._smartFillingData = null;

Evernote.FFQuickNoteController.prototype.initialize = function() {
    this.bindView();
    this.bindForm();
    this.bindTagControl();
};

Evernote.FFQuickNoteController.prototype.bindView = function() {
    Evernote.logger.debug( "FFQuickNoteController.bindView()" );

    var self = this;
    var notesView = Evernote.jQuery( "#notesView" );
    var quickNoteView = Evernote.jQuery( "#quickNoteView" );

    quickNoteView.bind( "show", function( event, data ) {
        try {
            Evernote.logger.debug( "quickNoteView.onShow()" );

            Evernote.viewManager.showBlock( Evernote.jQuery( "#header" ) );
            Evernote.viewManager.showBlock( notesView );

            var notebookSelect = quickNoteView.find( "select[name=notebookGuid]" );
            // populate notebook selection
            self.populateNotebookSelection( notebookSelect );

            // update clip actions
            self.updateQuickNoteActions();
            self._popup.setQuickNoteAction();

            // populate tag selection
            self._popup.jsonRpc.client.NoteStore.listTags( function( data, error ) {
                self.populateTags( data, error );
            }, Evernote.context.authenticationToken );

            // setup view options (defaults < options)
            var viewOpts = Evernote.jQuery.extend( true, { }, self.constructor.QUICK_NOTE_VIEW_DEFAULTS );

            // preferred notebook
            if ( Evernote.context.getPreferredNotebook() ) {
                viewOpts.notebookGuid = Evernote.context.getPreferredNotebook().guid;
            }

            // populate form with default view options
            self._quickNoteForm.populateWith( viewOpts );
            self._popup.removeNonapplicableActions();

            // populate form with clipNote and/or specific options passed via data
            self.populateQuickNoteForm( data, viewOpts );

            self.startNotebookLoading();
            self.populateSmartFilingInfo();
            self.populateLinkedNotebooks();

            // set site-memory button title
            var tabUrl = self._popup.getTabUrl();
            Evernote.logger.debug( "Tab url (new) " + tabUrl );
            Evernote.logger.debug( "Url top domain (new) " + Evernote.UrlHelpers.urlTopDomain( tabUrl ) );

            Evernote.jQuery( "#siteMemoryTabButton" ).text( Evernote.UrlHelpers.urlTopDomain( tabUrl ) );
            Evernote.jQuery( "#quickNoteView input[name=cancel]" ).val( Evernote.localizer.getMessage( "quickNote_cancel" ) );

            // focus on first tab element
            Evernote.jQuery( "#popup_body" ).focus();
        }
        catch ( e ) {
            Evernote.logger.error( "Exception during showing notes " + e );
        }
    } );

    var updateFunc = function( /*event*/ ) {
        Evernote.viewManager.updateBodyHeight();
    };
    Evernote.jQuery( "#comments textarea" ).focus( updateFunc ).blur( updateFunc );

    quickNoteView.bind( "hide", function( /*event*/ ) {
        Evernote.logger.debug( "quickNoteView.onHide()" );

        Evernote.viewManager.hideBlock( Evernote.jQuery( "#header" ) );
        Evernote.viewManager.hideBlock( notesView );
    } );
};

Evernote.FFQuickNoteController.prototype.bindForm = function() {
    Evernote.logger.debug( "FFQuickNoteController.bindForm()" );

    this._quickNoteForm = Evernote.ClipNoteForm.onForm( Evernote.jQuery( "form[name=quickNoteForm]" ) );
    var form = Evernote.jQuery( "form[name=quickNoteForm]" );

    this.updateNotebookControl();

    var titleField = form.find( "input[name=title]" );
    titleField.bind( "blur", function() {
        titleField.val( titleField.val().trim() );
    } );

    var self = this;
    var notebookSelect = form.find( "select[name=notebookGuid]" );

    if ( notebookSelect.length > 0 ) {
        notebookSelect.bind( "change", function( event ) {
            self.updateNotebookControl();
            self.shineNotebooks( false );
            self.tagsEnable( event );
        } );
    }

    var isCursorOverSubmitButton = false;
    form.find( ":submit[name=quickNote]" ).mouseenter( function() {
        isCursorOverSubmitButton = true;
    } ).mouseleave( function() {
        isCursorOverSubmitButton = false;
    } );

    var commentField = form.find( "textarea[name=comment]" );
    commentField.focus( function() {
        commentField.addClass( "focus" );
        Evernote.viewManager.updateBodyHeight();
    } );

    commentField.blur( function() {
        if ( commentField.val().trim().length > 0 ) {
            commentField.addClass( "focus" );
        }
        else if ( !isCursorOverSubmitButton ) {
            commentField.removeClass( "focus" );
            Evernote.viewManager.updateBodyHeight();
        }
    } );

    var clipAction = form.find( "#clipAction" );
    var updatePreviewFunc = function() {
        self.updateQuickNoteActions();
        self._popup.previewClipAction( clipAction.val() );
    };
    clipAction.bind( "change", updatePreviewFunc ).bind( "keyup", updatePreviewFunc );

    Evernote.jQuery( "#headerUsername" ).click( function() {
        Evernote.tabManager.create( { url : Evernote.getAccountSettingsUrl() } );
        self._popup.dismissPopup();
    } );

    var newFeatureOverlay = Evernote.jQuery("div.newFeatureOverlay");
    if ( Evernote.Platform.getFirefoxVersion() >= 15 ) {
        newFeatureOverlay.addClass("newFeatureOverlayFF15");
    }
    else {
        newFeatureOverlay.addClass("newFeatureOverlayFF14");
    }

    newFeatureOverlay.click( function() {
        self.disableNewFeatureOverlay();
    } );

    this.setFormValidator();
};

Evernote.FFQuickNoteController.prototype.bindTagControl = function() {
    this._tagControl = new Evernote.Chrome.TagEntryBox( this._popup.tab, function() {
        Evernote.viewManager.updateBodyHeight();
    } );
    this._tagControl.setTabIndex( 9 );
    document.querySelector( "#tagControlContainer" ).appendChild( this._tagControl.domElement );
};

Evernote.FFQuickNoteController.prototype.setFormValidator = function() {
    Evernote.logger.debug( "FFQuickNoteController.setFormValidator()" );

    var form = Evernote.jQuery( "form[name=quickNoteForm]" );
    var self = this;

    var options = {
        invalidHandler : function( /*form, validator*/ ) {
            Evernote.logger.debug( "Invalid handler call" );

            setTimeout( function() {
                Evernote.viewManager.updateBodyHeight();
                self._popup.updateNotebookArrowPosition();
            }, self.constructor.VIEW_UPDATE_TIMEOUT );
        },

        success: function() {
            setTimeout( function() {
                Evernote.viewManager.updateBodyHeight();
                self._popup.updateNotebookArrowPosition();
            }, self.constructor.VIEW_UPDATE_TIMEOUT );
        },

        submitHandler : function( /*form*/ ) {
            self.submitForm();
        },

        errorClass : Evernote.ViewManager.FORM_FIELD_ERROR_MESSAGE_CLASS,
        errorElement : "div",
        onkeyup : false,

        onfocusout :function() {
            if ( self._quickNoteValidator.numberOfInvalids() ) {
                self._quickNoteValidator.form();
            }
        },

        rules : {
            title : {
                trim : true,
                required : false,
                rangelength : [ Evernote.Limits.EDAM_NOTE_TITLE_LEN_MIN, Evernote.Limits.EDAM_NOTE_TITLE_LEN_MAX ],
                mask : Evernote.Limits.EDAM_NOTE_TITLE_REGEX
            },

            tagNames : {
                totalPartsRange : [ Evernote.Limits.EDAM_NOTE_TAGS_MIN, Evernote.Limits.EDAM_NOTE_TAGS_MAX ],
                partLengthRange : [ Evernote.Limits.EDAM_TAG_NAME_LEN_MIN, Evernote.Limits.EDAM_TAG_NAME_LEN_MAX ],
                partMask : Evernote.Limits.EDAM_TAG_NAME_REGEX
            }
        },

        messages : {
            title : {
                maxlength : Evernote.localizer.getMessage( "valueNotInRange", [ Evernote.localizer.getMessage( "quickNote_title" ),
                                                                                Evernote.Limits.EDAM_NOTE_TITLE_LEN_MIN,
                                                                                Evernote.Limits.EDAM_NOTE_TITLE_LEN_MAX ] ),
                mask : Evernote.localizer.getMessage( "invalidTitle" )
            },
            tagNames : {
                totalPartsRange : Evernote.localizer.getMessage( "tagNamesNotInRange", [ Evernote.Limits.EDAM_NOTE_TAGS_MIN,
                                                                                         Evernote.Limits.EDAM_NOTE_TAGS_MAX ] ),
                partLengthRange : Evernote.localizer.getMessage( "tagNameNotInRange", [ Evernote.Limits.EDAM_TAG_NAME_LEN_MIN,
                                                                                        Evernote.Limits.EDAM_TAG_NAME_LEN_MAX ] ),
                partMask : Evernote.localizer.getMessage( "invalidTagName" )
            }
        }
    };

    Evernote.logger.debug( "Setting up validation on quicknote form" );
    this._quickNoteValidator = form.validate( options );

    form.observeform( { freezeSubmit : false,
        changeKeyboardDelay : self.constructor.AUTO_SAVE_DELAY,
        onChange : function( /*changeEvent, changeElement, changeForm, observeOptions*/ ) {
                Evernote.viewManager.updateBodyHeight();
                form.addClass( "changed" );
            }
        } );
};

Evernote.FFQuickNoteController.prototype.submitForm = function() {
    Evernote.logger.debug( "FFQuickNoteController.submitForm()" );

    if ( this._quickNoteForm && this._tagControl ) {
        Evernote.logger.debug( "Grabbing data from form" );
        this._tagControl.blurTextEntry();

        Evernote.viewManager.hideErrors();
        var clipNote = this._quickNoteForm.asClipNote();
        if ( !clipNote.title ) {
            clipNote.title = Evernote.localizer.getMessage( "quickNote_untitledNote" );
        }
        delete clipNote.content;

        var tagNames = this._tagControl.getSelectedTags();
        if ( tagNames.length > 0 ) {
            clipNote.tagNames = tagNames.join( "," );
        }

        var clipMetadata = JSON.stringify( { notebookGuid: clipNote.notebookGuid,
                                             title: clipNote.title,
                                             tagNames: clipNote.tagNames,
                                             comment: clipNote.comment } );
        Evernote.logger.debug( "Clip note from " + clipNote.toSource() );
        Evernote.viewManager.hideView( "quickNoteView" );

        this._popup.onSubmitQuickNoteForm( clipMetadata );
    }
    else {
        Evernote.logger.debug( "Nothing to clip..." );
    }
};

Evernote.FFQuickNoteController.prototype.populateNotebookSelection = function( sel ) {
    Evernote.logger.debug( "FFQuickNoteController.populateNotebookSelection()" );

    var notebooks = Evernote.context.notebooks;
    if ( sel instanceof Evernote.jQuery && notebooks instanceof Array ) {
        Evernote.logger.debug( "Updating notebook selection with " + notebooks.length + " notebooks" );

        sel.empty();
        var preferredNotebook = Evernote.context.getPreferredNotebook();

        for ( var i = 0; i < notebooks.length; ++i ) {
            var notebook = notebooks[ i ];
            var notebookName = Evernote.jQuery( "<div>" ).text( notebook.name ).html();
            var opt = Evernote.jQuery( "<option>", { value : notebook.guid, text : notebookName } );

            if ( notebook == preferredNotebook ) {
                opt.attr( "selected", "selected" );
            }
            sel.append( opt );
        }

        sel.bind( "change", function( /*event*/ ) {
            var state = Evernote.context.state;
            state.notebookGuid = sel.val();
            Evernote.context.state = state;
        } );

        // update notebook selection element
        Evernote.DOMHelpers.updateSelectElementWidth( sel, function( guid ) {
            if ( !guid ) {
                return "";
            }
            var notebook = Evernote.context.getNotebookByGuid( guid );
            return (notebook) ? notebook.name : guid;
        } );
    }
    else {
        Evernote.logger.debug( "Nothing to update" );
    }
};

Evernote.FFQuickNoteController.prototype.updateQuickNoteActions = function() {
    Evernote.logger.debug( "FFQuickNoteController.updateQuickNoteActions()" );

    var form = Evernote.jQuery( "form[name=quickNoteForm]" );
    var clipActionElement = Evernote.jQuery( "form[name=quickNoteForm] #clipAction" );
    var clipSubmit = form.find( "input[type=submit]" );

    var val = clipActionElement.val();
    var textVal = clipActionElement.find( "option[value=" + val + "]" ).text();
    var textSize = Evernote.DOMHelpers.getTextSize( textVal );

    Evernote.DOMHelpers.resizeElement( clipActionElement, { width : textSize.width } );
    Evernote.DOMHelpers.resizeElement( clipSubmit, { width : textSize.width }, function( el, sizeObj ) {
        clipSubmit.css( { right : (0 - sizeObj.width) + "px" } );
    } );

    clipSubmit.val( textVal );
};

Evernote.FFQuickNoteController.prototype.populateQuickNoteForm = function( data, viewOpts ) {
    Evernote.logger.debug( "FFQuickNoteController.populateQuickNoteForm()" );

    if ( data && typeof data == 'object' ) {
        Evernote.logger.debug( "Overriding quick note form with passed arguments" );

        if ( typeof data.clipNote == 'object' ) {
            Evernote.logger.debug( "Using supplied clipNote object to populate quick note form" );

            var clipNote = (data.clipNote instanceof Evernote.ClipNote) ? data.clipNote : new Evernote.ClipNote( data.clipNote );
            this._quickNoteForm.populateWithNote( Evernote.context, clipNote );
        }
        else {
            Evernote.logger.debug( "No clipNote object passed, using as a blank quick note" );
        }

        var options = Evernote.jQuery.extend( true, { }, data );
        if ( typeof options.clipNote != 'undefined' ) {
            delete options.clipNote;
        }

        this._quickNoteForm.populateWith( options );
        Evernote.jQuery.extend( true, viewOpts, data );
    }
    else {
        Evernote.logger.debug( "No options were given, will operate as quick note with default options" );
    }
};

Evernote.FFQuickNoteController.prototype.populateTags = function( data, error ) {
    if ( error ) {
        Evernote.logger.error( "Error retrieving tags: " + JSON.stringify( error ) );
    }
    if ( data && data.list ) {
        var tags = [];
        for ( var i = 0; i < data.list.length; i++ ) {
            if ( data.list[i].name ) {
                tags.push( data.list[i].name );
            }
        }
        this._tagControl.setAutoCompleteList( tags );
    }
};

Evernote.FFQuickNoteController.prototype.tagsEnable = function( event ) {
    if ( event && event.target && event.target.value ) {
        this._tagControl.setDisabled( event.target.value.match( /^shared_/ ) );
    }
};

Evernote.FFQuickNoteController.prototype.populateSmartFilingInfo = function() {
    var isForbiddenUrl = Evernote.Utils.isForbiddenUrl( this._popup.getTabUrl() );
    // These indicate that we'll need to get called again, so we just return early and get called again later.
    var smartFillingEnabled = Evernote.context.options.smartFilingEnabled;
    if ( !isForbiddenUrl && smartFillingEnabled != Evernote.Options.SMART_FILLING_ENABLED_OPTIONS.NONE ) {
        var filingRecommendationRequest = {
            text: this._popup.pageInfo.getRecommendationText(),
            url: this._popup.tab.location.href
        };

        var self = this;
        this._popup.jsonRpc.client.NoteStoreExtra.getFilingRecommendations( function( data, error ) {
            if ( error ) {
                Evernote.logger.error( "populateSmartFilingInfo(): " + error );
            }
            else {
                self._smartFillingData = data;
            }

            self.smartFillingLoaded = true;
        }, Evernote.context.authenticationToken, filingRecommendationRequest );

        setTimeout( function() {
            self.smartFillingLoaded = true;
        }, 5000 );
    }
    else {
        this.smartFillingLoaded = true;
    }
};

Evernote.FFQuickNoteController.prototype.populateLinkedNotebooks = function() {
    var self = this;
    Evernote.linkedNotebooksSource.addLinkedNotebooksToSelect( document, document.querySelector( "#notebookControl" ), function() {
        var  destNotebook = Evernote.context.getPreferredNotebook();
        if ( destNotebook instanceof Evernote.Notebook || destNotebook instanceof Evernote.LinkedNotebook && self._quickNoteForm ) {
            self._quickNoteForm.notebookGuid = destNotebook.guid;
        }

        self.updateNotebookControl();
        self.linkedNotebooksLoaded = true;
    } );

    setTimeout( function() {
        self.linkedNotebooksLoaded = true;
    }, 5000 );
};

Evernote.FFQuickNoteController.prototype.startNotebookLoading = function() {
    var self = this;
    this._notebookLoadingInterval = setInterval( function() {
        self.tickNotebookLoading();
    }, 500 );
};

Evernote.FFQuickNoteController.prototype.tickNotebookLoading = function() {
    var el = document.querySelector( "#notebookActivity" );
    var newText = "";
    switch ( el.textContent ) {
        case "":
            newText = ".";
            break;
        case ".":
            newText = "..";
            break;
        case "..":
            newText = "...";
            break;
        default:
            newText = "";
    }
    el.textContent = newText;
};

Evernote.FFQuickNoteController.prototype.finishNotebookLoading = function() {
    if ( this._notebookLoadingInterval ) {
        clearInterval( this._notebookLoadingInterval );
        this._notebookLoadingInterval = null;
    }

    var smartFillingEnabled = Evernote.context.options.smartFilingEnabled;
    var data = this._smartFillingData;
    var select = document.querySelector( "#notebookControl" );

    if ( smartFillingEnabled == Evernote.Options.SMART_FILLING_ENABLED_OPTIONS.ALL || smartFillingEnabled == Evernote.Options.SMART_FILLING_ENABLED_OPTIONS.NOTEBOOKS ) {
        if ( data && data.notebook ) {
           for ( var i = 0; i < select.options.length; ++i ) {
                if ( select.options[ i ].value == data.notebook.guid ) {
                    select.selectedIndex = i;
                    this._quickNoteForm.notebookGuid = data.notebook.guid;
                    break;
                }
            }
        }
    }

    if ( smartFillingEnabled == Evernote.Options.SMART_FILLING_ENABLED_OPTIONS.ALL || smartFillingEnabled == Evernote.Options.SMART_FILLING_ENABLED_OPTIONS.TAGS ) {
        if ( data && data.tags && data.tags.list && data.tags.list.length > 0 ) {
            for ( i = 0; i < data.tags.list.length; ++i ) {
                this._tagControl.addTag( data.tags.list[i].name );
            }
        }
    }

    this.shineNotebooks( navigator.onLine && (smartFillingEnabled == Evernote.Options.SMART_FILLING_ENABLED_OPTIONS.ALL || smartFillingEnabled == Evernote.Options.SMART_FILLING_ENABLED_OPTIONS.NOTEBOOKS) );
    this.updateNotebookControl();

    select.style.display = "";
    document.querySelector( "#notebookPlaceholder" ).style.display = "none";

    this.tagsEnable( { target: select } );

    if ( smartFillingEnabled != Evernote.Options.SMART_FILLING_ENABLED_OPTIONS.NONE ) {
        if ( !Evernote.prefsManager.getPref( Evernote.PrefKeys.SUPPRESS_SMART_FILLING_NOTICE, "boolean" ) ) {
            document.querySelector( ".newFeatureOverlay" ).style.display = "block";
        }
    }
};

Evernote.FFQuickNoteController.prototype.shineNotebooks = function( shiny ) {
    var select = Evernote.jQuery( "#notebookControl" );
    if ( shiny ) {
        select.addClass( "notebookSelectSmartSelection" );
    }
    else {
        select.removeClass( "notebookSelectSmartSelection" );
    }

    this._popup.arrowHideMode = (shiny) ? Evernote.FFPopup.ARROW_HIDE_MODE_USE_SMART_FILLING : Evernote.FFPopup.ARROW_HIDE_MODE_DEFAULT;
};

Evernote.FFQuickNoteController.prototype.updateNotebookControl = function() {
    Evernote.DOMHelpers.updateSelectElementWidth( Evernote.jQuery( "select[name=notebookGuid]" ), function( guid ) {
        if ( !guid ) {
            return "";
        }
        else if ( guid.match( /^shared_/ ) ) {
            var notebook = Evernote.context.getLinkedNotebookByGuid( guid );
            return (notebook) ? Evernote.localizer.getMessage( "linkedNotebookOptionText", [notebook.name, notebook.owner] ) : guid;
        }
        else {
            notebook = Evernote.context.getNotebookByGuid( guid );
            return (notebook) ? notebook.name : guid;
        }

        return "";
    } );

    this._popup.updateNotebookArrowPosition();
};

Evernote.FFQuickNoteController.prototype.setSmartFillingLoaded = function() {
    if ( !this._smartFillingLoaded ) {
        this._smartFillingLoaded = true;
        if ( this._linkedNotebooksLoaded ) {
            this.finishNotebookLoading();
        }
    }
};

Evernote.FFQuickNoteController.prototype.setLinkedNotebooksLoaded = function() {
    if ( !this._linkedNotebooksLoaded ) {
        this._linkedNotebooksLoaded = true;
        if ( this._smartFillingLoaded ) {
            this.finishNotebookLoading();
        }
    }
};

Evernote.FFQuickNoteController.prototype.tryClipArticle = function( event ) {
    var clipAction = Evernote.jQuery( "form[name=quickNoteForm] #clipAction" );
    if ( clipAction.val() != "CLIP_ACTION_ARTICLE" ) {
        return;
    }

    if ( !this._tagControl.isFocused() ) {
        this.submitForm();
    }
};

Evernote.FFQuickNoteController.prototype.disableNewFeatureOverlay = function() {
    document.querySelector(".newFeatureOverlay").style.display = "none";
    Evernote.prefsManager.addPref( Evernote.PrefKeys.SUPPRESS_SMART_FILLING_NOTICE, true );
};

Evernote.FFQuickNoteController.prototype.cleanUp = function() {
    if ( this._tagControl ) {
        this._tagControl.cleanUp();
        this._tagControl = null;
    }
};