(function( a ) {
    a.tabDrawer = {TOP_EDGE:"top",BOTTOM_EDGE:"bottom",LEFT_EDGE:"left",RIGHT_EDGE:"right",LEFT_ALIGN:"left",RIGHT_ALIGN:"right",TOP_ALIGN:"top",BOTTOM_ALIGN:"bottom",DRAWER_GROUP_CLASS:"drawerGroup",DRAWER_CONTAINER_CLASS:"drawerContainer",HANDLE_CLASS:"drawerHandle",HANDLE_TITLE_CLASS:"drawerHandleTitle",DRAWER_CLASS:"drawer",DRAWER_CONTENT_CLASS:"drawerContent",OPEN_STATE_CLASS:"openDrawer",CLOSED_STATE_CLASS:"closedDrawer",FOCUSED_STATE_CLASS:"focusedDrawer",TOP_EDGE_CLASS:"drawerTop",BOTTOM_EDGE_CLASS:"drawerBottom",LEFT_EDGE_CLASS:"drawerLeft",RIGHT_EDGE_CLASS:"drawerRight",OPEN_STATE:"open",CLOSED_STATE:"closed",FOCUSED_STATE:"focused",SHOW_EVENT:"showdrawer",HIDE_EVENT:"hidedrawer",CLOSE_EVENT:"closedrawer",OPEN_EVENT:"opendrawer",CLOSE_EVENT:"closedrawer",FOCUS_EVENT:"focusdrawer",BLUR_EVENT:"blurdrawer",groupContainer:null,groups:[],showAll:function() {
        a( a.tabDrawer.groups ).each( function( c, d ) {
            if ( d instanceof Array ) {
                a( d ).each( function( e, f ) {
                    if ( f ) {
                        f.show()
                    }
                } )
            }
        } )
    },hideAll:function() {
        a( a.tabDrawer.groups ).each( function( c, d ) {
            if ( d instanceof Array ) {
                a( d ).each( function( e, f ) {
                    if ( f ) {
                        f.hide()
                    }
                } )
            }
        } )
    },toggleHandler:function( d ) {
        var c = null;
        if ( typeof d.data != "undefined" || typeof d.data.drawer != "undefined" ) {
            c = d.data.drawer
        }
        else {
            var c = a( d.target ).parentsUntil( "." + a.tabDrawer.DRAWER_CONTAINER_CLASS ).parent().get( 0 ).tabDrawer
        }
        if ( c && c.isFocused() ) {
            c.toggle.apply( c, [] )
        }
        else {
            if ( c && c.isOpen() ) {
                c.focus()
            }
            else {
                if ( c ) {
                    c.focus();
                    c.toggle.apply( c, [] )
                }
            }
        }
        return false
    },moveNextTo:function( m, l, g, e ) {
        var n = "";
        if ( g.length > 0 ) {
            var h = null;
            var c = null;
            if ( m == "left" ) {
                h = "width";
                c = "right"
            }
            else {
                if ( m == "right" ) {
                    h = "width";
                    c = "left"
                }
                else {
                    if ( m == "top" ) {
                        h = "height";
                        c = "bottom"
                    }
                    else {
                        if ( m == "bottom" ) {
                            h = "height";
                            c = "top"
                        }
                    }
                }
            }
            var k = h.substring( 0, 1 ).toUpperCase() + h.substring( 1 );
            var f = g[0].getBoundingClientRect();
            var d = window.getComputedStyle( l[0] );
            var j = parseInt( d.getPropertyValue( "border-" + c + "-width" ) );
            if ( isNaN( j ) ) {
                j = 0
            }
            n = window["inner" + k] - f[m] - j;
            n += "px"
        }
        var i = {};
        i[c] = n;
        var o = (typeof e == "number" && !isNaN( e )) ? e : 0;
        l.animate( i, e )
    }};
    a.tabDrawer.options = {edge:a.tabDrawer.BOTTOM_EDGE,align:a.tabDrawer.RIGHT_ALIGN,group:0,tabIndex:0,title:null,opened:false,focused:false,visible:false,groupElement:null,handleElement:null,handleTitleElement:null,drawerElement:null,drawerContentElement:null,transitionTime:0};
    a.fn.tabDrawer = function( d ) {
        var c = this.each( function( f, g ) {
            var h = null;
            if ( typeof g.tabDrawer == "undefined" ) {
                function i() {
                }

                i.prototype = b;
                var h = new i();
                h.initialize( a( g ), d );
                g.tabDrawer = h
            }
            else {
                h = g.tabDrawer
            }
        } );
        for ( var e in b ) {
            if ( typeof b[e] == "function" ) {
                c[e] = function() {
                    var f = arguments;
                    var g = arguments.callee.callName;
                    c.each( function( h, i ) {
                        if ( i && typeof i.tabDrawer != "undefined" ) {
                            i.tabDrawer[g].apply( i.tabDrawer, f )
                        }
                    } )
                };
                c[e].callName = e
            }
        }
        return c
    };
    var b = {group:0,edge:null,tabIndex:0,groupElement:null,drawerContainerElement:null,handleElement:null,handleTitleElement:null,drawerElement:null,drawerContentElement:null,transitionTime:0,updatesAllowed:true,initialize:function( g, d ) {
        var h = a.extend( {}, a.tabDrawer.options, d );
        for ( var f in this ) {
            if ( typeof this[f] != "undefined" && typeof this[f] != "function" ) {
                this[f] = h[f]
            }
        }
        if ( typeof h.transitionTime == "number" && !isNaN( h.transitionTime ) ) {
            this.transitionTime = h.transitionTime
        }
        this.drawerContainerElement = g;
        this.drawerContainerElement.addClass( a.tabDrawer.DRAWER_CONTAINER_CLASS );
        this.drawerContainerElement.addClass( a.tabDrawer[this.edge.toUpperCase() + "_EDGE_CLASS"] );
        this.groupElement = (h.groupElement) ? h.groupElement : this.drawerContainerElement.parent();
        if ( h.groupElement ) {
            this.groupElement = h.groupElement
        }
        else {
            if ( this.drawerContainerElement.parent().length == 1 ) {
                this.groupElement = this.drawerContainerElement.parent()
            }
            else {
                var c = document.getElementsByTagName( "body" );
                if ( c && c.length > 0 ) {
                    this.groupElement = c[0]
                }
            }
        }
        if ( h.handleElement ) {
            this.handleElement = h.handleElement
        }
        else {
            this.handleElement = a( "<div class='" + a.tabDrawer.HANDLE_CLASS + "'></div>" );
            this.drawerContainerElement.prepend( this.handleElement )
        }
        if ( h.handleTitleElement ) {
            this.handleTitleElement = h.handleTitleElement
        }
        else {
            this.handleTitleElement = a( "<div class='" + a.tabDrawer.HANDLE_TITLE_CLASS + "'></div>" );
            this.handleElement.append( this.handleTitleElement )
        }
        if ( h.drawerElement ) {
            this.drawerElement = h.drawerElement
        }
        else {
            this.drawerElement = a( "<div class='" + a.tabDrawer.DRAWER_CLASS + "'></div>" );
            this.drawerContainerElement.append( this.drawerElement )
        }
        if ( h.drawerContentElement ) {
            this.drawerContentElement = h.drawerContentElement
        }
        else {
            this.drawerContentElement = a( "<div class='" + a.tabDrawer.DRAWER_CONTENT_CLASS + "'></div>" );
            this.drawerElement.append( this.drawerContentElement )
        }
        if ( this.drawerContainerElement.parent().length == 0 ) {
            var e = this.getPreviousDrawer();
            if ( e ) {
                e.after( this.drawerContainerElement )
            }
            else {
                this.groupElement.prepend( this.drawerContainerElement )
            }
        }
        if ( typeof a.tabDrawer.groups[this.group] == "undefined" ) {
            a.tabDrawer.groups[this.group] = new Array()
        }
        a.tabDrawer.groups[this.group][this.tabIndex] = this;
        this.preventUpdates();
        this.initBindings();
        if ( h.title ) {
            this.setHandleTitle( h.title )
        }
        if ( h.visible ) {
            this.drawerContainerElement.show()
        }
        else {
            this.drawerContainerElement.hide()
        }
        if ( h.focused ) {
            this.focus()
        }
        else {
            this.blur()
        }
        this.allowUpdates();
        this.update()
    },initBindings:function() {
        this.handleElement.unbind( "click", a.tabDrawer.toggleHandler );
        this.handleElement.bind( "click", {drawer:this}, a.tabDrawer.toggleHandler );
        this.handleTitleElement.unbind( "click", a.tabDrawer.toggleHandler );
        this.handleTitleElement.bind( "click", {drawer:this}, a.tabDrawer.toggleHandler )
    },setHandleTitle:function( c ) {
        this.handleTitleElement.html( c );
        this.update()
    },preventUpdates:function() {
        this.updatesAllowed = false
    },allowUpdates:function() {
        this.updatesAllowed = true
    },open:function() {
        this.show();
        var e = [a.tabDrawer.OPEN_STATE_CLASS,a.tabDrawer.CLOSED_STATE_CLASS].join( " " );
        this.toggleClass( e );
        var c = this;
        var d = [];
        a( a.tabDrawer.groups[this.group] ).each( function( f, g ) {
            if ( g && g != c ) {
                d.push( g.drawerContainerElement );
                d.push( g.handleElement );
                d.push( g.drawerElement )
            }
        } );
        a( d ).toggleClass( e, this.transitionTime );
        a( document.body ).toggleClass( e, this.transitionTime );
        setTimeout( function() {
            c.update();
            c.focus()
        }, this.transitionTime );
        this.drawerContainerElement.trigger( a.tabDrawer.OPEN_EVENT, [this] )
    },close:function() {
        this.show();
        var e = [a.tabDrawer.OPEN_STATE_CLASS,a.tabDrawer.CLOSED_STATE_CLASS].join( " " );
        this.toggleClass( e );
        var c = this;
        var d = [];
        a( a.tabDrawer.groups[this.group] ).each( function( f, g ) {
            if ( g && g != c ) {
                d.push( g.drawerContainerElement );
                d.push( g.handleElement );
                d.push( g.drawerElement )
            }
        } );
        a( d ).toggleClass( e, this.transitionTime );
        a( document.body ).toggleClass( e, this.transitionTime );
        setTimeout( function() {
            c.update()
        }, this.transitionTime );
        this.drawerContainerElement.trigger( a.tabDrawer.CLOSE_EVENT, [this] )
    },show:function() {
        var c = this;
        this.drawerContainerElement.show();
        setTimeout( function() {
            c.update()
        }, this.transitionTime );
        this.drawerContainerElement.trigger( a.tabDrawer.SHOW_EVENT, [this] )
    },hide:function() {
        var c = this;
        this.drawerContainerElement.hide();
        setTimeout( function() {
            c.update()
        }, this.transitionTime );
        this.drawerContainerElement.trigger( a.tabDrawer.HIDE_EVENT, [this] )
    },update:function() {
        if ( !this.updatesAllowed ) {
            return
        }
        var c = a( a.tabDrawer.groups[this.group] ).filter( function( f, g ) {
            if ( g && g.isVisible() ) {
                return g
            }
        } );
        var e = c.map( function( f, g ) {
            return g.handleElement
        } );
        for ( var d = 1; d < e.length; d++ ) {
            a.tabDrawer.moveNextTo( "left", e[d], e[d - 1], this.transitionTime )
        }
    },focus:function() {
        var c = this;
        a( a.tabDrawer.groups[this.group] ).each( function( d, e ) {
            if ( e && e == c ) {
                e.drawerContainerElement.addClass( a.tabDrawer.FOCUSED_STATE_CLASS )
            }
            else {
                if ( e ) {
                    e.drawerContainerElement.removeClass( a.tabDrawer.FOCUSED_STATE_CLASS )
                }
            }
        } );
        setTimeout( function() {
            c.update()
        }, this.transitionTime );
        this.drawerContentElement.trigger( a.tabDrawer.FOCUS_EVENT, [this] )
    },blur:function() {
        this.drawerContentElement.removeClass( a.tabDrawer.FOCUSED_STATE_CLASS );
        this.drawerContentElement.trigger( a.tabDrawer.BLUR_EVENT, [this] )
    },getPreviousDrawer:function() {
        var d = null;
        var c = this.tabIndex;
        while ( c > 0 ) {
            c--;
            if ( typeof a.tabDrawer.groups[this.group][c] != "undefined" ) {
                d = a.tabDrawer.groups[this.group][c];
                break
            }
        }
        return d
    },getNextDrawer:function() {
        var d = null;
        var c = this.tabIndex;
        while ( c < (a.tabDrawer.groups[this.group].length - 1) ) {
            c++;
            if ( typeof a.tabDrawer.groups[this.group][c] != "undefined" ) {
                d = a.tabDrawer.groups[this.group][c];
                break
            }
        }
        return d
    },toggle:function() {
        if ( this.isOpen() ) {
            this.close()
        }
        else {
            this.open()
        }
    },isFocused:function() {
        return this.hasClass( a.tabDrawer.FOCUSED_STATE_CLASS )
    },isVisible:function() {
        return this.drawerContainerElement.css( "display" ) != "none"
    },isOpen:function() {
        return a( document.body ).hasClass( a.tabDrawer.OPEN_STATE_CLASS )
    },isClosed:function() {
        return this.hasClass( a.tabDrawer.CLOSED_STATE_CLASS )
    },addClass:function( c ) {
        this.drawerContainerElement.addClass( c, this.transitionTime );
        this.drawerElement.addClass( c, this.transitionTime );
        this.handleElement.addClass( c, this.transitionTime )
    },removeClass:function( c ) {
        this.drawerContainerElement.removeClass( c, this.transitionTime );
        this.drawerElement.removeClass( c, this.transitionTime );
        this.handleElement.removeClass( c, this.transitionTime )
    },toggleClass:function( c ) {
        this.drawerContainerElement.toggleClass( c, this.transitionTime );
        this.drawerElement.toggleClass( c, this.transitionTime );
        this.handleElement.toggleClass( c, this.transitionTime )
    },hasClass:function( c ) {
        return this.drawerContainerElement.hasClass( c )
    }}
})( jQuery );