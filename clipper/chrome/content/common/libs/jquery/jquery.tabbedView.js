(function( b ) {
    b.tabbedView = {options:{datasetButton:"tabbedviewbutton",datasetView:"tabbedview",onshowview:null,onhideview:null}};
    b.fn.getTabbedView = function() {
        var c = [];
        this.each( function( e, f ) {
            var d = b( f );
            var g = d.data();
            if ( g && typeof g.tabbedView != "undefined" ) {
                c.push( g.tabbedView )
            }
        } );
        return c
    };
    b.fn.tabbedView = function( c ) {
        return this.each( function( j, f ) {
            var n = b( f );
            var d = n.data();
            d = (d) ? d : {};
            var m = b.extend( {}, b.tabbedView.options, (c) ? c : {} );
            if ( typeof d.tabbedView == "undefined" ) {
                d.tabbedView = new a( n );
                var h = n.find( "*[data-tabbedview]" );
                var l = n.find( "*[data-tabbedviewbutton]" );
                for ( var g = 0; g < h.length; g++ ) {
                    var k = b( h[g] );
                    d.tabbedView.addView( k.attr( "data-tabbedview" ), k )
                }
                for ( var g = 0; g < l.length; g++ ) {
                    var k = b( l[g] );
                    d.tabbedView.addViewButton( k.attr( "data-tabbedviewbutton" ), k )
                }
                if ( m ) {
                    d.tabbedView.onshowview = m.onshowview;
                    d.tabbedView.onhideview = m.onhideview
                }
                n.data( d )
            }
        } )
    };
    var a = function a( c ) {
        this.initialize( c )
    };
    a.prototype.container = null;
    a.prototype.buttons = null;
    a.prototype.views = null;
    a.prototype.currentView = null;
    a.prototype.OPEN_CLASS = "open";
    a.prototype.CLOSED_CLASS = "closed";
    a.prototype.VIEW_SWITCH_DURATION = 0;
    a.prototype.onshowview = null;
    a.prototype.onhideview = null;
    a.prototype.initialize = function( c ) {
        this.container = c;
        this.buttons = {};
        this.views = {}
    };
    a.prototype.addView = function( d, c ) {
        if ( typeof d == "string" && d ) {
            this.views[d] = c
        }
    };
    a.prototype.addViewButton = function( d, c ) {
        if ( typeof d == "string" && d ) {
            this.buttons[d] = c;
            this._bindViewButton( d, c )
        }
    };
    a.prototype._bindViewButton = function( e, d ) {
        var c = this;
        d.bind( "click", function() {
            c.toggleView( e )
        } );
        d.bind( "keyup", function( f ) {
            if ( f.keyCode == 13 || f.keyCode == 32 ) {
                c.toggleView( e )
            }
        } )
    };
    a.prototype.updateViews = function() {
        for ( var c in this.views ) {
            if ( c != this.currentView ) {
                this.hideView( c )
            }
        }
    };
    a.prototype.toggleView = function( d ) {
        var c = this.views[d];
        if ( c ) {
            if ( c.hasClass( this.OPEN_CLASS ) ) {
                this.hideView( d )
            }
            else {
                if ( c.hasClass( this.CLOSED_CLASS ) ) {
                    this.currentView = d;
                    this.showView( d )
                }
                else {
                    this.currentView = d;
                    this.showView( d )
                }
            }
            this.updateViews()
        }
    };
    a.prototype.showView = function( e ) {
        var c = this.views[e];
        var d = this.buttons[e];
        if ( c ) {
            c.switchClass( this.CLOSED_CLASS, this.OPEN_CLASS, this.VIEW_SWITCH_DURATION );
            if ( d ) {
                d.switchClass( this.CLOSED_CLASS, this.OPEN_CLASS, this.VIEW_SWITCH_DURATION )
            }
            this._onshowview( e )
        }
    };
    a.prototype.hideView = function( e ) {
        var c = this.views[e];
        var d = this.buttons[e];
        if ( c ) {
            c.switchClass( this.OPEN_CLASS, this.CLOSED_CLASS, this.VIEW_SWITCH_DURATION );
            if ( d ) {
                d.switchClass( this.OPEN_CLASS, this.CLOSED_CLASS, this.VIEW_SWITCH_DURATION )
            }
            this._onhideview( e )
        }
    };
    a.prototype._onshowview = function( c ) {
        this.updateContainer();
        if ( typeof this.onshowview == "function" ) {
            this.onshowview( c, this.views[c] )
        }
    };
    a.prototype._onhideview = function( c ) {
        this.updateContainer();
        if ( typeof this.onhideview == "function" ) {
            this.onhideview( c, this.views[c] )
        }
    };
    a.prototype.updateContainer = function() {
        if ( this.isShowingView() ) {
            this.container.switchClass( this.CLOSED_CLASS, this.OPEN_CLASS, this.VIEW_SWITCH_DURATION )
        }
        else {
            this.container.switchClass( this.OPEN_CLASS, this.CLOSED_CLASS, this.VIEW_SWITCH_DURATION )
        }
    };
    a.prototype.isShowingView = function() {
        for ( var c in this.views ) {
            if ( this.views[c].hasClass( this.OPEN_CLASS ) ) {
                return true
            }
        }
    };
    a.prototype.getVisibleViews = function() {
        var c = [];
        for ( var d in this.views ) {
            if ( this.views[d].hasClass( this.OPEN_CLASS ) ) {
                c.push( d )
            }
        }
        return c
    };
    a.prototype.setVisibleButtonTitle = function( c ) {
        if ( this.currentView && this.buttons[this.currentView] ) {
            this.buttons[this.currentView].text( c )
        }
    }
})( jQuery );