//"use strict";

Evernote.AuthenticationResult = function AuthenticationResult( obj ) {
    this.__defineGetter__( "timestamp", this.getTimestamp );
    this.__defineGetter__( "user", this.getUser );
    this.__defineSetter__( "user", this.setUser );
    this.__defineGetter__( "expiration", this.getExpiration );
    this.__defineSetter__( "expiration", this.setExpiration );
    this.__defineGetter__( "currentTime", this.getCurrentTime );
    this.__defineSetter__( "currentTime", this.setCurrentTime );
    this.initialize( obj );
};

Evernote.AuthenticationResult.javaClass = "com.evernote.edam.userstore.AuthenticationResult";
Evernote.inherit( Evernote.AuthenticationResult, Evernote.AppModel, true );

Evernote.AuthenticationResult.prototype._expiration = null;
Evernote.AuthenticationResult.prototype._currentTime = null;
Evernote.AuthenticationResult.prototype.authenticationToken = null;
Evernote.AuthenticationResult.prototype._user = null;
/**
 * timestamp keeps epoch time of when currentTime is set making it possible to
 * compare relative expiration time
 */
Evernote.AuthenticationResult.prototype._timestamp = null;
/**
 * critical time holds default value indicating how many milliseconds before
 * expiration time that this authentication result is considered in critical
 * condition
 */
Evernote.AuthenticationResult.prototype.criticalTime = (1000 * 60 * 6);

Evernote.AuthenticationResult.prototype.getTimestamp = function() {
    return this._timestamp;
};

Evernote.AuthenticationResult.prototype.setCurrentTime = function( num ) {
    if ( num == null ) {
        this._currentTime = null;
    }
    else if ( typeof num == 'number' ) {
        this._currentTime = parseInt( num );
    }
    this._timestamp = new Date().getTime();
};

Evernote.AuthenticationResult.prototype.getCurrentTime = function() {
    return this._currentTime;
};

Evernote.AuthenticationResult.prototype.setUser = function( user ) {
    if ( user == null ) {
        this._user = null
    }
    else if ( user instanceof Evernote.User ) {
        this._user = user;
    }
};

Evernote.AuthenticationResult.prototype.getUser = function() {
    return this._user;
};

Evernote.AuthenticationResult.prototype.setExpiration = function( num ) {
    if ( num == null ) {
        this._expiration = null;
    }
    else if ( typeof num == 'number' ) {
        this._expiration = parseInt( num );
    }
};

Evernote.AuthenticationResult.prototype.getExpiration = function() {
    if ( this._expiration && this.currentTime ) {
        var delta = (this.timestamp) ? this.timestamp : 0;
        return (this._expiration - this.currentTime) + delta;
    }
    else {
        return this._expiration;
    }
};

Evernote.AuthenticationResult.prototype.isExpiring = function() {
    var now = new Date().getTime();
    var critical = this.expiration - now - this.criticalTime;
    return critical <= this.criticalTime;
};

Evernote.AuthenticationResult.prototype.isExpired = function() {
    var now = new Date().getTime();
    return this.expiration <= now;
};

Evernote.AuthenticationResult.prototype.expire = function() {
    this.expiration = null;
    this.currentTime = null;
    this._timestamp = null;
};

Evernote.AuthenticationResult.prototype.isEmpty = function() {
    return (!(this.authenticationToken != null && this.user instanceof Evernote.User));
};
