//"use strict";

(function() {
    if ( typeof Evernote != "undefined" && typeof Evernote.ScriptLoader != "undefined" ) {
        Evernote.ScriptLoader.load( [ "chrome://webclipper3-common/content/models/AppModel.js",
                                      "chrome://webclipper3-common/content/models/AppDataModel.js",
                                      "chrome://webclipper3-common/content/models/Snippet.js",
                                      "chrome://webclipper3-common/content/models/BasicNote.js",
                                      "chrome://webclipper3-common/content/models/NoteMetadata.js",
                                      "chrome://webclipper3-common/content/models/NoteAttributes.js",
                                      "chrome://webclipper3-common/content/models/User.js",
                                      "chrome://webclipper3-common/content/models/SyncState.js",
                                      "chrome://webclipper3-common/content/models/SyncChunk.js",
                                      "chrome://webclipper3-common/content/models/ClipNote.js",
                                      "chrome://webclipper3-common/content/models/BasicNote.js",
                                      "chrome://webclipper3-common/content/models/NotesMetadataResultSpec.js",
                                      "chrome://webclipper3-common/content/models/SnippetNoteMetadata.js",
                                      "chrome://webclipper3-common/content/models/Note.js",
                                      "chrome://webclipper3-common/content/models/SnippetNote.js",
                                      "chrome://webclipper3-common/content/models/Notebook.js",
                                      "chrome://webclipper3-common/content/models/LinkedNotebook.js",
                                      "chrome://webclipper3-common/content/models/Tag.js",
                                      "chrome://webclipper3-common/content/models/SavedSearch.js",
                                      "chrome://webclipper3-common/content/models/NoteSortOrder.js",
                                      "chrome://webclipper3-common/content/models/NoteFilter.js",
                                      "chrome://webclipper3-common/content/models/NoteList.js",
                                      "chrome://webclipper3-common/content/models/NotesMetadataList.js",
                                      "chrome://webclipper3-common/content/models/Options.js",
                                      "chrome://webclipper3-common/content/models/AppState.js" ] );
    }
})();
