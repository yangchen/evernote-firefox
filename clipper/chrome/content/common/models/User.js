//"use strict";

Evernote.User = function User( obj ) {
    this.__defineGetter__( "id", this.getId );
    this.__defineSetter__( "id", this.setId );
    this.__defineGetter__( "created", this.getCreated );
    this.__defineSetter__( "created", this.setCreated );
    this.__defineGetter__( "updated", this.getUpdated );
    this.__defineSetter__( "updated", this.setUpdated );
    this.__defineGetter__( "deleted", this.getDeleted );
    this.__defineSetter__( "deleted", this.setDeleted );
    this.__defineGetter__( "active", this.isActive );
    this.__defineSetter__( "active", this.setActive );
    this.__defineGetter__( "attributes", this.getAttributes );
    this.__defineSetter__( "attributes", this.setAttributes );
    this.__defineGetter__( "accounting", this.getAccounting );
    this.__defineSetter__( "accounting", this.setAccounting );
    this.initialize( obj );
};

Evernote.User.javaClass = "com.evernote.edam.type.User";
Evernote.inherit( Evernote.User, Evernote.AppModel, true );

Evernote.User.prototype._id = null;
Evernote.User.prototype.username = null;
Evernote.User.prototype.email = null;
Evernote.User.prototype.name = null;
Evernote.User.prototype.timezone = null;
Evernote.User.prototype.privilege = null;
Evernote.User.prototype._created = null;
Evernote.User.prototype._updated = null;
Evernote.User.prototype._deleted = null;
Evernote.User.prototype._active = false;
Evernote.User.prototype.shardId = null;
Evernote.User.prototype._attributes = null;
Evernote.User.prototype._accounting = null;

Evernote.User.prototype.setId = function( id ) {
    if ( id == null ) {
        this._id = null;
    }
    else if ( typeof id == 'number' ) {
        this._id = parseInt( id );
    }
};

Evernote.User.prototype.getId = function() {
    return this._id;
};

Evernote.User.prototype.setActive = function( bool ) {
    this._active = (bool) ? true : false;
};

Evernote.User.prototype.isActive = function() {
    return this._active;
};

Evernote.User.prototype.setCreated = function( num ) {
    if ( num == null ) {
        this._created = null;
    }
    else if ( typeof num == 'number' ) {
        this._created = parseInt( num );
    }
};

Evernote.User.prototype.getCreated = function() {
    return this._created;
};

Evernote.User.prototype.setUpdated = function( num ) {
    if ( num == null ) {
        this._updated = null;
    }
    else if ( typeof num == 'number' ) {
        this._updated = parseInt( num );
    }
};

Evernote.User.prototype.getUpdated = function() {
    return this._updated;
};

Evernote.User.prototype.setDeleted = function( num ) {
    if ( num == null ) {
        this._deleted = null;
    }
    else if ( typeof num == 'number' ) {
        this._deleted = parseInt( num );
    }
};

Evernote.User.prototype.getDeleted = function() {
    return this._deleted;
};

Evernote.User.prototype.setAccounting = function( /*accounting*/ ) {
};

Evernote.User.prototype.getAccounting = function() {
    return this._accounting;
};

Evernote.User.prototype.setAttributes = function( /*attrs*/ ) {
};

Evernote.User.prototype.getAttributes = function() {
    return this._attributes;
};