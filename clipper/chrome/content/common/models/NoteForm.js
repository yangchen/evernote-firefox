//"use strict";

Evernote.NoteForm = function NoteForm( /*form*/ ) {
};

Evernote.NoteForm.URL = "url";
Evernote.NoteForm.THUMBNAIL_URL = "thumbnailUrl";
Evernote.NoteForm.NOTE_URL = "noteUrl";
Evernote.NoteForm.GUID = "guid";

Evernote.inherit( Evernote.NoteForm, Evernote.AbstractNoteForm, true );

Evernote.NoteForm.onForm = function( form, fieldNames ) {
    form = this.constructor.parent.onForm.apply( this, [ form, fieldNames ] );
    form.__defineGetter__( "url", form.getUrl );
    form.__defineSetter__( "url", form.setUrl );
    form.__defineGetter__( "thumbnailUrl", form.getThumbnailUrl );
    form.__defineSetter__( "thumbnailUrl", form.setThumbnailUrl );
    form.__defineGetter__( "noteUrl", form.getNoteUrl );
    form.__defineSetter__( "noteUrl", form.setNoteUrl );
    form.__defineGetter__( "guid", form.getGuid );
    form.__defineSetter__( "guid", form.setGuid );

    return form;
};

Evernote.NoteForm.prototype.urlFieldName = Evernote.NoteForm.URL;
Evernote.NoteForm.prototype.thumbnailUrlFieldName = Evernote.NoteForm.THUMBNAIL_URL;
Evernote.NoteForm.prototype.noteUrlFieldName = Evernote.NoteForm.NOTE_URL;
Evernote.NoteForm.prototype.guidFieldName = Evernote.NoteForm.GUID;

Evernote.NoteForm.prototype.getUrl = function() {
    return this.getField( this.urlFieldName ).text();
};

Evernote.NoteForm.prototype.setUrl = function( url ) {
    this.getField( this.urlFieldName ).text( url );
};

Evernote.NoteForm.prototype.getThumbnailUrl = function() {
    return this.getField( this.thumbnailUrlFieldName ).attr( "src" );
};

Evernote.NoteForm.prototype.setThumbnailUrl = function( url ) {
    this.getField( this.thumbnailUrlFieldName ).attr( "src", url );
};

Evernote.NoteForm.prototype.getNoteUrl = function() {
    return this.getField( this.noteUrlFieldName ).val();
};

Evernote.NoteForm.prototype.setNoteUrl = function( url ) {
    this.getField( this.noteUrlFieldName ).val( url );
};

Evernote.NoteForm.prototype.setGuid = function( guid ) {
    if ( typeof guid == 'undefined' || guid == null ) {
        this.getField( this.guidFieldName ).val( null );
    }
    else {
        this.getField( this.guidFieldName ).val( guid );
    }
};

Evernote.NoteForm.prototype.getGuid = function() {
    return this.getField( this.guidFieldName ).val();
};

Evernote.NoteForm.prototype.getStorableProps = function() {
    var props = this.parent.getStorableProps();
    props.push( "url" );
    props.push( "thumbnailUrl" );
    props.push( "noteUrl" );
    props.push( "guid" );
    
    return props;
};

Evernote.NoteForm.prototype.populateWith = function( context, note ) {
    Evernote.logger.debug( "NoteForm.populateWith()" );
    
    if ( note instanceof Evernote.Note ) {
        this.parent.populateWith.apply( this, [ note ] );
        if ( note.guid ) {
            Evernote.logger.debug( "Populating guid: " + note.guid );
            this.guid = note.guid;

            var noteUrl = Evernote.getNoteUrl( note );
            Evernote.logger.debug( "Setting noteUrl: " + noteUrl );

            this.noteUrl = noteUrl;
            Evernote.logger.debug( "Setting thumbnailUrl: " + note.getThumbnailUrl( Evernote.getShardedUrl() ) );
            this.thumbnailUrl = note.getThumbnailUrl( Evernote.getShardedUrl() );
        }

        if ( note.attributes instanceof Evernote.NoteAttributes && note.attributes.sourceURL ) {
            Evernote.logger.debug( "Populating sourceURL: " + note.attributes.sourceURL );
            this.url = note.attributes.sourceURL;
        }
    }
    else {
        Evernote.logger.error( "Tried to populate Evernote.NoteForm with non-Evernote.Note" );
    }
};

Evernote.NoteForm.prototype.asNote = function() {
    Evernote.logger.debug( "NoteForm.asNote()" );

    var note = new Evernote.Note();
    if ( this.guid ) {
        Evernote.logger.debug( "Setting guid: " + this.guid );
        note.guid = this.guid;
    }

    if ( this.title ) {
        Evernote.logger.debug( "Setting title: " + this.title );
        note.title = this.title;
    }

    if ( this.notebookGuid ) {
        Evernote.logger.debug( "Setting notebookGuid: " + this.notebookGuid );
        note.notebookGuid = this.notebookGuid;
    }

    if ( this.comment ) {
        Evernote.logger.debug( "Setting comment: " + this.comment );
        note.comment = this.comment;
    }

    var doTrim = (typeof "".trim == 'function');
    if ( typeof this.tagNames == 'string' && this.tagNames.length > 0 ) {
        Evernote.logger.debug( "Setting tags: " + this.tagNames );

        var parts = this.tagNames.split( "," );
        if ( parts instanceof Array && parts.length > 0 ) {
            for ( var i = 0; i < parts.length; i++ ) {
                var t = new Evernote.Tag();
                t.name = (doTrim) ? parts[i].trim() : parts[i];
                Evernote.logger.debug( "Adding tag: " + t.name );
                note.addTag( t );
            }
        }
    }
    
    if ( this.url ) {
        Evernote.logger.debug( "Setting sourceURL attr: " + this.url );
        var attrs = (note.attributes instanceof Evernote.NoteAttributes) ? note.attributes : (new Evernote.NoteAttributes());
        attrs.sourceURL = this.url;
        note.attributes = attrs;
    }
    
    Evernote.logger.debug( "Got this so far: " + JSON.stringify( note ) );
    return note;
};

Evernote.NoteForm.prototype.getModelName = function() {
    return "Evernote.NoteForm";
};

Evernote.NoteForm.prototype.getStringDescription = function() {
    var superStr = this.parent.getStringDescription();
    superStr += "; URL: " + this.url + "; Guid: " + this.guid;
    return superStr;
};