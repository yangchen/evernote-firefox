//"use strict";

/**
 * I represent a basic Evernote.AppModel. My subclasses are more interesting than me, and
 * I keep a Registry for them in order to provide basic object un/marshalling
 * between server and client sides
 */
Evernote.AppModel = function AppModel( obj ) {
    this.__defineGetter__( "modelName", this.getModelName );
    this.initialize( obj );
};

Evernote.AppModel.handleInheritance = function( child/*, parent*/ ) {
    if ( typeof child[Evernote.AppModel.CLASS_FIELD] == 'string' ) {
        Evernote.AppModel.Registry[this[Evernote.AppModel.CLASS_FIELD]] = child;
    }
    child.prototype.constructor.modelName = child.prototype.constructor.toString().replace( /\n/g, "" ).replace( /^.*function\s+([^\(]+).*$/, "$1" );
};
// indicates whether model was updated...
Evernote.AppModel.prototype._updated = false;

Evernote.AppModel.prototype.initialize = function( obj ) {
    if ( obj != null && typeof obj == 'object' ) {
        for ( var i in obj ) {
            var methName = "set" + (i.substring( 0, 1 ).toUpperCase()) + i.substring( 1 );
            if ( typeof this[methName] == 'function' ) {
                this[methName].apply( this, [ obj[i] ] );
            }
            else if ( typeof this[i] != 'undefined' && i != "modelName" ) {
                this[i] = obj[i];
            }
        }
    }
    
    this.constructor.modelName = this.modelName;
};

/**
 * Description of a model, often used by getModelName() and getLabel() to
 * represent the model on canvas
 */
Evernote.AppModel.prototype._modelName = null;

Evernote.AppModel.prototype.getModelName = function() {
    return this.constructor.toString().replace( /\n/g, "" ).replace( /^.*function\s+([^\(]+).*$/, "$1" );
};

Evernote.AppModel.prototype.equals = function( model ) {
    return this == model;
};

Evernote.AppModel.prototype.serialize = function() {
    return JSON.stringify( this );
};

Evernote.AppModel.prototype.properties = function() {
    var props = [ ];
    for ( var i in this ) {
        if ( typeof this[i] != 'function' ) {
            props.push( i );
        }
    }

    return props;
};

Evernote.AppModel.prototype.methods = function() {
    var meths = [ ];
    for ( var i in this ) {
        if ( typeof this[i] == 'function' && i != 'constructor' && i != 'prototype' ) {
            meths.push( i );
        }
    }

    return meths;
};
/**
 * Answers object containing "storable" data. This is used to send to remote
 * services as part of automating translation between server side objects and
 * client side objects.
 */
Evernote.AppModel.prototype.toStorable = function( prefix ) {
    var storable = { };
    var params = this.getStorableProps();
    prefix = (typeof prefix == 'string') ? prefix : "";

    for ( var i = 0; i < params.length; i++ ) {
        if ( typeof this[params[i]] != 'undefined' && this[params[i]] != null ) {
            storable[prefix + params[i]] = this[params[i]];
        }
    }

    return storable;
};
/**
 * toStorable uses this to retrieve names of fields of an instance that are
 * storable.
 */
Evernote.AppModel.prototype.getStorableProps = function() {
    var params = [ ];
    for ( var i in this ) {
        if ( this[i] != null && i.indexOf( "_" ) != 0 // private props
            && i != "modelName" && (typeof this[i] == 'string' || typeof this[i] == 'number' || typeof this[i] == 'boolean') ) {
            params.push( i );
        }
    }

    return params;
};

Evernote.AppModel.prototype.toJSON = function() {
    return this.toStorable();
};

/**
 * Holds references to subclasses of Evernote.AppModel so as to provide quick and easy
 * way to unmarshall classifiable objects. These are often produced by other
 * languages which would e.g. produce JSON strings with a specific field
 * identifying the kind of class the struct represents. i.e. {"javaClass":
 * "java.util.List", "list": [...]} This registry holds references to subclasses
 * of Evernote.AppModel that claim responsibility for handling those identifiable
 * structs.
 */
Evernote.AppModel.Registry = {};
Evernote.AppModel.registeredClasses = function() {
    var str = [ ];
    for ( var i in Evernote.AppModel.Registry ) {
        str.push( i );
    }

    return str;
};
/**
 * Name of the object's field used to identify correlation between JSON struct
 * and typeof Evernote.AppModel
 */
Evernote.AppModel.CLASS_FIELD = "javaClass";

Evernote.AppModel.unmarshall = function( obj ) {
    var newObj = null;
    if ( typeof obj[Evernote.AppModel.CLASS_FIELD] == 'string' && typeof Evernote.AppModel.Registry[obj[Evernote.AppModel.CLASS_FIELD]] == 'function' ) {
        Evernote.logger.debug( "Found a function for class " + obj[Evernote.AppModel.CLASS_FIELD] );
        var constructor = Evernote.AppModel.Registry[obj[Evernote.AppModel.CLASS_FIELD]];
        newObj = new constructor();
    }
    else if ( obj[Evernote.AppModel.CLASS_FIELD] && obj[Evernote.AppModel.CLASS_FIELD].toLowerCase().indexOf( "list" ) > 0 && obj.list instanceof Array ) {
        newObj = [ ];
    }
    else if ( typeof obj == 'string' || typeof obj == 'number' || typeof obj == 'boolean' ) {
        return obj;
    }
    else if ( obj instanceof Array ) {
        newObj = [ ];
        for ( var i = 0; i < obj.length; i++ ) {
            newObj.push( Evernote.AppModel.unmarshall( obj[i] ) );
        }

        return newObj;
    }
    else {
        // create dummy Evernote.AppModel
        newObj = new this();
    }

    if ( newObj ) {
        for ( i in obj ) {
            // skip setters
            if ( i.indexOf( "set" ) == 0 ) {
                continue;
            }

            var setterName = "set" + i.substring( 0, 1 ).toUpperCase() + i.substring( 1 );
            if ( typeof obj[i] == 'string' || typeof obj[i] == 'number' || typeof obj[i] == 'boolean' ) {
                if ( typeof newObj[setterName] == 'function' ) {
                    newObj[setterName].apply( newObj, [ obj[i] ] );
                }
                else {
                    newObj[i] = obj[i];
                }
            }
            else if ( typeof obj[i] == 'object' && obj[i] != null ) {
                newObj[i] = Evernote.AppModel.unmarshall( obj[i] );
            }
        }
    }
    
    Evernote.logger.debug( "Done unmarshalling for object with type " + (typeof newObj) );
    return newObj;
};
/**
 * Marshalls instance of Evernote.AppModel into a classifiable object. Custom logic is
 * implemented by the subclass's toStorable method - which should return the
 * expected structure.
 */
Evernote.AppModel.marshall = function( data ) {
    if ( data instanceof Array ) {
        var newArray = [ ];
        for ( var i = 0; i < data.length; i++ ) {
            newArray.push( Evernote.AppModel._marshall( data[i] ) );
        }
        
        return newArray;
    }
    else {
        return Evernote.AppModel._marshall( data );
    }
};

Evernote.AppModel._marshall = function( data ) {
    if ( data && typeof data.toStorable == 'function' ) {
        return data.toStorable();
    }
    else {
        return data;
    }
};