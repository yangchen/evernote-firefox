//"use strict";

Evernote.NoteSortOrderTypes = {
    CREATED : 1,
    UPDATED : 2,
    RELEVANCE : 3,
    UPDATE_SEQUENCE_NUMBER : 4
};

Evernote.NoteSortOrder = function NoteSortOrder( obj ) {
    this.__defineGetter__( "order", this.getOrder );
    this.__defineSetter__( "order", this.setOrder );
    this.__defineGetter__( "ascending", this.isAscending );
    this.__defineSetter__( "ascending", this.setAscending );
    this.__defineGetter__( "type", this.getType );
    this.initialize( obj );
};

Evernote.NoteSortOrder.isValidOrder = function( num ) {
    if ( typeof num == 'number' ) {
        for ( var t in Evernote.NoteSortOrderTypes ) {
            if ( Evernote.NoteSortOrderTypes[t] == num ) {
                return true;
            }
        }
    }
    return false;
};

Evernote.NoteSortOrder.isValidType = function( orderType ) {
    if ( typeof orderType == 'string' && typeof Evernote.NoteSortOrderTypes[orderType.toUpperCase()] == 'number' ) {
        return true;
    }

    return false;
};

Evernote.NoteSortOrder.prototype._order = Evernote.NoteSortOrderTypes.CREATED;
Evernote.NoteSortOrder.prototype._ascending = false;

Evernote.NoteSortOrder.prototype.initialize = function( obj ) {
    if ( typeof obj == 'object' && obj != null ) {
        for ( var i in obj ) {
            if ( typeof this[i] != 'undefined' ) {
                this[i] = obj[i];
            }
        }
    }
};

Evernote.NoteSortOrder.prototype.setOrder = function( order ) {
    if ( typeof order == 'number' && !isNaN( order ) && Evernote.NoteSortOrder.isValidOrder( order ) ) {
        this._order = order;
    }
    else if ( typeof order == 'string' && Evernote.NoteSortOrder.isValidType( order ) ) {
        this._order = Evernote.NoteSortOrderTypes[order.toUpperCase()];
    }
    else {
        this._order = Evernote.NoteSortOrderTypes.CREATED;
    }
};

Evernote.NoteSortOrder.prototype.getOrder = function() {
    return this._order;
};

Evernote.NoteSortOrder.prototype.getType = function() {
    if ( this._order != null ) {
        for ( var t in Evernote.NoteSortOrderTypes ) {
            if ( Evernote.NoteSortOrderTypes[t] == this._order ) {
                return t;
            }
        }
    }
    
    return null;
};

Evernote.NoteSortOrder.prototype.setAscending = function( bool ) {
    this._ascending = (bool) ? true : false;
};

Evernote.NoteSortOrder.prototype.isAscending = function() {
    return this._ascending;
};

Evernote.NoteSortOrder.prototype.toJSON = function() {
    return {
        order : this.order,
        ascending : this.ascending
    };
};
