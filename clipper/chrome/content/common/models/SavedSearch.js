//"use strict";

Evernote.SavedSearch = function SavedSearch( obj ) {
    this.__defineGetter__( "query", this.getQuery );
    this.__defineSetter__( "query", this.setQuery );
    this.__defineGetter__( "format", this.getFormat );
    this.__defineSetter__( "format", this.setFormat );
    this.__defineGetter__( "name", this.getName );
    this.__defineSetter__( "name", this.setName );
    this.initialize( obj );
};

Evernote.SavedSearch.javaClass = "com.evernote.edam.type.SavedSearch";
Evernote.inherit( Evernote.SavedSearch, Evernote.AppDataModel, true );

Evernote.SavedSearch.prototype._query = null;
Evernote.SavedSearch.prototype._format = 1;
Evernote.SavedSearch.prototype._name = null;

Evernote.SavedSearch.prototype.getQuery = function() {
    return this._query;
};

Evernote.SavedSearch.prototype.setQuery = function( q ) {
    if ( q == null ) {
        this._query = null;
    }
    else {
        this._query = q + "";
    }
};

Evernote.SavedSearch.prototype.getFormat = function() {
    return this._format;
};

Evernote.SavedSearch.prototype.setFormat = function( f ) {
    this._format = Math.max( parseInt( f ), 1 );
};

Evernote.SavedSearch.prototype.setName = function( name ) {
    if ( name == null ) {
        this._name = null
    }
    else {
        this._name = name + "";
    }
};

Evernote.SavedSearch.prototype.getName = function() {
    return this._name;
};