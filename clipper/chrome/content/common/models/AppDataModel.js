//"use strict";

/**
 * My instances represent storable Models such as Notes, Notebooks, Tags,
 * SavedSearches etc
 */
Evernote.AppDataModel = function AppDataModel( obj ) {
    this.initialize( obj );
};

Evernote.inherit( Evernote.AppDataModel, Evernote.AppModel, true );

Evernote.AppDataModel.prototype.guid = null;
Evernote.AppDataModel.prototype.updateSequenceNum = -1;

Evernote.AppDataModel.prototype.getLabel = function() {
    if ( this.modelName ) {
        return this.modelName;
    }
    else {
        return null;
    }
};

Evernote.AppDataModel.prototype.equals = function( model ) {
    return model instanceof Evernote.AppDataModel && this.guid == model.guid && this.updateSequenceNum == model.updateSequenceNum;
};

Evernote.AppDataModel.prototype.isNew = function() {
    return typeof this.guid == 'string' && this.guid.length > 0;
};

Evernote.AppDataModel.prototype.toString = function() {
    return '[' + this.modelName + ':' + this.guid + ']';
};

Evernote.AppDataModel.toCSV = function( models ) {
    var modelNames = [ ];
    for ( var i = 0; i < models.length; i++ ) {
        if ( models[i] instanceof Evernote.AppDataModel ) {
            modelNames.push( models[i].modelName );
        }
    }
    return modelNames.join( ", " );
};

Evernote.AppDataModel.fromCSV = function( modelNames ) {
    var modelArray = [ ];
    var models = modelNames.split( ',' );

    for ( var i = 0; i < models.length; i++ ) {
        if ( models[i] instanceof Evernote.AppDataModel ) {
            var modelName = Evernote.AppDataModel.trim( models[i] );
            if ( modelName.length > 0 ) {
                modelArray.push( modelName );
            }
        }
    }

    return modelArray;
};

Evernote.AppDataModel.trim = function( modelName ) {
    return modelName.replace( /^\s*/, "" ).replace( /\s*$/, "" );
};

Evernote.AppDataModel._generateGUID = function() {
    return (((1 + Math.random()) * 0x10000) | 0).toString( 16 ).substring( 1 ).toUpperCase();
};