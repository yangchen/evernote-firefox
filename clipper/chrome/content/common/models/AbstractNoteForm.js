//"use strict";

Evernote.AbstractNoteForm = function AbstractNoteForm() {
};

Evernote.AbstractNoteForm.TITLE = "title";
Evernote.AbstractNoteForm.NOTEBOOK_GUID = "notebookGuid";
Evernote.AbstractNoteForm.TAG_NAMES = "tagNames";
Evernote.AbstractNoteForm.CONTENT = "content";
Evernote.AbstractNoteForm.COMMENT = "comment";

Evernote.inherit( Evernote.AbstractNoteForm, Evernote.jQuery );

Evernote.AbstractNoteForm.onForm = function( form, fieldNames ) {
    var props = { };
    for ( var i in this.prototype ) {
        props[i] = this.prototype[i];
    }

    var origForm = form.get( 0 );
    Evernote.jQuery.extend( true, form, props );
    form.form = Evernote.jQuery( origForm );
    
    form.__defineGetter__( "title", form.getTitle );
    form.__defineSetter__( "title", form.setTitle );
    form.__defineGetter__( "notebookGuid", form.getNotebookGuid );
    form.__defineSetter__( "notebookGuid", form.setNotebookGuid );
    form.__defineGetter__( "tagNames", form.getTagNames );
    form.__defineSetter__( "tagNames", form.setTagNames );
    form.__defineGetter__( "content", form.getContent );
    form.__defineSetter__( "content", form.setContent );
    form.__defineGetter__( "comment", form.getComment );
    form.__defineSetter__( "comment", form.setComment );

    if ( typeof fieldNames == 'object' && fieldNames != null ) {
        for ( i in fieldNames ) {
            if ( typeof this.prototype[i + "FieldName"] == 'string' ) {
                this[i + "FieldName"] = fieldNames[i];
            }
        }
    }

    return form;
};

Evernote.AbstractNoteForm.prototype.titleFieldName = Evernote.AbstractNoteForm.TITLE;
Evernote.AbstractNoteForm.prototype.notebookGuidFieldName = Evernote.AbstractNoteForm.NOTEBOOK_GUID;
Evernote.AbstractNoteForm.prototype.tagNamesFieldName = Evernote.AbstractNoteForm.TAG_NAMES;
Evernote.AbstractNoteForm.prototype.contentFieldName = Evernote.AbstractNoteForm.CONTENT;
Evernote.AbstractNoteForm.prototype.commentFieldName = Evernote.AbstractNoteForm.COMMENT;

Evernote.AbstractNoteForm.prototype.getField = function( fieldName ) {
    return this.form.find( "*[name=" + fieldName + "]" );
};

Evernote.AbstractNoteForm.prototype.getLabel = function( fieldName ) {
    return this.form.find( "label[for=" + fieldName + "]" );
};

Evernote.AbstractNoteForm.prototype.enableField = function( fieldName ) {
    var field = this.getField( fieldName );
    if ( field ) {
        if ( field.attr( "tagName" ).toLowerCase() == 'input' ) {
            field.removeAttr( "disabled" );
        }
        else {
            field.removeClass( "disabled" );
        }
        
        var label = this.getLabel( fieldName );
        if ( label && label.hasClass( "disabled" ) ) {
            label.removeClass( "disabled" );
        }
    }
};

Evernote.AbstractNoteForm.prototype.disableField = function( fieldName ) {
    var field = this.getField( fieldName );
    if ( field ) {
        if ( field.attr( "tagName" ).toLowerCase() == "input" ) {
            field.attr( "disabled", "disabled" );
        }
        else {
            field.addClass( "disabled" );
        }
        
        var label = this.getLabel( fieldName );
        if ( label && !(label.hasClass( "disabled" )) ) {
            label.addClass( "disabled" );
        }
    }
};

Evernote.AbstractNoteForm.prototype.isFieldEnabled = function( fieldName ) {
    var field = this.getField( fieldName );
    return field && !field.hasClass( "disabled" );
};

Evernote.AbstractNoteForm.prototype.getTitle = function() {
    return this.getField( this.titleFieldName ).val();
};

Evernote.AbstractNoteForm.prototype.setTitle = function( title ) {
    this.getField( this.titleFieldName ).val( title );
};

Evernote.AbstractNoteForm.prototype.getNotebookGuid = function() {
    return this.getField( this.notebookGuidFieldName ).val();
};

Evernote.AbstractNoteForm.prototype.setNotebookGuid = function( notebookGuid ) {
    this.getField( this.notebookGuidFieldName ).val( notebookGuid );
};

Evernote.AbstractNoteForm.prototype.getTagNames = function() {
    return this.getField( this.tagNamesFieldName ).val();
};

Evernote.AbstractNoteForm.prototype.setTagNames = function( tagNames ) {
    this.getField( this.tagNamesFieldName ).val( tagNames );
};

Evernote.AbstractNoteForm.prototype.getContent = function() {
    return this.getField( this.contentFieldName ).val();
};

Evernote.AbstractNoteForm.prototype.setContent = function( content ) {
    this.getField( this.contentFieldName ).val( content );
};

Evernote.AbstractNoteForm.prototype.getComment = function() {
    return this.getField( this.commentFieldName ).val();
};

Evernote.AbstractNoteForm.prototype.setComment = function( comment ) {
    this.getField( this.commentFieldName ).val( comment );
};

Evernote.AbstractNoteForm.prototype.reset = function() {
    var props = this.getStorableProps();
    for ( var i = 0; i < props.length; i++ ) {
        this[props[i]] = null;
    }
};

Evernote.AbstractNoteForm.prototype.populateWith = function( note ) {
    Evernote.logger.debug( "AbstractNoteForm.populateWith()" );

    this.reset();
    if ( note instanceof Evernote.AppModel ) {
        var props = note.toStorable();
        for ( var i in props ) {
            if ( typeof this[i] != 'undefined' ) {
                this[i] = note[i];
            }
        }
    }
};

Evernote.AbstractNoteForm.prototype.getStorableProps = function() {
    return [ "title", "notebookGuid", "tagNames", "content", "comment" ];
};

Evernote.AbstractNoteForm.prototype.toStorable = function() {
    var props = this.getStorableProps();
    var storable = { };
    for ( var i = 0; i < props.length; i++ ) {
        storable[props[i]] = this[props[i]];
    }
    
    return storable;
};

Evernote.AbstractNoteForm.prototype.getModelName = function() {
    return "Evernote.AbstractNoteForm";
};

Evernote.AbstractNoteForm.prototype.getStringDescription = function() {
    return "'" + this.title + "'; NotebookGuid: " + this.notebookGuid + "; TagNames: " + this.tagNames
           + "; Content length: " + ((typeof this.content == 'string') ? this.content.length : 0)
           + "; Comment length: " + ((typeof this.comment == 'string') ? this.comment.length : 0);
};

Evernote.AbstractNoteForm.prototype.toString = function() {
    return this.getModelName() + " " + this.getStringDescription();
};

Evernote.AbstractNoteForm.prototype.populateWithNote = function( context, note ) {
    Evernote.logger.debug( "AbstractNoteForm.populateWithNote()" );

    if ( note instanceof Evernote.AppModel ) {
        var props = note.toStorable();
        for ( var i in props ) {
            if ( typeof this[i] != 'undefined' && typeof note[i] != 'undefined' && note[i] != null ) {
                this[i] = note[i];
            }
        }
    }
};