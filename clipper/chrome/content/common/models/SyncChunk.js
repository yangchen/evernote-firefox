//"use strict";

Evernote.SyncChunk = function SyncChunk( obj ) {
    this.__defineGetter__( "chunkHighUSN", this.getChunkHighUSN );
    this.__defineSetter__( "chunkHighUSN", this.setChunkHighUSN );
    this.__defineGetter__( "currentTime", this.getCurrentTime );
    this.__defineSetter__( "currentTime", this.setCurrentTime );
    this.__defineGetter__( "expungedNotebooks", this.getExpungedNotebooks );
    this.__defineSetter__( "expungedNotebooks", this.setExpungedNotebooks );
    this.__defineGetter__( "expungedNotes", this.getExpungedNotes );
    this.__defineSetter__( "expungedNotes", this.setExpungedNotes );
    this.__defineGetter__( "expungedSearches", this.getExpungedSearches );
    this.__defineSetter__( "expungedSearches", this.setExpungedSearches );
    this.__defineGetter__( "expungedTags", this.getExpungedTags );
    this.__defineSetter__( "expungedTags", this.setExpungedTags );
    this.__defineGetter__( "notebooks", this.getNotebooks );
    this.__defineSetter__( "notebooks", this.setNotebooks );
    this.__defineGetter__( "notes", this.getNotes );
    this.__defineSetter__( "notes", this.setNotes );
    this.__defineGetter__( "searches", this.getSearches );
    this.__defineSetter__( "searches", this.setSearches );
    this.__defineGetter__( "tags", this.getTags );
    this.__defineSetter__( "tags", this.setTags );
    this.__defineGetter__( "updateCount", this.getUpdateCount );
    this.__defineSetter__( "updateCount", this.setUpdateCount );
    this.initialize( obj );
};

Evernote.SyncChunk.javaClass = "com.evernote.edam.notestore.SyncChunk";
Evernote.inherit( Evernote.SyncChunk, Evernote.AppModel, true );

Evernote.SyncChunk.prototype._chunkHighUSN = null;
Evernote.SyncChunk.prototype._currentTime = null;
Evernote.SyncChunk.prototype._expungedNotebooks = [ ];
Evernote.SyncChunk.prototype._expungedNotes = [ ];
Evernote.SyncChunk.prototype._expungedSearches = [ ];
Evernote.SyncChunk.prototype._expungedTags = [ ];
Evernote.SyncChunk.prototype._notebooks = [ ];
Evernote.SyncChunk.prototype._notes = [ ];
Evernote.SyncChunk.prototype._searches = [ ];
Evernote.SyncChunk.prototype._tags = [ ];
Evernote.SyncChunk.prototype._updateCount = null;

Evernote.SyncChunk.prototype.getChunkHighUSN = function() {
    return this._chunkHighUSN;
};

Evernote.SyncChunk.prototype.setChunkHighUSN = function( num ) {
    this._chunkHighUSN = parseInt( num );
};

Evernote.SyncChunk.prototype.getCurrentTime = function() {
    return this._currentTime;
};

Evernote.SyncChunk.prototype.setCurrentTime = function( millis ) {
    this._currentTime = parseInt( millis );
};

Evernote.SyncChunk.prototype.getExpungedNotebooks = function() {
    return this._expungedNotebooks;
};

Evernote.SyncChunk.prototype.setExpungedNotebooks = function( notebooks ) {
    if ( notebooks == null ) {
        this._expungedNotebooks = [ ];
    }
    else {
        this._expungedNotebooks = (notebooks instanceof Array) ? notebooks : [ notebooks ];
    }
};

Evernote.SyncChunk.prototype.getExpungedNotes = function() {
    return this._expungedNotes;
};

Evernote.SyncChunk.prototype.setExpungedNotes = function( notes ) {
    if ( notes == null ) {
        this._expungedNotes = [ ];
    }
    else {
        this._expungedNotes = (notes instanceof Array) ? notes : [ notes ];
    }
};

Evernote.SyncChunk.prototype.getExpungedSearches = function() {
    return this._expungedSearches;
};

Evernote.SyncChunk.prototype.setExpungedSearches = function( searches ) {
    if ( searches == null ) {
        this._expungedSearches = [ ];
    }
    else {
        this._expungedSearches = (searches instanceof Array) ? searches : [ searches ];
    }
};

Evernote.SyncChunk.prototype.getExpungedTags = function() {
    return this._expungedTags;
};

Evernote.SyncChunk.prototype.setExpungedTags = function( tags ) {
    if ( tags == null ) {
        this._expungedTags = [ ];
    }
    else {
        this._expungedTags = (tags instanceof Array) ? tags : [ tags ];
    }
};

Evernote.SyncChunk.prototype.getNotebooks = function() {
    return this._notebooks;
};

Evernote.SyncChunk.prototype.setNotebooks = function( notebooks ) {
    if ( notebooks == null ) {
        this._notebooks = [ ];
    }
    else {
        this._notebooks = (notebooks instanceof Array) ? notebooks : [ notebooks ];
    }
};

Evernote.SyncChunk.prototype.getNotes = function() {
    return this._notes;
};

Evernote.SyncChunk.prototype.setNotes = function( notes ) {
    if ( notes == null ) {
        this._notes = [ ];
    }
    else {
        this._notes = (notes instanceof Array) ? notes : [ notes ];
    }
};

Evernote.SyncChunk.prototype.getSearches = function() {
    return this._searches;
};

Evernote.SyncChunk.prototype.setSearches = function( searches ) {
    if ( searches == null ) {
        this._searches = [ ];
    }
    else {
        this._searches = (searches instanceof Array) ? searches : [ ];
    }
};

Evernote.SyncChunk.prototype.getTags = function() {
    return this._tags;
};

Evernote.SyncChunk.prototype.setTags = function( tags ) {
    if ( tags == null ) {
        this._tags = [ ];
    }
    else {
        this._tags = (tags instanceof Array) ? tags : [ tags ];
    }
};

Evernote.SyncChunk.prototype.getUpdateCount = function() {
    return this._updateCount;
};

Evernote.SyncChunk.prototype.setUpdateCount = function( num ) {
    this._updateCount = parseInt( num );
};

Evernote.SyncChunk.prototype.setResources = function( /*resources*/ ) {
};

Evernote.SyncChunk.prototype.toString = function() {
    return "Evernote.SyncChunk " + this.updateCount;
};