//"use strict";

Evernote.NotesMetadataResultSpec = function NotesMetadataResultSpec( obj ) {
    this.__defineGetter__( "includeTitle", this.isIncludeTitle );
    this.__defineSetter__( "includeTitle", this.setIncludeTitle );
    this.__defineGetter__( "includeContentLength", this.isIncludeContentLength );
    this.__defineSetter__( "includeContentLength", this.setIncludeContentLength );
    this.__defineGetter__( "includeCreated", this.isIncludeCreated );
    this.__defineSetter__( "includeCreated", this.setIncludeCreated );
    this.__defineGetter__( "includeUpdated", this.isIncludeUpdated );
    this.__defineSetter__( "includeUpdated", this.setIncludeUpdated );
    this.__defineGetter__( "includeUpdateSequenceNum", this.isIncludeUpdateSequenceNum );
    this.__defineSetter__( "includeUpdateSequenceNum", this.setIncludeUpdateSequenceNum );
    this.__defineGetter__( "includeNotebookGuid", this.isIncludeNotebookGuid );
    this.__defineSetter__( "includeNotebookGuid", this.setIncludeNotebookGuid );
    this.__defineGetter__( "includeTagGuids", this.isIncludeTagGuids );
    this.__defineSetter__( "includeTagGuids", this.setIncludeTagGuids );
    this.__defineGetter__( "includeAttributes", this.isIncludeAttributes );
    this.__defineSetter__( "includeAttributes", this.setIncludeAttributes );
    this.__defineGetter__( "includeLargestResourceMime", this.isIncludeLargestResourceMime );
    this.__defineSetter__( "includeLargestResourceMime", this.setIncludeLargestResourceMime );
    this.__defineGetter__( "includeLargestResourceSize", this.isIncludeLargestResourceSize );
    this.__defineSetter__( "includeLargestResourceSize", this.setIncludeLargestResourceSize );
    this.initialize( obj );
};

Evernote.NotesMetadataResultSpec.javaClass = "com.evernote.edam.notestore.NotesMetadataResultSpec";
Evernote.inherit( Evernote.NotesMetadataResultSpec, Evernote.AppModel );

Evernote.NotesMetadataResultSpec.prototype._includeTitle = false;
Evernote.NotesMetadataResultSpec.prototype._includeContentLength = false;
Evernote.NotesMetadataResultSpec.prototype._includeCreated = false;
Evernote.NotesMetadataResultSpec.prototype._includeUpdated = false;
Evernote.NotesMetadataResultSpec.prototype._includeUpdateSequenceNum = false;
Evernote.NotesMetadataResultSpec.prototype._includeNotebookGuid = false;
Evernote.NotesMetadataResultSpec.prototype._includeTagGuids = false;
Evernote.NotesMetadataResultSpec.prototype._includeAttributes = false;
Evernote.NotesMetadataResultSpec.prototype._includeLargestResourceMime = false;
Evernote.NotesMetadataResultSpec.prototype._includeLargestResourceSize = false;

Evernote.NotesMetadataResultSpec.prototype.isIncludeTitle = function() {
    return this._includeTitle;
};

Evernote.NotesMetadataResultSpec.prototype.setIncludeTitle = function( bool ) {
    this._includeTitle = (bool) ? true : false;
};

Evernote.NotesMetadataResultSpec.prototype.isIncludeContentLength = function() {
    return this._includeContentLength;
};

Evernote.NotesMetadataResultSpec.prototype.setIncludeContentLength = function( bool ) {
    this._includeContentLength = (bool) ? true : false;
};

Evernote.NotesMetadataResultSpec.prototype.isIncludeCreated = function() {
    return this._includeCreated;
};

Evernote.NotesMetadataResultSpec.prototype.setIncludeCreated = function( bool ) {
    this._includeCreated = (bool) ? true : false;
};

Evernote.NotesMetadataResultSpec.prototype.isIncludeUpdated = function() {
    return this._includeUpdated;
};

Evernote.NotesMetadataResultSpec.prototype.setIncludeUpdated = function( bool ) {
    this._includeUpdated = (bool) ? true : false;
};

Evernote.NotesMetadataResultSpec.prototype.isIncludeUpdateSequenceNum = function() {
    return this._includeUpdateSequenceNum;
};

Evernote.NotesMetadataResultSpec.prototype.setIncludeUpdateSequenceNum = function( bool ) {
    this._includeUpdateSequenceNum = (bool) ? true : false;
};

Evernote.NotesMetadataResultSpec.prototype.isIncludeNotebookGuid = function() {
    return this._includeNotebookGuid;
};

Evernote.NotesMetadataResultSpec.prototype.setIncludeNotebookGuid = function( bool ) {
    this._includeNotebookGuid = (bool) ? true : false;
};

Evernote.NotesMetadataResultSpec.prototype.isIncludeTagGuids = function() {
    return this._includeTagGuids;
};

Evernote.NotesMetadataResultSpec.prototype.setIncludeTagGuids = function( bool ) {
    this._includeTagGuids = (bool) ? true : false;
};

Evernote.NotesMetadataResultSpec.prototype.isIncludeAttributes = function() {
    return this._includeAttributes;
};

Evernote.NotesMetadataResultSpec.prototype.setIncludeAttributes = function( bool ) {
    this._includeAttributes = (bool) ? true : false;
};

Evernote.NotesMetadataResultSpec.prototype.isIncludeLargestResourceMime = function() {
    return this._includeLargestResourceMime;
};

Evernote.NotesMetadataResultSpec.prototype.setIncludeLargestResourceMime = function( bool ) {
    this._includeLargestResourceMime = (bool) ? true : false;
};

Evernote.NotesMetadataResultSpec.prototype.isIncludeLargestResourceSize = function() {
    return this._includeLargestResourceSize;
};

Evernote.NotesMetadataResultSpec.prototype.setIncludeLargestResourceSize = function( bool ) {
    this._includeLargestResourceSize = (bool) ? true : false;
};