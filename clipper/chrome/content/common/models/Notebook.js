//"use strict";

Evernote.Notebook = function Notebook( obj ) {
    this.__defineGetter__( "defaultNotebook", this.isDefaultNotebook );
    this.__defineSetter__( "defaultNotebook", this.setDefaultNotebook );
    this.__defineGetter__( "name", this.getName );
    this.__defineSetter__( "name", this.setName );
    this.__defineGetter__( "publishing", this.getPublishing );
    this.__defineSetter__( "publishing", this.setPublishing );
    this.initialize( obj );
};

Evernote.Notebook.javaClass = "com.evernote.edam.type.Notebook";
Evernote.inherit( Evernote.Notebook, Evernote.AppDataModel, true );

Evernote.Notebook.prototype._name = null;
Evernote.Notebook.prototype._defaultNotebook = false;
Evernote.Notebook.prototype._publishing = null;

Evernote.Notebook.prototype.setName = function( notebookName ) {
    if ( typeof notebookName == 'string' ) {
        this._name = notebookName;
    }
    else if ( notebookName == null ) {
        this._name = null;
    }
};

Evernote.Notebook.prototype.getName = function() {
    return this._name;
};

Evernote.Notebook.prototype.setDefaultNotebook = function( bool ) {
    this._defaultNotebook = (bool) ? true : false;
};

Evernote.Notebook.prototype.isDefaultNotebook = function() {
    return this._defaultNotebook;
};

Evernote.Notebook.prototype.setPublishing = function( /*publishing*/ ) {
};

Evernote.Notebook.prototype.getPublishing = function() {
    return this._publishing;
};

Evernote.Notebook.prototype.toString = function() {
    return '[' + this.modelName + ':' + this.guid + ':' + this.name + ']';
};