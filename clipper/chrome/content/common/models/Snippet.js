//"use strict";

Evernote.Snippet = function Snippet( obj ) {
    this.initialize( obj );
};

Evernote.Snippet.javaClass = "com.evernote.web.Snippet";
Evernote.inherit( Evernote.Snippet, Evernote.AppDataModel, true );

Evernote.Snippet.prototype.snippet = null;