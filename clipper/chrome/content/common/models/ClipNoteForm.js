//"use strict";

Evernote.ClipNoteForm = function ClipNoteForm( /*form*/ ) {
};

Evernote.ClipNoteForm.SAVE_URL = "saveUrl";
Evernote.ClipNoteForm.FULL_PAGE = "fullPage";
Evernote.ClipNoteForm.URL = "url";

Evernote.inherit( Evernote.ClipNoteForm, Evernote.AbstractNoteForm );

Evernote.ClipNoteForm.onForm = function( form, fieldNames ) {
    form = this.constructor.parent.onForm.apply( this, [ form, fieldNames ] );
    form.__defineGetter__( "url", form.getUrl );
    form.__defineSetter__( "url", form.setUrl );
    form.__defineGetter__( "saveUrl", form.isSaveUrl );
    form.__defineSetter__( "saveUrl", form.setSaveUrl );
    form.__defineGetter__( "fullPage", form.isFullPage );
    form.__defineSetter__( "fullPage", form.setFullPage );
    form.__defineGetter__( "saveUrlEnabled", form.isSaveUrlEnabled );
    form.__defineSetter__( "saveUrlEnabled", form.setSaveUrlEnabled );
    form.__defineGetter__( "fullPageEnabled", form.isFullPageEnabled );
    form.__defineSetter__( "fullPageEnabled", form.setFullPageEnabled );

    form.getField( form.saveUrlFieldName ).change( function() {
        form.setSaveUrl( Evernote.jQuery( this ).attr( "checked" ) );
    } );
    form.getField( form.fullPageFieldName ).change( function() {
        form.setFullPage( Evernote.jQuery( this ).attr( "checked" ) );
    } );

    return form;
};

Evernote.ClipNoteForm.prototype.saveUrlFieldName = Evernote.ClipNoteForm.SAVE_URL;
Evernote.ClipNoteForm.prototype.fullPageFieldName = Evernote.ClipNoteForm.FULL_PAGE;
Evernote.ClipNoteForm.prototype.urlFieldName = Evernote.ClipNoteForm.URL;

Evernote.ClipNoteForm.prototype.isSaveUrl = function() {
    return this.getField( this.saveUrlFieldName ).attr( "checked" );
};

Evernote.ClipNoteForm.prototype.setSaveUrl = function( bool ) {
    if ( bool ) {
        this.getField( this.saveUrlFieldName ).attr( "checked", "checked" );
    }
    else {
        this.getField( this.saveUrlFieldName ).removeAttr( "checked" );
        this.setFullPage( false );
    }
};

Evernote.ClipNoteForm.prototype.setSaveUrlEnabled = function( bool ) {
    if ( bool ) {
        this.enableSaveUrl();
    }
    else {
        this.disableSaveUrl();
    }
};

Evernote.ClipNoteForm.prototype.isSaveUrlEnabled = function() {
    return this.isFieldEnabled( this.saveUrlFieldName );
};

Evernote.ClipNoteForm.prototype.enableSaveUrl = function() {
    this.enableField( this.saveUrlFieldName );
};

Evernote.ClipNoteForm.prototype.disableSaveUrl = function() {
    this.disableField( this.saveUrlFieldName );
};

Evernote.ClipNoteForm.prototype.isFullPage = function() {
    return this.getField( this.fullPageFieldName ).attr( "checked" );
};

Evernote.ClipNoteForm.prototype.setFullPage = function( bool ) {
    if ( bool ) {
        this.getField( this.fullPageFieldName ).attr( "checked", "checked" );
        this.setSaveUrl( true );
    }
    else {
        this.getField( this.fullPageFieldName ).removeAttr( "checked" );
    }
};

Evernote.ClipNoteForm.prototype.disableFullPage = function() {
    this.disableField( this.fullPageFieldName );
};

Evernote.ClipNoteForm.prototype.enableFullPage = function() {
    this.enableField( this.fullPageFieldName );
};

Evernote.ClipNoteForm.prototype.isFullPageEnabled = function() {
    return this.isFieldEnabled( this.fullPageFieldName );
};

Evernote.ClipNoteForm.prototype.setFullPageEnabled = function( bool ) {
    if ( bool ) {
        this.enableFullPage();
    }
    else {
        this.disableFullPage();
    }
};

Evernote.ClipNoteForm.prototype.getUrl = function() {
    return this.getField( this.urlFieldName ).val();
};

Evernote.ClipNoteForm.prototype.setUrl = function( url ) {
    this.getField( this.urlFieldName ).val( url );
};

Evernote.ClipNoteForm.prototype.getStorableProps = function() {
    var props = this.parent.getStorableProps();
    props.push( "saveUrl" );
    props.push( "fullPage" );
    props.push( "url" );

    return props;
};

Evernote.ClipNoteForm.prototype.populateWith = function( context, clipNote ) {
    Evernote.logger.debug( "ClipNoteForm.populateWith()" );

    if ( clipNote instanceof Evernote.ClipNote ) {
        this.parent.populateWith.apply( this, [ clipNote ] );
        Evernote.logger.debug( "Setting fullPage: " + clipNote.fullPage );
        this.setFullPage( clipNote.fullPage );
        Evernote.logger.debug( "Setting saveUrl: " + clipNote.saveUrl );
        this.setSaveUrl( clipNote.saveUrl );
    }
};

Evernote.ClipNoteForm.prototype.populateWithNote = function( context, clipNote ) {
    Evernote.logger.debug( "ClipNoteForm.populateWithNote()" );

    if ( clipNote instanceof Evernote.ClipNote ) {
        this.parent.populateWithNote.apply( this, [ context, clipNote ] );
        if ( clipNote.notebookGuid ) {
            this.notebookGuid = clipNote.notebookGuid;
        }
    }
};

Evernote.ClipNoteForm.prototype.asClipNote = function() {
    Evernote.logger.debug( "ClipNoteForm.asClipNote()" );

    var clipNote = new Evernote.ClipNote( this.toStorable() );
    Evernote.logger.debug( "CLIPNOTE: " + clipNote.toString() );
    return clipNote;
};

Evernote.ClipNoteForm.prototype.getModelName = function() {
    return "Evernote.ClipNoteForm";
};

Evernote.ClipNoteForm.prototype.getStringDescription = function() {
    var superStr = this.parent.getStringDescription();
    superStr += "; URL: " + this.url + "; SaveUrl: " + this.saveUrl + "; FullPage: " + this.fullPage;
    return superStr;
};