//"use strict";

Evernote.SnippetNoteMetadata = function ( obj ) {
    this.snippet = "";
    this.initialize( obj );
};

Evernote.SnippetNoteMetadata.javaClass = "com.evernote.chromeExtension.SnippetNoteMetadata";
Evernote.inherit( Evernote.SnippetNoteMetadata, Evernote.NoteMetadata, true );

Evernote.SnippetNoteMetadata.prototype.contentLength = null;
Evernote.SnippetNoteMetadata.prototype.attributes = null;
Evernote.SnippetNoteMetadata.prototype.largestResourceMime = null;
Evernote.SnippetNoteMetadata.prototype.largestResourceSize = null;