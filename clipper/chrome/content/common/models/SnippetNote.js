//"use strict";

Evernote.SnippetNote = function ( obj ) {
    this.__defineGetter__( "snippet", this.getSnippet );
    this.__defineSetter__( "snippet", this.setSnippet );
    this.initialize( obj );
};

Evernote.SnippetNote.javaClass = "com.evernote.web.SnippetNote";
Evernote.inherit( Evernote.SnippetNote, Evernote.Note, true );

Evernote.SnippetNote.prototype._snippet = null;

Evernote.SnippetNote.prototype.setSnippet = function( snippetText ) {
    if ( typeof snippetText == 'string' || snippetText == null ) {
        this._snippet = snippetText;
    }
};

Evernote.SnippetNote.prototype.getSnippet = function() {
    return this._snippet;
};