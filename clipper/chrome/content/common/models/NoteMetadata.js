//"use strict";

Evernote.NoteMetadata = function NoteMetadata( obj ) {
    this.attributes = new Evernote.NoteAttributes();
    this.contentLength = 0;
    this.largestResourceMime = "";
    this.largestResourceSize = 0;
    this.initialize( obj );
};

Evernote.NoteMetadata.javaClass = "com.evernote.edam.notestore.NoteMetadata";
Evernote.inherit( Evernote.NoteMetadata, Evernote.BasicNote, true );

Evernote.NoteMetadata.prototype.contentLength = null;
Evernote.NoteMetadata.prototype.attributes = null;
Evernote.NoteMetadata.prototype.largestResourceMime = null;
Evernote.NoteMetadata.prototype.largestResourceSize = null;