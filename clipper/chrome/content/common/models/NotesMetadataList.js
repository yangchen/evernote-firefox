//"use strict";

Evernote.NotesMetadataList = function NotesMetadataList( obj ) {
    this.initialize( obj );
};

Evernote.NotesMetadataList.javaClass = "com.evernote.edam.notestore.NotesMetadataList";
Evernote.inherit( Evernote.NotesMetadataList, Evernote.NoteList, true );