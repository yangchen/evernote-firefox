//"use strict";

Evernote.BasicNote = function BasicNote( obj ) {
    this.__defineGetter__( "title", this.getTitle );
    this.__defineSetter__( "title", this.setTitle );
    this.notebookGuid = "";
    this.created = 0;
    this.updated = 0;
    this.tagNames = [ ];
    this.initialize( obj );
};

Evernote.BasicNote.javaClass = "com.evernote.chromeExtension.BasicNote";
Evernote.inherit( Evernote.BasicNote, Evernote.AppDataModel, true );

Evernote.BasicNote.prototype.NOTE_URL_PREFIX = "/view/";
Evernote.BasicNote.prototype.DEFAULT_LOCALE = "default";

Evernote.BasicNote.prototype.initialize = function( obj ) {
    this.tagNames = [ ];
    this.tagGuids = [ ];
    Evernote.BasicNote.prototype.parent.initialize.apply( this, [ obj ] );
};

Evernote.BasicNote.prototype.cleanTitle = function( title ) {
    if ( typeof title == 'string' ) {
        return title.replace( /[\n\r\t]+/, "" ).replace( /^\s+/, "" ).replace( /\s+$/, "" );
    }

    return title;
};

Evernote.BasicNote.prototype.getTitle = function() {
    return this._title;
};

Evernote.BasicNote.prototype.setTitle = function( title, defaultTitle ) {
    var newTitle = this.cleanTitle( "" + title );
    if ( (newTitle == null || newTitle.length == 0) && defaultTitle ) {
        newTitle = this.cleanTitle( "" + defaultTitle );
    }

    this._title = newTitle;
};

Evernote.BasicNote.prototype.addTag = function( tag ) {
    if ( tag instanceof Evernote.Tag ) {
        if ( tag.name ) {
            this.tagNames.push( tag.name );
        }
        if ( tag.guid ) {
            this.tagGuids.push( tag.guid );
        }
    }
};

Evernote.BasicNote.prototype.getThumbnailUrl = function( shardUrl, size ) {
    // /shard/s1/thm/note/f67f2848-f49d-493a-b94a-0275dac7f8c6
    shardUrl = shardUrl || "";
    var url = shardUrl + "/thm/note/" + this.guid;
    if ( typeof size == 'number' && size > 0 ) {
        url += "?size=" + size;
    }
    
    return url;
};

Evernote.BasicNote.prototype.getNoteUrl = function( shardUrl, shardId, searchWords, locale ) {
    // /view.jsp?locale=default&shard=s1#v=t&n=8d3c87f9-6df9-4475-8ebc-45f336cf9757&b=0
    locale = locale || this.DEFAULT_LOCALE;
    shardUrl = shardUrl || "";
    return shardUrl.replace( /\/$/, "" ) + this.NOTE_URL_PREFIX + this.guid + "?locale=" + locale
           + "&shard=" + shardId + "#v=t" + "&b=" + ((this.notebookGuid) ? this.notebookGuid : 0)
           + ((typeof searchWords == 'string' && searchWords.length > 0) ? ("&x=" + encodeURI( searchWords )) : "" );
};

Evernote.BasicNote.prototype.getStorableProps = function() {
    return [ "title", "notebookGuid", "tagNames", "created", "updated" ];
};

Evernote.BasicNote.prototype.toJSON = function() {
    return {
        title : this.title,
        notebookGuid : this.notebookGuid,
        tagNames : this.tagNames,
        created : this.created,
        updated : this.updated
    };
};