//"use strict";

Evernote.SyncState = function SyncState( obj ) {
    this.__defineGetter__( "currentTime", this.getCurrentTime );
    this.__defineSetter__( "currentTime", this.setCurrentTime );
    this.__defineGetter__( "fullSyncBefore", this.getFullSyncBefore );
    this.__defineSetter__( "fullSyncBefore", this.setFullSyncBefore );
    this.__defineGetter__( "updateCount", this.getUpdateCount );
    this.__defineSetter__( "updateCount", this.setUpdateCount );
    this.__defineGetter__( "uploaded", this.getUploaded );
    this.__defineSetter__( "uploaded", this.setUploaded );
    this.initialize( obj );
};

Evernote.SyncState.javaClass = "com.evernote.edam.notestore.SyncState";
Evernote.inherit( Evernote.SyncState, Evernote.AppModel, true );

Evernote.SyncState.prototype._currentTime = null;
Evernote.SyncState.prototype._fullSyncBefore = null;
Evernote.SyncState.prototype._updateCount = null;
Evernote.SyncState.prototype._uploaded = null;

Evernote.SyncState.prototype.getUpdateCount = function() {
    return this._updateCount;
};

Evernote.SyncState.prototype.setUpdateCount = function( num ) {
    if ( num == null ) {
        this._updateCount = null;
    }
    else {
        this._updateCount = parseInt( num );
    }
};

Evernote.SyncState.prototype.getFullSyncBefore = function() {
    return this._fullSyncBefore;
};

Evernote.SyncState.prototype.setFullSyncBefore = function( num ) {
    if ( num == null ) {
        this._fullSyncBefore = null;
    }
    else {
        this._fullSyncBefore = parseInt( num );
    }
};

Evernote.SyncState.prototype.getCurrentTime = function() {
    return this._currentTime;
};

Evernote.SyncState.prototype.setCurrentTime = function( num ) {
    if ( num == null ) {
        this._currentTime = null;
    }
    else {
        this._currentTime = parseInt( num );
    }
};

Evernote.SyncState.prototype.getUploaded = function() {
    return this._uploaded;
};

Evernote.SyncState.prototype.setUploaded = function( num ) {
    if ( num == null ) {
        this._uploaded = null;
    }
    else {
        this._uploaded = parseInt( num );
    }
};

Evernote.SyncState.prototype.isFullSyncRequired = function() {
    return this.currentTime != null && this.fullSyncBefore != null && this.currentTime < this.fullSyncBefore;
};

Evernote.SyncState.prototype.toString = function() {
    return "Evernote.SyncState " + this.updateCount;
};
