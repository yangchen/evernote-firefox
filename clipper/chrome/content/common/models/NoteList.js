//"use strict";

Evernote.NoteList = function NoteList( obj ) {
    this.__defineGetter__( "notes", this.getNotes );
    this.__defineSetter__( "notes", this.setNotes );
    this.__defineGetter__( "searchedWords", this.getSearchedWords );
    this.__defineSetter__( "searchedWords", this.setSearchedWords );
    this.__defineGetter__( "stoppedWords", this.getStoppedWords );
    this.__defineSetter__( "stoppedWords", this.setStoppedWords );
    this.__defineGetter__( "startIndex", this.getStartIndex );
    this.__defineSetter__( "startIndex", this.setStartIndex );
    this.__defineGetter__( "endIndex", this.getEndIndex );
    this.__defineGetter__( "nextIndex", this.getNextIndex );
    this.__defineGetter__( "remainingCount", this.getRemainingCount );
    this.__defineGetter__( "totalNotes", this.getTotalNotes );
    this.__defineSetter__( "totalNotes", this.setTotalNotes );
    this.initialize( obj );
};

Evernote.NoteList.javaClass = "com.evernote.edam.notestore.NoteList";
Evernote.inherit( Evernote.NoteList, Evernote.AppModel, true );

Evernote.NoteList.prototype._notes = [ ];
Evernote.NoteList.prototype._searchedWords = [ ];
Evernote.NoteList.prototype._stoppedWords = [ ];
Evernote.NoteList.prototype._startIndex = 0;
Evernote.NoteList.prototype._totalNotes = 0;

Evernote.NoteList.prototype.setNotes = function( notes ) {
    if ( notes instanceof Array ) {
        this._notes = notes;
    }
    else if ( notes instanceof Evernote.Note ) {
        this._notes = [ notes ];
    }
    else if ( notes == null ) {
        this._notes = [ ];
    }
};

Evernote.NoteList.prototype.getNotes = function() {
    return this._notes;
};

Evernote.NoteList.prototype.setSearchedWords = function( words ) {
    if ( words instanceof Array ) {
        this._searchedWords = words;
    }
    else if ( words != null ) {
        this._searchedWords = new Array( words + "" );
    }
    else if ( words == null ) {
        this._searchedWords = [ ];
    }
};

Evernote.NoteList.prototype.getSearchedWords = function() {
    return this._searchedWords;
};

Evernote.NoteList.prototype.setStoppedWords = function( words ) {
    if ( words instanceof Array ) {
        this._stoppedWords = words;
    }
    else if ( words != null ) {
        this._stoppedWords = new Array( words + "" );
    }
    else if ( words == null ) {
        this._stoppedWords = [ ];
    }
};

Evernote.NoteList.prototype.getStoppedWords = function() {
    return this._stoppedWords;
};

Evernote.NoteList.prototype.setStartIndex = function( index ) {
    this._startIndex = parseInt( index );
};

Evernote.NoteList.prototype.getStartIndex = function() {
    return this._startIndex;
};

Evernote.NoteList.prototype.getEndIndex = function() {
    return this.startIndex + this.notes.length - 1;
};

Evernote.NoteList.prototype.getNextIndex = function() {
    if ( this.isAtEnd() ) {
        return null;
    }
    
    return this.startIndex + this.notes.length;
};

Evernote.NoteList.prototype.getRemainingCount = function() {
    return this.totalNotes - this.nextIndex;
};

Evernote.NoteList.prototype.setTotalNotes = function( count ) {
    this._totalNotes = parseInt( count );
};

Evernote.NoteList.prototype.getTotalNotes = function() {
    return this._totalNotes;
};

Evernote.NoteList.prototype.isAtEnd = function() {
    return this.totalNotes <= this.startIndex + this.notes.length;
};

Evernote.NoteList.prototype.isAtStart = function() {
    return this.startIndex == 0;
};

Evernote.NoteList.prototype.getStorableProps = function() {
    return [ "notes", "searchedWords", "stoppedWords", "startIndex", "totalNotes" ];
};