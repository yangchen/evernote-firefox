//"use strict";

Evernote.LinkedNotebook = function LinkedNotebook( obj ) {
    this.__defineGetter__( "name", this.getName );
    this.__defineSetter__( "name", this.setName );
    this.__defineGetter__( "owner", this.getOwner );
    this.__defineSetter__( "owner", this.setOwner );
    this.__defineGetter__( "auth", this.getAuth );
    this.__defineSetter__( "auth", this.setAuth );
    this.__defineGetter__( "guid", this.getGuid );
    this.__defineSetter__( "guid", this.setGuid );
    this.__defineGetter__( "shareKey", this.getShareKey );
    this.__defineSetter__( "shareKey", this.setShareKey );
    this.initialize( obj );
};

Evernote.inherit( Evernote.LinkedNotebook, Evernote.AppDataModel, true );

Evernote.LinkedNotebook.prototype._name = null;
Evernote.LinkedNotebook.prototype._owner = null;
Evernote.LinkedNotebook.prototype._auth = null;
Evernote.LinkedNotebook.prototype._guid = null;
Evernote.LinkedNotebook.prototype._shareKey = null;

Evernote.LinkedNotebook.prototype.initialize = function( obj ) {
    if ( obj != null && typeof obj == 'object' ) {
        for ( var i in obj ) {
            var methName = "set" + (i.substring( 0, 1 ).toUpperCase()) + i.substring( 1 );
            if ( typeof this[methName] == 'function' ) {
                this[methName].apply( this, [ obj[i] ] );
            }
            else if ( typeof this[i] != 'undefined' && i != "modelName" ) {
                this[i] = obj[i];
            }
        }

        if ( this._guid && !this._guid.match( /^shared_/ ) ) {
            this._guid = "shared_" + this._guid;
        }
    }
};

Evernote.LinkedNotebook.prototype.setName = function( name ) {
    if ( typeof name == 'string' ) {
        this._name = name;
    }
    else if ( name == null ) {
        this._name = null;
    }
};

Evernote.LinkedNotebook.prototype.getName = function() {
    return this._name;
};

Evernote.LinkedNotebook.prototype.setOwner = function( owner ) {
    if ( typeof owner == 'string' ) {
        this._owner = owner;
    }
    else if ( owner == null ) {
        this._owner = null;
    }
};

Evernote.LinkedNotebook.prototype.getOwner = function() {
    return this._owner;
};

Evernote.LinkedNotebook.prototype.setAuth = function( auth ) {
    if ( typeof auth == 'string' ) {
        this._auth = auth;
    }
    else if ( auth == null ) {
        this._auth = null;
    }
};

Evernote.LinkedNotebook.prototype.getAuth = function() {
    return this._auth;
};

Evernote.LinkedNotebook.prototype.setGuid = function( guid ) {
    if ( typeof guid == 'string' ) {
        this._guid = guid;
    }
    else if ( guid == null ) {
        this._guid = null;
    }
};

Evernote.LinkedNotebook.prototype.getGuid = function() {
    return this._guid;
};

Evernote.LinkedNotebook.prototype.setShareKey = function( shareKey ) {
    if ( typeof shareKey == 'string' ) {
        this._shareKey = shareKey;
    }
    else if ( shareKey == null ) {
        this._shareKey = null;
    }
};

Evernote.LinkedNotebook.prototype.getShareKey = function() {
    return this._shareKey;
};

Evernote.LinkedNotebook.prototype.toString = function() {
    return this.name + " " + this.owner + " " + this.auth + " " + this.guid + " " + this.shareKey;
};