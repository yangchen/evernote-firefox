//"use strict";

Evernote.Tag = function Tag( obj ) {
    this.__defineGetter__( "name", this.getName );
    this.__defineSetter__( "name", this.setName );
    this.initialize( obj );
};

Evernote.Tag.javaClass = "com.evernote.edam.type.Tag";
Evernote.inherit( Evernote.Tag, Evernote.AppDataModel, true );

Evernote.Tag.prototype._name = null;

Evernote.Tag.prototype.setName = function( tagName ) {
    if ( typeof tagName == 'undefined' || tagName == null ) {
        this._name = null;
    }
    else {
        this._name = "" + tagName;
    }
};

Evernote.Tag.prototype.getName = function() {
    return this._name;
};

Evernote.Tag.prototype.toString = function() {
    return '[' + this.modelName + ':' + this.guid + ':' + this.name + ']';
};