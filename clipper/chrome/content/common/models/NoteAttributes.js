//"use strict";

Evernote.NoteAttributes = function NoteAttributes( obj ) {
    this.__defineGetter__( "altitude", this.getAltitude );
    this.__defineSetter__( "altitude", this.setAltitude );
    this.__defineGetter__( "longitude", this.getLongitude );
    this.__defineSetter__( "longitude", this.setLongitude );
    this.__defineGetter__( "latitude", this.getLatitude );
    this.__defineSetter__( "latitude", this.setLatitude );
    this.initialize( obj );
};

Evernote.NoteAttributes.WEB_CLIP = "web.clip";

Evernote.NoteAttributes.javaClass = 'com.evernote.edam.type.NoteAttributes';
Evernote.inherit( Evernote.NoteAttributes, Evernote.AppModel, true );

Evernote.NoteAttributes.prototype.author = null;
Evernote.NoteAttributes.prototype.sourceApplication = null;
Evernote.NoteAttributes.prototype.source = Evernote.NoteAttributes.WEB_CLIP;
Evernote.NoteAttributes.prototype.sourceURL = null;
Evernote.NoteAttributes.prototype.subjectDate = null;
Evernote.NoteAttributes.prototype._altitude = null;
Evernote.NoteAttributes.prototype._longitude = null;
Evernote.NoteAttributes.prototype._latitude = null;

Evernote.NoteAttributes.prototype.setAltitude = function( num ) {
    if ( typeof num == 'number' && !isNaN( num ) ) {
        this._altitude = num;
    }
    else if ( num == null || typeof num == 'undefined' ) {
        this._altitude = null;
    }
};

Evernote.NoteAttributes.prototype.getAltitude = function() {
    return this._altitude;
};

Evernote.NoteAttributes.prototype.setLongitude = function( num ) {
    if ( typeof num == 'number' && !isNaN( num ) ) {
        this._longitude = num;
    }
    else if ( num == null || typeof num == 'undefined' ) {
        this._longitude = null;
    }
};

Evernote.NoteAttributes.prototype.getLongitude = function() {
    return this._longitude;
};

Evernote.NoteAttributes.prototype.setLatitude = function( num ) {
    if ( typeof num == 'number' && !isNaN( num ) ) {
        this._latitude = num;
    }
    else if ( num == null || typeof num == 'undefined' ) {
        this._latitude = null;
    }
};

Evernote.NoteAttributes.prototype.getLatitude = function() {
    return this._latitude;
};

Evernote.NoteAttributes.prototype.getStorableProps = function() {
    return [ "author", "sourceApplication", "source", "sourceURL", "altitude", "subjectDate", "longitude", "latitude" ];
};