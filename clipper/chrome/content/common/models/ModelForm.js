//"use strict";

Evernote.ModelForm = function ModelForm() {
};

Evernote.inherit( Evernote.ModelForm, Evernote.jQuery );

Evernote.ModelForm.onForm = function( form, fieldSelector ) {
    var props = { };
    for ( var i in this.prototype ) {
        props[i] = this.prototype[i];
    }
    // Extend jQuery object and make a reference to the original jQuery object
    var origForm = form.get( 0 );
    Evernote.jQuery.extend( true, form, props );
    form.form = Evernote.jQuery( origForm );
    form._fields = { };

    fieldSelector = (typeof fieldSelector == 'string') ? fieldSelector : "input, textarea, select";
    var fields = form.form.find( fieldSelector );

    if ( fields ) {
        //set default arguments for function
        var SetPredefineArgumentsForFunction = function( fn ) {
            var _f = fn;
            var _a = Array( _f.length + 1 - arguments.length ).concat( Array.prototype.slice.apply( arguments ) );
            return function() {
                return _f.apply( this, Array.prototype.slice.apply( arguments ).concat( _a.slice( arguments.length + 1, _a.length ) ) );
            }
        };

        for ( i = 0; i < fields.length; i++ ) {
            var field = Evernote.jQuery( fields[i] );
            var fieldName = field.attr( "name" );
            var realFieldName = fieldName;

            if ( !fieldName ) {
                continue;
            }

            var fieldNameParts = fieldName.replace( /[^a-z0-9]+/i, " " ).split();
            if ( fieldNameParts.length > 1 ) {
                for ( var n = 0; n < fieldNameParts.length; n++ ) {
                    if ( n == 0 ) {
                        fieldName = fieldNameParts[n].toLowerCase();
                    }
                    else {
                        fieldName += fieldNameParts[n].substring( 0, 1 ).toUpperCase() + fieldNameParts[n].substring( 1 ).toLowerCase();
                    }
                }
            }
            // skip fields that have been processed already
            if ( typeof form._fields[fieldName] != 'undefined' ) {
                continue;
            }
            // add mapping of fieldName to field object
            Evernote.logger.debug( "FieldName: " + fieldName );
            form._fields[fieldName] = field;

            // setup accessors
            form.__defineGetter__( "storableProps", form.getStorableProps );
            form.__defineSetter__( "storableProps", form.setStorableProps );
            var methName = fieldName.substring( 0, 1 ).toUpperCase() + fieldName.substring( 1 );

            if ( field.attr( "type" ) == "checkbox" ) {
                form["is" + methName] = SetPredefineArgumentsForFunction( function( realFieldName ) {
                    return this.getField( realFieldName ).attr( 'checked' );
                }, realFieldName );
                form.__defineGetter__( fieldName, form["is" + methName] );

                form["set" + methName] = SetPredefineArgumentsForFunction( function( bool, fieldName/*, realFieldName*/ ) {
                    if ( typeof bool != 'undefined' && bool ) {
                        this.getField( fieldName ).attr( 'checked', 'checked' );
                    }
                    else {
                        this.getField( fieldName ).removeAttr( 'checked' );
                    }
                }, fieldName, realFieldName );
                form.__defineSetter__( fieldName, form["set" + methName] );
            }
            else if ( field.attr( "type" ) == "radio" ) {
                form["get" + methName] = SetPredefineArgumentsForFunction( function( fieldName ) {
                    var checked = null;
                    this.getField( fieldName ).each( function( index, element ) {
                            var $element = Evernote.jQuery( element );
                            if ( $element.attr( 'checked' ) ) {
                                checked = $element.val()
                            }
                        } );
                    return checked;
                }, realFieldName );
                form.__defineGetter__( fieldName, form["get" + methName] );
                
                form["set" + methName] = SetPredefineArgumentsForFunction( function( val, fieldName ) {
                    this.getField( fieldName ).each( function( index, element ) {
                            var $element = Evernote.jQuery( element );
                            if ( $element.val() == val ) {
                                $element.attr( 'checked', 'checked' );
                            }
                            else {
                                $element.removeAttr( 'checked' );
                            }
                        } );
                }, realFieldName );
                form.__defineSetter__( fieldName, form["set" + methName] );
            }
            else {
                form["get" + methName] = SetPredefineArgumentsForFunction( function( fieldName ) {
                    return this.getField( fieldName ).val();
                }, realFieldName );
                form.__defineGetter__( fieldName, form["get" + methName] );

                form["set" + methName] = SetPredefineArgumentsForFunction( function( value, fieldName ) {
                    return this.getField( fieldName ).val( value );
                }, realFieldName );
                form.__defineSetter__( fieldName, form["set" + methName] );
            }
        }
    }

    return form;
};

Evernote.ModelForm.prototype._storableProps = null;

Evernote.ModelForm.prototype.getField = function( fieldName ) {
    return this.form.find( "*[name=" + fieldName + "]" );
};

Evernote.ModelForm.prototype.clear = function() {
    // TODO: clear form
};

Evernote.ModelForm.prototype.populateWith = function( object ) {
    Evernote.logger.debug( "ModelForm.populateWith()" );

    if ( typeof object == 'object' && object != null ) {
        for ( var i in object ) {
            if ( typeof this._fields[i] != 'undefined' ) {
                this[i] = object[i];
            }
        }
    }
};

Evernote.ModelForm.prototype.setStorableProps = function( arrayOfPropNames ) {
    var a = (arrayOfPropNames instanceof Array) ? arrayOfPropNames : new Array( arrayOfPropNames );
    this._storableProps = [ ];
    for ( var i = 0; i < a.length; i++ ) {
        if ( typeof a[i] == 'string' && a[i].length > 0 ) {
            this._storableProps.push( a[i] );
        }
    }
};

Evernote.ModelForm.prototype.getStorableProps = function() {
    if ( this._storableProps == null ) {
        var fieldNames = [ ];
        for ( var i in this._fields ) {
            fieldNames.push( i );
        }
        this._storableProps = fieldNames;
    }

    return this._storableProps;
};

Evernote.ModelForm.prototype.toStorable = function() {
    var props = this.storableProps;
    var storable = { };
    for ( var i = 0; i < props.length; i++ ) {
        storable[props[i]] = this[props[i]];
    }
    
    return storable;
};