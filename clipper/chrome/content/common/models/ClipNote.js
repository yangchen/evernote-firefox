//"use strict";

Evernote.ClipNote = function ClipNote( obj ) {
    this.__defineGetter__( "length", this.getLength );
    this.__defineGetter__( "title", this.getTitle );
    this.__defineSetter__( "title", this.setTitle );
    this.__defineGetter__( "url", this.getUrl );
    this.__defineSetter__( "url", this.setUrl );
    this.__defineGetter__( "fullPage", this.isFullPage );
    this.__defineSetter__( "fullPage", this.setFullPage );
    this.__defineGetter__( "content", this.getContent );
    this.__defineSetter__( "content", this.setContent );
    this.__defineGetter__( "comment", this.getComment );
    this.__defineSetter__( "comment", this.setComment );
    this.__defineGetter__( "notebookGuid", this.getNotebookGuid );
    this.__defineSetter__( "notebookGuid", this.setNotebookGuid );
    this.__defineGetter__( "tagNames", this.getTagNames );
    this.__defineSetter__( "tagNames", this.setTagNames );
    this.__defineGetter__( "saveUrl", this.isSaveUrl );
    this.__defineSetter__( "saveUrl", this.setSaveUrl );
    this.initialize( obj );
};

Evernote.ClipNote.javaClass = "com.evernote.web.ClipNote";
Evernote.inherit( Evernote.ClipNote, Evernote.AppModel, true );

Evernote.ClipNote.cleanTitle = function( title ) {
    if ( typeof title == 'string' ) {
        return title.replace( /[\n\r\t]+/g, " " ).replace( /^\s+/, "" ).replace( /\s+$/, "" );
    }

    return title;
};

Evernote.ClipNote.prototype._title = null;
Evernote.ClipNote.prototype._content = null;
Evernote.ClipNote.prototype._comment = null;
Evernote.ClipNote.prototype._notebookGuid = null;
Evernote.ClipNote.prototype._tagNames = null;
Evernote.ClipNote.prototype._url = null;
Evernote.ClipNote.prototype._saveUrl = true;
Evernote.ClipNote.prototype._fullPage = false;

Evernote.ClipNote.prototype.setTitle = function( title ) {
    if ( typeof title == 'string' ) {
        this._title = this.constructor.cleanTitle( title );
    }
    else if ( title == null ) {
        this._title = null;
    }
};

Evernote.ClipNote.prototype.getTitle = function() {
    return this._title;
};

Evernote.ClipNote.prototype.setUrl = function( url ) {
    if ( typeof url == 'string' ) {
        this._url = (typeof "".trim == 'function') ? url.trim() : url;
    }
    else if ( url == null ) {
        this._url = null;
    }
};

Evernote.ClipNote.prototype.getUrl = function() {
    return this._url;
};

Evernote.ClipNote.prototype.setFullPage = function( fullPage ) {
    this._fullPage = (typeof fullPage != 'undefined' && fullPage) ? true : false;
};

Evernote.ClipNote.prototype.isFullPage = function() {
    return this._fullPage;
};

Evernote.ClipNote.prototype.setContent = function( content ) {
    if ( typeof content == 'undefined' || content == null ) {
        this._content = null;
    }
    else {
        this._content = "" + content;
    }
};

Evernote.ClipNote.prototype.getContent = function() {
    return this._content;
};

Evernote.ClipNote.prototype.setComment = function( comment ) {
    if ( typeof comment == 'undefined' || comment == null ) {
        this._comment = null;
    }
    else {
        this._comment = "" + comment;
    }
};

Evernote.ClipNote.prototype.getComment = function() {
    return this._comment;
};

Evernote.ClipNote.prototype.setNotebookGuid = function( guid ) {
    if ( typeof guid == 'undefined' || guid == null ) {
        this._notebookGuid = null;
    }
    else if ( typeof guid == 'string' && guid.length > 0 ) {
        this._notebookGuid = guid;
    }
};

Evernote.ClipNote.prototype.getNotebookGuid = function() {
    return this._notebookGuid;
};

Evernote.ClipNote.prototype.setTagNames = function( tagNames ) {
    if ( typeof tagNames == 'string' ) {
        this._tagNames = tagNames;
    }
    else if ( typeof tagNames == 'undefined' || tagNames == null ) {
        this._tagNames = null;
    }
};

Evernote.ClipNote.prototype.getTagNames = function() {
    return this._tagNames;
};

Evernote.ClipNote.prototype.setSaveUrl = function( bool ) {
    this._saveUrl = (typeof bool != 'undefined' && bool) ? true : false;
};

Evernote.ClipNote.prototype.isSaveUrl = function() {
    return this._saveUrl;
};

// Returns total length of the instance as it would be POSTed
Evernote.ClipNote.prototype.getLength = function() {
    var total = 0;
    var props = this.getStorableProps();
    for ( var i = 0; i < props.length; i++ ) {
        if ( this[props[i]] ) {
            total += ("" + this[props[i]]).length + props[i].length + 1;
        }
    }
    
    total += (props.length - 1);
    return total;
};

Evernote.ClipNote.prototype.getStorableProps = function() {
    return [ "title", "url", "saveUrl", "fullPage", "notebookGuid", "tagNames", "content", "comment" ];
};

Evernote.ClipNote.prototype.toString = function() {
    return "Evernote.ClipNote [" + this.url + "] " + this.title + " (fullPage: " + this.fullPage + "; saveUrl: "
           + this.saveUrl + "; notebookGuid: " + this.notebookGuid + "; tagNames: " + this.tagNames + "; content length: "
           + ((typeof this.content == 'string') ? this.content.length : 0) + "; comment length: "
           + ((typeof this.comment == 'string') ? this.comment.length : 0) + ")";
};

Evernote.ClipNote.prototype.initMetadata = function( metadata, isSaveUrl ) {
    this.comment = metadata.comment;
    this.tagNames = metadata.tagNames;
    this.title = metadata.title;
    this.notebookGuid = metadata.notebookGuid;
    if ( isSaveUrl ) {
        this.saveUrl = isSaveUrl;
    }
};