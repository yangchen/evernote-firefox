//"use strict";

Evernote.Options = function Options( opts ) {
    this.__defineGetter__( "noteSortOrder", this.getNoteSortOrder );
    this.__defineSetter__( "noteSortOrder", this.setNoteSortOrder );
    this.__defineGetter__( "useSearchHelper", this.getUseSearchHelper );
    this.__defineSetter__( "useSearchHelper", this.setUseSearchHelper );
    this.__defineGetter__( "noteList", this.getNoteList );
    this.__defineSetter__( "noteList", this.setNoteList );
    this.__defineGetter__( "clipNotebook", this.getClipNotebook );
    this.__defineSetter__( "clipNotebook", this.setClipNotebook );
    this.__defineGetter__( "clipNotebookGuid", this.getClipNotebookGuid );
    this.__defineSetter__( "clipNotebookGuid", this.setClipNotebookGuid );
    this.__defineGetter__( "clipStyle", this.getClipStyle );
    this.__defineSetter__( "clipStyle", this.setClipStyle );
    this.__defineGetter__( "clipAction", this.getClipAction );
    this.__defineSetter__( "clipAction", this.setClipAction );
    this.__defineGetter__( "selectionNudging", this.getSelectionNudging );
    this.__defineSetter__( "selectionNudging", this.setSelectionNudging );
    this.__defineGetter__( "smartFilingEnabled", this.getSmartFilingEnabled );
    this.__defineSetter__( "smartFilingEnabled", this.setSmartFilingEnabled );
    this.__defineGetter__( "useNative", this.getUseNative );
    this.__defineSetter__( "useNative", this.setUseNative );
    this.__defineGetter__( "clipNotificationEnabled", this.getClipNotificationEnabled );
    this.__defineSetter__( "clipNotificationEnabled", this.setClipNotificationEnabled );
    this.initialize( opts );
};

Evernote.Options.NOTE_LIST_OPTIONS = {
    NEVER : 0,
    ALWAYS : 1,
    REMEMBER : 2
};

Evernote.Options.CLIP_NOTEBOOK_OPTIONS = {
    DEFAULT : 0,
    SELECT : 1,
    REMEMBER : 2
};

Evernote.Options.CLIP_STYLE_OPTIONS = {
    NONE : 0,
    TEXT : 1,
    FULL : 2
};

Evernote.Options.CLIP_ACTION_OPTIONS = {
    ARTICLE : 0,
    FULL_PAGE : 1,
    URL : 2
};

Evernote.Options.SELECTION_NUDGING_OPTIONS = {
   ENABLED : 0,
   DISABLED : 1,
   NO_HINTS : 2
};

Evernote.Options.SMART_FILLING_ENABLED_OPTIONS = {
    ALL : 0,
    NOTEBOOKS : 1,
    TAGS : 2,
    NONE : 3
};

Evernote.Options.USE_NATIVE_OPTIONS = {
    NEVER : 0,
    ALWAYS : 1
};

Evernote.Options.nativeClipperSupported = (function() {
    try {
        return (Evernote.NativeClipperImplFactory && typeof Evernote.NativeClipperImplFactory.getImplementationFor == "function" && Evernote.NativeClipperImplFactory.getImplementationFor( navigator )) ? true : false;
    }
    catch( e ) {
        return false;
    }
})();

Evernote.Options.DEFAULTS = {
    noteSortOrder : new Evernote.NoteSortOrder(),
    noteList : Evernote.Options.NOTE_LIST_OPTIONS.REMEMBER,
    clipNotebook : Evernote.Options.CLIP_NOTEBOOK_OPTIONS.REMEMBER,
    clipNotebookGuid : null,
    useSearchHelper : false,
    clipNotificationEnabled : true,
    clipStyle : Evernote.Options.CLIP_STYLE_OPTIONS.FULL,
    clipAction : Evernote.Options.CLIP_ACTION_OPTIONS.ARTICLE,
    selectionNudging : Evernote.Options.SELECTION_NUDGING_OPTIONS.ENABLED,
    smartFilingEnabled : Evernote.Options.SMART_FILLING_ENABLED_OPTIONS.ALL,
    useNative : Evernote.Options.USE_NATIVE_OPTIONS.NEVER
};

Evernote.Options.isValidNoteListOptionValue = function( value ) {
    return this.isValidOptionValue( value, Evernote.Options.NOTE_LIST_OPTIONS );
};

Evernote.Options.isValidClipNotebookOptionValue = function( value ) {
    return this.isValidOptionValue( value, Evernote.Options.CLIP_NOTEBOOK_OPTIONS );
};

Evernote.Options.isValidClipStyleOptionValue = function( value ) {
    return this.isValidOptionValue( value, Evernote.Options.CLIP_STYLE_OPTIONS );
};

Evernote.Options.isValidClipActionOptionValue = function( value ) {
    return this.isValidOptionValue( value, Evernote.Options.CLIP_ACTION_OPTIONS );
};

Evernote.Options.isValidSelectionNudgingOptionValue = function( value ) {
    return this.isValidOptionValue( value, Evernote.Options.SELECTION_NUDGING_OPTIONS );
};

Evernote.Options.isValidSmartFillingEnabledOptionValue = function( value ) {
    return this.isValidOptionValue( value, Evernote.Options.SMART_FILLING_ENABLED_OPTIONS );
};

Evernote.Options.isValidUseNativeOptionValue = function( value ) {
    return this.isValidOptionValue( value, Evernote.Options.USE_NATIVE_OPTIONS );
};

Evernote.Options.isValidOptionValue = function( value, allOptions ) {
    if ( typeof allOptions == 'object' && allOptions != null ) {
        for ( var i in allOptions ) {
            if ( value == allOptions[ i ] ) {
                return true;
            }
        }
    }
    return false;
};

Evernote.Options.prototype._noteSortOrder = Evernote.Options.DEFAULTS.noteSortOrder;
Evernote.Options.prototype._noteList = Evernote.Options.DEFAULTS.noteList;
Evernote.Options.prototype._clipNotebook = Evernote.Options.DEFAULTS.clipNotebook;
Evernote.Options.prototype._clipNotebookGuid = Evernote.Options.DEFAULTS.clipNotebookGuid;
Evernote.Options.prototype._clipStyle = Evernote.Options.DEFAULTS.clipStyle;
Evernote.Options.prototype._clipAction =  Evernote.Options.DEFAULTS.clipAction;
Evernote.Options.prototype._selectionNudging = Evernote.Options.DEFAULTS.selectionNudging;
Evernote.Options.prototype._smartFilingEnabled = Evernote.Options.DEFAULTS.smartFilingEnabled;
Evernote.Options.prototype._useNative = Evernote.Options.DEFAULTS.useNative;
Evernote.Options.prototype._useSearchHelper = Evernote.Options.DEFAULTS.useSearchHelper;
Evernote.Options.prototype._clipNotificationEnabled = Evernote.Options.DEFAULTS.clipNotificationEnabled;

Evernote.Options.prototype.initialize = function( options ) {
    var opts = (typeof options == 'object' && options != null) ? options : Evernote.Options.DEFAULTS;
    for ( var i in opts ) {
        if ( typeof this[ i ] != 'undefined' ) {
            this[ i ] = opts[ i ];
        }
    }
};

Evernote.Options.prototype.setNoteSortOrder = function( noteSortOrder ) {
    if ( noteSortOrder instanceof Evernote.NoteSortOrder ) {
        this._noteSortOrder = noteSortOrder;
    }
    else if ( typeof noteSortOrder == 'object' && noteSortOrder != null ) {
        this._noteSortOrder = new Evernote.NoteSortOrder( noteSortOrder );
    }
    else if ( noteSortOrder == null ) {
        this._noteSortOrder = null;
    }
};

Evernote.Options.prototype.getNoteSortOrder = function() {
    return this._noteSortOrder;
};

Evernote.Options.prototype.setNoteList = function( val ) {
    if ( this.constructor.isValidNoteListOptionValue( val ) ) {
        this._noteList = val;
    }
    else if ( val == null ) {
        this._noteList = this.constructor.DEFAULTS.noteList;
    }
};

Evernote.Options.prototype.getNoteList = function() {
    return this._noteList;
};

Evernote.Options.prototype.setClipNotebook = function( val ) {
    if ( this.constructor.isValidClipNotebookOptionValue( val ) ) {
        this._clipNotebook = val;
    }
    else if ( val == null ) {
        this._clipNotebook = this.constructor.DEFAULTS.clipNotebook;
    }
};

Evernote.Options.prototype.getClipNotebook = function() {
    return this._clipNotebook;
};

Evernote.Options.prototype.setClipNotebookGuid = function( guid ) {
    if ( typeof guid == 'string' && guid.length > 0 ) {
        this._clipNotebookGuid = guid;
    }
    else if ( guid instanceof Evernote.Notebook ) {
        this._clipNotebookGuid = guid.guid;
    }
    else if ( guid == null ) {
        this._clipNotebookGuid = null;
    }
};

Evernote.Options.prototype.getClipNotebookGuid = function() {
    return this._clipNotebookGuid;
};

Evernote.Options.prototype.setClipStyle = function( val ) {
    if ( this.constructor.isValidClipStyleOptionValue( val ) ) {
        this._clipStyle = val;
    }
    else if ( val == null ) {
        this._clipStyle = this.constructor.DEFAULTS.clipStyle;
    }
};

Evernote.Options.prototype.getClipStyle = function() {
    return this._clipStyle;
};

Evernote.Options.prototype.setClipAction = function( val ) {
    if ( this.constructor.isValidClipActionOptionValue( val ) ) {
        this._clipAction = val;
    }
    else if ( val == null ) {
        this._clipAction = this.constructor.DEFAULTS.clipAction;
    }
};

Evernote.Options.prototype.getClipAction = function() {
    return this._clipAction;
};

Evernote.Options.prototype.setSelectionNudging = function( val ) {
    if ( this.constructor.isValidSelectionNudgingOptionValue( val ) ) {
        this._selectionNudging = val;
    }
    else if ( val == null ) {
        this._selectionNudging = this.constructor.DEFAULTS.selectionNudging;
    }
};

Evernote.Options.prototype.getSelectionNudging = function() {
    return this._selectionNudging;
};

Evernote.Options.prototype.setSmartFilingEnabled = function( val ) {
    if ( this.constructor.isValidSmartFillingEnabledOptionValue( val ) ) {
        this._smartFilingEnabled = val;
    }
    else if ( val == null ) {
        this._smartFilingEnabled = this.constructor.DEFAULTS.smartFilingEnabled;
    }
};

Evernote.Options.prototype.getSmartFilingEnabled = function() {
    return this._smartFilingEnabled;
};

Evernote.Options.prototype.setUseNative = function( val ) {
    if ( this.constructor.isValidUseNativeOptionValue( val ) ) {
        this._useNative = val;
    }
    else if ( val == null ) {
        this._useNative = this.constructor.DEFAULTS.useNative;
    }
};

Evernote.Options.prototype.getUseNative = function() {
    return this._useNative;
};

Evernote.Options.prototype.setUseSearchHelper = function( bool ) {
    this._useSearchHelper = (bool) ? true : false;
};

Evernote.Options.prototype.getUseSearchHelper = function() {
    return this._useSearchHelper;
};

Evernote.Options.prototype.setClipNotificationEnabled = function( bool ) {
    this._clipNotificationEnabled = (bool) ? true : false;
};

Evernote.Options.prototype.getClipNotificationEnabled = function() {
    return this._clipNotificationEnabled;
};

Evernote.Options.prototype.reset = function() {
    this.initialize( Evernote.context.options );
};

Evernote.Options.prototype.toJSON = function() {
    return {
        noteSortOrder : this.noteSortOrder,
        noteList : this.noteList,
        clipNotebook : this.clipNotebook,
        clipNotebookGuid : this.clipNotebookGuid,
        clipStyle : this.clipStyle,
        clipAction : this.clipAction,
        selectionNudging : this.selectionNudging,
        smartFilingEnabled : this.smartFilingEnabled,
        useNative : this.useNative,
        useSearchHelper : this.useSearchHelper,
        clipNotificationEnabled : this.clipNotificationEnabled
    };
};