//"use strict";

Evernote.Note = function Note( obj ) {
    this.__defineGetter__( "title", this.getTitle );
    this.__defineSetter__( "title", this.setTitle );
    this.__defineGetter__( "content", this.getContent );
    this.__defineSetter__( "content", this.setContent );
    this.__defineGetter__( "tags", this.getTags );
    this.__defineSetter__( "tags", this.setTags );
    this.__defineGetter__( "tagGuids", this.getTagGuids );
    this.__defineSetter__( "tagGuids", this.setTagGuids );
    this.__defineGetter__( "notebook", this.getNotebook );
    this.__defineSetter__( "notebook", this.setNotebook );
    this.__defineGetter__( "notebookGuid", this.getNotebookGuid );
    this.__defineSetter__( "notebookGuid", this.setNotebookGuid );
    this.__defineGetter__( "status", this.getStatus );
    this.__defineSetter__( "status", this.setStatus );
    this.__defineGetter__( "statusReason", this.getStatusReason );
    this.__defineSetter__( "statusReason", this.setStatusReason );
    this.__defineGetter__( "attributes", this.getAttributes );
    this.__defineSetter__( "attributes", this.setAttributes );
    this.__defineGetter__( "comment", this.getComment );
    this.__defineSetter__( "comment", this.setComment );
    this.__defineGetter__( "thumbnailUrl", this.getThumbnailUrl );
    this.__defineGetter__( "noteUrl", this.getNoteUrl );
    this.initialize( obj );
};

Evernote.Note.javaClass = "com.evernote.edam.type.Note";
Evernote.inherit( Evernote.Note, Evernote.AppDataModel, true );

Evernote.Note.dateFormatter = new Evernote.SimpleDateFormat( "MMMM d, yyyy h:mm a" );

/**
 * Indicates an unknown status of a note... shouldn't happen
 */
Evernote.Note.STATUS_UNKNOWN = -1;
/**
 * Indicates that the note is new
 */
Evernote.Note.STATUS_NEW = 0;
/**
 * For new notes that are ready to be created, i.e. sync'd with server
 */
Evernote.Note.STATUS_NEW_PENDING = 1;
/**
 * Indicates that an existing note is awaiting to be sync'd with server
 */
Evernote.Note.STATUS_PENDING = 2;
/**
 * Indicates any existing note that is not new
 */
Evernote.Note.STATUS_OPEN = 3;
/**
 * Indicates that an existing note is known to have been modified
 */
Evernote.Note.STATUS_DIRTY = 4;
/**
 * Indicates the note is deleted
 */
Evernote.Note.STATUS_DELETED = 5;
/**
 * Indicates that a new note is currently being sync'd
 */
Evernote.Note.STATUS_NEW_SYNCING = 6;
/**
 * Indicates that an existing note is currently being sync'd
 */
Evernote.Note.STATUS_SYNCING = 7;
/**
 * Indicates that the note is not syncable
 */
Evernote.Note.STATUS_NEW_SYNC_PREVENT = 8;
/**
 * Indicates that a new note is not to be sync'd
 */
Evernote.Note.STATUS_SYNC_PREVENT = 9;
/**
 * Indicates the a new note failed to be sync'd with server
 */
Evernote.Note.STATUS_NEW_SYNC_FAILED = 10;
/**
 * Indicates that an existing note failed to be sync'd with server
 */
Evernote.Note.STATUS_SYNC_FAILED = 11;

Evernote.Note.TYPE_UNKNOWN = -1;
Evernote.Note.TYPE_TEXT = 0;
Evernote.Note.TYPE_SNAPSHOT = 1;
Evernote.Note.TYPE_AUDIO = 2;
Evernote.Note.TYPE_FILE = 3;

Evernote.Note.THUMB_URL_PREFIX = "thumb";
Evernote.Note.NOTE_URL_PREFIX = "/view/";

Evernote.Note.prototype._title = null;
Evernote.Note.prototype._content = null;
Evernote.Note.prototype.created = null;
Evernote.Note.prototype.updated = null;
Evernote.Note.prototype._notebookGuid = null;
Evernote.Note.prototype._tagGuids = null;
Evernote.Note.prototype.active = false;
Evernote.Note.prototype._attributes = null;
Evernote.Note.prototype._comment = null;
Evernote.Note.prototype._status = Evernote.Note.STATUS_NEW;
Evernote.Note.prototype._statusReason = null;
Evernote.Note.prototype._tags = [ ];
Evernote.Note.prototype._notebook = null;
Evernote.Note.prototype.formattedContent = null;
Evernote.Note.prototype.highlightedFormattedContent = null;
Evernote.Note.prototype.updateSequenceNum = -1;
Evernote.Note.prototype.attachmentFilename = null;
Evernote.Note.prototype.attachment = null;
Evernote.Note.prototype.type = Evernote.Note.TYPE_UNKNOWN;

Evernote.Note.cleanTitle = function( title ) {
    if ( typeof title == 'string' ) {
        return title.replace( /[\n\r\t]+/, "" ).replace( /^\s+/, "" ).replace( /\s+$/, "" );
    }

    return title;
};

Evernote.Note.prototype.isNew = function() {
    if ( this.status == Evernote.Note.STATUS_NEW || this.status == Evernote.Note.STATUS_NEW_PENDING ) {
        return true;
    }
    else {
        return false;
    }
};

Evernote.Note.prototype.isPending = function() {
    if ( this.isNew() || this.status == Evernote.Note.STATUS_PENDING ) {
        return true;
    }
    else {
        return false;
    }
};

Evernote.Note.prototype.isNewPending = function() {
    return this.status == Evernote.Note.STATUS_NEW_PENDING;
};

Evernote.Note.prototype.isEmpty = function() {
    return !this.formattedContent;
};

Evernote.Note.prototype.isSyncing = function() {
    return this.status == Evernote.Note.STATUS_NEW_SYNCING || this.status == Evernote.Note.STATUS_SYNCING;
};

Evernote.Note.prototype.isSyncable = function() {
    return this.status == Evernote.Note.STATUS_NEW_PENDING || this.status == Evernote.Note.STATUS_PENDING;
};

Evernote.Note.prototype.makeUnsyncable = function() {
    var newStatus = null;
    if ( this.isNew() && this.isSyncable() ) {
        newStatus = Evernote.Note.STATUS_NEW_SYNC_PREVENT;
    }
    else if ( this.isSyncable() ) {
        newStatus = Evernote.Note.STATUS_SYNC_PREVENT;
    }
    if ( newStatus != null ) {
        this.status = newStatus;
    }
};

Evernote.Note.prototype.makeSyncable = function() {
    if ( this.isNew() ) {
        this.status = Evernote.Note.STATUS_NEW_PENDING;
    }
    else {
        this.status = Evernote.Note.STATUS_PENDING;
    }
};

Evernote.Note.prototype.isPreventedFromSync = function() {
    return this.status == Evernote.Note.STATUS_SYNC_PREVENT || this.status == Evernote.Note.STATUS_NEW_SYNC_PREVENT;
};

Evernote.Note.prototype.setFailed = function() {
    if ( this.isNew() ) {
        this.status = Evernote.Note.STATUS_NEW_SYNC_FAILED;
    }
    else {
        this.status = Evernote.Note.STATUS_SYNC_FAILED;
    }
};

Evernote.Note.prototype.isFailed = function() {
    return this.status == Evernote.Note.STATUS_NEW_SYNC_FAILED || this.status == Evernote.Note.STATUS_SYNC_FAILED;
};

Evernote.Note.prototype.getFormattedCreateDate = function() {
    return Evernote.Note.dateFormatter.format( new Date( this.created ) );
};

Evernote.Note.prototype.getFormattedUpdateDate = function() {
    return Evernote.Note.dateFormatter.format( new Date( this.updated ) );
};

Evernote.Note.prototype.getTagNames = function() {
    var names = [ ];
    if ( !this.tags || this.tags.length == 0 ) {
        return names;
    }

    for ( var i = 0; i < this.tags.length; i++ ) {
        var tag = this.tags[i];
        if ( tag && tag.name ) {
            names.push( tag.name );
        }
    }

    return names;
};

Evernote.Note.prototype.getTagNamesAsString = function() {
    var names = this.getTagNames();
    return names.join( ", " );
};

Evernote.Note.prototype.getTagGuidsAsString = function() {
    if ( !this.tags || this.tags.length == 0 ) {
        return "";
    }
    var guids = [ ];
    for ( var i = 0; i < this.tags.length; i++ ) {
        var tag = this.tags[i];
        if ( tag && tag.name ) {
            guids.push( tag.guid );
        }
    }

    return guids.join( "," );
};

Evernote.Note.prototype.getStatus = function() {
    return this._status;
};

Evernote.Note.prototype.setStatus = function( newStatus ) {
    this._status = newStatus;
};

Evernote.Note.prototype.setStatusReason = function( message ) {
    this._statusReason = message;
};

Evernote.Note.prototype.getStatusReason = function() {
    return this._statusReason;
};

Evernote.Note.prototype.getNotebook = function() {
    return this._notebook;
};

Evernote.Note.prototype.setNotebook = function( notebook ) {
    if ( notebook instanceof Evernote.Notebook ) {
        this._notebook = notebook;
        this.notebookGuid = notebook.guid;
    }
};

Evernote.Note.prototype.getNotebookGuid = function() {
    return this._notebookGuid;
};

Evernote.Note.prototype.setNotebookGuid = function( guid ) {
    if ( typeof guid == 'undefined' || guid == null ) {
        this._notebookGuid = null;
    }
    else if ( typeof guid == 'string' && guid.length > 0 ) {
        this._notebookGuid = guid;
    }
};

Evernote.Note.prototype.setTags = function( tags ) {
    if ( tags instanceof Evernote.Tag ) {
        this._tags = [ tags ];
    }
    else if ( tags instanceof Array ) {
        this._tags = tags;
    }
    else if ( tags == null ) {
        this._tags = [ ];
    }

    if ( this._tags instanceof Array ) {
        this.tagGuids = null;
        for ( var i = 0; i < this._tags.length; i++ ) {
            this.tagGuids.push( this._tags[i].guid );
        }
    }
};

Evernote.Note.prototype.getTags = function() {
    return this._tags;
};

Evernote.Note.prototype.setTagGuids = function( guids ) {
    if ( guids instanceof Array ) {
        this._tagGuids = guids
    }
    else if ( typeof guids == 'string' ) {
        this._tagGuids = [ guids ];
    }
    else if ( guids == null ) {
        this._tagGuids = [ ];
    }
};

Evernote.Note.prototype.getTagGuids = function() {
    return this._tagGuids;
};

Evernote.Note.prototype.getTitle = function() {
    return this._title;
};

Evernote.Note.prototype.setTitle = function( title, defaultTitle ) {
    var newTitle = null;
    if ( typeof title == 'number' ) {
        newTitle = "" + title;
    }
    if ( typeof title == 'string' ) {
        newTitle = this.constructor.cleanTitle( title );
    }
    
    if ( (newTitle == null || newTitle.length == 0) && typeof defaultTitle == 'string' ) {
        newTitle = this.constructor.cleanTitle( defaultTitle );
    }
    if ( typeof newTitle == 'string' ) {
        this._title = newTitle;
    }
};

Evernote.Note.prototype.getContent = function() {
    return this._content;
};

Evernote.Note.prototype.setContent = function( content ) {
    if ( typeof content == 'undefined' || content == null ) {
        this._content = null;
    }
    else {
        this._content = "" + content;
    }
};

Evernote.Note.prototype.getComment = function() {
    return this._comment;
};

Evernote.Note.prototype.setComment = function( comment ) {
    if ( typeof comment == 'undefined' || comment == null ) {
        this._comment = null;
    }
    else {
        this._comment = "" + comment;
    }
};

Evernote.Note.prototype.setAttributes = function( noteAttributesOrObject ) {
    if ( noteAttributesOrObject instanceof Evernote.NoteAttributes ) {
        this._attributes = noteAttributesOrObject;
    }
    else if ( typeof noteAttributesOrObject == 'object' ) {
        this._attributes = new Evernote.NoteAttributes( noteAttributesOrObject );
    }
};

Evernote.Note.prototype.getAttributes = function() {
    return this._attributes;
};

Evernote.Note.prototype.getThumbnailUrl = function( shardedUrl ) {
    // /thumb/8d3c87f9-6df9-4475-8ebc-45f336cf9757
    if ( this.guid ) {
        var base = (typeof shardedUrl == 'string') ? shardedUrl : "";
        return base + "/" + Evernote.Note.THUMB_URL_PREFIX + "/" + this.guid;
    }
    return null;
};

Evernote.Note.prototype.getNoteUrl = function( base, shard, searchWords ) {
    // /view.jsp?locale=default&shard=s1#v=t&n=8d3c87f9-6df9-4475-8ebc-45f336cf9757&b=0
    if ( this.guid ) {
        if ( typeof base != 'string' ) {
            base = "";
        }
        else if ( base.indexOf( "/", base.length - 1 ) == base.length - 1 ) {
            base = base.substring( 0, base.length - 1 );
        }
        return base + Evernote.Note.NOTE_URL_PREFIX + this.guid + "&shard=" + shard + "#v=t" + "&b=0"
               + ((typeof searchWords == 'string' && searchWords.length > 0) ? ("&x=" + encodeURI( searchWords )) : "");
    }
    return null;
};

Evernote.Note.prototype.getStatusName = function( /*value*/ ) {
    switch ( this._status ) {
        case Evernote.Note.STATUS_DELETED:
            return "DELETED";
        case Evernote.Note.STATUS_NEW:
            return "NEW";
        case Evernote.Note.STATUS_NEW_PENDING:
            return "NEW PENDING";
        case Evernote.Note.STATUS_OPEN:
            return "OPEN";
        case Evernote.Note.STATUS_DIRTY:
            return "DIRTY";
        case Evernote.Note.STATUS_PENDING:
            return "PENDING";
        default:
            return "UNKNOWN";
    }
};

Evernote.Note.prototype.addTag = function( tag ) {
    if ( !(tag instanceof Evernote.Tag) ) {
        return;
    }

    for ( var i = 0; i < this._tags.length; i++ ) {
        if ( this._tags[i] == tag ) {
            return;
        }
    }
    this._tags.push( tag );
};

Evernote.Note.prototype.removeTag = function( tag ) {
    var tagName = null;
    if ( this._tags.length == 0 ) {
        return;
    }

    if ( tag instanceof Evernote.Tag ) {
        tagName = tag.name.toLowerCase();
    }
    else if ( typeof tag == 'string' ) {
        tagName = tag.toLowerCase();
    }

    if ( tagName == null ) {
        return;
    }
    var newTags = [ ];
    for ( var i = 0; i < this._tags.length; i++ ) {
        if ( this._tags[i].name.toLowerCase() != tagName ) {
            newTags.push( this.tags[i] );
        }
    }
    this._tags = newTags;
};

Evernote.Note.prototype.hasTags = function() {
    return this._tags && this._tags.length > 0;
};

Evernote.Note.prototype.hasTag = function( tag ) {
    if ( this._tags.length == 0 ) {
        return false;
    }

    var tagName = null;
    if ( tag instanceof Evernote.Tag ) {
        tagName = tag.name.toLowerCase();
    }
    else if ( typeof tag == 'string' ) {
        tagName = tag.toLowerCase();
    }

    if ( tagName == null ) {
        return false;
    }
    for ( var i = 0; i < this._tags.length; i++ ) {
        if ( this._tags[i].name.toLowerCase() == tagName ) {
            return true;
        }
    }
    
    return false;
};

Evernote.Note.prototype.hasLocation = function() {
    return this.attributes != null && typeof this.attributes.longitude == 'number' && typeof this.attributes.latitude == 'number';
};

Evernote.Note.prototype.getStorableProps = function() {
    return [ "guid", "title", "content", "comment", "created", "updated", "notebookGuid", "tagGuids", "active", "attributes" ];
};

Evernote.Note.fromClipNote = function( clipNote ) {
    var note = new Evernote.Note();
    var noteAttributes = null;

    if ( clipNote.title ) {
        note.title = clipNote.title;
    }
    if ( clipNote.content ) {
        note.content = clipNote.content;
    }
    
    if ( clipNote.url ) {
        noteAttributes = new Evernote.NoteAttributes();
        noteAttributes.sourceURL = clipNote.url;
    }
    if ( noteAttributes ) {
        note.attributes = noteAttributes;
    }
};