//"use strict";

Evernote.NoteFilter = function NoteFilter( obj ) {
    this.__defineGetter__( "order", this.getOrder );
    this.__defineSetter__( "order", this.setOrder );
    this.__defineGetter__( "ascending", this.isAscending );
    this.__defineSetter__( "ascending", this.setAscending );
    this.__defineGetter__( "words", this.getWords );
    this.__defineSetter__( "words", this.setWords );
    this.__defineGetter__( "notebookGuid", this.getNotebookGuid );
    this.__defineSetter__( "notebookGuid", this.setNotebookGuid );
    this.__defineGetter__( "tagGuids", this.getTagGuids );
    this.__defineSetter__( "tagGuids", this.setTagGuids );
    this.__defineGetter__( "timeZone", this.getTimeZone );
    this.__defineSetter__( "timeZone", this.setTimeZone );
    this.__defineGetter__( "inactive", this.isInactive );
    this.__defineSetter__( "inactive", this.setInactive );
    this.__defineGetter__( "fuzzy", this.isFuzzy );
    this.__defineSetter__( "fuzzy", this.setFuzzy );
    this.initialize( obj );
};

Evernote.NoteFilter.javaClass = "com.evernote.edam.notestore.NoteFilter";
Evernote.inherit( Evernote.NoteFilter, Evernote.AppModel, true );

Evernote.NoteFilter.ENGLISH_STOP_WORDS = [ "a", "an", "and", "are", "as", "at", "be", "but", "by", "for", "if", "in",
    "into", "is", "it", "no", "not", "of", "on", "or", "such", "that", "the", "their", "then", "there", "these",  "they",
    "this", "to", "was", "will", "with"
];

Evernote.NoteFilter.ALL_NOTES_MARKER = "notebook:*";

Evernote.NoteFilter.prototype._order = Evernote.NoteSortOrderTypes.CREATED;
Evernote.NoteFilter.prototype._ascending = false;
Evernote.NoteFilter.prototype._words = null;
Evernote.NoteFilter.prototype._notebookGuid = null;
Evernote.NoteFilter.prototype._tagGuids = null;
Evernote.NoteFilter.prototype._timeZone = null;
Evernote.NoteFilter.prototype._inactive = false;
Evernote.NoteFilter.prototype._fuzzy = false;

Evernote.NoteFilter.prototype.getOrder = function() {
    return this._order;
};

Evernote.NoteFilter.prototype.setOrder = function( order ) {
    this._order = Math.max( parseInt( order ), 1 );
};

Evernote.NoteFilter.prototype.isAscending = function() {
    return this._ascending;
};

Evernote.NoteFilter.prototype.setAscending = function( bool ) {
    this._ascending = (bool) ? true : false;
};

Evernote.NoteFilter.prototype.getWords = function() {
    return this._words;
};

Evernote.NoteFilter.prototype.setWords = function( words ) {
    if ( words == null ) {
        this._words = null;
    }
    else {
        var wordArray = (words instanceof Array) ? words : Evernote.NoteFilter.separateWords( (words + "") );
        if ( wordArray instanceof Array && wordArray.length > 0 ) {
            if ( this.fuzzy ) {
                var newWords = [ ];
                for ( var i = 0; i < wordArray.length; i++ ) {
                    newWords.push( Evernote.NoteFilter.makeWordFuzzy( wordArray[i] ) );
                }

                if ( newWords.length > 0 ) {
                    this._words = newWords.join( " " );
                }
            }
            else {
                this._words = wordArray.join( " " );
            }
        }
        this.convertIfAllNotesMarker();
    }
};

Evernote.NoteFilter.prototype.convertIfAllNotesMarker = function() {
    if ( this._words == Evernote.NoteFilter.ALL_NOTES_MARKER ) {
        this._words = "*";
    }
};

Evernote.NoteFilter.prototype.getNotebookGuid = function() {
    return this._notebookGuid;
};

Evernote.NoteFilter.prototype.setNotebookGuid = function( guid ) {
    if ( typeof guid == 'string' && guid.length > 0 ) {
        this._notebookGuid = guid;
    }
    else if ( guid == null ) {
        this._notebookGuid = null;
    }
};

Evernote.NoteFilter.prototype.getTagGuids = function() {
    return this._tagGuids;
};

Evernote.NoteFilter.prototype.setTagGuids = function( guids ) {
    if ( guids instanceof Array ) {
        this._tagGuids = guids;
    }
    else if ( typeof guids == 'string' ) {
        this._tagGuids = new Array( guids );
    }
    else if ( guids == null ) {
        this._tagGuids = null;
    }
};

Evernote.NoteFilter.prototype.getTimeZone = function() {
    return this._timeZone;
};

Evernote.NoteFilter.prototype.setTimeZone = function( tz ) {
    if ( typeof tz == 'string' ) {
        this._timeZone = tz;
    }
    else if ( tz == null ) {
        this._timeZone = null;
    }
};

Evernote.NoteFilter.prototype.isInactive = function() {
    return this._inactive;
};

Evernote.NoteFilter.prototype.setInactive = function( bool ) {
    this._inactive = (bool) ? true : false;
};

Evernote.NoteFilter.prototype.setFuzzy = function( bool ) {
    this._fuzzy = (bool) ? true : false;
};

Evernote.NoteFilter.prototype.isFuzzy = function() {
    return this._fuzzy;
};

Evernote.NoteFilter.prototype.isEmpty = function() {
    return this._words == null && this._notebookGuid == null && this._tagGuids == null && this._timeZone == null;
};

Evernote.NoteFilter.prototype.resetQuery = function() {
    this.words = null;
    this.tagGuids = null;
    this.notebookGuid = null;
    this.inactive = false;
    this.timeZone = null;
};

/**
 * Separates given string by given separator (defaults to
 * Evernote.NoteFilter.WORD_SEPARATOR). Returns array of words found. Keeps quoted words
 * together. If quote charater is not given, defaults to Evernote.NoteFilter.QUOTE.
 */
Evernote.NoteFilter.WORD_SEPARATOR = " ";
Evernote.NoteFilter.QUOTE = '"';
Evernote.NoteFilter.TOKEN_SEPARATOR = ":";
Evernote.NoteFilter.FUZZY_SUFFIX = "*";

Evernote.NoteFilter.RESERVED_TOKENS = [ "any", "notebook", "tag", "intitle", "created", "updated", "resource", "subjectDate",
    "latitude", "longitude", "altitude", "author", "source", "sourceApplication", "recoType", "todo"
];

Evernote.NoteFilter.separateWords = function( str, separator, quote ) {
    if ( typeof separator == 'undefined' || separator == null ) {
        separator = Evernote.NoteFilter.WORD_SEPARATOR;
    }
    if ( typeof quote == 'undefined' || quote == null ) {
        quote = Evernote.NoteFilter.QUOTE;
    }

    var words = [ ];
    var i = 0;
    var buffer = "";
    var quoted = false;

    function addWord( word ) {
        if ( word ) {
            word = word.replace( /^\s+/, "" ).replace( /\s+$/, "" );
            if ( word.length > 0 ) {
                words.push( word );
            }
        }
    }

    str = str.replace( /^\s+/, "" ).replace( /\s+$/, "" );
    while ( i < str.length ) {
        var c = str.charAt( i );
        if ( c == quote ) {
            buffer += c;
            quoted = !quoted;
        }
        else if ( !quoted && c == separator ) {
            addWord( buffer );
            buffer = "";
        }
        else {
            buffer += c;
        }
        i++;
    }

    if ( buffer.length > 0 ) {
        addWord( buffer );
    }
    return words;
};

Evernote.NoteFilter.isWordFuzzy = function( word ) {
    return typeof word == 'string' && word.charAt( word.length - 1 ) == Evernote.NoteFilter.FUZZY_SUFFIX;
};

Evernote.NoteFilter.canWordBeFuzzy = function( word ) {
    if ( typeof word == 'string' && word.length > 0 && word.indexOf( " " ) < 0 && word.indexOf( Evernote.NoteFilter.QUOTE ) < 0
         && !Evernote.NoteFilter.isWordFuzzy( word ) && Evernote.NoteFilter.ENGLISH_STOP_WORDS.indexOf( word.toLowerCase() ) < 0 ) {
        if ( word.indexOf( Evernote.NoteFilter.TOKEN_SEPARATOR ) <= 0 ) {
            return true;
        }
        else {
            var w = word.toLowerCase();
            for ( var i = 0; i < Evernote.NoteFilter.RESERVED_TOKENS.length; i++ ) {
                var re = new RegExp( "^-?" + Evernote.NoteFilter.RESERVED_TOKENS[i] + Evernote.NoteFilter.TOKEN_SEPARATOR );
                if ( w.match( re ) ) {
                    return false;
                }
            }
            return true;
        }
    }

    return false;
};

Evernote.NoteFilter.makeWordFuzzy = function( word ) {
    if ( Evernote.NoteFilter.canWordBeFuzzy( word ) ) {
        word += Evernote.NoteFilter.FUZZY_SUFFIX;
    }

    return word;
};

Evernote.NoteFilter.formatWord = function( str ) {
    if ( str.indexOf( Evernote.NoteFilter.WORD_SEPARATOR ) > 0 ) {
        return Evernote.NoteFilter.QUOTE + str + Evernote.NoteFilter.QUOTE;
    }
    else {
        return str;
    }
};

Evernote.NoteFilter.isWordQuoted = function( word ) {
    return typeof word == 'string' && word.length > 2 && word.indexOf( Evernote.NoteFilter.QUOTE ) == 0
           && word.indexOf( Evernote.NoteFilter.QUOTE, 1 ) == (word.length - 1);
};

Evernote.NoteFilter.unquoteWord = function( word ) {
    if ( Evernote.NoteFilter.isWordQuoted( word ) ) {
        return word.substring( 1, (word.length - 1) );
    }
    else {
        return word;
    }
};

Evernote.NoteFilter.extractTokenValue = function( token, word ) {
    if ( typeof word == 'string' && typeof token == 'string' ) {
        var x = word.indexOf( token + Evernote.NoteFilter.TOKEN_SEPARATOR );
        if ( x >= 0 && x <= 1 ) {
            return Evernote.NoteFilter.unquoteWord( word.substring( x + token.length + Evernote.NoteFilter.TOKEN_SEPARATOR.length ) );
        }
    }
    
    return null;
};

Evernote.NoteFilter.prototype.getStorableProps = function() {
    return [ "order", "ascending", "words", "notebookGuid", "tagGuids", "timeZone", "inactive" ];
};
