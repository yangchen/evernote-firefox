pref('extensions.readable-by-evernote.theme', 					'--def--');

pref('extensions.readable-by-evernote.keys_activation', 		'--def--');
pref('extensions.readable-by-evernote.keys_clip', 				'--def--');
pref('extensions.readable-by-evernote.keys_highlight', 		    '--def--');

pref('extensions.readable-by-evernote.clip_tag', 				'--def--');
pref('extensions.readable-by-evernote.clip_notebook', 			'--def--');

pref('extensions.readable-by-evernote.text_font', 				'--def--');
pref('extensions.readable-by-evernote.text_font_header', 		'--def--');
pref('extensions.readable-by-evernote.text_font_monospace', 	'--def--');
pref('extensions.readable-by-evernote.text_size', 				'--def--');
pref('extensions.readable-by-evernote.text_line_height', 		'--def--');
pref('extensions.readable-by-evernote.box_width', 				'--def--');
pref('extensions.readable-by-evernote.color_background', 		'--def--');
pref('extensions.readable-by-evernote.color_text', 				'--def--');
pref('extensions.readable-by-evernote.color_links', 			'--def--');
pref('extensions.readable-by-evernote.text_align', 				'--def--');
pref('extensions.readable-by-evernote.base', 					'--def--');
pref('extensions.readable-by-evernote.footnote_links', 			'--def--');
pref('extensions.readable-by-evernote.large_graphics', 			'--def--');
pref('extensions.readable-by-evernote.custom_css', 				'--def--');

pref('extensions.readable-by-evernote.related_notes', 	        '--def--');
pref('extensions.readable-by-evernote.smart_filing', 	        '--def--');

pref('extensions.readable-by-evernote.custom_theme_options', 	'--def--');


pref('extensions.readable-by-evernote.storedEvernoteUsername', 	'');
pref('extensions.readable-by-evernote.storedEvernoteServer', 	'');

pref('extensions.readable-by-evernote.storedEvernoteLogoutOnNextAction', '');

pref('extensions.readable-by-evernote.notFirstRun', '');
pref('extensions.readable-by-evernote.firstShowIdentifier', '');
