
	//	bind fast
	//	=========
		
	//	hooks
	//	=====
		__readable_by_evernote.readable_by_evernote__menu__call = 			function () { __readable_by_evernote.__readable_by_evernote__launch(); };
		__readable_by_evernote.readable_by_evernote__context_menu__call = 	function () { __readable_by_evernote.__readable_by_evernote__launch(); };
		__readable_by_evernote.readable_by_evernote__button__call = 		function () { __readable_by_evernote.__readable_by_evernote__launch(); };

		
	//	jquery, keyboard
	//	================
		window.addEventListener('load', function ()
		{
			//	decode
			
	//	encode
	//	======
		function __encodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == '') { return 'none'; }
			
			//	encode
			return encodeURIComponent(_string)
				.replace(/!/g, '%21')
				.replace(/'/g, '%27')
				.replace(/\(/g, '%28')
				.replace(/\)/g, '%29')
				.replace(/\*/g, '%2A')
			;
		}

		
	//	decode
	//	======
		function __decodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == 'none') { return ''; }
			
			//	decode
			return decodeURIComponent(_string);
		}
	
	

				
			//	keyboard
			//	========
				var 
					_vars = __readable_by_evernote.__get_saved__vars(),
					__key_activation = __decodeURIComponentForReadable(_vars['keys_activation']),
					__key_clip = __decodeURIComponentForReadable(_vars['keys_clip']),
					__key_highlight = __decodeURIComponentForReadable(_vars['keys_highlight'])
				;
				
				gBrowser.addEventListener('keydown', function(_event)
				{
					//	include key combo detection
						
	/*
		_event must be defined
		_key_combo and _key_code will be defined at end of code
	*/

	var _key_code = 'NONE';
	switch (true)
	{
		case (_event.keyCode && (_event.keyCode >= 65 && _event.keyCode <= 90)):
			_key_code = String.fromCharCode(_event.keyCode).toUpperCase();
			break;
			
		case (_event.keyCode == 27):	_key_code = 'Escape';		break;
		case (_event.keyCode == 37):	_key_code = 'Left Arrow';	break;
		case (_event.keyCode == 39):	_key_code = 'Right Arrow';	break;
		case (_event.keyCode == 38):	_key_code = 'Up Arrow';		break;
		case (_event.keyCode == 40):	_key_code = 'Down Arrow';	break;
	}

	//	get
	//	===
		var _modifierKeys = (_event.originalEvent ? _event.originalEvent : _event);
		//	jQuery screws up -- fucks up the metaKey property badly
		
		var _key_combo = ''
			+ (_modifierKeys.ctrlKey ? 'Control + ' : '')
			+ (_modifierKeys.shiftKey ? 'Shift + ' : '')
			+ (_modifierKeys.altKey ? 'Alt + ' : '')
			+ (_modifierKeys.metaKey ? 'Command + ' : '')
			+ _key_code
		;

	//	needs a modifier -- if not just Escape key
	//	================
		if ((_key_code != 'Escape') && (_key_code == _key_combo))
		{
			_key_code = 'NONE';
			_key_combo = 'NONE';
		}

					
					switch (true)
					{
						case ((__key_activation > '') && (_key_combo == __key_activation)):
						case ((__key_clip > '') && (_key_combo == __key_clip)):
						case ((__key_highlight > '') && (_key_combo == __key_highlight)):
							
                            //  visible already; ignore
                            //  =======================
                            
                                var _body = gBrowser.contentDocument.getElementsByTagName('body')[0];
                                if (_body && _body.className && _body.className.match(/readableVisible/))
                                    { break; }
                            
							//	also?
                            //  =====

                                //  clip
                                var __clip_on_launch = ((__key_clip > '') && (_key_combo == __key_clip));

                                //	highlight
                                var __highlight_on_launch = ((__key_highlight > '') && (_key_combo == __key_highlight));

                                
							//	stop
							//	====
								_event.stopPropagation();
								_event.preventDefault();
								
							//	do
							//	==
								__readable_by_evernote.__readable_by_evernote__launch(__clip_on_launch, __highlight_on_launch);

							break;
					}
				}, true);
                
            //  expose
            //  ======
                
    //  expose clearly
    //  ==============
    
        gBrowser.addEventListener('DOMContentLoaded', function (_event)
        {
        
            //  invalid
            if (_event.originalTarget.nodeName == "#document"); else { return; }
        
            //  get page
            var 
                _document = _event.originalTarget,
                _window = _document.defaultView
            ;
            
            //  return
            if (_window.location.hostname.match(/(evernote|yinxiang).com$/gi)); else { return; }
            
            //  add to head
            try
            {
                var _meta = _document.createElement("meta");
                    _meta.name = "evernote-clearly-extension";
                    _meta.content = "installed";
                
                _document.head.appendChild(meta);
            }
            catch (e) {}
            
            //  add to body
            try
            {
                _document.body.className += ' evernote-clearly-extension';
            }
            catch (e) {}
        
        }, true);
    
                
		}, false);

	
	//	launcher
	//	========
		__readable_by_evernote.__readable_by_evernote__launch = function (__clip_on_launch, __highlight_on_launch)
		{
			//	default
			if (__clip_on_launch); else { __clip_on_launch = false; }
			if (__highlight_on_launch); else { __highlight_on_launch = false; }
			
			var 
				_d = gBrowser.contentDocument, 
				_b = _d.getElementsByTagName('body')[0], 
				_o = _d.getElementById('__readable_extension_definitions'), 
				_l = _d.createElement('script')
			;

			//	create, if not present
			//	======================
				if (_o); else
				{
					_o = _d.createElement('dl');
					_o.setAttribute('style', 'display: none;');
					_o.setAttribute('id', '__readable_extension_definitions');
					_b.appendChild(_o);
				}

			//	set options
			//	===========
				var
					_options = __readable_by_evernote.__get_saved__options(),
					_vars = __readable_by_evernote.__get_saved__vars(),
					_translations = __readable_by_evernote.__get_translations(),
					
					__definition_items_html = __readable_by_evernote.__get__stuffAsDefinitionItemsHTML
					({
						'option': _options,
						'var': _vars,
						'translation': _translations
					})
				;

				_o.innerHTML = __definition_items_html;

			//	launch in context
			//	=================
				_l.setAttribute('src', 'chrome://readable-by-evernote/content/js/__bookmarklet_to_inject'+(__clip_on_launch ? '__andClipOnLaunch' : (__highlight_on_launch ? '__andHighlightOnLaunch' : ''))+'.js');
				_l.className = 'bookmarklet_launch';
				_b.appendChild(_l);
			
			//	custom events
			//	=============
				__readable_by_evernote.__add_custom_events_handler();
		};


		
	//	include
	//	=======
		
		//	options
		
	//	get options
	//	===========
		__readable_by_evernote.__get_saved__options = function ()
		{
			
	//	encode
	//	======
		function __encodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == '') { return 'none'; }
			
			//	encode
			return encodeURIComponent(_string)
				.replace(/!/g, '%21')
				.replace(/'/g, '%27')
				.replace(/\(/g, '%28')
				.replace(/\)/g, '%29')
				.replace(/\*/g, '%2A')
			;
		}

		
	//	decode
	//	======
		function __decodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == 'none') { return ''; }
			
			//	decode
			return decodeURIComponent(_string);
		}
	
	

			
	//	__encodeURIComponentForReadable must be defined

	var __default_options = 
	{
		'text_font': 			__encodeURIComponentForReadable('"PT Serif"'),
		'text_font_header': 	__encodeURIComponentForReadable('"PT Serif"'),
		'text_font_monospace': 	__encodeURIComponentForReadable('Inconsolata'),
		'text_size': 			__encodeURIComponentForReadable('16px'),
		'text_line_height': 	__encodeURIComponentForReadable('1.5em'),
		'box_width': 			__encodeURIComponentForReadable('36em'),
		'color_background': 	__encodeURIComponentForReadable('#f3f2ee'),
		'color_text': 			__encodeURIComponentForReadable('#1f0909'),
		'color_links': 			__encodeURIComponentForReadable('#065588'),
		'text_align': 			__encodeURIComponentForReadable('normal'),
		'base': 				__encodeURIComponentForReadable('theme-1'),
		'footnote_links': 		__encodeURIComponentForReadable('on_print'),
		'large_graphics': 		__encodeURIComponentForReadable('do_nothing'),
		'custom_css': 			__encodeURIComponentForReadable('')
	};


			var 
				_prefs_service = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService),
				_prefs = _prefs_service.getBranch("extensions.readable-by-evernote.")
			;

			var _return = {};
			for (var _x in __default_options) { _return[_x] = _prefs.getCharPref(_x); }
			
			return _return;
		};

		
	//	get vars	
	//	========
		__readable_by_evernote.__get_saved__vars = function ()
		{
			
	//	encode
	//	======
		function __encodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == '') { return 'none'; }
			
			//	encode
			return encodeURIComponent(_string)
				.replace(/!/g, '%21')
				.replace(/'/g, '%27')
				.replace(/\(/g, '%28')
				.replace(/\)/g, '%29')
				.replace(/\*/g, '%2A')
			;
		}

		
	//	decode
	//	======
		function __decodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == 'none') { return ''; }
			
			//	decode
			return decodeURIComponent(_string);
		}
	
	

			
	//	__encodeURIComponentForReadable must be defined

	var __default_vars = 
	{
		'theme': 				    __encodeURIComponentForReadable('theme-1'),     /* theme-1, theme-2, theme-3, custom */
		
		'keys_activation': 		    __encodeURIComponentForReadable('Control + Alt + Right Arrow'),
		'keys_clip': 			    __encodeURIComponentForReadable('Control + Alt + Up Arrow'),
		'keys_highlight': 		    __encodeURIComponentForReadable('Control + Alt + H'),

		'clip_tag': 			    __encodeURIComponentForReadable(''),
		'clip_notebook': 			__encodeURIComponentForReadable(''),
		
        'related_notes':            __encodeURIComponentForReadable('enabled'),     /* enabled, just_at_bottom, disabled */
        'smart_filing':             __encodeURIComponentForReadable('enabled'),     /* enabled, just_notebooks, just_tags, disabled */
        
		'custom_theme_options':	    __encodeURIComponentForReadable('')             /* the custom theme options get serialized into this */
	};

		
			var 
				_prefs_service = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService),
				_prefs = _prefs_service.getBranch("extensions.readable-by-evernote.")
			;

			var _return = {};
			for (var _x in __default_vars) { _return[_x] = _prefs.getCharPref(_x); }
			
			return _return;
		};
		

	//	save
	//	====
		__readable_by_evernote.__save_someStuff = function (_to_save)
		{
			var 
				_prefs_service = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService),
				_prefs = _prefs_service.getBranch("extensions.readable-by-evernote.")
			;
		
			for (var _x in _to_save)
				{ _prefs.setCharPref(_x, _to_save[_x]); }
		};
		
		
	//	get definitionList
	//	==================
		__readable_by_evernote.__get__stuffAsDefinitionItemsHTML = function (_stuffToTransform)
		{
			/*
				stuffToTransform = {
					'prefix-id': {
						'key': 'value'
					}
				};
			*/
			
            //  __escapeForHTML
            
    //  escapeForHTML
    //  =============
        function __escapeForHTML(_string)
        {
            var _replace = {
                "&": "amp", 
                '"': "quot", 
                "<": "lt", 
                ">": "gt"
            };
            
            return _string.replace(
                /[&"<>]/g,
                function (_match) { return ("&" + _replace[_match] + ";"); }
            );
        }

            
            //  create html
			var _html = '';
			for (var _prefix in _stuffToTransform)
			{
				for (var _x in _stuffToTransform[_prefix])
				{
					_html += ''
						+ '<dd id="__readable_' + __escapeForHTML(_prefix) + '__' + __escapeForHTML(_x) + '">'
						+ 	__escapeForHTML(_stuffToTransform[_prefix][_x])
						+ '</dd>'
					;
				}
			}
			
			return _html;
		};

		
	//	set defaults -- are set in defaults/preferences/prefs.js to '--def--'
	//	============
		(function ()
		{
			
	//	encode
	//	======
		function __encodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == '') { return 'none'; }
			
			//	encode
			return encodeURIComponent(_string)
				.replace(/!/g, '%21')
				.replace(/'/g, '%27')
				.replace(/\(/g, '%28')
				.replace(/\)/g, '%29')
				.replace(/\*/g, '%2A')
			;
		}

		
	//	decode
	//	======
		function __decodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == 'none') { return ''; }
			
			//	decode
			return decodeURIComponent(_string);
		}
	
	

			
	//	__encodeURIComponentForReadable must be defined

	var __default_options = 
	{
		'text_font': 			__encodeURIComponentForReadable('"PT Serif"'),
		'text_font_header': 	__encodeURIComponentForReadable('"PT Serif"'),
		'text_font_monospace': 	__encodeURIComponentForReadable('Inconsolata'),
		'text_size': 			__encodeURIComponentForReadable('16px'),
		'text_line_height': 	__encodeURIComponentForReadable('1.5em'),
		'box_width': 			__encodeURIComponentForReadable('36em'),
		'color_background': 	__encodeURIComponentForReadable('#f3f2ee'),
		'color_text': 			__encodeURIComponentForReadable('#1f0909'),
		'color_links': 			__encodeURIComponentForReadable('#065588'),
		'text_align': 			__encodeURIComponentForReadable('normal'),
		'base': 				__encodeURIComponentForReadable('theme-1'),
		'footnote_links': 		__encodeURIComponentForReadable('on_print'),
		'large_graphics': 		__encodeURIComponentForReadable('do_nothing'),
		'custom_css': 			__encodeURIComponentForReadable('')
	};

			
	//	__encodeURIComponentForReadable must be defined

	var __default_vars = 
	{
		'theme': 				    __encodeURIComponentForReadable('theme-1'),     /* theme-1, theme-2, theme-3, custom */
		
		'keys_activation': 		    __encodeURIComponentForReadable('Control + Alt + Right Arrow'),
		'keys_clip': 			    __encodeURIComponentForReadable('Control + Alt + Up Arrow'),
		'keys_highlight': 		    __encodeURIComponentForReadable('Control + Alt + H'),

		'clip_tag': 			    __encodeURIComponentForReadable(''),
		'clip_notebook': 			__encodeURIComponentForReadable(''),
		
        'related_notes':            __encodeURIComponentForReadable('enabled'),     /* enabled, just_at_bottom, disabled */
        'smart_filing':             __encodeURIComponentForReadable('enabled'),     /* enabled, just_notebooks, just_tags, disabled */
        
		'custom_theme_options':	    __encodeURIComponentForReadable('')             /* the custom theme options get serialized into this */
	};

		
			var 
				_prefs_service = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService),
				_prefs = _prefs_service.getBranch("extensions.readable-by-evernote.")
			;
		
			//	mac or pc
			//	=========
				var osString = Components.classes["@mozilla.org/xre/app-info;1"].getService(Components.interfaces.nsIXULRuntime).OS;
				if (osString.toLowerCase() == 'darwin')
				{
					__default_vars['keys_activation'] = __encodeURIComponentForReadable('Control + Command + Right Arrow');
					__default_vars['keys_clip'] = 		__encodeURIComponentForReadable('Control + Command + Up Arrow');
					__default_vars['keys_highlight'] = 	__encodeURIComponentForReadable('Control + Command + H');
				}
		
			//	options
			//	=======
				for (var _x in __default_options)
				{
					var 
						_valueNow = _prefs.getCharPref(_x),
						_valueDefault = __default_options[_x]
					;
					
					switch (true)
					{
						case (!(_valueNow > '')):
						case (_valueNow == '--def--'):
							_prefs.setCharPref(_x, _valueDefault);
							break;
					}
				}
				
			//	vars
			//	====
				for (var _x in __default_vars)
				{
					var 
						_valueNow = _prefs.getCharPref(_x),
						_valueDefault = __default_vars[_x]
					;
					
					switch (true)
					{
						case (!(_valueNow > '')):
						case (_valueNow == '--def--'):
							_prefs.setCharPref(_x, _valueDefault);
							break;
					}
				}
		})();
		

		//	evernote remote
		
	//	namespace
	//	=========
		__readable_by_evernote.__evernote_bootstrap =
		{
            /* predefined */
            'servers':                  false,
            'server_main':              '',     /* ends in slash */
            'server_china':             '',     /* ends in slash */
            
            /* set on load */
            'saved_server':             false,  /* main | china */
            'client_locale':            false,
            'has_chinese_locale':       false,
            'simulate_chinese_locale':  false,
            
            /* set on bootstrap() */
            'rpc__userStore':           false,
            'profiles':                 false,
            'profiles_as_string':       false,
            'server':                   false,
            'remote_domain':            false,  /* ends in slash */
            'remote_domain_marketing':  false,  /* ends in slash */
            'connected':                false
		};

        
    //  servers
    //  =======
        __readable_by_evernote.__evernote_bootstrap.servers = 
        {
            'live':
            {
                'main':      'https://www.evernote.com/',
                'china':     'https://app.yinxiang.com/'
            },
            'stage':
            {
                'main':      'https://stage.evernote.com/',
                'china':     'https://stage-china.evernote.com/'
            }
        };
        
        
    //  bootstrap
    //  =========
        __readable_by_evernote.__evernote_bootstrap.bootstrap = function (_onSuccess, _onFailure)
        {
            //  already connected
            //  =================
                if (__readable_by_evernote.__evernote_bootstrap.connected) { _onSuccess(); return; }
        
        
            //  figure out order
            //  ================
                var _order = [];
                switch (true)
                {
                    case (__readable_by_evernote.__evernote_bootstrap.saved_server == 'main'):      _order = ['main', 'china']; break;
                    case (__readable_by_evernote.__evernote_bootstrap.saved_server == 'china'):     _order = ['china', 'main']; break;
                    case (__readable_by_evernote.__evernote_bootstrap.has_chinese_locale):          _order = ['china', 'main']; break;
                    case (__readable_by_evernote.__evernote_bootstrap.simulate_chinese_locale):     _order = ['china', 'main']; break;
                    default:                                                                        _order = ['main', 'china']; break;
                }

                
            //  try connect to one
            //  ==================
                var _try_connect_to_one = function (_order_number)
                {
                    //  invalid number; failed
                    //  ======================
                        if (_order[_order_number]); else
                        {
                            //  failed
                            _onFailure('connection / invalid');
                            
                            //  break
                            return;
                        };

                    
                    //  try current number
                    //  ==================
                        var _rpcBootstrapClient = new __readable_by_evernote.JSONRpcClient
                        (
                            function ()
                            { 
                                //	error / timeout
                                //  ===============
                                    if (this.UserStore); else
                                    {
                                        //  next -- will kill self on invalid
                                        _try_connect_to_one(_order_number + 1);
                                    
                                        //  break
                                        return;
                                    }

                                    
                                //	set conected
                                //  ============
                                    __readable_by_evernote.__evernote_bootstrap.connected = true;
                                    __readable_by_evernote.__evernote_bootstrap.rpc__userStore = this;

                                
                                //  get profiles
                                //  ============
                                    __readable_by_evernote.__evernote_bootstrap.rpc__userStore.UserStore.getBootstrapInfo
                                    (
                                        function (_rpc_result, _rpc_exception)
                                        {
                                            var 
                                                _bootstrapInfoResult = _rpc_result,
                                                _bootstrapInfoError = _rpc_exception
                                            ;
                                        
                                            //  error
                                            //  =====
                                                if (_bootstrapInfoError)
                                                {
                                                    _bootstrapInfoResult = {
                                                        'profiles': {
                                                           'list': [ {
                                                                'name':         'Evernote',
                                                                
                                                                'setName':      true,
                                                                'setSettings':  true,
                                                                
                                                                'settings': {
                                                                    'marketingUrl': 'http://www.evernote.com',
                                                                    'serviceHost':  'www.evernote.com',
                                                                    'supportUrl':   'https://evernote.com/contact/support/'
                                                                }
                                                            } ] 
                                                        }
                                                    };
                                                }
                                            
                                            
                                            //  result
                                            //  ======
                                            
                                                //  set profiles
                                                //  ============
                                                    __readable_by_evernote.__evernote_bootstrap.profiles = _bootstrapInfoResult.profiles.list;
                                                
                                                
                                                //  profiles as string
                                                //  ==================
                                                    __readable_by_evernote.__evernote_bootstrap.profiles_as_string = '';
                                                    for (var zz=0,_zz=__readable_by_evernote.__evernote_bootstrap.profiles.length; zz<_zz; zz++)
                                                        { __readable_by_evernote.__evernote_bootstrap.profiles_as_string += '_' + __readable_by_evernote.__evernote_bootstrap.getProfileName__short(__readable_by_evernote.__evernote_bootstrap.profiles[zz]); }
                                                    __readable_by_evernote.__evernote_bootstrap.profiles_as_string = __readable_by_evernote.__evernote_bootstrap.profiles_as_string.substr(1);
                                                
                                                
                                                //  select first
                                                //  ============
                                                    __readable_by_evernote.__evernote_bootstrap.server = __readable_by_evernote.__evernote_bootstrap.getProfileName__long(__readable_by_evernote.__evernote_bootstrap.profiles[0]);
                                                    __readable_by_evernote.__evernote_bootstrap.remote_domain = __readable_by_evernote.__evernote_bootstrap['server_' + __readable_by_evernote.__evernote_bootstrap.server];
                                                    __readable_by_evernote.__evernote_bootstrap.remote_domain_marketing = __readable_by_evernote.__evernote_bootstrap.profiles[0].settings.marketingUrl + '/';
                                                
                                                
                                                //  first different than saved -- switch
                                                //  ==========================
                                                    if (true
                                                        && (__readable_by_evernote.__evernote_bootstrap.profiles_as_string == 'in_cn')
                                                        && (__readable_by_evernote.__evernote_bootstrap.server == 'main')
                                                        && (__readable_by_evernote.__evernote_bootstrap.saved_server == 'china')
                                                    ){
                                                        __readable_by_evernote.__evernote_bootstrap.server = 'china';
                                                        __readable_by_evernote.__evernote_bootstrap.remote_domain = __readable_by_evernote.__evernote_bootstrap['server_' + 'china'];
                                                        __readable_by_evernote.__evernote_bootstrap.remote_domain_marketing = __readable_by_evernote.__evernote_bootstrap.profiles[1].settings.marketingUrl + '/';
                                                        __readable_by_evernote.__evernote_bootstrap.profiles_as_string == 'cn_in';
                                                    }
                                                    else if (true
                                                        && (__readable_by_evernote.__evernote_bootstrap.profiles_as_string == 'cn_in')
                                                        && (__readable_by_evernote.__evernote_bootstrap.server == 'china')
                                                        && (__readable_by_evernote.__evernote_bootstrap.saved_server == 'main')
                                                    ){
                                                        __readable_by_evernote.__evernote_bootstrap.server = 'main';
                                                        __readable_by_evernote.__evernote_bootstrap.remote_domain = __readable_by_evernote.__evernote_bootstrap['server_' + 'main'];
                                                        __readable_by_evernote.__evernote_bootstrap.remote_domain_marketing = __readable_by_evernote.__evernote_bootstrap.profiles[1].settings.marketingUrl + '/';
                                                        __readable_by_evernote.__evernote_bootstrap.profiles_as_string == 'in_cn';
                                                    }
                                                
                                                
                                                //  run sucess
                                                //  ==========
                                                    _onSuccess();
                                        },
                                        (__readable_by_evernote.__evernote_bootstrap.simulate_chinese_locale ? 'zh_cn' : __readable_by_evernote.__evernote_bootstrap.client_locale)
                                    );
                            }, 
                            __readable_by_evernote.__evernote_bootstrap['server_' + _order[_order_number]] + 'json'
                        );
                };

                
            //  try first
            //  =========
                _try_connect_to_one(0);
        };
        
        
    //  profile name
    //  ============
        __readable_by_evernote.__evernote_bootstrap.getProfileName__short = function (_profile)
        {
            switch (_profile.name.toLowerCase())
            {
                case 'evernote':            return 'in';
                case 'evernote-china':      return 'cn';
            }
            
            return '';
        };
        
        __readable_by_evernote.__evernote_bootstrap.getProfileName__long = function (_profile)
        {
            switch (_profile.name.toLowerCase())
            {
                case 'evernote':            return 'main';
                case 'evernote-china':      return 'china';
            }
            
            return '';
        };
    
    
    //  set locale
    //  ==========
        __readable_by_evernote.__evernote_bootstrap.setLocale = function (_browser_locale)
        {
            //  which
            var _locale = _browser_locale;
                _locale = _locale.replace(/[-]/gi, '_');
                _locale = _locale.replace(/\s+/gi, '');
                _locale = _locale.toLowerCase()
            
            //   set
            __readable_by_evernote.__evernote_bootstrap.client_locale = _locale;
            
            //  chinese?
            __readable_by_evernote.__evernote_bootstrap.has_chinese_locale = ('|zh|zh_cn|zh_hans|zh_hans_cn|'.indexOf('|'+_locale+'|') > -1);
        };
    
    
    //  QA testing
    //  ==========
    
        __readable_by_evernote.__evernote_bootstrap.set_servers_to_stage = function ()
        {
            __readable_by_evernote.__evernote_bootstrap['server_main'] = __readable_by_evernote.__evernote_bootstrap.servers['stage']['main'];
            __readable_by_evernote.__evernote_bootstrap['server_china'] = __readable_by_evernote.__evernote_bootstrap.servers['stage']['china'];
        };
        
        __readable_by_evernote.__evernote_bootstrap.set_servers_to_live = function ()
        {
            __readable_by_evernote.__evernote_bootstrap['server_main'] = __readable_by_evernote.__evernote_bootstrap.servers['live']['main'];
            __readable_by_evernote.__evernote_bootstrap['server_china'] = __readable_by_evernote.__evernote_bootstrap.servers['live']['china'];
        };

        __readable_by_evernote.__evernote_bootstrap.set_simulate_chinese_locale = function ()
        {
            __readable_by_evernote.__evernote_bootstrap.simulate_chinese_locale = true;
        };
        
        __readable_by_evernote.__evernote_bootstrap.set_do_not_simulate_chinese_locale = function ()
        {
            __readable_by_evernote.__evernote_bootstrap.simulate_chinese_locale = false;
        };

        __readable_by_evernote.__evernote_bootstrap.disconnect = function ()
        {
            __readable_by_evernote.__evernote_bootstrap['connected'] =      false;
            __readable_by_evernote.__evernote_bootstrap['profiles'] =       false;
            __readable_by_evernote.__evernote_bootstrap['server'] =         false;
            __readable_by_evernote.__evernote_bootstrap['remote_domain'] =  false;
            __readable_by_evernote.__evernote_bootstrap['rpc__userStore'] = false;
        };
        
		
	//	included code
	//	json-rpc must exist in context

	
	//	namespace
	//	=========
		__readable_by_evernote.__evernote_remote =
		{
            /* predefined */
			'api__key': 		'en-clearly',
			'api__secret': 		'6798bce902c29b73',

            /* set on init */
            'setting__related_notes':   '',
            'setting__smart_filing':    '',
            
            'setting__clip_tag':        '',
            'setting__clip_notebook':   '',
            
            /* stores */
            'store__id_to_guid':            {},
            'store__id_to_info':            {},
            'store__id_to_recommendation':  {},
            
            /* set on login */
			'rpc__userStore': 	false,
			'rpc__noteStore': 	false,
			
			'user__authToken': 	false,
			'user__shardId': 	false,
			'user__privilege': 	false,
            'user__username':   false,
            'user__email':      false,
			
			'is__connected': 	false,
			'is__loggedIn': 	false
		};
        
	
	//	connect
	//	=======
		__readable_by_evernote.__evernote_remote.connect = function (_onSuccess, _onFailure)
		{
            //  bootstrap first
            //  ===============
                __readable_by_evernote.__evernote_bootstrap.bootstrap
                (
                    function ()
                    {
                        //  bootstrap succesfull
                        //  ====================
                        try
                        {
                            var _rpcUserStoreClient = new __readable_by_evernote.JSONRpcClient
                            (
                                function ()
                                {
                                    //	error / timeout
                                    //  ===============
                                        if (this.UserStore); else
                                        {
                                            //  fail
                                            _onFailure('connection / invalid');
                                            
                                            //  break
                                            return;
                                        }
                                
                                    //	set conected
                                    //  ============
                                        __readable_by_evernote.__evernote_remote.is__connected = true;
                                        __readable_by_evernote.__evernote_remote.rpc__userStore = this;
                                    
                                    //  run success
                                    //  ===========
                                        _onSuccess();
                                }, 
                                __readable_by_evernote.__evernote_bootstrap.remote_domain + 'json'
                            );
                        }
                        catch (_error) { _onFailure('connection / invalid'); return; }
                    },
                    function () { _onFailure('connection / invalid'); }
                );
		};

	
	//	logout
	//	======
		__readable_by_evernote.__evernote_remote.logout = function ()
		{
			__readable_by_evernote.__evernote_remote['rpc__userStore'] = 	false;
			__readable_by_evernote.__evernote_remote['rpc__noteStore'] = 	false;

			__readable_by_evernote.__evernote_remote['user__authToken'] = 	false;
			__readable_by_evernote.__evernote_remote['user__shardId'] = 	false;
			__readable_by_evernote.__evernote_remote['user__privilege'] = 	false;
			__readable_by_evernote.__evernote_remote['user__username'] = 	false;
			__readable_by_evernote.__evernote_remote['user__email'] = 	    false;

			__readable_by_evernote.__evernote_remote['is__connected'] = 	false;
			__readable_by_evernote.__evernote_remote['is__loggedIn'] = 	    false;
		};
		
		
	//	login
	//	=====
		__readable_by_evernote.__evernote_remote.login = function (_user, _pass, _onSuccess, _onFailure)
		{
            //  login function
            //  ===============
                var _loginFunction = function ()
                {
                    //	login
                    //	=====
                        __readable_by_evernote.__evernote_remote.rpc__userStore.UserStore.authenticate
                        (
                            function (_rpc_result, _rpc_exception)
                            {
                                var 
                                    _loginResult = _rpc_result,
                                    _loginError = _rpc_exception
                                ;
                            
                                //  error
                                //  =====
                                
                                    if (_loginError)
                                    {
                                        //	unknown error
                                        //	=============
                                            switch (true)
                                            {
                                                case (!(_loginError.trace)):
                                                case (!(_loginError.trace.indexOf)):
                                                case (!(_loginError.trace.indexOf(')') > -1)):
                                                    _onFailure('login / exception / no trace');
                                                    return;
                                            }
                                        
                                        //	figure out error
                                        //	================
                                            var _trace = _loginError.trace.substr(0, _loginError.trace.indexOf(')')+1);
                                            switch (_trace)
                                            {
                                                case 'EDAMUserException(errorCode:INVALID_AUTH, parameter:username)':
                                                    _onFailure('username');
                                                    return;

                                                case 'EDAMUserException(errorCode:INVALID_AUTH, parameter:password)':
                                                    _onFailure('password');
                                                    return;
                                            }

                                        //	could not figure out error
                                        //	==========================
                                            _onFailure('login / exception / unknown');
                                            return;
                                    }
                                    
                                
                                //  result
                                //  ======
                                    
                                    //	check
                                    //	=====
                                        switch (true)
                                        {
                                            case (!(_loginResult.authenticationToken)):
                                            case (!(_loginResult.user)):
                                            case (!(_loginResult.user.shardId)):
                                                _onFailure('login / invalid result');
                                                return;
                                        }

                                        
                                    //	set userInfo
                                    //	============
                                        __readable_by_evernote.__evernote_remote.user__authToken = 		_loginResult.authenticationToken;
                                        __readable_by_evernote.__evernote_remote.user__shardId = 		_loginResult.user.shardId;
                                        __readable_by_evernote.__evernote_remote.user__privilege = 		_loginResult.user.privilege.value;
                                        __readable_by_evernote.__evernote_remote.user__name = 		    _loginResult.user.name;
                                        __readable_by_evernote.__evernote_remote.user__username = 		_loginResult.user.username;
                                        __readable_by_evernote.__evernote_remote.user__email = 		    _loginResult.user.email;
                                    
                                        __readable_by_evernote.__evernote_remote.is__loggedIn = true;


                                    //	note store
                                    //	==========
                                    
                                        //	make connection
                                        var _rpcNoteStoreClient = new __readable_by_evernote.JSONRpcClient
                                        (
                                            function ()
                                            {
                                                //	error / timeout
                                                //  ===============
                                                    if (this.NoteStore); else
                                                    {
                                                        //  failed
                                                        _onFailure('login / note store / invalid');
                                                        
                                                        //  break
                                                        return;
                                                    }

                                                //  set connected
                                                //  =============
                                                    __readable_by_evernote.__evernote_remote.rpc__noteStore = this;
                                                    
                                                //  run success
                                                //  ===========
                                                    _onSuccess();
                                            },
                                            __readable_by_evernote.__evernote_bootstrap.remote_domain + 'shard/' + __readable_by_evernote.__evernote_remote.user__shardId + '/json'
                                        );
                                        
                            },
                            _user, 						                        	// username
                            _pass, 			                        				// password
                            __readable_by_evernote.__evernote_remote.api__key,		// consumer key
                            __readable_by_evernote.__evernote_remote.api__secret	// consumer secret
                        );
                };

                
			//	allready connected, connect, or error
			//	=====================================
				if (__readable_by_evernote.__evernote_remote.is__connected)
                {
                    //  do
                    _loginFunction();
                }
                else
				{
					//	connect
					__readable_by_evernote.__evernote_remote.connect
                    (
                        function () { _loginFunction(); },
                        function () { _onFailure('connection / invalid'); }
                    );
				}
        };
	
	
	//	clip
	//	====
		__readable_by_evernote.__evernote_remote.clip = function (_id, _url, _title, _body, _onSuccess, _onFailure)
		{
			//	preliminary check
			//	=================
				switch (true)
				{
					case (!(__readable_by_evernote.__evernote_remote.rpc__noteStore.NoteStore)):
					case (!(__readable_by_evernote.__evernote_remote.is__connected)):
					case (!(__readable_by_evernote.__evernote_remote.is__loggedIn)):
						_onFailure('login');
						return;
				}

                
            //  smart filing -- and the recommendation hasn't been requested yet
            //  ============
                if (__readable_by_evernote.__evernote_remote.setting__smart_filing == 'disabled'); else
                {
                    switch (true)
                    {
                        case (__readable_by_evernote.__evernote_remote.setting__smart_filing == 'enabled'):
                        case (__readable_by_evernote.__evernote_remote.setting__smart_filing == 'just_notebooks'):
                        case (__readable_by_evernote.__evernote_remote.setting__smart_filing == 'just_tags'):
                            if (__readable_by_evernote.__evernote_remote.store__id_to_recommendation[_id]); else
                            {
                                __readable_by_evernote.__evernote_remote.get_recommendation
                                (
                                    _id,
                                    _url,
                                    _body,
                                    function ()
                                    {
                                        //  do it again; recommendatin was added to the global store
                                        __readable_by_evernote.__evernote_remote.clip(
                                            _id, _url, _title, _body, _onSuccess, _onFailure
                                        );
                                    },
                                    function ()
                                    {
                                        //  set to dummy; only try this once
                                        __readable_by_evernote.__evernote_remote.store__id_to_recommendation[_id] = true;
                                        
                                        //  do with no smart filing
                                        __readable_by_evernote.__evernote_remote.clip(
                                            _id, _url, _title, _body, _onSuccess, _onFailure
                                        );
                                    }
                                );
                                return;
                            }
                    }
				}
                
			//	create tags array
			//	=================
				var
                    _tags = __readable_by_evernote.__evernote_remote.setting__clip_tag,
					_tags_we_should_set = _tags.split(','),
                    
					_tags_we_will_set__array = [],
                    _tags_we_will_set__object = {}
				;

				for (var i=0, _i=_tags_we_should_set.length; i<_i;i++)
				{
					var _t = _tags_we_should_set[i].replace(/^ /, '').replace(/ $/, '')
					
                    //  invalid
                    if (_t > ''); else { continue; }
					
                    //  already in there
                    if (_tags_we_will_set__object[_t]) { continue; }
                    
                    //  set
					_tags_we_will_set__array.push(_t);
                    _tags_we_will_set__object[_t] = true;
				}

                
            //  Smart Filing :: set tags
            //  ============
                if (__readable_by_evernote.__evernote_remote.setting__smart_filing == 'enabled' || __readable_by_evernote.__evernote_remote.setting__smart_filing == 'just_tags')
                {
                    if (__readable_by_evernote.__evernote_remote.store__id_to_recommendation[_id].tags && __readable_by_evernote.__evernote_remote.store__id_to_recommendation[_id].tags.list)
                    {
                        for (var i=0, _t='', _i=__readable_by_evernote.__evernote_remote.store__id_to_recommendation[_id].tags.list.length; i<_i;i++)
                        {
                            //  get
                            _t = __readable_by_evernote.__evernote_remote.store__id_to_recommendation[_id].tags.list[i].name;
                            
                            //  invalid
                            if (_t > ''); else { continue; }
                            
                            //  already in there
                            if (_tags_we_will_set__object[_t]) { continue; }
                            
                            //  set
                            _tags_we_will_set__array.push(_t);
                            _tags_we_will_set__object[_t] = true;
                        }
                    }
                }
				
				
			//	try and clip
			//	============

                //  filing info
                //  ===========
                    var _filingInfo =
                    {
                        'tagNameList': _tags_we_will_set__array,
                        'createTags': true,
                        'source': 'Clearly'
                    };                    
            
                    //  previous version
                    if (_id in __readable_by_evernote.__evernote_remote.store__id_to_guid)
                    {
                        _filingInfo['originalNoteGuid'] = __readable_by_evernote.__evernote_remote.store__id_to_guid[_id];
                    }
            
                    //  set notebook
                    if (__readable_by_evernote.__evernote_remote.setting__clip_notebook > '')
                    {
                        _filingInfo['notebookName'] = __readable_by_evernote.__evernote_remote.setting__clip_notebook;
                        _filingInfo['createNotebook'] = true;
                    }
            
                    //  Smart Filing :: set notebook
                    if (__readable_by_evernote.__evernote_remote.setting__smart_filing == 'enabled' || __readable_by_evernote.__evernote_remote.setting__smart_filing == 'just_notebooks')
                    {
                        if (__readable_by_evernote.__evernote_remote.store__id_to_recommendation[_id].notebook && __readable_by_evernote.__evernote_remote.store__id_to_recommendation[_id].notebook.name)
                        {
                            _filingInfo['notebookName'] = __readable_by_evernote.__evernote_remote.store__id_to_recommendation[_id].notebook.name;
                            _filingInfo['createNotebook'] = false;
                        }
                    }


                    //  clip do
                    //  =======
                        var _clip_do = function (_default_notebook_name)
                        {
                            //  set default notebook
                            if (_filingInfo['notebookName']); else
                            {
                                _filingInfo['notebookName'] = _default_notebook_name;
                                _filingInfo['createNotebook'] = false;
                            }
                            
                            //  clip
                            __readable_by_evernote.__evernote_remote.rpc__noteStore.NoteStoreExtra.clipNote
                            (
                                function (_rpc_result, _rpc_exception)
                                {
                                    var 
                                        _clipResult = _rpc_result,
                                        _clipError = _rpc_exception
                                    ;
                                
                                    //  error
                                    //  =====
                                    
                                        if (_clipError)
                                        {
                                            //  Firebug.Console.log(_clipError);
                                            //  console.log(_clipError);
                                            
                                            //	unknown error
                                            //	=============
                                                switch (true)
                                                {
                                                    case (!(_clipError.trace)):
                                                    case (!(_clipError.trace.indexOf)):
                                                    case (!(_clipError.trace.indexOf(')') > -1)):
                                                        _onFailure('clip / exception / no trace');
                                                        return;
                                                }
                                            
                                            //	figure out error
                                            //	================
                                                var _trace = _clipError.trace.substr(0, _clipError.trace.indexOf(')')+1);
                                                switch (_trace)
                                                {
                                                    case 'EDAMUserException(errorCode:BAD_DATA_FORMAT, parameter:authenticationToken)':
                                                    case 'EDAMSystemException(errorCode:INVALID_AUTH, message:authenticationToken)':
                                                    case 'EDAMUserException(errorCode:AUTH_EXPIRED, parameter:authenticationToken)':
                                                        _onFailure('login');
                                                        return;
                                                }

                                            //	could not figure out error
                                            //	==========================
                                                _onFailure('clip / exception / unknown');
                                                return;
                                        }

                                        
                                    //  result
                                    //  ======

                                        //	check
                                        //	=====
                                            switch (true)
                                            {
                                                case (!(_clipResult > '')):
                                                case (!(_clipResult.split('-').length == 5)):
                                                    _onFailure('clip / invalid result');
                                                    return;
                                            }
                                            
                                        //  store this version
                                        //  ==================
                                            __readable_by_evernote.__evernote_remote.store__id_to_guid[_id] = _clipResult;
                                            __readable_by_evernote.__evernote_remote.store__id_to_info[_id] =
                                            {
                                                'guid':             __readable_by_evernote.__evernote_remote.store__id_to_guid[_id],

                                                'tag_names':        _filingInfo['tagNameList'],
                                                'notebook_name':    _filingInfo['notebookName'],

                                                'url_view':         ''
                                                                    + __readable_by_evernote.__evernote_bootstrap.remote_domain
                                                                    + 'setAuthToken?auth=' + encodeURIComponent(__readable_by_evernote.__evernote_remote.user__authToken)
                                                                    + '&redirect=' + encodeURIComponent(''
                                                                        + __readable_by_evernote.__evernote_bootstrap.remote_domain
                                                                        + 'view/'
                                                                        + __readable_by_evernote.__evernote_remote.store__id_to_guid[_id]
                                                                    ),

                                                'url_edit':         ''
                                                                    + __readable_by_evernote.__evernote_bootstrap.remote_domain
                                                                    + 'setAuthToken?auth=' + encodeURIComponent(__readable_by_evernote.__evernote_remote.user__authToken)
                                                                    + '&redirect=' + encodeURIComponent(''
                                                                        + __readable_by_evernote.__evernote_bootstrap.remote_domain
                                                                        + 'edit/'
                                                                        + __readable_by_evernote.__evernote_remote.store__id_to_guid[_id]
                                                                    )
                                                                    
                                            };

                                            
                                        //	success
                                        //	=======
                                            _onSuccess();
                                },
                                __readable_by_evernote.__evernote_remote.user__authToken, 	// authentification token
                                _filingInfo,                                                // filing information
                                (_title > '' ? _title : _url), 								// title
                                _url, 														// url
                                _body														// html
                            );
                        };
                        
                    //  do the actual clip
                    //  ==================
                        if (_filingInfo['notebookName'])
                        {
                            _clip_do('');
                        }
                        else
                        {
                            __readable_by_evernote.__evernote_remote.rpc__noteStore.NoteStore.getDefaultNotebook
                            (
                                function (_rpc_result, _rpc_exception)
                                {
                                    var 
                                        _getResult = _rpc_result,
                                        _getError = _rpc_exception
                                    ;
                                
                                    //  set default
                                    var _default_notebook = (_getError ? '' : ((_getResult && _getResult.name) ? _getResult.name : ''));
                                        
                                    //  do clip
                                    _clip_do(_default_notebook);
                                },
                                __readable_by_evernote.__evernote_remote.user__authToken 	// authentification token
                            );
                        }
		};

        
	//	recommendation
	//	==============
		__readable_by_evernote.__evernote_remote.get_recommendation = function (_id, _url, _body, _onSuccess, _onFailure)
		{
			//	preliminary check
			//	=================
				switch (true)
				{
					case (!(__readable_by_evernote.__evernote_remote.rpc__noteStore.NoteStore)):
					case (!(__readable_by_evernote.__evernote_remote.is__connected)):
					case (!(__readable_by_evernote.__evernote_remote.is__loggedIn)):
						_onFailure('login');
						return;
				}

                
            //  _body -- just text
            //  =====
            
                //  just text
                _body = _body.replace(/<[^>]+?>/gi, ' ');
                
                //  remove spaces
                _body = _body.replace(/\s+/gi, ' ');
                _body = _body.replace(/^\s+/gi, '');
                
                //  cut off
                _body = _body.substr(0, 5000);

                
            //  log
            //  ===
            /*  console.log('NoteStoreExtra.getFilingRecommendations Called with:');
                console.log({
                    'url': _url,
                    'text': _body,
                    'relatedNotesResultSpec':
                    {
                        'includeTitle': true,
                        'includeSnippet': true,
                        
                        'includeCreated': true,
                        
                        'includeLargestResourceMime': true,
                        'includeLargestResourceSize': true,

                        'includeAttributes': false,
                        'includeTagNames': false
                    }
                }); */
                
                
            //	try and get -- a FilingReccomendation object
			//	===========
                __readable_by_evernote.__evernote_remote.rpc__noteStore.NoteStoreExtra.getFilingRecommendations
                (
                    function (_rpc_result, _rpc_exception)
                    {
                        var 
                            _getResult = _rpc_result,
                            _getError = _rpc_exception
                        ;
                    
                        //  error
                        //  =====
                        
                            if (_getError)
                            {
                                //  Firebug.Console.log(_getError);
                                //  console.log(_getError);
                                
                                //	unknown error
                                //	=============
                                    switch (true)
                                    {
                                        case (!(_getError.trace)):
                                        case (!(_getError.trace.indexOf)):
                                        case (!(_getError.trace.indexOf(')') > -1)):
                                            _onFailure('get_recommendation / exception / no trace');
                                            return;
                                    }
                                
                                //	figure out error
                                //	================
                                    var _trace = _getError.trace.substr(0, _getError.trace.indexOf(')')+1);
                                    switch (_trace)
                                    {
                                        case 'EDAMUserException(errorCode:BAD_DATA_FORMAT, parameter:authenticationToken)':
                                        case 'EDAMSystemException(errorCode:INVALID_AUTH, message:authenticationToken)':
                                        case 'EDAMUserException(errorCode:AUTH_EXPIRED, parameter:authenticationToken)':
                                            _onFailure('login');
                                            return;
                                    }

                                //	could not figure out error
                                //	==========================
                                    _onFailure('get_recommendation / exception / unknown');
                                    return;
                            }
                            
                        //  result
                        //  ======
                        
                            //	check
                            //	=====
                                switch (true)
                                {
                                    case (!(_getResult.notebook) && !(_getResult.tags) && !(_getResult.relatedNotes)):
                                        _onFailure('get_recommendation / invalid result');
                                        return;
                                }
                            
                            //  fill in
                            //  =======
                                if (_getResult.notebook); else { _getResult.notebook = false; }
                                if (_getResult.tags); else { _getResult.tags = false; }
                                if (_getResult.relatedNotes); else { _getResult.relatedNotes = false; }
                            
                            //  add absoluteURLs
                            //  ================
                                if (_getResult.relatedNotes && _getResult.relatedNotes.list && _getResult.relatedNotes.list.length > 0)
                                {
                                    for (var i=0, _i=_getResult.relatedNotes.list.length; i<_i; i++)
                                    {
                                        //  absoluteURL__thumbnail
                                        _getResult.relatedNotes.list[i]['absoluteURL__thumbnail'] = ''
                                            + __readable_by_evernote.__evernote_bootstrap.remote_domain
                                            + 'shard/'
                                            + __readable_by_evernote.__evernote_remote.user__shardId + '/'
                                            + 'thm/note/'
                                            + _getResult.relatedNotes.list[i].guid
                                            + '?auth=' + encodeURIComponent(__readable_by_evernote.__evernote_remote.user__authToken)
                                            + '&size=75'
                                        ;
                                        
                                        //  absoluteURL__noteView
                                        _getResult.relatedNotes.list[i]['absoluteURL__noteView'] = ''
                                            + __readable_by_evernote.__evernote_bootstrap.remote_domain
                                            + 'setAuthToken?auth=' + encodeURIComponent(__readable_by_evernote.__evernote_remote.user__authToken)
                                            + '&redirect=' + encodeURIComponent(''
                                                + __readable_by_evernote.__evernote_bootstrap.remote_domain
                                                + 'view/'
                                                + _getResult.relatedNotes.list[i].guid
                                            )
                                        ;
                                    }
                                }
                            
                            //  store result
                            //  ============
                                __readable_by_evernote.__evernote_remote.store__id_to_recommendation[_id] = _getResult;
                                
                            //	success
                            //	=======
                                _onSuccess(_getResult);
                                
                            //  log
                            //  ===
                            //  console.log(_getResult);
                    },
                    __readable_by_evernote.__evernote_remote.user__authToken, 	// authentification token
                    {                                                           // recommendation request
                        'url': _url,
                        'text': _body,
                        'relatedNotesResultSpec':
                        {
                            'includeTitle': true,
                            'includeSnippet': true,
                            
                            'includeCreated': true,
                            
                            'includeLargestResourceMime': true,
                            'includeLargestResourceSize': true,

                            'includeAttributes': false,
                            'includeTagNames': false
                        }
                    }															
                );
        };


	//	recommendation
	//	==============
		__readable_by_evernote.__evernote_remote.get_notebooks = function (_onSuccess, _onFailure)
		{
			//	preliminary check
			//	=================
				switch (true)
				{
					case (!(__readable_by_evernote.__evernote_remote.rpc__noteStore.NoteStore)):
					case (!(__readable_by_evernote.__evernote_remote.is__connected)):
					case (!(__readable_by_evernote.__evernote_remote.is__loggedIn)):
						_onFailure('login');
						return;
				}

            //	try and get -- a list of notebooks
			//	===========
                __readable_by_evernote.__evernote_remote.rpc__noteStore.NoteStore.listNotebooks
                (
                    function (_rpc_result, _rpc_exception)
                    {
                        var 
                            _getResult = _rpc_result,
                            _getError = _rpc_exception
                        ;
                    
                        //  error
                        //  =====
                        
                            if (_getError)
                            {
                                //  Firebug.Console.log(_getError);
                                //  console.log(_getError);
                                
                                //	unknown error
                                //	=============
                                    switch (true)
                                    {
                                        case (!(_getError.trace)):
                                        case (!(_getError.trace.indexOf)):
                                        case (!(_getError.trace.indexOf(')') > -1)):
                                            _onFailure('get_notebooks / exception / no trace');
                                            return;
                                    }
                                
                                //	figure out error
                                //	================
                                    var _trace = _getError.trace.substr(0, _getError.trace.indexOf(')')+1);
                                    switch (_trace)
                                    {
                                        case 'EDAMUserException(errorCode:BAD_DATA_FORMAT, parameter:authenticationToken)':
                                        case 'EDAMSystemException(errorCode:INVALID_AUTH, message:authenticationToken)':
                                        case 'EDAMUserException(errorCode:AUTH_EXPIRED, parameter:authenticationToken)':
                                            _onFailure('login');
                                            return;
                                    }

                                //	could not figure out error
                                //	==========================
                                    _onFailure('get_notebooks / exception / unknown');
                                    return;
                            }
                            
                        //  result
                        //  ======
                        
                            //	check
                            //	=====
                            
                                switch (true)
                                {
                                    case (!(_getResult.list)):
                                    case (!(_getResult.list.length)):
                                        _onFailure('get_notebooks / invalid result');
                                        return;
                                }
                            
                            //	success
                            //	=======
                                _onSuccess(_getResult);
                    },
                    __readable_by_evernote.__evernote_remote.user__authToken 	// authentification token
                );
        };
        
        
    //  disconnect -- QA testing
    //  ==========
        __readable_by_evernote.__evernote_remote.disconnect = function ()
        {
            __readable_by_evernote.__evernote_remote['rpc__userStore'] = 	false;
            __readable_by_evernote.__evernote_remote['rpc__noteStore'] = 	false;
            
            __readable_by_evernote.__evernote_remote['user__authToken'] =	false;
            __readable_by_evernote.__evernote_remote['user__shardId'] = 	false;
            __readable_by_evernote.__evernote_remote['user__privilege'] =	false;
            
            __readable_by_evernote.__evernote_remote['is__connected'] =	    false;
            __readable_by_evernote.__evernote_remote['is__loggedIn'] =  	false;
        };
		/*
 * Evernote.XORCrypt
 * Evernote
 *
 * Created by Pavel Skaldin on 3/7/10
 * Copyright 2010-2012 Evernote Corp. All rights reserved
 */
/**
 * Naive implementation of XOR encryption with padding. It is not meant to be a
 * strong encryption of any kind, just an obfuscation. The algorithm uses a seed
 * string for XORing. The string to be encrypted is first XOR'd with a random
 * number to avoid recognizable patterns; the result is then padded and then
 * XOR'd with the seed string.
 * 
 * Make sure that XORCrypt.LENGTH is larger than the strings you're trying to
 * encrypt.
 * 
 * <pre>
 * Usage: 
 * var enc = Evernote.XORCrypt.encrypt(secret, seed); 
 * var dec = Evernote.XORCrypt.decrypt(enc, seed);
 * </pre>
 */
__readable_by_evernote.__evernote_xor = {
  DELIMIT : ":",
  LENGTH : 128,
  /**
   * Pads string to make it XORCrypt.LENGTH characters in length. Padding is
   * done by prepending the string with random chars from a range of printable
   * ascii chars.
   */
  _padString : function(str) {
    var counter = 0;
    if (str.length < 128) {
      for ( var i = str.length; i <= 128; i++) {
        str = String.fromCharCode(this._randomForChar()) + str;
        counter++;
      }
    }
    return counter + this.DELIMIT + str;
  },
  /**
   * Returns random number that would correspond to a printable ascii char.
   */
  _randomForChar : function() {
    var r = 0;
    var c = 0;
    while (r < 33 || r > 126) {
      // just a waiting... this shouldn't happen frequently
      r = parseInt(Math.random() * 150);
      c++;
    }
    return r;
  },
  /**
   * Returns random non-zero integer.
   */
  _randomNonZero : function() {
    var r = 0;
    while ((r = parseInt(Math.random() * 100)) == 0) {
      // just a waiting... this shouldn't happen frequently
    }
    return r;
  },
  /**
   * Shifts string by XOR'ing it with a number larger than 100 so as to avoid
   * non-printable characters. The resulting string will be prepended with the
   * number used to XOR followed by DELIMITER.
   */
  _shiftString : function(str) {
    var delta = this._randomNonZero() + 100;
    var shifted = [];
    for ( var i = 0; i < str.length; i++) {
      shifted.push(String.fromCharCode(str.charCodeAt(i) + delta));
    }
    return delta + this.DELIMIT + shifted.join("");
  },
  /**
   * Unshifts and returns a shifted string. The argument should be in a format
   * produced by _shitString(), i.e. begin with the shift coefficient followed
   * by DELIMITER, followed by the shifted string.
   */
  _unshiftString : function(str) {
    var delta = parseInt(str.substring(0, str.indexOf(this.DELIMIT)));
    var unshifted = [];
    if (!isNaN(delta)) {
      for ( var i = ((delta + "").length + this.DELIMIT.length); i < str.length; i++) {
        unshifted.push(String.fromCharCode(str.charCodeAt(i) - delta));
      }
    }
    return unshifted.join("");
  },
  /**
   * Encrypts string with a seed string and returns encrypted string padded to
   * be XORCrypt.LENGTH characters long.
   */
  encrypt : function(str, seed) {
    str += "";
    seed += "";
    var newStr = this._padString(this._shiftString(str));
    var enc = [];
    for ( var i = 0; i < newStr.length; i++) {
      var e = newStr.charCodeAt(i);
      for ( var j = 0; j < seed.length; j++) {
        e = seed.charCodeAt(j) ^ e;
      }
      enc.push(String.fromCharCode(e + 100));
    }
    return enc.join("");
  },
  /**
   * Decrypts string using seed string. The seed string has to be the same
   * string that was used in encrypt()ing.
   */
  decrypt : function(str, seed) {
    str += "";
    seed += "";
    var dec = [];
    for ( var i = 0; i < str.length; i++) {
      var e = str.charCodeAt(i) - 100;
      for ( var j = seed.length - 1; j >= 0; j--) {
        e = seed.charCodeAt(j) ^ e;
      }
      dec.push(String.fromCharCode(e));
    }
    var decStr = dec.join("");
    var pad = parseInt(decStr.substring(0, decStr.indexOf(this.DELIMIT)));
    if (!isNaN(pad)) {
      return this._unshiftString(decStr.substring(("" + pad).length
          + this.DELIMIT.length + pad));
    }
    return "";
  }
};


		//	stored credentials
		
	//	get/set stored evernote credentials
	//	requires __evernote_xor present in context
	//	==========================================

	//	get
	//	===
		__readable_by_evernote.__get_stored_evernote_credentials = function ()
		{
			var
				_prefs_service = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService),
				_prefs = _prefs_service.getBranch("extensions.readable-by-evernote.")
			;
		
			var 
				_username = _prefs.getCharPref('storedEvernoteUsername'),
                _server = _prefs.getCharPref('storedEvernoteServer'),
				_password = ''
				
				//_unicodeEncodedPassword = _prefs.getComplexValue("storedEvernotePassword", Components.interfaces.nsISupportsString)
				//_password = (_unicodeEncodedPassword.data > '' ? _unicodeEncodedPassword.data : '')
			;
				
			var 
				_login_service = Components.classes["@mozilla.org/login-manager;1"].getService(Components.interfaces.nsILoginManager),
				_logins = _login_service.findLogins({}, 'chrome://readable-by-evernote', '', 'Evernote Login')
			;

			for (var i=0, _i=_logins.length; i<_i; i++)
			{
				if (_logins[i].username == _username)
					{ _password = _logins[i].password; break; }
			}
				
			switch (true)
			{
				case (!(_username > '')):
				case (!(_password > '')):
					return false;
			}
		
			var _r = {};
				_r.username = _username;
                _r.server = ((_server == 'main' || _server == 'china') ? _server : 'none');
				_r.password = _password;
				//_r.password = __readable_by_evernote.__evernote_xor.decrypt(_password, _r.username);
			return _r;
		};

		
	//	set
	//	===
		__readable_by_evernote.__set_stored_evernote_credentials = function (_o)
		{
			var 
				_prefs_service = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService),
				_prefs = _prefs_service.getBranch("extensions.readable-by-evernote.")
			;
		
			switch (true)
			{
				case (!(_o['username'] > '')):
				case (!(_o['password'] > '')):
					return false;
			}
			
			var 
				_login_service = Components.classes["@mozilla.org/login-manager;1"].getService(Components.interfaces.nsILoginManager),
				_login_info__class = new Components.Constructor("@mozilla.org/login-manager/loginInfo;1", Components.interfaces.nsILoginInfo, "init") 
				_login_info = new _login_info__class('chrome://readable-by-evernote', null, 'Evernote Login', _o.username, _o.password, '', '')
				_logins = _login_service.findLogins({}, 'chrome://readable-by-evernote', '', 'Evernote Login')
			;
			
			_prefs.setCharPref('storedEvernoteServer', __readable_by_evernote.__evernote_bootstrap.server);
			_prefs.setCharPref('storedEvernoteUsername', _o.username);
			
            try
            {
                //  remove all logins
                for (var i=0, _i=_logins.length; i<_i; i++)
                {
                    if (_logins[i].username == _o.username)
                        { _login_service.removeLogin(_logins[i]); break; }
                }
            
                //  add new login
                _login_service.addLogin(_login_info);
            }
            catch (e) { }
			
			//	var _unicodeEncodedPassword = Components.classes["@mozilla.org/supports-string;1"].createInstance(Components.interfaces.nsISupportsString);
			//		_unicodeEncodedPassword.data = __readable_by_evernote.__evernote_xor.encrypt(_o['password'], _o['username']);
			//		_prefs.setComplexValue('storedEvernotePassword', Components.interfaces.nsISupportsString, _unicodeEncodedPassword);
			
			return true;
		};
		
		
	//	logout on next action
	//	=====================
	
		__readable_by_evernote.__get_stored_evernote_logoutOnNextAction = function ()
		{
			var
				_prefs_service = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService),
				_prefs = _prefs_service.getBranch("extensions.readable-by-evernote.")
			;
		
			if (_prefs.getCharPref('storedEvernoteLogoutOnNextAction') > ''); else { return false; }
			if (_prefs.getCharPref('storedEvernoteLogoutOnNextAction') == 'yes'); else { return false; }
			
			//	reset
			_prefs.setCharPref('storedEvernoteLogoutOnNextAction', '');
			
			//	return true
			return true;
		};
	
		__readable_by_evernote.__set_stored_evernote_logoutOnNextAction = function ()
		{
			var
				_prefs_service = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService),
				_prefs = _prefs_service.getBranch("extensions.readable-by-evernote.")
			;

			//	set
			_prefs.setCharPref('storedEvernoteLogoutOnNextAction', 'yes');
		};

	
		//	select theme and size
		
	//	select theme
	//	============
		__readable_by_evernote.__save__select_theme = function (_theme_id)
		{
			//	the themes
			
	//	encode
	//	======
		function __encodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == '') { return 'none'; }
			
			//	encode
			return encodeURIComponent(_string)
				.replace(/!/g, '%21')
				.replace(/'/g, '%27')
				.replace(/\(/g, '%28')
				.replace(/\)/g, '%29')
				.replace(/\*/g, '%2A')
			;
		}

		
	//	decode
	//	======
		function __decodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == 'none') { return ''; }
			
			//	decode
			return decodeURIComponent(_string);
		}
	
	

			
	//	__encodeURIComponentForReadable must be defined

	var __the_themes = 
	{
		'theme-1':
		{
			'text_font': 			__encodeURIComponentForReadable('"PT Serif"'),
			'text_font_header': 	__encodeURIComponentForReadable('"PT Serif"'),
			'text_font_monospace': 	__encodeURIComponentForReadable('Inconsolata'),
			'text_size': 			__encodeURIComponentForReadable('16px'),
			'text_line_height': 	__encodeURIComponentForReadable('1.5em'),
			'box_width': 			__encodeURIComponentForReadable('36em'),
			'color_background': 	__encodeURIComponentForReadable('#f3f2ee'),
			'color_text': 			__encodeURIComponentForReadable('#1f0909'),
			'color_links': 			__encodeURIComponentForReadable('#065588'),
			'text_align': 			__encodeURIComponentForReadable('normal'),
			'base': 				__encodeURIComponentForReadable('theme-1'),
			'footnote_links': 		__encodeURIComponentForReadable('on_print'),
			'large_graphics': 		__encodeURIComponentForReadable('do_nothing'),
			'custom_css': 			__encodeURIComponentForReadable('')
		},
		
		'theme-2':
		{
			'text_font': 			__encodeURIComponentForReadable('Helvetica, Arial'),
			'text_font_header': 	__encodeURIComponentForReadable('Helvetica, Arial'),
			'text_font_monospace': 	__encodeURIComponentForReadable('"Droid Sans Mono"'),
			'text_size': 			__encodeURIComponentForReadable('14px'),
			'text_line_height': 	__encodeURIComponentForReadable('1.5em'),
			'box_width': 			__encodeURIComponentForReadable('42em'),
			'color_background': 	__encodeURIComponentForReadable('#fff'),
			'color_text': 			__encodeURIComponentForReadable('#333'),
			'color_links': 			__encodeURIComponentForReadable('#090'),
			'text_align': 			__encodeURIComponentForReadable('normal'),
			'base': 				__encodeURIComponentForReadable('theme-2'),
			'footnote_links': 		__encodeURIComponentForReadable('on_print'),
			'large_graphics': 		__encodeURIComponentForReadable('do_nothing'),
			'custom_css': 			__encodeURIComponentForReadable('')
		},
		
		'theme-3':
		{
			'text_font': 			__encodeURIComponentForReadable('"PT Serif"'),
			'text_font_header': 	__encodeURIComponentForReadable('"PT Serif"'),
			'text_font_monospace': 	__encodeURIComponentForReadable('Inconsolata'),
			'text_size': 			__encodeURIComponentForReadable('16px'),
			'text_line_height': 	__encodeURIComponentForReadable('1.5em'),
			'box_width': 			__encodeURIComponentForReadable('36em'),
			'color_background': 	__encodeURIComponentForReadable('#2d2d2d'),
			'color_text': 			__encodeURIComponentForReadable('#e3e3e3'),
			'color_links': 			__encodeURIComponentForReadable('#e3e3e3'),
			'text_align': 			__encodeURIComponentForReadable('normal'),
			'base': 				__encodeURIComponentForReadable('theme-3'),
			'footnote_links': 		__encodeURIComponentForReadable('on_print'),
			'large_graphics': 		__encodeURIComponentForReadable('do_nothing'),
			'custom_css': 			__encodeURIComponentForReadable('')
		}
	};
			
			var _to_save = __the_themes[_theme_id];
				_to_save['theme'] = _theme_id;
				
			//	save
			__readable_by_evernote.__save_someStuff(_to_save);
		}

		__readable_by_evernote.__save__select_theme__custom = function ()
		{
			//	the themes
			
	//	encode
	//	======
		function __encodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == '') { return 'none'; }
			
			//	encode
			return encodeURIComponent(_string)
				.replace(/!/g, '%21')
				.replace(/'/g, '%27')
				.replace(/\(/g, '%28')
				.replace(/\)/g, '%29')
				.replace(/\*/g, '%2A')
			;
		}

		
	//	decode
	//	======
		function __decodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == 'none') { return ''; }
			
			//	decode
			return decodeURIComponent(_string);
		}
	
	


			//	to save
			var _to_save = {};
				_to_save['theme'] = 'custom';
				
			//	get custom
			var _vars = __readable_by_evernote.__get_saved__vars();
			__decodeURIComponentForReadable(_vars['custom_theme_options']).replace
			(
				/\[\[=(.*?)\]\[=(.*?)\]\]/gi,
				function (_match, _name, _value) { _to_save[_name] = _value; }
			);
				
			//	save
			__readable_by_evernote.__save_someStuff(_to_save);
		}
		
	
	//	select size
	//	===========
		__readable_by_evernote.__save__select_size = function (_size)
		{
			//	the sizes
			
	//	encode
	//	======
		function __encodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == '') { return 'none'; }
			
			//	encode
			return encodeURIComponent(_string)
				.replace(/!/g, '%21')
				.replace(/'/g, '%27')
				.replace(/\(/g, '%28')
				.replace(/\)/g, '%29')
				.replace(/\*/g, '%2A')
			;
		}

		
	//	decode
	//	======
		function __decodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == 'none') { return ''; }
			
			//	decode
			return decodeURIComponent(_string);
		}
	
	

			
	var __the_sizes = 
	{
		'small':
		{
			'theme-1': '12px',
			'theme-2': '12px',
			'theme-3': '12px',
			'custom':  '12px'
		},
	
		'medium':
		{
			'theme-1': '16px',
			'theme-2': '16px',
			'theme-3': '16px',
			'custom':  '16px'
		},
		
		'large':
		{
			'theme-1': '20px',
			'theme-2': '20px',
			'theme-3': '20px',
			'custom':  '20px'
		}
	};

		
			//	current vars
			var _current_vars = __readable_by_evernote.__get_saved__vars();
			
			//	save
			__readable_by_evernote.__save_someStuff(
				{ 'text_size': __the_sizes[_size][_current_vars['theme']] }
			);
		}

        
	//	select related notes
	//	====================
		__readable_by_evernote.__save__select_related_notes = function (_setting)
		{
			//	encode
			
	//	encode
	//	======
		function __encodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == '') { return 'none'; }
			
			//	encode
			return encodeURIComponent(_string)
				.replace(/!/g, '%21')
				.replace(/'/g, '%27')
				.replace(/\(/g, '%28')
				.replace(/\)/g, '%29')
				.replace(/\*/g, '%2A')
			;
		}

		
	//	decode
	//	======
		function __decodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == 'none') { return ''; }
			
			//	decode
			return decodeURIComponent(_string);
		}
	
	

		
			//	save
			__readable_by_evernote.__save_someStuff(
				{ 'related_notes': __encodeURIComponentForReadable(_setting) }
			);
		}
        

		//	translations
		
	__readable_by_evernote.__get_translations = function ()
	{
		//	include
		
	//	encode
	//	======
		function __encodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == '') { return 'none'; }
			
			//	encode
			return encodeURIComponent(_string)
				.replace(/!/g, '%21')
				.replace(/'/g, '%27')
				.replace(/\(/g, '%28')
				.replace(/\)/g, '%29')
				.replace(/\*/g, '%2A')
			;
		}

		
	//	decode
	//	======
		function __decodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == 'none') { return ''; }
			
			//	decode
			return decodeURIComponent(_string);
		}
	
	


		//	translations include
		
	function __get_translations_inside()
	{
		var 
			_return = {},
			_list =
			[
				'menu__close__tooltip',
				'menu__clip_to_evernote__tooltip',
                'menu__highlight_to_evernote__tooltip',
				'menu__print__tooltip',
				'menu__settings__tooltip',
				'menu__fitts__tooltip',
				
				'rtl__main__label',
				'rtl__ltr__label',
				'rtl__rtl__label',
				
				'blank_error__heading',
				'blank_error__body',
				
                'related_notes__title',
                'related_notes__disable_short',
                'related_notes__disable_long',

                'filing_info__title_normal',
                'filing_info__title_smart',
                'filing_info__default_notebook',
                'filing_info__view',
                'filing_info__edit',
                'filing_info__sentence',
                'filing_info__sentence_no_tags',
                'filing_info__sentence_and',
                'filing_info__sentence_other_tags',
                
				'evernote_clipping',
				'evernote_clipping_failed',
				
				'evernote_login__heading',
				'evernote_login__spinner',
				'evernote_login__create_account',
				'evernote_login__button_do__label',
				'evernote_login__button_cancel__label',
				
				'evernote_login__username__label',
				'evernote_login__password__label',
				'evernote_login__rememberMe__label',
				
				'evernote_login__username__error__required',
				'evernote_login__username__error__length',
				'evernote_login__username__error__format',
				'evernote_login__username__error__invalid',
				
				'evernote_login__password__error__required',
				'evernote_login__password__error__length',
				'evernote_login__password__error__format',
				'evernote_login__password__error__invalid',
				
				'evernote_login__general__error',
                
				'settings__theme__1',
				'settings__theme__2',
				'settings__theme__3',
				'settings__theme__custom',
				
				'settings__fontSize__small',
				'settings__fontSize__medium',
				'settings__fontSize__large',
                
                'features__title__new',
                'features__title__all',
                
                'features__clipping__title',
                'features__clipping__text',

                'features__highlighting__title',
                'features__highlighting__text',

                'features__related_notes__title',
                'features__related_notes__text',

                'features__smart_filing__title',
                'features__smart_filing__text',
                
                
                'misc__page'
			]
		;
		
		for (var i=0,_i=_list.length; i<_i; i++) {
			_return[_list[i]] = __get_translations_inside__getForKey(_list[i]);
		}
		
		return _return;
	}


		//	translation function
		function __get_translations_inside__getForKey(_key)
		{
			//	will fail on missing string
			try {
				var _t = document.getElementById('readable_by_evernote__string_bundle__inside').getString('inside__'+_key);
				return (_t > '' ? _t : '');
			}
			catch (e) { }
			
			return '';
		}

		return __get_translations_inside();
	}


		
	//	events
	//	======
		
	__readable_by_evernote.__add_custom_events_handler = function ()
	{
		//	include
		
	//	encode
	//	======
		function __encodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == '') { return 'none'; }
			
			//	encode
			return encodeURIComponent(_string)
				.replace(/!/g, '%21')
				.replace(/'/g, '%27')
				.replace(/\(/g, '%28')
				.replace(/\)/g, '%29')
				.replace(/\*/g, '%2A')
			;
		}

		
	//	decode
	//	======
		function __decodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == 'none') { return ''; }
			
			//	decode
			return decodeURIComponent(_string);
		}
	
	

					
		//	document
		var _d = gBrowser.contentDocument;
	
		//	just once
		if (_d.__readable_event_monitor_attached) { return; }
		_d.__readable_event_monitor_attached = true;
	
		//	include events
			
	/*
		first three variables will be defined
	*/

	var 
		__custom_events__names_to_keys = {},
		__custom_events__keys_to_names = {},
		__custom_events__names_to_objects = {},
		
		__custom_events =
		[
			['to-extension--open-settings', 				        'click-111-111-111-111-1-1-1'],
			['to-extension--open-settings-theme', 			        'click-112-112-112-112-1-1-1'],
			
			['to-extension--evernote-clip', 				        'click-121-121-121-121-1-1-1'],
			['to-extension--evernote-clip-highlights', 		        'click-122-122-122-122-1-1-1'],

			/*['to-extension--evernote-unset-tag', 				    'click-123-123-123-123-1-1-1'],*/
			/*['to-extension--evernote-unset-notebook', 		    'click-124-124-124-124-1-1-1'],*/

			
            ['to-extension--evernote-login', 				        'click-131-131-131-131-1-1-1'],
			['to-extension--evernote-login--switch-to-cn',          'click-132-132-132-132-1-1-1'],
            ['to-extension--evernote-login--switch-to-in',          'click-133-133-133-133-1-1-1'],

            ['to-extension--evernote-get-recommendation', 		    'click-141-141-141-141-1-1-1'],
            ['to-extension--evernote-get-user', 		            'click-142-142-142-142-1-1-1'],
            
			['to-extension--select-theme-1', 			    	    'click-151-151-151-151-1-1-1'],
			['to-extension--select-theme-2', 				        'click-152-152-152-152-1-1-1'],
			['to-extension--select-theme-3', 		    		    'click-153-153-153-153-1-1-1'],
			['to-extension--select-theme-custom', 	    		    'click-154-154-154-154-1-1-1'],

			['to-extension--select-size-small',     			    'click-161-161-161-161-1-1-1'],
			['to-extension--select-size-medium', 			        'click-162-162-162-162-1-1-1'],
			['to-extension--select-size-large', 		    	    'click-163-163-163-163-1-1-1'],

			['to-extension--select-related-notes-just-at-bottom',   'click-171-171-171-171-1-1-1'],
			['to-extension--select-related-notes-disabled', 		'click-172-172-172-172-1-1-1'],
            
			['to-extension--track--view', 			                'click-181-181-181-181-1-1-1'],
			['to-extension--track--clip', 		    	            'click-182-182-182-182-1-1-1'],
			['to-extension--track--theme-popup', 			        'click-183-183-183-183-1-1-1'],
			['to-extension--track--settings', 			            'click-184-184-184-184-1-1-1'],

			['to-extension--first-show--check', 			        'click-191-191-191-191-1-1-1'],
			['to-extension--first-show--mark', 			            'click-192-192-192-192-1-1-1'],
            
            /* ================================================================================= */
            
            ['to-browser--evernote-login-show', 		            'click-211-211-211-211-1-1-1'],
            ['to-browser--evernote-login-show--in',     		    'click-212-212-212-212-1-1-1'],
            ['to-browser--evernote-login-show--in-cn', 	    	    'click-213-213-213-213-1-1-1'],
			['to-browser--evernote-login-show--cn', 	            'click-214-214-214-214-1-1-1'],
            ['to-browser--evernote-login-show--cn-in',  		    'click-215-215-215-215-1-1-1'],
            
			['to-browser--evernote-login-failed', 		    	    'click-221-221-221-221-1-1-1'],
			['to-browser--evernote-login-failed--username',         'click-222-222-222-222-1-1-1'],
			['to-browser--evernote-login-failed--password',         'click-223-223-223-223-1-1-1'],
			['to-browser--evernote-login-successful', 		        'click-224-224-224-224-1-1-1'],

			['to-browser--evernote-clip-successful', 		        'click-231-231-231-231-1-1-1'],
			['to-browser--evernote-clip-failed', 			        'click-232-232-232-232-1-1-1'],

			['to-browser--evernote-clip-highlights-successful',     'click-241-241-241-241-1-1-1'],
			['to-browser--evernote-clip-highlights-failed',         'click-242-242-242-242-1-1-1'],

			['to-browser--evernote-get-recommendation-successful',  'click-251-251-251-251-1-1-1'],
			['to-browser--evernote-get-recommendation-failed',      'click-252-252-252-252-1-1-1'],
            
			['to-browser--first-show--all-features',                'click-261-261-261-261-1-1-1'],
			['to-browser--first-show--new-features',                'click-262-262-262-262-1-1-1']
            
        ]
	;

	for (var i=0,_i=__custom_events.length,e=false,k=false; i<_i; i++)
	{
		e = __custom_events[i];
		k = e[1].split('-');
		
		__custom_events__names_to_keys[e[0]] = e[1];
		__custom_events__keys_to_names[e[1]] = e[0];
		__custom_events__names_to_objects[e[0]] =
		{
			'_1': k[1],
			'_2': k[2],
			'_3': k[3],
			'_4': k[4],
			'_5': (k[5] == 1 ? true : false),
			'_6': (k[6] == 1 ? true : false),
			'_7': (k[7] == 1 ? true : false)
		};
	}
	
	var __custom_events__get_key = function (_event)
	{
		return 'click'
			+'-'+_event.screenX
			+'-'+_event.screenY
			+'-'+_event.clientX
			+'-'+_event.clientY
			+'-'+(_event.ctrlKey ? 1 : 0)
			+'-'+(_event.altKey ? 1 : 0)
			+'-'+(_event.shiftKey ? 1 : 0)
		;
	};
	
	var __custom_events__dispatch = function (_custom_event_object, _document, _window)
	{
		var _e = _document.createEvent("MouseEvents");
		
		_e.initMouseEvent(
			"click", true, true, _window, 0, 
                _custom_event_object['_1'], _custom_event_object['_2'], 
                _custom_event_object['_3'], _custom_event_object['_4'], 
                _custom_event_object['_5'], 
                _custom_event_object['_6'], 
                _custom_event_object['_7'], 
			false, 0, null
		);
		
		_document.dispatchEvent(_e);
	};
	
		
        var __event_dispatch = function (_event_name, _arguments)
        {
            //  custom logic
            switch (true)
            {
                case (_event_name == 'to-browser--evernote-get-recommendation-successful'):
                    var 
                        _iframe = _d.getElementById('readable_iframe'),
                        
                        _recommendationInject_documentToInjectInto = (_iframe.contentDocument || _iframe.contentWindow.document),
                        _recommendationInject_filingRecommendation = __readable_by_evernote.__evernote_remote.store__id_to_recommendation[_arguments['_page_id']]
                    ;

                    //	include
                    
    //  global vars
    //      _recommendationInject_documentToInjectInto
    //      _recommendationInject_filingRecommendation

    //  log
    //  console.log(_recommendationInject_filingRecommendation);
        
    
    //  RelatedNotes
    //  ============
    
        (function () 
        {
            //  escapeForHTML
            //  =============
                function __escapeForHTML(_string)
                {
                    var _replace = {
                        "&": "amp", 
                        '"': "quot", 
                        "<": "lt", 
                        ">": "gt"
                    };
                    
                    return _string.replace(
                        /[&"<>]/g,
                        function (_match) { return ("&" + _replace[_match] + ";"); }
                    );
                }
        
            //  return?
            //  =======
                var _injected_element = _recommendationInject_documentToInjectInto.getElementById('relatedNotes__injected');
                
                //  check
                if (_injected_element.innerHTML == 'yup') { return; }
                
                //  set
                _injected_element.innerHTML = 'yup';
                
                
            //  inject RelatedNote function
            //  ===========================
                var _recommendationInject_injectRelatedNote = function (_html_id, _note_index)
                {
                    var
                        _element = _recommendationInject_documentToInjectInto.getElementById(_html_id),
                        _data = _recommendationInject_filingRecommendation.relatedNotes.list[_note_index]
                    ;
                    
                    //  invalid
                    if (_element && _data); else { return; }

                    //  thumbnail
                    var _thumbnail = _data.absoluteURL__thumbnail;
                    
                    //  thumbnail retina
                    if (true
                        && (_recommendationInject_documentToInjectInto.defaultView)
                        && (_recommendationInject_documentToInjectInto.defaultView.devicePixelRatio)
                        && (_recommendationInject_documentToInjectInto.defaultView.devicePixelRatio == 2))
                    { _thumbnail = _thumbnail.replace(/size=75$/, 'size=150'); }
                    
                    //  write
                    _element.innerHTML = ''
                        + '<div class="text">'
                        +   '<a target="_blank" href="'+__escapeForHTML(_data.absoluteURL__noteView)+'" class="title">'+__escapeForHTML(_data.title)+'</a>'
                      //+   '<a target="_blank" href="'+__escapeForHTML(_data.absoluteURL__noteView)+'" class="date">'+__escapeForHTML(((new Date(_data.created)).toDateString()))+'</a>'
                        +   '<a target="_blank" href="'+__escapeForHTML(_data.absoluteURL__noteView)+'" class="snippet">'+__escapeForHTML(_data.snippet)+'</a>'
                        +   '<a target="_blank" href="'+__escapeForHTML(_data.absoluteURL__noteView)+'" class="img" style="background-image: url(\''+__escapeForHTML(_thumbnail)+'\')"></a>'
                        + '</div>'
                    ;
                };
                
            //  inject RelatedNotes
            //  ===================
                if (true
                    && _recommendationInject_filingRecommendation.relatedNotes 
                    && _recommendationInject_filingRecommendation.relatedNotes.list 
                    && _recommendationInject_filingRecommendation.relatedNotes.list.length
                ){
                    //  show
                    var _notes = _recommendationInject_documentToInjectInto.getElementById('relatedNotes');
                    if (_notes) { _notes.className = 'none'; }
                    
                    //  need to be in this order
                    _recommendationInject_injectRelatedNote('relatedNotes__first', 0);
                    _recommendationInject_injectRelatedNote('relatedNotes__second', 1);
                }
        })();

                    break;
                    
                case (_event_name == 'to-browser--evernote-clip-successful'):
                case (_event_name == 'to-browser--evernote-clip-highlights-successful'):
                    var 
                        _iframe = _d.getElementById('readable_iframe'),
                        
                        _infoInject_documentToInjectInto = (_iframe.contentDocument || _iframe.contentWindow.document),
                        _infoInject_filingInfo = __readable_by_evernote.__evernote_remote.store__id_to_info[_arguments['_page_id']]
                    ;

                    //	include
                        //  global vars
    //      _infoInject_documentToInjectInto
    //      _infoInject_filingInfo

    //  log
    //  console.log(_infoInject_filingInfo);

    //  FilingInfo
    //  ==========
    
        (function ()
        {
            //  escapeForHTML
            //  =============
                function __escapeForHTML(_string)
                {
                    var _replace = {
                        "&": "amp", 
                        '"': "quot", 
                        "<": "lt", 
                        ">": "gt"
                    };
                    
                    return _string.replace(
                        /[&"<>]/g,
                        function (_match) { return ("&" + _replace[_match] + ";"); }
                    );
                }
        
            //  return?
            //  =======
                var _injected_element = _infoInject_documentToInjectInto.getElementById('filingInfo_injected');
                
                //  check
                if (_injected_element.innerHTML == 'yup') { return; }
                
                //  set
                _injected_element.innerHTML = 'yup';
        

            //  notebook
            //  ========
                var _notebook_name = (_infoInject_filingInfo.notebook_name);
                    _notebook_name = (_notebook_name > '' ? _notebook_name : _infoInject_documentToInjectInto.getElementById('filingInfo_notebook_default').innerHTML);
        
                _infoInject_documentToInjectInto.getElementById('filingInfo_notebook').innerHTML = __escapeForHTML(_notebook_name);

                
            //  tags
            //  ====
                var _tags_element = _infoInject_documentToInjectInto.getElementById('filingInfo_tags');
                for (var _s=false, i=0, _i=_infoInject_filingInfo.tag_names.length; i<_i; i++)
                {
                    _s = _infoInject_documentToInjectInto.createElement('span');
                    _s.innerHTML = __escapeForHTML(_infoInject_filingInfo.tag_names[i]);
                    _tags_element.appendChild(_s);
                }
                
            
            //  url
            //  ===
                var _links = _infoInject_documentToInjectInto.getElementById('filingInfo_links');
                    _links.innerHTML = _links.innerHTML.replace('#url-edit', __escapeForHTML(_infoInject_filingInfo.url_edit));
                  //_links.innerHTML = _links.innerHTML.replace('#url-view', __escapeForHTML(_infoInject_filingInfo.url_view));
                
        })();

                    break;
            }
        
            __custom_events__dispatch(
                __custom_events__names_to_objects[_event_name], 
                _d, 
                _d.defaultView
            );
        };
        
		//	custom events
		_d.addEventListener('click', function(_event)
		{
			var 
				_event_key = __custom_events__get_key(_event),
				_event_name = __custom_events__keys_to_names[_event_key],
				_stop = false
			;
			
			switch (_event_name)
			{
				/* select theme */
				/* ============ */
					case 'to-extension--select-theme-1': __readable_by_evernote.__save__select_theme('theme-1'); _stop = true; break;
					case 'to-extension--select-theme-2': __readable_by_evernote.__save__select_theme('theme-2'); _stop = true; break;
					case 'to-extension--select-theme-3': __readable_by_evernote.__save__select_theme('theme-3'); _stop = true; break;

					case 'to-extension--select-theme-custom': __readable_by_evernote.__save__select_theme__custom(); _stop = true; break;
					
				/* select size */
				/* =========== */
					case 'to-extension--select-size-small': 	__readable_by_evernote.__save__select_size('small'); 	_stop = true; break;
					case 'to-extension--select-size-medium': 	__readable_by_evernote.__save__select_size('medium'); 	_stop = true; break;
					case 'to-extension--select-size-large': 	__readable_by_evernote.__save__select_size('large'); 	_stop = true; break;
			
                /* related notes */
                /* ============= */
                    case 'to-extension--select-related-notes-just-at-bottom': __readable_by_evernote.__save__select_related_notes('just_at_bottom');    _stop = true; break;
                    case 'to-extension--select-related-notes-disabled': 	  __readable_by_evernote.__save__select_related_notes('disabled'); 	        _stop = true; break;
					
				/* evernote */
				/* ======== */
                    case 'to-extension--evernote-get-recommendation':
                        
    //  global
    //      _d

	//	check login
	//		if not logged in, fail
	//		on failed login, fail
	//	
	//	try get
	//		on fail, fail
	//	============================================================

    
	//	update settings
	//	===============
		
    //  set evernote remote settings
    //  ============================

        (function ()
        {
			
	//	encode
	//	======
		function __encodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == '') { return 'none'; }
			
			//	encode
			return encodeURIComponent(_string)
				.replace(/!/g, '%21')
				.replace(/'/g, '%27')
				.replace(/\(/g, '%28')
				.replace(/\)/g, '%29')
				.replace(/\*/g, '%2A')
			;
		}

		
	//	decode
	//	======
		function __decodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == 'none') { return ''; }
			
			//	decode
			return decodeURIComponent(_string);
		}
	
	

        
            var _storedVars = __readable_by_evernote.__get_saved__vars();
            
            __readable_by_evernote.__evernote_remote.setting__related_notes = __decodeURIComponentForReadable(_storedVars['related_notes']);
            __readable_by_evernote.__evernote_remote.setting__smart_filing = __decodeURIComponentForReadable(_storedVars['smart_filing']);

            __readable_by_evernote.__evernote_remote.setting__clip_tag = __decodeURIComponentForReadable(_storedVars['clip_tag']);
            __readable_by_evernote.__evernote_remote.setting__clip_notebook = __decodeURIComponentForReadable(_storedVars['clip_notebook']);
        })();

    
	
	//	logout on next action
	//	=====================
		if (__readable_by_evernote.__get_stored_evernote_logoutOnNextAction()) { __readable_by_evernote.__evernote_remote.logout(); }
	
	
	//	stored login
	//	============
		var _storedLogin = __readable_by_evernote.__get_stored_evernote_credentials();
	
	
    //  success function
    //  ================
        var _get_successful = function (_id)
        {
            //  send event
            __event_dispatch(
                'to-browser--evernote-get-recommendation-successful',
                { '_page_id': _id }
            );
        };

        
    //  failed function
    //  ===============
        var _get_failed = function ()
        {
            //  send event
            __event_dispatch('to-browser--evernote-get-recommendation-failed');
        };
    
        
	var 
		_iframe = _d.getElementById('readable_iframe'),
		_doc = (_iframe.contentDocument || _iframe.contentWindow.document),
		_bodyElement = _doc.getElementById('text'),
		
        __id = _doc.getElementById('body').getAttribute('readable__page_id'),
		__url = _d.defaultView.location.href, 
		__body = _bodyElement.innerHTML
	;

    //  include prepare/clean
    
    //  global vars
    /*
        __id
		__url 
		__title
		__body
    */
    
    
    //  remove all spans
    //  ================
        __body = __body.replace(/<span([^>]*?)>/gi, '');
        __body = __body.replace(/<\/span>/gi, '');

        
    //  remove delete buttons
    //  =====================
        __body = __body.replace(/<em ([^>]*?)highlight([^>]*?)><a ([^>]*?)delete([^>]*?)><\/a>([\s\S]*?)<\/em>/gi, '<em class="highlight">$5</em>');
    
    
    //  highlight element
    //  =================
        __body = __body.replace(/<em ([^>]*?)highlight([^>]*?)>([^>]+?)<\/em>/gi, '<highlight>$3</highlight>');
    
    
    //  double EMs
    //  ==========
        while (true && __body.match(/<highlight>([\s\S]*?)<\/highlight>([\n\r\t]*?)<highlight>([\s\S]*?)<\/highlight>/gi)) {
            __body = __body.replace(/<highlight>([\s\S]*?)<\/highlight>([\n\r\t]*?)<highlight>([\s\S]*?)<\/highlight>/gi, '<highlight>$1$3</highlight>');
        }
    
    
    //  replace EMs
    //  ===========
        __body = __body.replace(/<highlight>([\s\S]*?)<\/highlight>/gi, '<span style="x-evernote: highlighted; background-color: #f6ee96">$1</span>');
    
    
    //  remove link footnotes
    //  =====================
        __body = __body.replace(/<sup class="readableLinkFootnote">([^<]*)<\/sup>/gi, '');

    
	//	remove extraneous stuff -- nothing after footnoted links
    //  =======================
        var 
            __f_marker = '<ol id="footnotedLinks"',
            __f_marker_pos = __body.lastIndexOf(__f_marker)
        ;
        
        if (__f_marker_pos > -1) {
            __body = __body.substr(0, __f_marker_pos);
        }
        

	//  do
	switch (true)
	{

		//	not connected / logged-in
		//	and we know this -- so do connect/login
		//	=======================================
		case (!(__readable_by_evernote.__evernote_remote.is__connected)):
		case (!(__readable_by_evernote.__evernote_remote.is__loggedIn)):
		
			//	no stored login
			//	===============
			if (_storedLogin == false) { _get_failed(); return; }
		
			//	do stored login
			//	================
			__readable_by_evernote.__evernote_remote.login
			(
				_storedLogin.username,
				_storedLogin.password,

				//	success | login
				function ()
				{
					__readable_by_evernote.__evernote_remote.get_recommendation
					(
                        __id,
						__url,
						__body,

						//	success | get
						function (_recommendation) { _get_successful(__id); },

						//	failure | get
						function (_failReason) { _get_failed(); }
					);
				},

				//	failure | login
				function (_failReason) { _get_failed(); }
			);

			break;
			
			
		//	should be both connected and logged in
		//		in case it fails because of login, we try another stored login
		//	===================================================================
		default:

			__readable_by_evernote.__evernote_remote.get_recommendation
			(
                __id,
				__url,
				__body,
				
				//	success | get
				function (_recommendation)	{ _get_successful(__id); },
				
				//	failure | get
				function (_failReason) { _get_failed(); }
			);
			break;
	}

						_stop = true;
                        break;
                
					case 'to-extension--evernote-clip':
						
    //  global
    //      _d

	//	check login
	//		if not logged in, try stored log in 
	//			on successfull login, do clip again
	//			on failed login, or no stored login, show login form
	//	
	//	try clip
	//		on fail, because of login, try stored login
	//			on successfull login, try clip again
	//			on failed login, or no stored login, show login form
	//	============================================================

    
	//	update settings
	//	===============
		
    //  set evernote remote settings
    //  ============================

        (function ()
        {
			
	//	encode
	//	======
		function __encodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == '') { return 'none'; }
			
			//	encode
			return encodeURIComponent(_string)
				.replace(/!/g, '%21')
				.replace(/'/g, '%27')
				.replace(/\(/g, '%28')
				.replace(/\)/g, '%29')
				.replace(/\*/g, '%2A')
			;
		}

		
	//	decode
	//	======
		function __decodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == 'none') { return ''; }
			
			//	decode
			return decodeURIComponent(_string);
		}
	
	

        
            var _storedVars = __readable_by_evernote.__get_saved__vars();
            
            __readable_by_evernote.__evernote_remote.setting__related_notes = __decodeURIComponentForReadable(_storedVars['related_notes']);
            __readable_by_evernote.__evernote_remote.setting__smart_filing = __decodeURIComponentForReadable(_storedVars['smart_filing']);

            __readable_by_evernote.__evernote_remote.setting__clip_tag = __decodeURIComponentForReadable(_storedVars['clip_tag']);
            __readable_by_evernote.__evernote_remote.setting__clip_notebook = __decodeURIComponentForReadable(_storedVars['clip_notebook']);
        })();

    
	
	//	logout on next action
	//	=====================
		if (__readable_by_evernote.__get_stored_evernote_logoutOnNextAction()) { __readable_by_evernote.__evernote_remote.logout(); }
	
	
	//	stored login
	//	============
		var _storedLogin = __readable_by_evernote.__get_stored_evernote_credentials();
	
	
    //  success function
    //  ================
        var _clipping_successful = function (_id)
        {
            //  send event
            __event_dispatch(
                'to-browser--evernote-clip-successful',
                { '_page_id': _id }
            );
        };

        
    //  failed function
    //  ===============
        var _clipping_failed = function ()
        {
            //  send event
            __event_dispatch('to-browser--evernote-clip-failed');
        };

        
    //  bootstrap + show login
    //  ======================
        var _request_login = function ()
        {
            __readable_by_evernote.__evernote_bootstrap.bootstrap(
                function ()
                { 
                    if (__readable_by_evernote.__evernote_bootstrap.profiles_as_string)
                    {
                        __event_dispatch(''
                            + 'to-browser--evernote-login-show--' 
                            + (__readable_by_evernote.__evernote_bootstrap.profiles_as_string.replace('_', '-'))
                        );
                    }
                    else
                    {
                        __event_dispatch('to-browser--evernote-login-show--in');
                    }
                },
                function () { _clipping_failed(); }
            );
        };
    
        
	var 
		_iframe = _d.getElementById('readable_iframe'),
		_doc = (_iframe.contentDocument || _iframe.contentWindow.document),
		
		_bodyElement = _doc.getElementById('text'),
		
        __id = _doc.getElementById('body').getAttribute('readable__page_id'),
		__url = _d.defaultView.location.href, 
		__title = _d.title, 
		__body = _bodyElement.innerHTML 
	;

    //  include prepare/clean
    
    //  global vars
    /*
        __id
		__url 
		__title
		__body
    */
    
    
    //  remove all spans
    //  ================
        __body = __body.replace(/<span([^>]*?)>/gi, '');
        __body = __body.replace(/<\/span>/gi, '');

        
    //  remove delete buttons
    //  =====================
        __body = __body.replace(/<em ([^>]*?)highlight([^>]*?)><a ([^>]*?)delete([^>]*?)><\/a>([\s\S]*?)<\/em>/gi, '<em class="highlight">$5</em>');
    
    
    //  highlight element
    //  =================
        __body = __body.replace(/<em ([^>]*?)highlight([^>]*?)>([^>]+?)<\/em>/gi, '<highlight>$3</highlight>');
    
    
    //  double EMs
    //  ==========
        while (true && __body.match(/<highlight>([\s\S]*?)<\/highlight>([\n\r\t]*?)<highlight>([\s\S]*?)<\/highlight>/gi)) {
            __body = __body.replace(/<highlight>([\s\S]*?)<\/highlight>([\n\r\t]*?)<highlight>([\s\S]*?)<\/highlight>/gi, '<highlight>$1$3</highlight>');
        }
    
    
    //  replace EMs
    //  ===========
        __body = __body.replace(/<highlight>([\s\S]*?)<\/highlight>/gi, '<span style="x-evernote: highlighted; background-color: #f6ee96">$1</span>');
    
    
    //  remove link footnotes
    //  =====================
        __body = __body.replace(/<sup class="readableLinkFootnote">([^<]*)<\/sup>/gi, '');

    
	//	remove extraneous stuff -- nothing after footnoted links
    //  =======================
        var 
            __f_marker = '<ol id="footnotedLinks"',
            __f_marker_pos = __body.lastIndexOf(__f_marker)
        ;
        
        if (__f_marker_pos > -1) {
            __body = __body.substr(0, __f_marker_pos);
        }
        

	
	switch (true)
	{

		//	not connected / logged-in
		//	and we know this -- so do connect/login
		//	=======================================
		case (!(__readable_by_evernote.__evernote_remote.is__connected)):
		case (!(__readable_by_evernote.__evernote_remote.is__loggedIn)):
		
			//	no stored login
			//	===============
			if (_storedLogin == false) { _request_login(); return; }
		
			//	do stored login
			//	================
			__readable_by_evernote.__evernote_remote.login
			(
				_storedLogin.username,
				_storedLogin.password,

				//	success | login
				function ()
				{
					__readable_by_evernote.__evernote_remote.clip
					(
                        __id,
						__url,
						__title,
						__body,

						//	success | clip
						function () { _clipping_successful(__id); },

						//	failure | clip
						function (_failReason) { _clipping_failed(); }
					);
				},

				//	failure | login
				function (_failReason) { _request_login(); }
			);
		
			break;
			
			
		//	should be both connected and logged in
		//		in case it fails because of login, we try another stored login
		//	===================================================================
		default:

			__readable_by_evernote.__evernote_remote.clip
			(
                __id,
				__url,
				__title,
				__body,
				
				//	success | clip
				function ()	{ _clipping_successful(__id); },
				
				//	failure | clip
				function (_failReason)
				{
					//	failure because of soemthing else
					if (_failReason == 'login'); else { _clipping_failed(); return; }

					//	no stored login
					if (_storedLogin == false) { _request_login(); return; }
					
					//	try stored login
					__readable_by_evernote.__evernote_remote.login
					(
						_storedLogin.username,
						_storedLogin.password,
						
						//	success | login
						function ()
						{
							__readable_by_evernote.__evernote_remote.clip
							(
                                __id,
								__url,
								__title,
								__body,
								
								//	success | clip
								function () { _clipping_successful(__id); },
								
								//	failure | clip
								function (_failReason) { _clipping_failed(); }
							);
						},
						
						//	failure | login
						function (_failReason) { _request_login(); }
					);
				}
			);
		
			break;
	}

						_stop = true;
						break;

					case 'to-extension--evernote-clip-highlights':
						
    //  global
    //      _d

	//	check login
	//		if not logged in, fail
	//		on failed login, fail
	//	
	//	try clip
	//		on fail, fail
	//	============================================================

    
	//	update settings
	//	===============
		
    //  set evernote remote settings
    //  ============================

        (function ()
        {
			
	//	encode
	//	======
		function __encodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == '') { return 'none'; }
			
			//	encode
			return encodeURIComponent(_string)
				.replace(/!/g, '%21')
				.replace(/'/g, '%27')
				.replace(/\(/g, '%28')
				.replace(/\)/g, '%29')
				.replace(/\*/g, '%2A')
			;
		}

		
	//	decode
	//	======
		function __decodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == 'none') { return ''; }
			
			//	decode
			return decodeURIComponent(_string);
		}
	
	

        
            var _storedVars = __readable_by_evernote.__get_saved__vars();
            
            __readable_by_evernote.__evernote_remote.setting__related_notes = __decodeURIComponentForReadable(_storedVars['related_notes']);
            __readable_by_evernote.__evernote_remote.setting__smart_filing = __decodeURIComponentForReadable(_storedVars['smart_filing']);

            __readable_by_evernote.__evernote_remote.setting__clip_tag = __decodeURIComponentForReadable(_storedVars['clip_tag']);
            __readable_by_evernote.__evernote_remote.setting__clip_notebook = __decodeURIComponentForReadable(_storedVars['clip_notebook']);
        })();

    
	
	//	logout on next action
	//	=====================
		if (__readable_by_evernote.__get_stored_evernote_logoutOnNextAction()) { __readable_by_evernote.__evernote_remote.logout(); }
	
	
	//	stored login
	//	============
		var _storedLogin = __readable_by_evernote.__get_stored_evernote_credentials();
	
	
    //  success function
    //  ================
        var _clipping_successful = function (_id)
        {
            //  send event
            __event_dispatch(
                'to-browser--evernote-clip-highlights-successful',
                { '_page_id': _id }
            );
        };

        
    //  failed function
    //  ===============
        var _clipping_failed = function ()
        {
            //  send event
            __event_dispatch('to-browser--evernote-clip-highlights-failed');
        };
    
    
    //  bootstrap + show login
    //  ======================
        var _request_login = function ()
        {
            __readable_by_evernote.__evernote_bootstrap.bootstrap(
                function ()
                { 
                    if (__readable_by_evernote.__evernote_bootstrap.profiles_as_string)
                    {
                        __event_dispatch(''
                            + 'to-browser--evernote-login-show--' 
                            + (__readable_by_evernote.__evernote_bootstrap.profiles_as_string.replace('_', '-'))
                        );
                    }
                    else
                    {
                        __event_dispatch('to-browser--evernote-login-show--in');
                    }
                },
                function () { _clipping_failed(); }
            );
        };
    
        
	var 
		_iframe = _d.getElementById('readable_iframe'),
		_doc = (_iframe.contentDocument || _iframe.contentWindow.document),
		
		_bodyElement = _doc.getElementById('text'),
		
        __id = _doc.getElementById('body').getAttribute('readable__page_id'),
		__url = _d.defaultView.location.href, 
		__title = _d.title, 
		__body = _bodyElement.innerHTML
	;

    //  include prepare/clean
    
    //  global vars
    /*
        __id
		__url 
		__title
		__body
    */
    
    
    //  remove all spans
    //  ================
        __body = __body.replace(/<span([^>]*?)>/gi, '');
        __body = __body.replace(/<\/span>/gi, '');

        
    //  remove delete buttons
    //  =====================
        __body = __body.replace(/<em ([^>]*?)highlight([^>]*?)><a ([^>]*?)delete([^>]*?)><\/a>([\s\S]*?)<\/em>/gi, '<em class="highlight">$5</em>');
    
    
    //  highlight element
    //  =================
        __body = __body.replace(/<em ([^>]*?)highlight([^>]*?)>([^>]+?)<\/em>/gi, '<highlight>$3</highlight>');
    
    
    //  double EMs
    //  ==========
        while (true && __body.match(/<highlight>([\s\S]*?)<\/highlight>([\n\r\t]*?)<highlight>([\s\S]*?)<\/highlight>/gi)) {
            __body = __body.replace(/<highlight>([\s\S]*?)<\/highlight>([\n\r\t]*?)<highlight>([\s\S]*?)<\/highlight>/gi, '<highlight>$1$3</highlight>');
        }
    
    
    //  replace EMs
    //  ===========
        __body = __body.replace(/<highlight>([\s\S]*?)<\/highlight>/gi, '<span style="x-evernote: highlighted; background-color: #f6ee96">$1</span>');
    
    
    //  remove link footnotes
    //  =====================
        __body = __body.replace(/<sup class="readableLinkFootnote">([^<]*)<\/sup>/gi, '');

    
	//	remove extraneous stuff -- nothing after footnoted links
    //  =======================
        var 
            __f_marker = '<ol id="footnotedLinks"',
            __f_marker_pos = __body.lastIndexOf(__f_marker)
        ;
        
        if (__f_marker_pos > -1) {
            __body = __body.substr(0, __f_marker_pos);
        }
        

	
	switch (true)
	{

		//	not connected / logged-in
		//	and we know this -- so do connect/login
		//	=======================================
		case (!(__readable_by_evernote.__evernote_remote.is__connected)):
		case (!(__readable_by_evernote.__evernote_remote.is__loggedIn)):
		
			//	no stored login
			//	===============
			if (_storedLogin == false) { _request_login(); return; }
		
			//	do stored login
			//	================
			__readable_by_evernote.__evernote_remote.login
			(
				_storedLogin.username,
				_storedLogin.password,

				//	success | login
				function ()
				{
					__readable_by_evernote.__evernote_remote.clip
					(
                        __id,
						__url,
						__title,
						__body,

						//	success | clip
						function () { _clipping_successful(__id); },

						//	failure | clip
						function (_failReason) { _clipping_failed(); }
					);
				},

				//	failure | login
				function (_failReason) { _request_login(); }
			);

			break;
			
			
		//	should be both connected and logged in
		//		in case it fails because of login, we try another stored login
		//	===================================================================
		default:

			__readable_by_evernote.__evernote_remote.clip
			(
                __id,
				__url,
				__title,
				__body,
				
				//	success | clip
				function ()	{ _clipping_successful(__id); },
				
				//	failure | clip
				function (_failReason)
                {
					//	failure because of soemthing else
					if (_failReason == 'login'); else { _clipping_failed(); return; }

					//	no stored login
					if (_storedLogin == false) { _request_login(); return; }
					
					//	try stored login
					__readable_by_evernote.__evernote_remote.login
					(
						_storedLogin.username,
						_storedLogin.password,
						
						//	success | login
						function ()
						{
							__readable_by_evernote.__evernote_remote.clip
							(
                                __id,
								__url,
								__title,
								__body,
								
								//	success | clip
								function () { _clipping_successful(__id); },
								
								//	failure | clip
								function (_failReason) { _clipping_failed(); }
							);
						},
						
						//	failure | login
						function (_failReason) { _request_login(); }
					);
                }
			);
			break;
	}

						_stop = true;
						break;
                        
					/*case 'to-extension--evernote-unset-tag':
                        alert('unset tag');
						_stop = true;
						break;*/
                    
					/*case 'to-extension--evernote-unset-notebook':
                        alert('unset notebook');
						_stop = true;
						break;*/
                        
					case 'to-extension--evernote-login':
						
	//	do login
	//		store login, if rememberMe
	//	==============================

    
	//	update settings
	//	===============
		
    //  set evernote remote settings
    //  ============================

        (function ()
        {
			
	//	encode
	//	======
		function __encodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == '') { return 'none'; }
			
			//	encode
			return encodeURIComponent(_string)
				.replace(/!/g, '%21')
				.replace(/'/g, '%27')
				.replace(/\(/g, '%28')
				.replace(/\)/g, '%29')
				.replace(/\*/g, '%2A')
			;
		}

		
	//	decode
	//	======
		function __decodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == 'none') { return ''; }
			
			//	decode
			return decodeURIComponent(_string);
		}
	
	

        
            var _storedVars = __readable_by_evernote.__get_saved__vars();
            
            __readable_by_evernote.__evernote_remote.setting__related_notes = __decodeURIComponentForReadable(_storedVars['related_notes']);
            __readable_by_evernote.__evernote_remote.setting__smart_filing = __decodeURIComponentForReadable(_storedVars['smart_filing']);

            __readable_by_evernote.__evernote_remote.setting__clip_tag = __decodeURIComponentForReadable(_storedVars['clip_tag']);
            __readable_by_evernote.__evernote_remote.setting__clip_notebook = __decodeURIComponentForReadable(_storedVars['clip_notebook']);
        })();

    
    
	var 
		_iframe = _d.getElementById('readable_iframe'),
		_doc = (_iframe.contentDocument || _iframe.contentWindow.document),
		
		_userElement = _doc.getElementById('evernote_login__username'),
		_passElement = _doc.getElementById('evernote_login__password'),
		_rememberMeElement = _doc.getElementById('evernote_login__rememberMe'),

		__user = (_userElement.value > '' ? _userElement.value : ''),
		__pass = (_userElement.value > '' ? _passElement.value : ''),
		__rememberMe = (_rememberMeElement.checked == true ? true : false)
	;

	__readable_by_evernote.__evernote_remote.login
	(
		__user,
		__pass,
		
		//	success | login
		function ()
		{
			//	save credentials
			//	================
				if (__rememberMe)
				{
					__readable_by_evernote.__set_stored_evernote_credentials({
						'username': __user,
						'password': __pass
					});
				}
			
			//	raise event	
			__event_dispatch('to-browser--evernote-login-successful');
		},
		
		//	fail | login
		function (_failReason)
		{
			switch (_failReason)
			{
				case 'username': 	__event_dispatch('to-browser--evernote-login-failed--username');    break;
				case 'password': 	__event_dispatch('to-browser--evernote-login-failed--password');    break;
				default: 			__event_dispatch('to-browser--evernote-login-failed');              break;
			}
		}
	);

						_stop = true;
						break;

            /* first show */
            /* ========== */
                
                case 'to-extension--first-show--check':
                    switch (true)
                    {
                        case (!(__readable_by_evernote['firstShow'])):
                            _stop = true;
                            break;
                            
                        case (__readable_by_evernote['firstShow'] == 'new-features'):
                            __event_dispatch('to-browser--first-show--new-features');
                            _stop = true;
                            break;
                            
                        case (__readable_by_evernote['firstShow'] == 'all-features'):
                            __event_dispatch('to-browser--first-show--all-features');
                            _stop = true;
                            break;
                    }
                    break;

                case 'to-extension--first-show--mark':
                    //  this instance
                    __readable_by_evernote['firstShow'] = true;
                    
                    //  include
                    //  Options:
//      "the curent version number"
//      "nope"

var __first_show_identifier = '3346.272.754';
//  __first_show_identifier = 'nope';

                    
                    //  set
                    var 
                        _prefs_service = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService),
                        _prefs = _prefs_service.getBranch("extensions.readable-by-evernote.")
                    ;
                    
                    _prefs.setCharPref('firstShowIdentifier', __first_show_identifier);

                    
                    _stop = true;
                    break;

                        
            /* switch login */
            /* ============ */
				case 'to-extension--evernote-login--switch-to-cn':
                    if (__readable_by_evernote.__evernote_bootstrap.profiles_as_string == 'in_cn')
                    {
                        __readable_by_evernote.__evernote_bootstrap.profiles_as_string = 'cn_in';
                        __readable_by_evernote.__evernote_bootstrap.server = 'china';
                        __readable_by_evernote.__evernote_bootstrap.remote_domain = __readable_by_evernote.__evernote_bootstrap['server_'+'china'];
                        __readable_by_evernote.__evernote_bootstrap.remote_domain_marketing = false;
                        
                        __readable_by_evernote.__evernote_remote.disconnect();
                        
                        __event_dispatch('to-browser--evernote-login-show--cn-in');
                        sendResponse({});
                    }
                    break;
            
				case 'to-extension--evernote-login--switch-to-in':
                    if (__readable_by_evernote.__evernote_bootstrap.profiles_as_string == 'cn_in')
                    {
                        __readable_by_evernote.__evernote_bootstrap.profiles_as_string = 'in_cn';
                        __readable_by_evernote.__evernote_bootstrap.server = 'main';
                        __readable_by_evernote.__evernote_bootstrap.remote_domain = __readable_by_evernote.__evernote_bootstrap['server_'+'main'];
                        __readable_by_evernote.__evernote_bootstrap.remote_domain_marketing = false;
                        
                        __readable_by_evernote.__evernote_remote.disconnect();
                        
                        __event_dispatch('to-browser--evernote-login-show--in-cn');
                        sendResponse({});
                    }
                    break;

                    
				/* settings */
				/* ======== */
					case 'to-extension--open-settings-theme':
						window.openDialog('chrome://readable-by-evernote/content/options.xul#showCustom');
						_stop = true;
						break;

					case 'to-extension--open-settings':
						window.openDialog('chrome://readable-by-evernote/content/options.xul');
						_stop = true;
						break;

			}
		
			if (_stop)
			{
				_event.stopPropagation();
				_event.preventDefault();
			}
		
		}, true);
	}



    //  set bootstrap info
    //  ==================
        
        //  set to live
        __readable_by_evernote.__evernote_bootstrap.set_servers_to_live();
        
        //  set locale
        __readable_by_evernote.__evernote_bootstrap.setLocale(window.navigator.language);
        //mike.c.yang@gmail.com: Force using zh_CN local to make sure clearly bootstrap with yinxiang.com
        __readable_by_evernote.__evernote_bootstrap.setLocale("zh_CN");

        //  saved server
        (function ()
        {
            var _storedCredentialsForBootstrap = __readable_by_evernote.__get_stored_evernote_credentials();
            if (_storedCredentialsForBootstrap && _storedCredentialsForBootstrap.server)
                { __readable_by_evernote.__evernote_bootstrap.saved_server = _storedCredentialsForBootstrap.server; }
        })();
        
        
	//	get settings
	//	============
		
    //  set evernote remote settings
    //  ============================

        (function ()
        {
			
	//	encode
	//	======
		function __encodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == '') { return 'none'; }
			
			//	encode
			return encodeURIComponent(_string)
				.replace(/!/g, '%21')
				.replace(/'/g, '%27')
				.replace(/\(/g, '%28')
				.replace(/\)/g, '%29')
				.replace(/\*/g, '%2A')
			;
		}

		
	//	decode
	//	======
		function __decodeURIComponentForReadable(_string)
		{
			//	none
			if (_string == 'none') { return ''; }
			
			//	decode
			return decodeURIComponent(_string);
		}
	
	

        
            var _storedVars = __readable_by_evernote.__get_saved__vars();
            
            __readable_by_evernote.__evernote_remote.setting__related_notes = __decodeURIComponentForReadable(_storedVars['related_notes']);
            __readable_by_evernote.__evernote_remote.setting__smart_filing = __decodeURIComponentForReadable(_storedVars['smart_filing']);

            __readable_by_evernote.__evernote_remote.setting__clip_tag = __decodeURIComponentForReadable(_storedVars['clip_tag']);
            __readable_by_evernote.__evernote_remote.setting__clip_notebook = __decodeURIComponentForReadable(_storedVars['clip_notebook']);
        })();

        
        
	//	first run
	//	=========
		
	window.addEventListener('load', function ()
	{
    
        //  include
        //  =======
            //  Options:
//      "the curent version number"
//      "nope"

var __first_show_identifier = '3346.272.754';
//  __first_show_identifier = 'nope';


            
		//	check
		//	=====
			
            //  already done -- somehow
            if (__readable_by_evernote['firstShow']) { return; }
            
			var 
				_prefs_service = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService),
				_prefs = _prefs_service.getBranch("extensions.readable-by-evernote.")
			;

			//	check -- maybe return
			if (_prefs.getCharPref('notFirstRun') != 'yes')
            {
                __readable_by_evernote['firstShow'] = 'all-features';
                return;
            }
            else
            {
                if (_prefs.getCharPref('firstShowIdentifier') == __first_show_identifier); else
                {
                    __readable_by_evernote['firstShow'] = 'new-features';
                    return;
                }
            }
	
	}, false);

		
	window.addEventListener('load', function ()
	{
		//	check
		//	=====
			
			var 
				_prefs_service = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService),
				_prefs = _prefs_service.getBranch("extensions.readable-by-evernote.")
			;
			
			//	check -- maybe return
			if (_prefs.getCharPref('notFirstRun') == 'yes') { return; }
			
			//	set
			_prefs.setCharPref('notFirstRun', 'yes');

			
		//	First Run
		//	=========
		
			//	add button
            //  ==========
                __readable_by_evernote.installButtonForReadable("nav-bar", "readable_by_evernote__button");
			
            
			//	open info url
            //  =============
                var defaultInfoURL = 'http://www.evernote.com/clearly/';

                //  bootstrap first
                //  ===============
                    __readable_by_evernote.__evernote_bootstrap.bootstrap
                    (
                        function ()
                        {
                            var url = '';
                                url = (url > '' ? url : __readable_by_evernote.__evernote_bootstrap.remote_domain_marketing);
                                url = (url > '' ? url + 'clearly/' : defaultInfoURL);
                                
                            gBrowser.selectedTab = gBrowser.addTab(url); 
                        },
                        function ()
                        {
                            gBrowser.selectedTab = gBrowser.addTab(defaultInfoURL); 
                        }
                    );
                
			
	
	}, false);
	
	
	/** 
	 * https://developer.mozilla.org/en/Code_snippets/Toolbar#Adding_button_by_default
	 * + the old toolbar-button-init.js in the Firefox Evernote Cliper
	 * + some meddling
	 */  
	__readable_by_evernote.installButtonForReadable = function (_toolbarId, _buttonId)
	{
		//	already exists
		if (document.getElementById(_buttonId)) { return; }

		//	toolbar
		var _toolbar = document.getElementById(_toolbarId);  
		if (_toolbar); else { return; }
  
		//	insert
		var _set = _toolbar.getAttribute('currentset');
			_set = (_set > '' ? _set : _toolbar.getAttribute('defaultset'));
			_set += ',' + _buttonId;
			
		//	add
		_toolbar.insertItem(_buttonId, null, null, false);
		_toolbar.setAttribute('currentset', _set);
		_toolbar.currentset = _set;
		
		//	persist
		try { BrowserToolboxCustomizeDone(true); } catch (e) { }
		document.persist(_toolbarId, 'currentset');
	};


        
