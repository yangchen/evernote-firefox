CLEARLY=clearly_yixiang.xpi
CLIPPER=clipper_yixiang.xpi

default : $(CLEARLY) $(CLIPPER)

$(CLEARLY) :
	cd clearly && zip -r ../$(CLEARLY) *

$(CLIPPER) :
	cd clipper && zip -r ../$(CLIPPER) *

clean:
	rm -f $(CLEARLY) $(CLIPPER)
